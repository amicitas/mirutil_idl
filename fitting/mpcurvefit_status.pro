     ; =================================================================
     ; =================================================================
     ; Just a little routine to prind out a string with the meaning
     ; of the MPCURVEFIT status.
     FUNCTION MPCURVEFIT_STATUS, status
       
       ;   STATUS - an integer status code is returned.  All values other
       ;            than zero can represent success.  It can have one of the
       ;            following values:
       ;
       ;	   0  improper input parameters.
       ;         
       ;	   1  both actual and predicted relative reductions
       ;	      in the sum of squares are at most FTOL.
       ;         
       ;	   2  relative error between two consecutive iterates
       ;	      is at most XTOL
       ;         
       ;	   3  conditions for STATUS = 1 and STATUS = 2 both hold.
       ;         
       ;	   4  the cosine of the angle between fvec and any
       ;	      column of the jacobian is at most GTOL in
       ;	      absolute value.
       ;         
       ;	   5  the maximum number of iterations has been reached
       ;         
       ;	   6  FTOL is too small. no further reduction in
       ;	      the sum of squares is possible.
       ;         
       ;	   7  XTOL is too small. no further improvement in
       ;	      the approximate solution x is possible.
       ;         
       ;	   8  GTOL is too small. fvec is orthogonal to the
       ;	      columns of the jacobian to machine precision.

       CASE status OF
         0: BEGIN
           status_str = 'improper input parameters or internal error'
         END
         1: BEGIN
           status_str = 'both actual and predicted relative reductions in the sum of squares are at most FTOL.'
         END
         2: BEGIN
           status_str = 'relative error between two consecutive iterates is at most XTOL'
         END
         3: BEGIN
           status_str = 'conditions for STATUS = 1 and STATUS = 2 both hold.'
         END
         4: BEGIN
           status_str = 'the cosine of the angle between fvec and any column of the jacobian is at most GTOL in absolute value.'
         END
         5: BEGIN
           status_str = 'the maximum number of iterations has been reached'
         END
         6: BEGIN
           status_str = 'FTOL is too small. no further reduction in the sum of squares is possible.'
         END
         7: BEGIN
           status_str = 'XTOL is too small. no further improvement in the approximate solution x is possible.'
         END
         8: BEGIN
           status_str = 'GTOL is too small. fvec is orthogonal to the columns of the jacobian to machine precision.'
         END
         ELSE: BEGIN
           status_str = 'unknown error code.'
         ENDELSE
       ENDCASE

         RETURN, status_str

     END ;FUNCTION MPCURVEFIT_STATUS
