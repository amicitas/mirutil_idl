;+======================================================================
; =================================================================
; This is a little program to switch between 
; MPCURVEFIT and OO_CURVEFIT.
;
; I have two reasons for doing this:
;
; 1. It's nice to go back and forth and see how each does sometimes.
;
; 2. There are situations in which one of the fitters will not 
;    work well.
;
;
;
; This function is to be used as a replacement for
; OO_CURVEFIT and OO_MPCURVEFIT.
;
;
;-======================================================================

     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Provide a uniform interface to OO_CURVEFIT and OO_MPCURVEFIT.
     ;   
     ;   Allow the user to switch between the two fitters using
     ;   a common inteface.
     ;
     ;   Also do better error handling and return some additonal
     ;   usefull parameters.
     ;
     ; DESCRIPTION:
     ;   This function provides a uniform interface to OO_CURVEFIT 
     ;   and OO_MPCURVEFIT. 
     ;
     ;   Unless specified using a keyword, OO_MPCURVEFIT will always
     ;   be used.
     ;
     ;   If there are no free variables, no fitting will be done
     ;   and the function will simply be evaluated.
     ;
     ;
     ; RETURN VALUE:
     ;   yfit
     ;
     ;
     ; STATUS OPTIONS:
     ;
     ;   FIT_STATUS    - 0=failure, 1=success, 2=possible failure
     ;   STRING_STATUS - Returns a string with a status message.
     ;   STATUS        - Will return the status code directly from
     ;                   OO_CURVEFIT or OO_MPCURVEFIT.
     ;
     ;-=================================================================

     FUNCTION CURVEFIT_SWITCH, x $
                               ,y $
                               ,wts $
                               ,coeff $
                               ,sigma $

                               ,PARINFO=parinfo $                     
                               ,FITA=fita $

                               ,FUNCTION_NAME=function_name $
                               ,OBJECT_REFERENCE=object_ref $

                               ,DOUBLE=double $
                               ,YERROR=yerror $

                               ,FUNCTARGS=fa $                 
                               ,QUERY=query $
                               ,CHISQ=chisq $
                               ,ITER=iter $
                               ,ITMAX=maxiter $
                               ,NODERIVATIVE=noderivative $
                               ,TOL=tol $
                               ,COVAR=covar $
                               ,ERRMSG=errmsg $

                               ,VERBOSE=verbose $

                               ,REDUCED_CHISQ=reduced_chisq $
                               ,PRINT_STATUS_MESSAGE=print_status_message $
                               ,USE_MPCURVEFIT=use_mpcurvefit $
                               ,USE_CURVEFIT=use_curvefit $

                               ,FIT_STATUS=fit_status $
                               ,STATUS=status $
                               ,STRING_STATUS=string_status $
                                 
                               ,MESSAGE_PROCEDURE=message_pro $
                               ,MESSAGE_OBJECT=message_obj $

                               ,DEBUG=debug $
                               ,HELP=help $

                               ,_REF_EXTRA=extra

       FORWARD_FUNCTION MPCURVEFIT_STATUS

       IF KEYWORD_SET(help) THEN BEGIN
         INFO
         RETURN, 0
       ENDIF

       ; Setup default parameters
       MIR_DEFAULT, use_mpcurvefit, KEYWORD_SET(use_mpcurvefit)
       MIR_DEFAULT, use_curvefit, KEYWORD_SET(use_curvefit)
       MIR_DEFAULT, object_ref, OBJ_NEW()



       ; If inital coefficents were not given, but parinfo was,
       ; then extract the inital values from parinfo.
       IF ~ ISA(coeff) AND ISA(parinfo) THEN BEGIN
         IF ~ HAS_TAG(parinfo[0], 'VALUE') THEN BEGIN
           MESSAGE, 'No inital values given.'
         ENDIF
         coeff = parinfo.value
       ENDIF

                    
       ; Find the number of fit parameters
       num_fit_var = N_ELEMENTS(coeff)


       ; Initialize the error state
       fit_error = 0
       fit_status = 1

       status_message = ''
       string_status = 'ok'
       
       ; Set the default message_procedure
       MIR_DEFAULT, message_pro, ''


       IF KEYWORD_SET(use_mpcurvefit) AND KEYWORD_SET(use_curvefit) THEN BEGIN
         MESSAGE, 'Both /MPCURVEFIT & /CURVEFIT are set. Intent unknown.'
       ENDIF


       ; Check to see if parinfo was passed in
       CASE 1 OF
         N_ELEMENTS(parinfo) EQ 0: BEGIN
           ; Check to see if fit was passed in instead
           IF N_ELEMENTS(fita) NE 0 THEN BEGIN
             IF N_ELEMENTS(fita) NE num_fit_var THEN BEGIN
               MESSAGE, 'Size of FITA does not match size of COEFF.'
             ENDIF
               
             ; We have a fita but not a parinfo, so lets generate a parinfo
             parinfo =  REPLICATE({fixed:0}, num_fit_var)
             parinfo[*].fixed = ~fita
             
           ENDIF ELSE BEGIN
             num_free_var = N_ELEMENTS(coeff)
           ENDELSE
             
         END

         N_ELEMENTS(parinfo) EQ N_ELEMENTS(coeff): BEGIN
           parinfo_tag_names = TAG_NAMES(parinfo)
           ; Check to see if parinfo has the tag 'fixed'
           IF (WHERE(parinfo_tag_names EQ 'FIXED'))[0] NE -1 THEN BEGIN
             ; Generate the fita array.
             fita = ~parinfo[*].fixed
             
             ; Lets find the number of free variables
             free_var = WHERE(parinfo[*].fixed NE 1)
             IF free_var[0] NE -1 THEN BEGIN
               num_free_var = N_ELEMENTS(free_var)
             ENDIF ELSE BEGIN
               num_free_var = 0
             ENDELSE
           ENDIF ELSE BEGIN
             num_free_var = N_ELEMENTS(parinfo)
           ENDELSE
         END

         ELSE: BEGIN
           MESSAGE, 'Size of PARINFO does not match size of COEFF.'
         END
       ENDCASE




       ; Now we need to choose between oo_curvefit & oo_mpcurvefit
       ; if there are no free variables do neither.
       ; keywords take precedence.
       CASE 1 OF
         num_free_var EQ 0: BEGIN
           MESSAGE_SWITCH, 'No free variables.', MESSAGE_PRO=message_pro, MESSAGE_OBJ=message_obj
           use_mpcurvefit = 0
           use_curvefit = 0
         END
         KEYWORD_SET(use_mpcurvefit):
         KEYWORD_SET(use_curvefit):
         ELSE: BEGIN
           use_mpcurvefit = 1
         ENDELSE
       ENDCASE




       IF fit_error EQ 0 THEN BEGIN
         CASE 1 OF 
           KEYWORD_SET(use_mpcurvefit): BEGIN

             yfit = OO_MPCURVEFIT(x $
                                  ,y $
                                  ,wts $
                                  ,coeff $
                                  ,sigma $
                                  ,FUNCTARGS=fa $                 
                                  ,STATUS=status $
                                  ,QUIET=~KEYWORD_SET(verbose) $
                                  ,QUERY=query $
                                  ,CHISQ=chisq $
                                  ,FUNCTION_NAME=function_name $
                                  ,OBJECT_REFERENCE=object_ref $
                                  ,ITER=iter $
                                  ,ITMAX=maxiter $
                                  ,PARINFO=parinfo $
                                  ,NODERIVATIVE=noderivative $
                                  ,TOL=tol $
                                  ,COVAR=covar $
                                  ,ERRMSG=errmsg $
                                  ,_EXTRA=extra)
             
             IF KEYWORD_SET(print_status_message) OR KEYWORD_SET(verbose) THEN BEGIN
               status_message = MPCURVEFIT_STATUS(status)

               MESSAGE_SWITCH, STRCOMPRESS(STRING('MPCURVEFIT status:', status $
                                                  ,' ', status_message)) $
                               , MESSAGE_PRO=message_pro, MESSAGE_OBJ=message_obj
             ENDIF

             CASE 1 OF
               status LE 0: BEGIN
                 ; Status equals 0 means that something bad happend.
                 MESSAGE, 'MPCURVEFIT failure: '+errmsg
                 string_status = 'MPCURVEFIT failure'
                 fit_status = 0
               END
               status GE 4: BEGIN
                 MESSAGE_SWITCH, STRCOMPRESS(STRING('MPCURVEFIT possible failure')) $
                   ,MESSAGE_PRO=message_pro, MESSAGE_OBJ=message_obj
                 string_status = 'MPCURVEFIT possible failure'
                 fit_status = 2
               END
               ELSE: BEGIN
                 fit_status = 1
               ENDELSE
             ENDCASE
             
           END ;KEYWORD_SET(use_mpcurvefit)


           KEYWORD_SET(use_curvefit): BEGIN

             yfit = OO_CURVEFIT(x $
                                ,y $
                                ,wts $
                                ,coeff $
                                ,sigma $
                                ,CHISQ=chisq $
                                ,DOUBLE=double $
                                ,FITA=fita $
                                ,FUNCTION_NAME=function_name $
                                ,OBJECT_REFERENCE=object_ref $
                                ,ITER=iter $
                                ,ITMAX=maxiter $
                                ,NODERIVATIVE=noderivative $
                                ,STATUS=status $
                                ,TOL=tol $
                                ,YERROR=yerror)
             
             ; OO_CURVEFIT returnes the reduced chisq, I want the actual chisq.
             chisq = TOTAL((y - yfit)^2 * wts)
             
             IF KEYWORD_SET(print_status_message) OR KEYWORD_SET(verbose) THEN BEGIN
               MESSAGE_SWITCH, STRCOMPRESS(STRING('CURVEFIT status:', status)) $
                               ,MESSAGE_PRO=message_pro, MESSAGE_OBJ=message_obj
             ENDIF

             IF status NE 0 THEN BEGIN
               fit_error = 1
               MESSAGE_SWITCH, STRCOMPRESS(STRING('CURVEFIT failure.')) $
                               ,MESSAGE_PRO=message_pro, MESSAGE_OBJ=message_obj

               string_status = 'CURVEFIT failure'
               fit_status = 0
             ENDIF ELSE BEGIN
               fit_status = 1
             ENDELSE

           END ;KEYWORD_SET(use_curvefit)
           ELSE:
         ENDCASE
       ENDIF ;fit_error EQ 0



       ; If there were no free variables, or if we encountered an error, 
       ; then just calculate the current yfit.
       IF (fit_error NE 0) OR (num_free_var EQ 0) THEN BEGIN
         ; Calculate yfit
         IF OBJ_VALID(object_ref) THEN BEGIN
           CALL_METHOD, function_name, object_ref, x, coeff, yfit
         ENDIF ELSE BEGIN
           CALL_PROCEDURE, function_name, x, coeff, yfit
         ENDELSE

         ; Calculete Chisq
         chisq = TOTAL((y - yfit)^2 * wts)
           
         ; Generate an empty sigma array
         sigma = DBLARR(num_fit_var)
       ENDIF


       IF fit_error NE 0 THEN BEGIN
         fit_status = 0
       ENDIF


       ; Calculate the reduced chisq
       reduced_chisq = chisq / (N_ELEMENTS(x) - num_free_var)
       

       ; Some debugging output
       IF KEYWORD_SET(debug) THEN BEGIN
         PRINT, 'CURVEFIT_SWITCH DEBUG:'
         PRINT, '  num_fit_var', num_fit_var
         PRINT, '  num_free_var', num_free_var
       ENDIF
       

       RETURN, yfit

     END ;FUNCTION CURVEFIT_SWITCH

