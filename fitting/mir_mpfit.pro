

;+==============================================================================
; NAME:
;   MIR_MPFIT
;
; AUTHOR:
;   Novimir A. Pablant
;      npablant@pppl.gov
;      novimir.pablant@amicitas.com
;
; PURPOSE:
;   This is a wrapper for MPFIT that allows an object method to be called
;   instead of only a function.
;
; DESCRIPTION:
;   Input parameters and keywords are identical to MPFIT except for the
;   addition of an OBJECT_REF keyword.
;
;   See the MPFIT documentation for mor information.
;-==============================================================================


;+==============================================================================
; This is the call-back function for MPFIT.  It evaluates the
; function, subtracts the data, and returns the residuals.
;-==============================================================================
function mir_mpfit_eval, p, dp

  COMPILE_OPT strictarr
  common mir_mpfit_common, c_mir_mpfit

  ;; The function is evaluated here.  There are two choices,
  ;; depending on whether the derivative parameter "dp" is passed, 
  ;; meaning that derivatives should be calculated analytically by 
  ;; the function itself.
  ;;
  ;; In addtion if an object reference was given then interprect the
  ;; the given function name as a method of the given object.

  if c_mir_mpfit.object_ref EQ obj_new() then begin
    if n_params() GT 1 then begin
      residual = call_function(c_mir_mpfit.function_name, p, dp)
    endif else begin
      residual = call_function(c_mir_mpfit.function_name, p)
    endelse
  endif else begin
    if n_params() GT 1 then begin
      residual = call_method(c_mir_mpfit.function_name, c_mir_mpfit.object_ref, p, dp)
    endif else begin
      residual = call_method(c_mir_mpfit.function_name, c_mir_mpfit.object_ref, p)
    endelse
  endelse
      
  ;; Make sure the returned result is one-dimensional.
  residual = reform(residual, n_elements(residual), /overwrite)
  return, residual
  
end

;+==============================================================================
; My mpfit wrapper.
;-==============================================================================
function mir_mpfit, function_name $
                    ,p $
                    ,OBJECT_REFERENCE=object_ref $

                    ,STATUS=status $
                    ,QUERY=query $

                    ,QUIET=quiet $
                    ,ERRMSG=errmsg $
                    ,_REF_EXTRA=_extra


  COMPILE_OPT strictarr
  status = 0L
  errmsg = ''

  ;; Detect MPFIT and crash if it was not found
  catch, catcherror
  if catcherror NE 0 then begin
      MPFIT_NOTFOUND:
      catch, /cancel
      message, 'ERROR: the required function MPFIT must be in your IDL path', /info
      return, !values.d_nan
  endif
  if mpfit(/query) NE 1 then goto, MPFIT_NOTFOUND
  catch, /cancel
  if keyword_set(query) then return, 1

  if n_params() EQ 0 then begin
      message, "USAGE: params = MIR_MPFIT('MYFUNCT', start_params, ... )", /info
      return, !values.d_nan
  endif

  mir_default, object_ref, obj_new()

  ;; Use common block to pass data back and forth
  common mir_mpfit_common, c_mir_mpfit
  c_mir_mpfit = {function_name:function_name $
                 ,object_ref:object_ref $
                }


  result = mpfit('mir_mpfit_eval' $
                 ,p $
                 ,STATUS=status $
  
                 ,ERRMSG=errmsg $
                 ,QUIET=quiet $
                 ,_STRICT_EXTRA=_extra)


  ;; Some cleanup
  c_mir_mpfit = {}


  ;; Print error message if there is one.
  if NOT keyword_set(quiet) AND errmsg NE '' then begin
    message, errmsg, /info
  endif

  return, result
end
