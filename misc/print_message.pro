
;+==============================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
; DATE:
;   2010-09
;
; PURPOSE:
;   Define a standard way to print out error messages.
;
;
;-==============================================================================



     PRO PRINT_MESSAGE, message_array, ERROR=error, _REF_EXTRA=extra

       ; print the message.
       FOR ii = 0, N_ELEMENTS(message_array)-1 DO BEGIN
         PRINT, message_array[ii]
       ENDFOR

       IF KEYWORD_SET(error) THEN BEGIN
         HELP, OUTPUT=traceback, /LAST_MESSAGE
         traceback = MIR_STRREPLACE(STRJOIN(traceback), 'Execution halted at:', '          Traceback:')
         traceback = '%' + STRSPLIT(traceback, '%', /EXTRACT)
         
         PRINT_MESSAGE, ''
         PRINT_MESSAGE, traceback
         PRINT_MESSAGE, ''
       ENDIF

     END ;PRO PRINT_MESSAGE
