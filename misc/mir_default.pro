

;+==============================================================================
; AUTHOR:
;   Wayne Solomon
;
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2012-02
;
; PURPOSE:
;   Sets var to default, unless it already has a value.
;   much clearer than if n_elements(foo) eq 0.....
;
;
;-==============================================================================


     PRO MIR_DEFAULT, var, default $
                      ,ARRAY_SIZE=array_size $
                      ,COMMENT=comment $
                      ,VERBOSE=verbose
      
       
       IF (N_ELEMENTS(var) EQ 0) THEN BEGIN
         var=default 
         IF KEYWORD_SET(verbose) THEN BEGIN
           IF N_ELEMENTS(comment) EQ 0 THEN comment=''
           PRINT, 'DEFAULT [',default,'] taken, ' + comment
         ENDIF
       ENDIF ELSE BEGIN
         IF KEYWORD_SET(verbose) THEN BEGIN
           PRINT, 'DEFAULT: variable was defined,  ', $
                  STRTRIM(n_elements(var),2), ' elements'
         ENDIF
       ENDELSE
       
       ;  Now deal with the array_size parameters
       IF N_ELEMENTS(array_size) GT 0 THEN BEGIN
         var_size = SIZE(var, /DIM)
         IF NOT ARRAY_EQUAL(array_size, var_size) THEN BEGIN
           IF var_size EQ 0 OR ARRAY_EQUAL(var_size, 1) THEN BEGIN
             var = REPLICATE(var, array_size)
           ENDIF ELSE BEGIN
             MESSAGE, 'DEFAULT: Value does not match given array size.'
           ENDELSE
         ENDIF
       ENDIF

     END ; PRO MIR_DEFAULT

