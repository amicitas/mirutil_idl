
;+==============================================================================
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
; PURPOSE:
;   MIR_PROFILER is a wrapper for the native profileing tool in idl (PROFILER).
;   It simplifies starting and stopping the profiler, and produces more 
;   readable output.
;
;   Note that the native profileing tools can be very inaccurate in many (most)
;   cases.  The time to profile short routines is much much longer than the
;   the actuall running of the routines.  This means that programs can take
;   significant longer to run with profiling turned on, and the results are
;   not meaningful.
;
;   To get accurate results for program timing use [MIR_TIMER] instead.
;
;-==============================================================================


     ;+=========================================================================
     ; PURPOSE:
     ;   
     ;
     ;-=========================================================================
     PRO MIR_PROFILER_REPORT, data $
                              ,NUM_DISPLAY=num_display $
                              ,SORT_ONLY=sort_only $
                              ,SORT_TIME=sort_time $
                              ,SORT_AVG=sort_avg
       
       

       CASE 1 OF
         KEYWORD_SET(sort_time): data = data[REVERSE(SORT(data.time))]
         KEYWORD_SET(sort_avg): data = data[REVERSE(SORT(data.only_time/data.count))]
         ELSE:  data = data[REVERSE(SORT(data.only_time))]
       ENDCASE

       ; Only display the top 20 longest runing routines.
       MIR_DEFAULT, num_display, 20

       ii_max = num_display-1 < N_ELEMENTS(data)-1


       label_length = MAX(STRLEN(data.name))
       label_length = label_length > 40
       ll = STRING(label_length, FORMAT='(i0)')


       fmt =       '(a-'+ll+', 1x, i9, 3(2x, f12.6))'
       fmt_label = '(a-'+ll+', 1x, a9, 3(2x, a12))'
       PRINT, ''
       PRINT, 'Module', 'Count', 'Only (s)', 'Time (s)', 'Avg. (s)', FORMAT=fmt_label
       FOR ii=0,ii_max DO BEGIN
         PRINT, data[ii].name $
                ,data[ii].count $
                ,data[ii].only_time $
                ,data[ii].time $
                ,data[ii].time/data[ii].count $
                ,FORMAT=fmt
       ENDFOR
       PRINT, ''

     END ; PRO MIR_PROFILER_REPORT



     ;+=========================================================================
     ; PURPOSE:
     ;   
     ;
     ;-=========================================================================
     PRO MIR_PROFILER, START=start_key $
                       ,STOP=stop_key $
                       ,RESET=reset_key $
                       ,REPORT=report_key $
                       ,LAST=last $
                       ,_REF_EXTRA=extra

       COMMON MIR_PROFILER, c_mir_profiler

       stop = KEYWORD_SET(stop_key)
       start = KEYWORD_SET(start_key)
       reset = KEYWORD_SET(reset_key)
       report = KEYWORD_SET(report_key)
       last = KEYWORD_SET(last)

       IF ~stop AND ~start AND ~reset AND ~report AND ~last THEN BEGIN
         MESSAGE, 'At least one of the following keywords must be given: /START, /STOP, /REPORT, /RESET, /LAST.'
       ENDIF

       IF last THEN BEGIN
         IF ISA(c_mir_profiler) THEN BEGIN
           MIR_PROFILER_REPORT, c_mir_profiler.last_report, _STRICT_EXTRA=extra
         ENDIF ELSE BEGIN
           MESSAGE, 'No previous report found.'
         ENDELSE

         RETURN
       ENDIF

       IF stop OR report THEN BEGIN
         PROFILER, /REPORT, DATA=data
         MIR_PROFILER_REPORT, data, _STRICT_EXTRA=extra

         c_mir_profiler = {last_report:data}
       ENDIF

       IF reset THEN BEGIN
         PROFILER, /RESET
       ENDIF

       IF stop THEN BEGIN
         PROFILER, /RESET
         PROFILER, /CLEAR
       ENDIF

       IF start THEN BEGIN
         PROFILER, /CLEAR
         PROFILER, /RESET
         PROFILER
         PROFILER, /SYSTEM
       ENDIF


     END ; PRO MIR_PROFILER
