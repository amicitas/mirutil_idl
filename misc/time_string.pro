
;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;   novimir.pablant@amicitas.com
;
; DATE:
;   2010-04
;
; VERSION:
;   1.0.0
;
; PURPOSE:
;   Return standardized time strings.
;   
; DESCRIPTION:
;   This routine will return a time string of the format:
;   YYYY-MM-DD HH:MM:SS
;
; OPTIONAL INPUT:
;   time
;       A time given as a double precision julian time/date.
;
; KEYWORDS:
;   If no keywords are given the default is both /DATE and /TIME.
;
;   /DATE
;       Return the date as part of the time string.
;
;   /TIME
;       Return the time as part of the time string.
;
;
;-======================================================================
     FUNCTION TIME_STRING, time, DATE=date_key, TIME=time_key

       IF ~ ISA(time) THEN time = SYSTIME(/JULIAN)
       
       time_string = ''

       time_key = KEYWORD_SET(time_key)
       date_key = KEYWORD_SET(date_key)
       IF ~ time_key AND ~ date_key THEN BEGIN
         time_key = 1
         date_key = 1
       ENDIF


       IF date_key THEN BEGIN
         ; Get the current date.
         time_string += STRING( time $
                               ,FORMAT='(C(CYI4,"-",CMOI2.2, "-", CDI2.2))')
       ENDIF

       IF time_key THEN BEGIN
         IF date_key THEN BEGIN
           time_string += ' '
         ENDIF

         ; Get the current time.
         time_string += STRING( time $
                                ,FORMAT='(C(CHI2.2,":",CMI2.2,":",CSI2.2))')
       ENDIF

       RETURN, time_string

     END ;FUNCTION TIME_STRING
