

;+==============================================================================
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
; PURPOSE:
;
;-=============================================================================


     ;+=========================================================================
     ;
     ; PURPOSE:  
     ;
     ;
     ;-=========================================================================
     FUNCTION MIR_TIMER::INIT
       COMPILE_OPT STRICTARR

       self.runtime = HASH()
       self.starttime = HASH()

       RETURN, self.MIR_HASH_OBJECT::INIT()

     END ; FUNCTION MIR_TIMER::INIT



     ;+=========================================================================
     ;
     ; PURPOSE:  
     ;
     ;
     ;-=========================================================================
     PRO MIR_TIMER::RESET
       COMPILE_OPT STRICTARR

       self.runtime.REMOVE, /ALL
       self.starttime.REMOVE, /ALL

     END ; PRO MIR_TIMER::INIT



     ;+=========================================================================
     ;
     ; PURPOSE:  
     ;
     ;
     ;-=========================================================================
     PRO MIR_TIMER::START, name
       COMPILE_OPT STRICTARR

       IF ~ ISA(name) THEN BEGIN
         name = (SCOPE_TRACEBACK(/STRUCTURE))[-3].routine
       ENDIF

       IF ~ self.starttime.HASKEY(name) THEN BEGIN
         (self.starttime)[name] = SYSTIME(/SECONDS)
       ENDIF

     END ; PRO MIR_TIMER::STOP, name



     ;+=========================================================================
     ;
     ; PURPOSE:  
     ;
     ;
     ;-=========================================================================
     PRO MIR_TIMER::STOP, name
       COMPILE_OPT STRICTARR

       time =  SYSTIME(/SECONDS)


       IF ~ ISA(name) THEN BEGIN
         name = (SCOPE_TRACEBACK(/STRUCTURE))[-3].routine
       ENDIF

       IF ~ self.starttime.HASKEY(name) THEN BEGIN
         MIR_LOGGER, 'No start time found for: '+name, /ERROR
         RETURN
       ENDIF

       runtime = time - self.starttime.REMOVE(name)

       IF self.runtime.HASKEY(name) THEN BEGIN
         value = (self.runtime)[name]
       ENDIF ELSE BEGIN
         value = {name:name $
                  ,time:0D $
                  ,count:0L}
       ENDELSE

       value.time += runtime
       value.count += 1

       (self.runtime)[name] = value

     END ; PRO MIR_TIMER::STOP, name




     ;+=========================================================================
     ;
     ; PURPOSE:  
     ;
     ;
     ;-=========================================================================
     PRO MIR_TIMER::REPORT, NUM_DISPLAY=num_display $
                              ,SORT_AVERAGE=sort_average
       COMPILE_OPT STRICTARR

       values = (self.runtime.VALUES()).TOARRAY()

       IF N_ELEMENTS(values) EQ 0 THEN BEGIN
         MIR_LOGGER, /INFO, 'No timing information available.'
         RETURN
       ENDIF

       CASE 1 OF
         KEYWORD_SET(sort_avg): values = values[REVERSE(SORT(values.time/values.count))]
         ELSE:  values = values[REVERSE(SORT(values.time))]
       ENDCASE
       

       ; Only display the top 20 longest runing routines.
       MIR_DEFAULT, num_display, 20

       ii_max = num_display-1 < N_ELEMENTS(values)-1


       label_length = MAX(STRLEN(values.name))
       label_length = label_length > 30
       ll = STRING(label_length, FORMAT='(i0)')


       fmt =       '(a-'+ll+', 1x, i9, 2(2x, f12.6))'
       fmt_label = '(a-'+ll+', 1x, a9, 2(2x, a12))'
       PRINT, ''
       PRINT, 'Module', 'Count', 'Time (s)', 'Avg. (s)', FORMAT=fmt_label
       FOR ii=0,ii_max DO BEGIN
         PRINT, values[ii].name $
                ,values[ii].count $
                ,values[ii].time $
                ,values[ii].time/values[ii].count $
                ,FORMAT=fmt
       ENDFOR
       PRINT, ''


     END ; PRO MIR_TIMER::STOP, name



     ;+=========================================================================
     ;
     ; PURPOSE:
     ;   Setup the MIR_TIMER object definition.
     ;
     ;
     ;-=========================================================================
     PRO MIR_TIMER__DEFINE

       struct = { MIR_TIMER $
                  ,runtime:OBJ_NEW() $
                  ,starttime:OBJ_NEW() $
                  ,INHERITS MIR_HASH_OBJECT $
                }

     END ; PRO MIR_TIMER__DEFINE
