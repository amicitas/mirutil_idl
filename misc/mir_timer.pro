
;+==============================================================================
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
; PURPOSE:
;   A profiling tool to monitor the time it takes to run IDL routines.
;   
;   This tool can be used to accuratly profile IDL routines.  It is meant to
;   be used as a replacement for the native IDL profiling routine [PROFILER].
;   While [MIR_TIMER] is a bit more cumbersome to use, it produces accurate
;   results. 
;
; OPTIONAL INPUTS:
;   name
;       A string to be used as a key in reporting run time.
;       If not given, then the name of the calling routine will be used.
;
; KEYWORDS:
;   /START
;       Start the timer for the current function or the given name.
;
;   /STOP
;       Stop the timer for he current function or the given name.
;
;   /RESET
;       Clear all saved data.  This should generally be called before starting
;       any profiling.
;
;   /REPORT
;       Print a timing report to the terminal.
;
;   /BEGIN_TIMING
;       Enable the MIR_TIMER.  Before this is called no profileing will be
;       and any calls with /START or /STOP will be ignored.
;
;   /END_TIMING
;       Disable the MIR_TIMER. See the /BEGIN_TIMING option for a description.
;
;   /HELP
;       Display this help message.
;
;-==============================================================================



     PRO MIR_TIMER, name $
                    ,START=start_key $
                    ,STOP=stop_key $
                    ,RESET=reset_key $
                    ,REPORT=report_key $
                    ,BEGIN_TIMING=begin_timing_key $
                    ,END_TIMING=end_timing_key $
                    ,HELP=help
       COMPILE_OPT STRICTARR
       COMMON MIR_TIMER, c_mir_timer, c_mir_timer_status

       IF KEYWORD_SET(help) THEN BEGIN INFO & RETURN & ENDIF

       ; Allow the timer to be enabled or disabled.
       ; If disabled we just return.  This will minimize the performance impact
       ; of adding timing statements when timing is disabled.
       IF ~ ISA(c_mir_timer_status) THEN BEGIN
         c_mir_timer_status = 0
       ENDIF


       CASE 1 OF
         KEYWORD_SET(begin_timing_key):BEGIN
           c_mir_timer_status = 1
           RETURN
         END
         KEYWORD_SET(end_timing_key):BEGIN
           c_mir_timer_status = 0
           RETURN
         END
         ELSE:
       ENDCASE


       start_key = KEYWORD_SET(start_key)
       stop_key = KEYWORD_SET(stop_key)

       IF start_key OR stop_key THEN BEGIN
         IF ~ c_mir_timer_status THEN RETURN
       ENDIF


       ; Retrive the timer object.
       IF ~ OBJ_VALID(c_mir_timer) THEN BEGIN
         c_mir_timer = OBJ_NEW('MIR_TIMER')
       ENDIF


       IF start_key OR stop_key THEN BEGIN
         IF N_PARAMS() EQ 0 THEN BEGIN
           ; No name was given, get the name from the calling routine.
           call_back = SCOPE_TRACEBACK(/STRUCTURE)
           caller = call_back[N_ELEMENTS(call_back)-2]
           name = caller.routine
         ENDIF
       ENDIF


       ; Call the appropriate methods of the timer object.
       CASE 1 OF
         start_key: c_mir_timer.START, name
         stop_key:  c_mir_timer.STOP, name
         KEYWORD_SET(reset_key):  c_mir_timer.RESET
         KEYWORD_SET(report_key): c_mir_timer.REPORT
         ELSE: BEGIN
           MESSAGE, 'One of the following keywords must be given: /START, /STOP, /REPORT, /RESET, /BEGIN, /END'
         ENDELSE
       ENDCASE

     END ; PRO MIR_TIMER
