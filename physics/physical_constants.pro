
     

;+====================================================================== 
; 
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-08
;
; PURPOSE:
;   Return a structure containing physics constants.
;
; TAGS:
;   EXCLUDE_FILE_HEADER
;
;
;-======================================================================



     ;+=================================================================
     ;
     ; Return a structure containing physics constants.
     ;
     ;-=================================================================
     FUNCTION PHYSICAL_CONSTANTS
     
       ; DATA TAKEN FROM 
       ; http://physics.nist.gov/cuu/Constants/Table/allascii.txt
       ; 2007, June
  
       ; WAVELENGTH OF D-ALPHA LINE
       lambda_dalpha = 6561.032D ;angstroms
  
       ; WAVELENGTH OF H-ALPHA LINE
       ; In air. Taken from the NIST line database.
       lambda_halpha = 6562.852D ;angstroms
  
       ; BOHR RADIUS
       a0 = 0.52917720859D-10 ;m
       
       ; RYDBERG CONSTANT
       ry = 1.0973731568527D7 ;1/m
  
       ; PLANK CONSTANT
       h = 6.62606896D-34 ;Js
  
       ; SPEED OF LIGHT
       c = 2.99792458D8 ;m/s
  
       ; ELEMENTARY CHARGE
       e = 1.602176487D-19 ;C
  
       ; PERMEABILITY OF FREE SPACE
       e0 = 8.854187817D-12 ;F/m
  
       ; ELECTRON MASS
       me = 9.10938215D-31 ; kg
  
       ; ATOMIC MASS OF HYDROGEN
       m_H = 1.00727646688D0 ;amu
       ; ATOMIC MASS OF DEUTERIUM
       m_D = 2.014101777D0 ;amu
  
       
       ; BOLTZMANN CONSTANT
       k = 1.3806504D-23 ; J/K


       ; CONVERSION amu TO kg
       amu_kg = 1.660538782D-27 ;kg/amu
  
       ; CONVERSION amu TO eV
       amu_ev = 931.494028D6 ;eV/amu
  
       ; PROTON MASS
       mp = m_H * amu_kg ;kg
  
       ; CONVERSION FROM eV TO J
       ev_J = 1.602176487D-19
  
       ; CONVERSION FROM eV TO K
       ev_K = 1.1604505D4

       data = { $
                lambda_dalpha:lambda_dalpha $
                ,lambda_halpha:lambda_halpha $
                ,a0:a0 $
                ,ry:ry $
                ,h:h $
                ,c:c $
                ,e:e $
                ,e0:e0 $
                ,me:me $
                ,mp:mp $
                ,m_H:m_H $
                ,m_D:m_D $
                ,k:k $
                ,amu_kg:amu_kg $
                ,amu_ev:amu_ev $
                ,ev_J:ev_J $
                ,ev_K:ev_K $
              }
  
       RETURN, data

     END ;FUNCTION PHYSICAL_CONSTANTS
