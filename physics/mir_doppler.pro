

;+==============================================================================
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
; PURPOSE:
;  Calculate a doppler shift.
;
;-==============================================================================


     FUNCTION MIR_DOPPLER, wavelength, velocity, angle

       constant = PHYSICAL_CONSTANTS()

       RETURN, wavelength*(1-velocity*COS(angle)/constant.c)/SQRT(1-(velocity/constant.c)^2)

     END ; FUNCTION MIR_DOPPLER
