


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;   amicitas@gmail.com
;
; DATE:
;   2009-11
;
; VERSION:
;   1.0.0
;
; PURPOSE:
;   Define an obejct that will be inherited by all object based
;   dialog widgets.
;
;-======================================================================


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Temporary for testing.  Put a button into the gui.
     ;
     ;-=================================================================
     ;PRO WIDGET_DIALOG::GUI_DEFINE

     ;  ui_base = self.gui_param.gui_id

     ;  ui_button = WIDGET_BUTTON(ui_base, VALUE='0000000ooooooo')

     ;END ;PRO WIDGET_DIALOG::GUI_DEFINE



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Initialize the <WIDGET_DIALOG::> object.
     ;
     ;   This will be called when the component object is created.
     ;
     ; DESCRIPTION:
     ;   For a dialog object we take care of destroying the object
     ;   manually, so make sure it does not get destroid with the
     ;   widget.
     ;
     ;-=================================================================
     FUNCTION WIDGET_DIALOG::INIT, group_leader $
                                   ,SHOW=show $
                                   ,STATUS=status $
                                   ,MODAL=modal

       IF ISA(group_leader) THEN BEGIN
         self.group_leader = group_leader
       ENDIF

       self.modal = KEYWORD_SET(modal)

       init_status = self->WIDGET_OBJECT::INIT(DESTROY_OBJECT_WITH_GUI=0)
       IF ~ init_status THEN RETURN, init_status
       
       IF KEYWORD_SET(show) THEN BEGIN
         status = self->SHOW(/NO_DESTROY)
         RETURN, 0
       ENDIF

       RETURN, 1

     END ;FUNCTION WIDGET_DIALOG::INIT



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   This is the main method to show the GUI and return the
     ;   result.
     ;
     ;-=================================================================
     FUNCTION WIDGET_DIALOG::SHOW, NO_DESTROY=no_destroy

       self->GUI_INIT

       self->GUI_UPDATE

       self->START_XMANAGER

       status = self->GET_STATUS()

       IF ~ KEYWORD_SET(no_destroy) THEN BEGIN
         OBJ_DESTROY, self
       ENDIF

       RETURN, status

     END ;FUNCTION WIDGET_DIALOG::SHOW



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return the status of the dialog widget.
     ;   This method will be called just before <::SHOW()> returns
     ;   and will be used as the return value.
     ;
     ;   This routine should be reimplemented by any dialogs derived
     ;   from this object.
     ;
     ;-=================================================================
     FUNCTION WIDGET_DIALOG::GET_STATUS

       RETURN, 1

     END ;FUNCTION WIDGET_DIALOG::GET_STATUS

     

     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Create A gui for this object.
     ;
     ;   The gui can either be created in its own window or as part
     ;   of another widget.
     ;
     ; DESCRIPTION:
     ;   This routine creates the base window, calls GUI_DEFINE,
     ;   then start the event manager.
     ;
     ;-=================================================================
     PRO WIDGET_DIALOG::GUI_INIT, GROUP_LEADER=group_leader $
                                  ,TITLE=title $
                                  ,FRAME=frame $
                                  ,CREATE_MENU_BAR=create_menu_bar

       uname = 'UI_BASE_'+OBJ_CLASS(self)
       uvalue = self
       event_pro = 'WIDGET_OBJECT_EVENT'
       cleanup_pro = 'WIDGET_OBJECT_CLEANUP'
       MIR_DEFAULT, title, 'WIDGET_DIALOG object'
       MIR_DEFAULT, frame, 0
       MIR_DEFAULT, group_leader, self.group_leader
       
       ; Check if the group_leader is a valid id. 
       ; If so make the dialog floating.
       floating = WIDGET_INFO(group_leader, /VALID_ID)

       ; Setup the base widget
       IF KEYWORD_SET(create_menu_bar) THEN BEGIN
         self.gui_param.gui_id = WIDGET_BASE(/COLUMN $
                                             ,UNAME=uname $
                                             ,UVALUE=uvalue $
                                             ,FRAME=frame $
                                             ,EVENT_PRO=event_pro $
                                             ,TITLE=title $
                                             ,GROUP_LEADER=group_leader $
                                             ,/TLB_KILL_REQUEST_EVENTS $
                                             ,/TLB_SIZE_EVENTS $
                                             ,MBAR=ui_menu_bar $
                                             ,FLOATING=floating $
                                            )
         self.gui_param.ui_menu_bar = ui_menu_bar
       ENDIF ELSE BEGIN
         self.gui_param.gui_id = WIDGET_BASE(/COLUMN $
                                             ,UNAME=uname $
                                             ,UVALUE=uvalue $
                                             ,FRAME=frame $
                                             ,EVENT_PRO=event_pro $
                                             ,TITLE=title $
                                             ,GROUP_LEADER=group_leader $
                                             ,/TLB_KILL_REQUEST_EVENTS $
                                             ,/TLB_SIZE_EVENTS $
                                             ,FLOATING=floating $
                                            )
       ENDELSE

       ; Now build up the actuall gui components.
       self->GUI_DEFINE

      END ; PRO WIDGET_DIALOG::GUI_INIT



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Start the xmanager.
     ;
     ;-=================================================================
     PRO WIDGET_DIALOG::START_XMANAGER

       event_pro = 'WIDGET_OBJECT_EVENT'
       cleanup_pro = 'WIDGET_OBJECT_CLEANUP'

       modal = (self.modal AND WIDGET_INFO(self.group_leader, /VALID_ID))

       IF modal THEN BEGIN
         sensitive_orig = WIDGET_INFO(self.group_leader, /SENSITIVE)
         WIDGET_CONTROL, self.group_leader, SENSITIVE = 0
       ENDIF


       WIDGET_CONTROL, self.gui_param.gui_id, /REALIZE

       ; Regester with the event handler
       XMANAGER, 'WIDGET_DIALOG::START_XMANAGER', self.gui_param.gui_id $
                 ,EVENT_HANDLER=event_pro $
                 ,CLEANUP=cleanup_pro

       IF modal THEN BEGIN
         WIDGET_CONTROL, self.group_leader, SENSITIVE = sensitive_orig
       ENDIF

     END ;FUNCTION WIDGET_DIALOG::START_XMANAGER



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Define the <WIDGET_DIALOG::> object as a derived class
     ;   of the <WIDGET_OBJECT::>
     ;
     ; TAGS:
     ;   USE_FILE_HEADER
     ;
     ;-=================================================================
     PRO WIDGET_DIALOG__DEFINE

       
       struct = { WIDGET_DIALOG $
                  ,group_leader:0L $
                  ,modal:0 $
                  ,INHERITS WIDGET_OBJECT $
                }


     END ;PRO WIDGET_DIALOG__DEFINE
