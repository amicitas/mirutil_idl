


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
; DATE:
;   2009-09
;
; VERSION:
;   0.0.0
;
; PURPOSE:
;   A more advanced combobox widget.  
;   This is meant to be a replacement for WIDGET_COMBOBOX.
;
;-======================================================================

     ;+=================================================================
     ;
     ; Cleanup the combobox widget.
     ;
     ;-=================================================================
     PRO CW_COMBOBOX_KILL, state_holder

       WIDGET_CONTROL, state_holder, GET_UVALUE=state

       OBJ_DESTROY, state.list
       OBJ_DESTROY, state.menu_id

     END ;PRO CW_COMBOBOX_KILL

     ;+=================================================================
     ;
     ; Main function for the CW_COMBOBOX widget
     ;
     ;-=================================================================
     PRO CW_COMBOBOX_GENERATE_MENU, base
         
       state_holder = WIDGET_INFO(base, /CHILD)
       WIDGET_CONTROL, state_holder, GET_UVALUE=state, /NO_COPY


       FOR ii=0,state.menu_id->N_ELEMENTS()-1 DO BEGIN
         IF WIDGET_INFO(state.menu_id->GET(ii), /VALID) THEN BEGIN
           WIDGET_CONTROL, state.menu_id->GET(ii), /DESTROY
         ENDIF
       ENDFOR
       state.menu_id->REMOVE, /ALL

       IF state.list->N_ELEMENTS() EQ 0 THEN BEGIN
         ; Put in a place holder if the list in empty
         value = STRJOIN(REPLICATE(' ', state.xsize))
         button = WIDGET_BUTTON(state.id.context, VALUE=value)
         state.menu_id->APPEND, button
       ENDIF ELSE BEGIN
         FOR ii=0,state.list->N_ELEMENTS()-1 DO BEGIN
           value = state.list->GET(ii)
           
           IF STRLEN(value) LT state.xsize THEN BEGIN
             value += STRJOIN(REPLICATE(' ', state.xsize - STRLEN(value)))
           ENDIF
           
           button = WIDGET_BUTTON(state.id.context, VALUE=value)
           state.menu_id->APPEND, button
         ENDFOR
       ENDELSE


       WIDGET_CONTROL, state_holder, SET_UVALUE=state, /NO_COPY

     END ; PRO CW_COMBOBOX_GENERATE_MENU


     ;+=================================================================
     ;
     ; Main function for the CW_COMBOBOX widget
     ;
     ;-=================================================================
     PRO CW_COMBOBOX_SET, base, input, NO_COPY=no_copy

       ; There are a number of different ways to use this routine.

       CASE SIZE(input, /TYPE) OF

         11: BEGIN
           ; Input is an object, assume it is a 'list' object.
           list = input
           IF KEYWORD_SET(no_copy) THEN BEGIN
             input = OBJ_NEW()
           ENDIF
         END

         8: BEGIN
           IF HAS_TAG(input, 'TEXT') THEN text = input.text
           IF HAS_TAG(input, 'INDEX') THEN index = input.index
           IF HAS_TAG(input, 'LIST') THEN array = input.list
           IF HAS_TAG(input, 'SELECTION') THEN selection = input.selection
         END

         ELSE: BEGIN
           array = input
         ENDELSE

       ENDCASE


       IF ISA(list) THEN BEGIN
         IF KEYWORD_SET(no_copy) THEN BEGIN
           list = input
         ENDIF ELSE BEGIN
           list = input->GET_COPY()
         ENDELSE
       ENDIF ELSE BEGIN

         IF ISA(array) THEN BEGIN
           ; Assume that the input is an array.
           list = OBJ_NEW('MIR_LIST_IDL7')

           ; If the null string, or an array only containing the null
           ; string is passed, then leave the list empty.
           IF SIZE(array, /DIM) GT 0 OR array[0] NE '' THEN BEGIN
             list->FROM_ARRAY, array

             ; Reset the index, unless it was user specified.
             MIR_DEFAULT, index, 0
           ENDIF

           ; Clear the text box
           CW_COMBOBOX_SET_TEXT, base, ''
         ENDIF

       ENDELSE


       ; If the input contained a menu list, then set it.
       IF ISA(list) THEN BEGIN
         CW_COMBOBOX_SET_LIST, base, list
       ENDIF

       IF ISA(text) THEN BEGIN
         CW_COMBOBOX_SET_TEXT, base, text
       ENDIF

       IF ISA(index) THEN BEGIN
         CW_COMBOBOX_SET_INDEX, base, index
       ENDIF

       IF ISA(selection) THEN BEGIN
         CW_COMBOBOX_SET_SELECTION, base, selection 
       ENDIF
         
     END ;PRO CW_COMBOBOX_SET



     ;+=================================================================
     ;
     ; Set the menu list.
     ;
     ;-=================================================================
     PRO CW_COMBOBOX_SET_LIST, base, list
       state_holder = WIDGET_INFO(base, /CHILD)
       WIDGET_CONTROL, state_holder, GET_UVALUE=state, /NO_COPY
       
       OBJ_DESTROY, state.list
       state.list = list
       
       WIDGET_CONTROL, state_holder, SET_UVALUE=state, /NO_COPY
       
       CW_COMBOBOX_GENERATE_MENU, base
     END ;PRO CW_COMBOBOX_SET_LIST



     ;+=================================================================
     ;
     ; Set the text in the text widget.
     ;
     ;-=================================================================
     PRO CW_COMBOBOX_SET_TEXT, base, text
       state_holder = WIDGET_INFO(base, /CHILD)
       WIDGET_CONTROL, state_holder, GET_UVALUE=state, /NO_COPY
       
       WIDGET_CONTROL, state.id.text, SET_VALUE=text
       
       WIDGET_CONTROL, state_holder, SET_UVALUE=state, /NO_COPY
     END ;PRO CW_COMBOBOX_SET_TEXT



     ;+=================================================================
     ;
     ; Set the text in the text widget.
     ;
     ;-=================================================================
     PRO CW_COMBOBOX_SET_INDEX, base, index
       state_holder = WIDGET_INFO(base, /CHILD)
       WIDGET_CONTROL, state_holder, GET_UVALUE=state, /NO_COPY
       
       IF (index LT 0) OR (index GT state.list->N_ELEMENTS()-1) THEN BEGIN
         MESSAGE, 'Index is out of range.'
       ENDIF

       WIDGET_CONTROL, state.id.text, SET_VALUE=state.list->GET(index)
       
       WIDGET_CONTROL, state_holder, SET_UVALUE=state, /NO_COPY
     END ;PRO CW_COMBOBOX_SET_INDEX



     ;+=================================================================
     ;
     ; Set the selection to the item with the given text.
     ;
     ;-=================================================================
     PRO CW_COMBOBOX_SET_SELECTION, base, selection

       state_holder = WIDGET_INFO(base, /CHILD)
       WIDGET_CONTROL, state_holder, GET_UVALUE=state, /NO_COPY
       
       IF state.list->N_ELEMENTS() EQ 0 THEN BEGIN
         IF selection NE '' THEN BEGIN
           MESSAGE, 'Cannot set selection, no items in list.'
         ENDIF
       ENDIF


       array = state.list->TO_ARRAY()
       
       w = WHERE(array EQ selection, count)
       IF count EQ 0 THEN BEGIN
         MESSAGE, 'Given selection not found in list.'
       ENDIF
       IF count GT 1 THEN BEGIN
         MESSAGE, 'More than one item in the list matches selection.'
       ENDIF

       WIDGET_CONTROL, state_holder, SET_UVALUE=state, /NO_COPY

       CW_COMBOBOX_SET_INDEX, base, w[0]

     END ;PRO CW_COMBOBOX_SET_SELECTION



     ;+=================================================================
     ;
     ; Main function for the CW_COMBOBOX widget
     ;
     ;-=================================================================
     FUNCTION CW_COMBOBOX_EVENT, event

       state_holder = WIDGET_INFO(event.handler, /CHILD)
       WIDGET_CONTROL, state_holder, GET_UVALUE=state


       ; Swallow context events.
       IF (TAG_NAMES(event, /STRUCTURE_NAME) EQ 'WIDGET_CONTEXT') THEN BEGIN
         RETURN, 0
       ENDIF


       ; Create the return structure.
       return_structure = { $
                            id:event.handler $
                            ,top:event.top $
                            ,handler:0L $
                            ,value:'' $
                            ,index:-1 $
                          }
       ; Create the return value.
       ; When appropriate this will be replaced by the return structure.
       return_value = 0



       CASE event.id OF

         state.id.button: BEGIN
           ; First get the geometry of the text box
           geometry = WIDGET_INFO(state.id.text, /GEOMETRY)
           WIDGET_DISPLAYCONTEXTMENU, state.id.text, 0, geometry.scr_ysize, state.id.context
         END

         state.id.text: BEGIN
           return_structure.value = event.value
           return_value = return_structure
         END

         ELSE: BEGIN

           ; Check to see if this is an id from the pull down menu
           which_button = WHERE(event.id EQ state.menu_id->TO_ARRAY())
           IF which_button[0] NE - 1 THEN BEGIN
             ; We found the button
             
             ; Only take action if the list has elements
             IF state.list->N_ELEMENTS() NE 0 THEN BEGIN
               ; Set the text.
               WIDGET_CONTROL, state.id.text, SET_VALUE=state.list->GET(which_button[0])
               
               return_structure.value = state.list->GET(which_button[0])
               return_structure.index = which_button[0]
               return_value = return_structure
             ENDIF

             BREAK
           ENDIF

           PRINT, 'Unknown event id.'
         END

       ENDCASE
         
       RETURN, return_value

     END ;FUNCTION CW_COMBOBOX_EVENT



     ;+=================================================================
     ;
     ; Main function for the CW_COMBOBOX widget
     ;
     ;-=================================================================
     FUNCTION CW_COMBOBOX, parent $
                           ,GROUP_LEADER=group_leader $
                           ,KILL_NOTIFY=kill_notify $
                           ,NOTIFY_REALIZE=notifiy_realize $

                           ,NO_COPY=no_copy $
                           ,TAB_MODE=tab_mode $

                           ,UVALUE=uvalue $
                           ,UNAME=uname $
                           ,SENSITIVE=sensitive $
                           ,FRAME=frame $

                           ,PRO_SET_VALUE=pro_set_value $
                           ,FUNC_GET_VALUE=func_get_value $

                           ,EVENT_FUNC=event_func $
                           ,EVENT_PRO=event_pro $



                           ,RESOURCE_NAME=resource_name $
                           ,IGNORE_ACCELERATORS=ignore_accelerators $



                           ,DYNAMIC_RESIZE=dynamic_resize $

                           ,EDITABLE=editable $
                           ,ALL_EVENTS=all_events $
                           ,RETURN_EVENTS=return_events $

                           ,VALUE=value $

                           ,UNITS=units $

                           ,XSIZE=xsize $
                           ,YSIZE=ysize $

                           ,SCR_XSIZE=scr_xsize $
                           ,SCR_YSIZE=scr_ysize $

                           ,XOFFSET=xoffset $
                           ,YOFFSET=yoffset

       MIR_DEFAULT, event_func, 'CW_COMBOBOX_EVENT'
       MIR_DEFAULT, pro_set_value, 'CW_COMBOBOX_SET'
       MIR_DEFAULT, func_get_value, 'CW_COMBOBOX_GET'

       MIR_DEFAULT, uname, 'CW_COMBOBOX'

       MIR_DEFAULT, editable, 0

       MIR_DEFAULT, xsize, 20

       ; Build the base widget.
       base = WIDGET_BASE(parent $
                          ,GROUP_LEADER=group_leader $
                          ,UNAME=uname $
                          ,UVALUE=uvalue $
                          ,/ROW $
                          ,EVENT_FUNC=event_func $
                          ,EVENT_PRO=event_pro $
                          ,PRO_SET_VALUE=pro_set_value $
                          ,FUNC_GET_VALUE=func_get_value $
                          ,FRAME=frame $
                          ,SENSITIVE=sensitive $
                          ,UNITS=units $
                          ,XOFFSET=xoffset $
                          ,YOFFSET=yoffset $
                          ,KILL_NOTIFY=kill_notify $
                          ,NOTIFY_REALIZE=notifiy_realize $
                          ,NO_COPY=no_copy $
                          ,TAB_MODE=tab_mode $
                          ,TRACKING_EVENTS=tracking_events $
                         )

       ; Build the text widget.
       text = WIDGET_TEXT(base $
                          ,UNAME=uname+'_TEXT' $
                          ,XSIZE=xsize $
                          ,YSIZE=ysize $
                          ,EDITABLE=editable $
                          ,/CONTEXT_EVENTS $
                          ,ALL_EVENTS=all_events $
                         )

       context = WIDGET_BASE(text $
                             ,UNAME=uname+'_CONTEXT' $
                             ,/CONTEXT_MENU $
                            )

       ; Build the button for the menu.
       icon_filepath = FILEPATH('arrowsm_down.bmp', SUBDIRECTORY=['resource','bitmaps'])
       button = WIDGET_BUTTON(base $
                              ,VALUE=icon_filepath $
                              ,/BITMAP $
                              ,FRAME=0)

      ; Save our internal state in the first child widget
       State   = { $
                   value:'' $
                   ,list:OBJ_NEW('MIR_LIST_IDL7') $
                   ,menu_id:OBJ_NEW('MIR_LIST_IDL7') $
                   ,editable:editable $
                   ,id:{text:text $
                        ,context:context $
                        ,button:button $
                       } $
                   ,xsize:xsize $
                 }

       WIDGET_CONTROL, WIDGET_INFO(Base, /CHILD), SET_UVALUE=State, /NO_COPY
       WIDGET_CONTROL, WIDGET_INFO(Base, /CHILD), KILL_NOTIFY='CW_COMBOBOX_KILL'

       IF ISA(value) THEN BEGIN
         CW_COMBOBOX_SET, base, value
       ENDIF

       RETURN, Base
     END ;FUNCTION CW_COMBOBOX
