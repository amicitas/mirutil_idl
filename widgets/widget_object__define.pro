


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;   amicitas@gmail.com
;
; DATE:
;   2009-11
;
; VERSION:
;   1.0.0
;
; PURPOSE:
;   Define an obejct that will be inherited by all gui components
;   of an object oriented GUI application.
;
;   This base class is parcicularly useful when using an addon
;   archetecture.
;   
;
;-======================================================================



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Initialize the <WIDGET_OBJECT::> object.
     ;
     ;   This will be called when the component object is created.
     ;
     ;-=================================================================
     FUNCTION WIDGET_OBJECT::INIT, DESTROY_OBJECT_WITH_GUI=destroy_object_with_gui

       MIR_DEFAULT, destroy_object_with_gui, 1
       self.gui_param.destroy_object_with_gui = KEYWORD_SET(destroy_object_with_gui)

       self->SET_ID_STRUCT, {NULL}

       RETURN, 1

     END ;FUNCTION WIDGET_OBJECT::INIT



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Create A gui for this object.
     ;
     ;   The gui can either be created in its own window or as part
     ;   of another widget.
     ;
     ; DESCRIPTION:
     ;   This routine creates the base window, calls GUI_DEFINE,
     ;   then start the event manager.
     ;
     ;-=================================================================
     PRO WIDGET_OBJECT::GUI_INIT, DESTINATION=ui_destination_base $
                                  ,GROUP_LEADER=group_leader $
                                  ,TITLE=title $
                                  ,UPDATE=update $
                                  ,MAP=map $
                                  ,FRAME=frame $
                                  ,MENUBAR=menubar


       uname = 'UI_BASE_'+OBJ_CLASS(self)
       uvalue = self
       event_pro = 'WIDGET_OBJECT_EVENT'
       cleanup_pro = 'WIDGET_OBJECT_CLEANUP'
       MIR_DEFAULT, title, 'WIDGET_OBJECT component'
       MIR_DEFAULT, frame, 0

       ; There are two ways that the GUI can be built:
       ;   1. Inside of a destination base.
       ;   2. In a separate window.
       ; 
       ; Here we take different action for each possibility.
       IF ISA(ui_destination_base) THEN BEGIN

         ; Get the current update state.
         update_state = WIDGET_INFO(ui_destination_base, /UPDATE)

         ; Diable updating of gui while we add the new widget
         WIDGET_CONTROL, ui_destination_base, UPDATE=0


         ; Setup the base widget
         self.gui_param.gui_id = WIDGET_BASE( ui_destination_base $
                                              ,/COLUMN $
                                              ,UNAME=uname $
                                              ,UVALUE=uvalue $
                                              ,FRAME=frame $
                                              ,EVENT_PRO=event_pro $
                                              ,KILL_NOTIFY=cleanup_pro $
                                              ,TITLE=title $
                                            )
       ENDIF ELSE BEGIN

         ; Setup the base widget
         ; If a menubar is desired, then add the MBAR keyword.
         IF KEYWORD_SET(menubar) THEN BEGIN
           self.gui_param.gui_id = WIDGET_BASE(/COLUMN $
                                               ,UNAME=uname $
                                               ,UVALUE=uvalue $
                                               ,FRAME=frame $
                                               ,EVENT_PRO=event_pro $
                                               ,TITLE=title $
                                               ,GROUP_LEADER=group_leader $
                                               ,/TLB_KILL_REQUEST_EVENTS $
                                               ,/TLB_SIZE_EVENTS $
                                               ,MAP=0 $
                                               ,MBAR=ui_menu_bar $
                                              )
           self.gui_param.ui_menu_bar = ui_menu_bar
         ENDIF ELSE BEGIN
           self.gui_param.gui_id = WIDGET_BASE(/COLUMN $
                                               ,UNAME=uname $
                                               ,UVALUE=uvalue $
                                               ,FRAME=frame $
                                               ,EVENT_PRO=event_pro $
                                               ,TITLE=title $
                                               ,GROUP_LEADER=group_leader $
                                               ,/TLB_KILL_REQUEST_EVENTS $
                                               ,/TLB_SIZE_EVENTS $
                                               ,MAP=0 $
                                              )
         ENDELSE

         ; Realize the window, but don't allow updating yet.
         WIDGET_CONTROL, self.gui_param.gui_id, /REALIZE, UPDATE=0
         update_state = 1
         map_state = 1

         ; Regester with the event handler
         XMANAGER, 'WIDGET_OBJECT::GUI_INIT', self.gui_param.gui_id $
                   ,EVENT_HANDLER=event_pro $
                   ,CLEANUP=cleanup_pro $
                   ,/NO_BLOCK

         ui_destination_base = self.gui_param.gui_id

       ENDELSE


       ; Now build up the actuall gui components.
       self->GUI_DEFINE

       IF ISA(update) THEN BEGIN
         update_state = update
       ENDIF

       IF ISA(map) THEN BEGIN
         map_state = map
       ENDIF

       ; Reenable update of the gui now that everything is built.
       WIDGET_CONTROL, ui_destination_base, UPDATE=update_state, MAP=map_state


      END ; PRO WIDGET_OBJECT::GUI_INIT



     ;+=================================================================
     ;
     ; Build the GUI components within the base widget.
     ;
     ; This must be reimplemented by any derived classes.
     ;
     ;-=================================================================
     PRO WIDGET_OBJECT::GUI_DEFINE

       MESSAGE, 'This method must be reimplemented by any derived classes.'

     END ; PRO WIDGET_OBJECT::GUI_DEFINE



     ;+=================================================================
     ;
     ; Update all of the gui components
     ;
     ; This must be reimplemented by any derived classes.
     ;
     ;-=================================================================
     PRO WIDGET_OBJECT::GUI_UPDATE

       ; This method should be reimplemented by any derived classes.

     END ; PRO WIDGET_OBJECT::GUI_UPDATE



     ;+=================================================================
     ;
     ; This routine is used to set the UPDATE flag on the main
     ; gui base.
     ;
     ; This can be used as part of [::GUI_UDPATE] to speed up the
     ; gui responce.
     ;
     ;-=================================================================
     PRO WIDGET_OBJECT::SET_GUI_UPDATE, update $
                                   ,CURRENT_STATUS=current_status

       IF ARG_PRESENT(current_status) THEN BEGIN
         current_status = WIDGET_INFO(self.gui_param.gui_id, /UPDATE)
       ENDIF

       WIDGET_CONTROL, self.gui_param.gui_id, UPDATE=update

     END ; PRO WIDGET_OBJECT::SET_GUI_UPDATE



     ;+=================================================================
     ;
     ; This routine is used to set the MAP flag on the main
     ; gui base.
     ;
     ; This can be used as part of [::GUI_UDPATE] to speed up the
     ; gui responce.
     ;
     ;-=================================================================
     PRO WIDGET_OBJECT::SET_GUI_MAP, map $
                                     ,CURRENT_STATUS=current_status

       IF ARG_PRESENT(map) THEN BEGIN
         current_status = WIDGET_INFO(self.gui_param.gui_id, /MAP)
       ENDIF

       WIDGET_CONTROL, self.gui_param.gui_id, MAP=map

     END ; PRO WIDGET_OBJECT::SET_GUI_MAP



     ;+=================================================================
     ;
     ; This is the event handler for the gui.
     ;
     ; All derived classes should reimplement this method. The
     ; reimplemented methed should first call this method, check
     ; the event handled flag, then continue processing.
     ;
     ;-=================================================================
     PRO WIDGET_OBJECT::EVENT_HANDLER, event $
                                       ,EVENT_HANDLED=event_handled

       event_handled = 0

       ; Handle kill events
       IF TAG_NAMES(event, /STRUCTURE_NAME) EQ 'WIDGET_KILL_REQUEST' THEN BEGIN
         self->CLEANUP_HANDLER
         event_handled = 1
         RETURN
       ENDIF


     END ; PRO WIDGET_OBJECT::EVENT_HANDLER



     ;+=================================================================
     ; PURPOSE:
     ;   Take any action when the widget is destroyed.
     ;
     ;-=================================================================
     PRO WIDGET_OBJECT::CLEANUP_HANDLER

       ; If 'destroy_object_with_gui' is set then destroy the 
       ; entire object.
       IF self.gui_param.destroy_object_with_gui THEN BEGIN
         OBJ_DESTROY, self
       ENDIF ELSE BEGIN
         self->GUI_DESTROY
       ENDELSE

     END ; PRO WIDGET_OBJECT::CLEANUP_HANDLER


     
     ;+=================================================================
     ;
     ; Destroy the gui.
     ;
     ;-=================================================================
     PRO WIDGET_OBJECT::GUI_DESTROY
       
       ; Destroy all widgets
       IF WIDGET_INFO(self.gui_param.gui_id, /VALID_ID) THEN BEGIN
         WIDGET_CONTROL, self.gui_param.gui_id, /DESTROY
       ENDIF

       ; Reset the id_structure
       self->SET_ID_STRUCT, {NULL}

     END ; PRO WIDGET_OBJECT::GUI_DESTROY



     ;+=================================================================
     ;
     ; Clean up on object destruction.
     ;
     ;-=================================================================
     PRO WIDGET_OBJECT::CLEANUP

       self->GUI_DESTROY

       ; Destroy the id_structure
       IF PTR_VALID(self.gui_param.ptr_id_struct) THEN BEGIN
         PTR_FREE, self.gui_param.ptr_id_struct
       ENDIF

     END ; PRO WIDGET_OBJECT::CLEANUP



     ;+=================================================================
     ;
     ; Set the structure that holds all of the gui ids.
     ;
     ;-=================================================================
     PRO WIDGET_OBJECT::SET_ID_STRUCT, id_struct, NO_COPY=no_copy

       IF PTR_VALID(self.gui_param.ptr_id_struct) THEN BEGIN

         IF KEYWORD_SET(no_copy) THEN BEGIN
           *self.gui_param.ptr_id_struct = TEMPORARY(id_struct)
         ENDIF ELSE BEGIN
           *self.gui_param.ptr_id_struct = id_struct
         ENDELSE

       ENDIF ELSE BEGIN

         IF KEYWORD_SET(no_copy) THEN BEGIN
           self.gui_param.ptr_id_struct = PTR_NEW(TEMPORARY(id_struct))
         ENDIF ELSE BEGIN
           self.gui_param.ptr_id_struct = PTR_NEW(id_struct)
         ENDELSE

       ENDELSE

     END ; PRO WIDGET_OBJECT::SET_ID_STRUCTURE



     ;+=================================================================
     ;
     ; Retrive the structure that holds all of the gui ids.
     ;
     ;-=================================================================
     FUNCTION WIDGET_OBJECT::GET_ID_STRUCT, NO_COPY=no_copy

       IF KEYWORD_SET(no_copy) THEN BEGIN
         RETURN, TEMPORARY(*self.gui_param.ptr_id_struct)
       ENDIF ELSE BEGIN
         RETURN, *self.gui_param.ptr_id_struct
       ENDELSE
         
     END ; FUNCTION WIDGET_OBJECT::GET_ID_STRUCT



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   This object is to be inherited by all of the gui component
     ;   of the BST_ANALYSIS_SUITE.
     ;
     ;   It defines the basic gui functions and calls.
     ;
     ; PROGRAMING NOTES:
     ;   This object does NOT inherit from [OBJECT::].
     ;     This is done to allow derived classes additonal flexability
     ;     interms of inheritance structure.  It is recomended that
     ;     multiple inheritance be used to inherit [OBJECT::].
     ;
     ;-=================================================================
     PRO WIDGET_OBJECT__DEFINE

       
       struct = { WIDGET_OBJECT $
                  ,gui_param:{ WIDGET_OBJECT__GUI_PARAM $
                               ,destroy_object_with_gui:0 $
                               ,gui_id:0L $
                               ,ui_menu_bar:0L $
                               ,ptr_id_struct:PTR_NEW() $
                             } $
                }


     END ;PRO WIDGET_OBJECT__DEFINE
