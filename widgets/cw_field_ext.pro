

;=======================================================================
; $Id: cw_field_ext.pro,v 1.3 2009-05-25 11:30:02 antoniuk Exp $
;
; Copyright (c) 1992-2005, Research Systems, Inc.  All rights reserved.
;   Unauthorized reproduction prohibited.
;
;+======================================================================
;
; NAME:
;   CW_FIELD_EXT
;
; PURPOSE:
;   This is a modified version of the IDL compound widget CW_FIELD. 
;
;   The original source can be found in rsi/idl/lib/cw_field.pro
; 
;   Changes from original:
;    1. Can handle double parameters.
;    2. The current value is stored separatly from the displayed vaulue.
;       This is done to allow for display formats.
;    2. Allows an input format string with the keyword FORMAT.
;       The enterd text will formatted when <CR> in entered
;       or when SET_VALUE is used.
;    3. Added keyword SENSITIVE.
;    4. Added keyword MAXLENGTH that can be used to limit the number
;       of characters that can be input.
;
;
;
;   This widget cluster function manages a data entry field widget.
;   The field consists of a label and a text widget.  CW_FIELD's can
;   be string fields, integer fields or floating-point fields.  The
;   default is an editable string field.
;
; CATEGORY:
;   Widget Clusters.
;
; CALLING SEQUENCE:
;   Result = CW_FIELD(Parent)
;
; INPUTS:
;   Parent: The widget ID of the widget to be the field's parent.
;
; KEYWORD PARAMETERS:
;
;   /STRING
;       Set this keyword to have the field accept only string values.
;       Numbers entered in the field are converted to their string
;       equivalents.  This is the default.
;
;   /FLOATING
;       Set this keyword to have the field accept only floating-point
;       values.  Any number or string entered is converted to its
;       floating-point equivalent.
;
;   /DOUBLE
;       Set this keyword to have the field accept only double precision
;       floating-point values.  Any number or string entered is converted
;       to its double equivalent.
;
;   /INTEGER
;       Set this keyword to have the field accept only integer values.
;       Any number or string entered is converted to its integer
;       equivalent (using FIX).  For example, if 12.5 is entered in
;       this type of field, it is converted to 12.
;
;   /LONG
;       Set this keyword to have the field accept only long integer
;       values.  Any number or string entered is converted to its
;       long integer equivalent (using LONG).
;  
;   TYPE
;       Set the type code.
;       Note: Not all type codes are supported.  Only the following may
;             used:  2, 3, 4, 5, 7, 12, 13, 14, 15
;
;   /GET_STRING
;       If set GET_VALUE will return the contents of the
;       field as a validated string.  This is usefull when the parsing and
;       formatting of the string according to a data type is needed,
;       but the exact contents of the field are wanted on output.  
;
;       One of the type keywords should be set along with this keyword.
;
;       This keyword is usefull when it is important to know whether the
;       text field is empty.
;
;   MAXLENGTH
;       The maximum number of characters that the user is allowed
;       to enter into the box.  If the user evnt would cause the number
;       of characters to exceed this number, then the event will be ignored.
;
;   FORMAT
;       A format string to use to format the display of the value.
;       This will format the display under two circumstances:
;         1. Return (carrage return, <CR>) is pressed.
;         2. SET_VALUE is used.
;
;       Before the formatting is done, the current value will be saved.
;       When GET_VALUE is called, this saved value will be returned,
;       ensuring that the return value is not truncated by the formatting.
;
;       Warnings:
;         Using formatting can cause numbers to become truncated if
;           user editing is allowed.
; 
;           When formatting is active and return is pressed, whatever is
;           currently displayed is saved as the value.  This means that
;           if a value is entered, and return is pressed twice, the
;           saved value can get truncated by the formatting.
;
;         If user editing is allowed, and the field is a numerical type,
;           then the  formatting string must result in a single number.
;           Any other characters will cause incorret values to be retured
;           once the user edits the field.
;         
;   EVENT_FUNC
;       The name of an optional user-supplied event function
;       for events. This function is called with the return
;       value structure whenever the slider value is changed, and
;       follows the conventions for user-written event
;       functions.
;
;   TITLE
;       A string containing the text to be used as the label for the
;       field.  The default is "Input Field:".
;
;   VALUE
;       The initial value in the text widget.  This value is
;       automatically converted to the type set by the STRING,
;       INTEGER, and FLOATING keywords described below.
;
;   UVALUE
;       A user value to assign to the field cluster.  This value
;       can be of any type.
;
;   UNAME
;       A user supplied string name to be stored in the
;       widget's user name field.
;
;   FRAME
;       The width, in pixels, of a frame to be drawn around the
;       entire field cluster.  The default is no frame.
;
;   /RETURN_EVENTS
;       Set this keyword to make cluster return an event when a
;       <CR> is pressed in a text field.  The default is
;       not to return events.  Note that the value of the text field
;       is always returned when the WIDGET_CONTROL, field, GET_VALUE=X
;       command is used.
;
;   /ALL_EVENTS
;       Like /RETURN_EVENTS but return an event whenever the
;       contents of a text field have changed.
;
;   /COLUMN
;       Set this keyword to center the label above the text field.
;       The default is to position the label to the left of the text
;       field.
;
;   /ROW
;       Set this keyword to position the label to the left of the text
;       field.  This is the default.
;
;   XSIZE
;       An explicit horizontal size (in characters) for the text input
;       area.  The default is to let the window manager size the
;       widget.  Using the XSIZE keyword is not recommended.
;
;   YSIZE
;       An explicit vertical size (in lines) for the text input
;       area.  The default is 1.
;
;   LABEL_XSIZE
;       An explicit horizontal size (in pixels) for the TITLE of
;       the field.  The default is to let the window manager size the
;       widget
;
;   FONT
;       A string containing the name of the X Windows font to use
;       for the TITLE of the field.
;
;   FIELDFONT
;       A string containing the name of the X Windows font to use
;       for the TEXT part of the field.
;
;   /NOEDIT
;       Normally, the value in the text field can be edited.  Set this
;       keyword to make the field non-editable.
;
; OUTPUTS:
;   This function returns the widget ID of the newly-created cluster.
;
; COMMON BLOCKS:
;   None.
;
; PROCEDURE:
;   Create the widgets, set up the appropriate event handlers, and return
;   the widget ID of the newly-created cluster.
;
; EXAMPLE:
;   The code below creates a main base with a field cluster attached
;   to it.  The cluster accepts string input, has the title "Name:", and
;   has a frame around it:
;
;       base = WIDGET_BASE()
;       field = CW_FIELD(base, TITLE="Name:", /FRAME)
;       WIDGET_CONTROL, base, /REALIZE
;
;
;
;-======================================================================
;
; MODIFICATION HISTORY:
;   Written by: Keith R. Crosley   June 1992
;           KRC, January 1993 -- Added support for LONG
;                        integers.
;               AB, 7 April 1993, Removed state caching.
;           JWG, August 1993, Completely rewritten to make
;               use of improved TEXT widget functionality
;           ACY, 25 March, 1994, fix usage of FRAME keyword
;                       KDB, May 1994, Initial value =0 would result
;                                      in a null text field. Fixed
;                                      keyword check.
;   CT, RSI, March 2001: Pass keywords directly into WIDGET_BASE,
;         without assigning default values, since the defaults are
;         handled by WIDGET_BASE. Avoids assigning defaults if user passes
;         in undefined variables.
;   CT, RSI, July 2001: Fix bug in previous mod. If user passes in a
;         numeric VALUE but forgets to set the /FLOAT, we still need
;         to convert to a string before passing onto WIDGET_TEXT.
;   
;   2008-03-19: novi - Renamed to CW_FIELD_EXT. See top header for details
;   2009-05-22: novi - Added a keyword 'MAXLENGTH' that can be used to
;                        limit the number of characters that are allowed
;                        to be added.
;                      Value is now stored separatly from the text.
;                        This allows formats to be used for the display
;                        with out causing truncation of the number.
;                        Added keyword 'FORMAT'.
;   2009-08-28: novi - String validation now removes any leading or
;                        trailing whitespace.
;   2010-03-10: novi - Added the /GET_STRING keyword.  MAXLENGTH now
;                        implies a default XSIZE.
;           
;                    
;
; ======================================================================

     ;+=================================================================
     ;
     ;
     ;  Check and return the portion of a string that
     ;   is a valid floating point number.
     ;
     ;
     ;-=================================================================
     FUNCTION CW_FIELD_EXT_VALIDATE, value_in
       ;   Look for invalid mantissa

       COMPILE_OPT hidden

       ; First remove any leading or trailing whitespace.
       value = STRTRIM(value_in, 2)

       IF Value EQ '' THEN RETURN, ''

       Chars   = [ BYTE(Value), 0b ]
       Curr    = 0
       NeedDecimal = 1

       ;   Valid #s
       ;   [+-]<number>[exponent]
       ;   number ::= [0-9]+[.[0-9]*] or .[0-9]+
       ;   exponent ::= {eEdD}[+-][0-9]+

       ;   Signed value?
       IF Chars[Curr] EQ 43b OR Chars[Curr] EQ 45b THEN Curr = Curr + 1

       ;   Look for digits before the decimal point
       IF Chars[Curr] GE 48b AND Chars[Curr] LE 57b THEN BEGIN
         NeedDecimal = 0

         ;   while(isdigit(*p))++p;
         WHILE Chars[Curr] GE 48b AND Chars[Curr] LE 57b DO Curr = Curr + 1
       ENDIF

       ;   Must have .[0-9]+

       IF NeedDecimal THEN BEGIN
         IF Chars[Curr] NE 46b THEN RETURN,''    ; invalid #
         Curr    = Curr + 1
         IF Chars[Curr] LT 48b OR Chars[Curr] GT 57b THEN RETURN,''

         ;   while(isdigit(*p))++p;
         WHILE Chars[Curr] GE 48b AND Chars[Curr] LE 57b DO Curr = Curr + 1
       ENDIF ELSE BEGIN

         ;   Might have .[0-9]*

         IF Chars[Curr] EQ 46b THEN BEGIN
           Curr    = Curr + 1
           ;   while(isdigit(*p))++p;
           WHILE Chars[Curr] GE 48b AND Chars[Curr] LE 57b DO Curr = Curr + 1
         ENDIF
       ENDELSE

       ;   Exponent?
       Dummy   = WHERE(Chars[Curr] EQ BYTE("dDeE"), Count)
       IF Count THEN BEGIN
         ; Save exponent position in case the exponent is invalid
         ; and only mantissa is valid number.
         SaveCurr    = Curr - 1

         Curr        = Curr + 1  ; skip 'e'
         ;   Signed exponent?
         IF Chars[Curr] EQ 43b OR Chars[Curr] EQ 45b THEN Curr = Curr + 1

         ;   At least one digit after 'e' or exponent is malformed
         IF Chars[Curr] LT 48b OR Chars[Curr] GT 57b THEN BEGIN
           Curr    = SaveCurr  ; Revert -- invalid exponent
         ENDIF ELSE BEGIN
           ;   find end of exponent digits
           WHILE Chars[Curr] GE 48b AND Chars[Curr] LE 57b DO Curr = Curr + 1
         ENDELSE
       ENDIF

       RETURN,STRING(Chars[0:Curr])    ; Chars from 0-Curr are valid
     END



     ;+=================================================================
     ; 
     ; Return a value of the requested type from the given string.
     ;
     ;-=================================================================
     FUNCTION CW_FIELD_EXT_VALUE, string, Type

       COMPILE_OPT hidden

       IF Type EQ 7 THEN RETURN, string

       validated_string  = CW_FIELD_EXT_VALIDATE(string[0])

       RETURN, FIX(validated_string, TYPE=type)

     END


     ;+=================================================================
     ; 
     ;
     ;   Procedure to set the value of a CW_FIELD_EXT
     ;
     ;
     ;-=================================================================
     PRO CW_FIELD_EXT_SET, Base, Value

       COMPILE_OPT hidden

       Child   = WIDGET_INFO(Base, /CHILD)
       WIDGET_CONTROL, Child, GET_UVALUE=State, /NO_COPY

       ValueType  = SIZE(Value, /TYPE)


       ; Check to see if the null string was passed, if so clear the text box.
       ; And set the internal value to zero.
       IF ValueType EQ 7 THEN BEGIN
         IF Value EQ '' THEN BEGIN
           WIDGET_CONTROL, State.TextId, SET_VALUE=''
           state.value = FIX(Value, TYPE=State.Type)
           WIDGET_CONTROL, Child, SET_UVALUE=State, /NO_COPY
           RETURN
         ENDIF
       ENDIF



       ; Save the input value internally.
       ; Convert the type if necessary.
       IF State.Type EQ ValueType THEN BEGIN
         state.value = Value
       ENDIF ELSE BEGIN
         IF ValueType EQ 7 THEN BEGIN
           state.value = CW_FIELD_EXT_VALUE(Value, State.Type)
         ENDIF ELSE BEGIN
           state.value = FIX(Value, TYPE=State.Type)
         ENDELSE
       ENDELSE
    

       ; Generate the string that will be displayed.
       ; Remove whitspace unless both the current and input types
       ; are strings.
       CASE 1 OF
         (State.Type EQ 7) AND (ValueType EQ 7): BEGIN
           string = Value
         END
         (State.Format EQ '') OR (ValueType EQ 7): BEGIN
           string = STRTRIM(state.Value,2)
         END
         ELSE: BEGIN
           string = STRING(FORMAT=State.Format, state.Value)
         END
       ENDCASE
           
           
       WIDGET_CONTROL, State.TextId, SET_VALUE=string

       WIDGET_CONTROL, Child, SET_UVALUE=State, /NO_COPY
     END


     ;+=================================================================
     ;
     ;   Function to get the value of a CW_FIELD_EXT
     ;
     ;-=================================================================
     FUNCTION CW_FIELD_EXT_GET, Base

       COMPILE_OPT hidden

       Child   = WIDGET_INFO(Base, /CHILD)
       WIDGET_CONTROL, Child, GET_UVALUE=State, /NO_COPY

       ; Choose whether to return the result as a string or 
       ; as the given type.
       IF state.get_string THEN BEGIN
         WIDGET_CONTROL, State.TextId, GET_VALUE=current_text
         Ret = CW_FIELD_EXT_VALIDATE(current_text)
       ENDIF ELSE BEGIN
         Ret = State.Value
       ENDELSE

       WIDGET_CONTROL, Child, SET_UVALUE=State, /NO_COPY
       RETURN, Ret
     END



     ;+=================================================================
     ;
     ;   Ascii assumptions
     ;
     ;   + - .   = 43,45,46
     ;   0-9 = 48-57
     ;   DEde    = 68,69,100,101
     ;
     ;
     ;
     ;   Examine an input stream of characters.
     ;   Alter the field contents to reflect this.
     ;   If any character inserted in a single operation is invalid,
     ;       ignore the entire operation.
     ;   Consider that field may contain a (known) invalid state
     ;
     ;
     ;-=================================================================
     PRO CW_FIELD_EXT_INT, Ch, State, Event, Altered

       COMPILE_OPT hidden

       Altered = 0     ; nothing so far
       Nil     = 0     ; field has contents
       Minus   = 0     ; field is not just a '-'
       Negate  = 0     ; new text has no '-'s in it
       TextId  = State.TextId

       ; Special Cases:
       ;   We don't actually care where in the input string a
       ;   '-' is.  If there is an odd number of them, we
       ;   negate the value (see below)
       ;
       ;   Current String      Char        Result
       ;   Nil         '-'     '-'
       ;   -           '-'     Nil
       ;   <any number>        '-'     -<number>

       WIDGET_CONTROL, TextId, GET_VALUE=Value
       Value   = Value[0]

       IF Value EQ '' THEN Nil = 1     ; Value is nil string
       IF Value EQ '-' THEN Minus = 1  ; Value is an invalid number


       ;   <CR>
       IF Ch EQ 10b THEN BEGIN
         Altered = 2
         RETURN
       ENDIF

       IF Ch EQ 45b THEN Negate = 1 $
       ELSE IF Ch GE 48b AND Ch LE 57b THEN BEGIN
         Nil = 0 & Minus = 0
       ENDIF ELSE RETURN   ; ![0-9]

       ;   Add new character (if any)

       Selection   = WIDGET_INFO(TextId, /TEXT_SELECT)
       TIP     = Selection[0]+1-Negate ; Text Insertion Point

       IF Negate EQ 0 THEN BEGIN
         WIDGET_CONTROL, TextId, SET_VALUE=STRING(Ch), /USE_TEXT_SELECT
         Altered = 1
       ENDIF

       IF Negate THEN BEGIN
         IF Nil THEN BEGIN
           WIDGET_CONTROL, TextId, SET_VALUE='-'
           TIP = 1
         ENDIF ELSE IF Minus THEN BEGIN
           WIDGET_CONTROL, TextId, SET_VALUE=''
         ENDIF ELSE BEGIN
           ;   We actually have a number to negate

           WIDGET_CONTROL, TextId, GET_VALUE=Value
           IValue  = LONG(Value)
           TIP     = TIP + (IValue GT 0) - (IValue LT 0)
           WIDGET_CONTROL, TextId, SET_VALUE=STRTRIM(-IValue,2)
         ENDELSE
         Altered = 1
       ENDIF

       ; Set selection point
       IF Altered THEN WIDGET_CONTROL, TextId, SET_TEXT_SELECT=[TIP,0]
     END


     FUNCTION CW_FIELD_EXT_EXPONENT, Value, Idx

       COMPILE_OPT hidden

       BValue  = BYTE(Value)

       Idx = WHERE(BValue EQ 68b, Count)
       IF Count EQ 1 THEN RETURN, 1
       Idx = WHERE(BValue EQ 69b, Count)
       IF Count EQ 1 THEN RETURN, 1
       Idx = WHERE(BValue EQ 100b, Count)
       IF Count EQ 1 THEN RETURN, 1
       Idx = WHERE(BValue EQ 101b, Count)
       IF Count EQ 1 THEN RETURN, 1
       RETURN, 0
     END


     ;+=================================================================
     ;
     ; Floating point number are even more complicated.
     ; There are more invalid states available.
     ; Currently, we are more lax.
     ; 
     ;-=================================================================
     PRO CW_FIELD_EXT_FLOAT, Ch, State, Event, Altered

       COMPILE_OPT hidden

       TextId  = State.TextId
       WIDGET_CONTROL, TextId, GET_VALUE=Value
       Value   = Value[0]

       ; This last character was the carrage return.
       ; <CR>
       IF Ch EQ 10b THEN BEGIN
         Value   = CW_FIELD_EXT_VALIDATE(Value)
         WIDGET_CONTROL, TextId, SET_VALUE=Value
         WIDGET_CONTROL, TextId, SET_TEXT_SELECT=[Strlen(Value),0]
         Altered = 2
         RETURN
       ENDIF

       ;   Unfortunately, we have a lot of invalid states
       ;   possible that aren't a problem.
       ;   We make sure of just a minimum number of
       ;   things:
       ;   One EeDd per field.
       ;   One decimal point, before the exponent
       ;   - sign must be 1st char or follow the exponent.

       Selection   = WIDGET_INFO(TextId, /TEXT_SELECT)

       IF Ch GE 48b AND Ch LE 57b THEN BEGIN
         WIDGET_CONTROL, TextId, SET_VALUE=STRING(Ch), /USE_TEXT_SELECT
       ENDIF ELSE BEGIN
         CASE Ch OF

           46b:    BEGIN
             ;   Ignore it if there is one already
             ;   New decimal point must precede exponent

             Idx = WHERE(BYTE(Value) EQ 46b, Count)
             IF Count EQ 1 THEN RETURN
             IF CW_FIELD_EXT_EXPONENT( Value, Idx ) THEN BEGIN
               IF Idx[0] LT Selection[0] THEN RETURN
             ENDIF
             WIDGET_CONTROL, TextId, SET_VALUE=STRING(Ch), /USE_TEXT_SELECT
           END
           43b:    BEGIN   ; + must follow exponent
             IF CW_FIELD_EXT_EXPONENT( Value, Idx ) THEN BEGIN
               IF Idx[0]+1 EQ Selection[0] THEN BEGIN
                 WIDGET_CONTROL, TextId, SET_VALUE='+', /USE_TEXT_SELECT
               ENDIF ELSE RETURN
             ENDIF ELSE RETURN
           END
           45b:    BEGIN
             HaveExp = CW_FIELD_EXT_EXPONENT( Value, Idx )
             IF  (HaveExp AND Idx[0]+1 EQ Selection[0]) OR $
               (Selection[0] EQ 0 AND STRMID(Value,0,1) NE "-") THEN BEGIN
               WIDGET_CONTROL, TextId, SET_VALUE='-', /USE_TEXT_SELECT
             ENDIF ELSE RETURN
           END

           68b:    GOTO, Exponent
           69b:    GOTO, Exponent
           100b:   GOTO, Exponent
           101b:   BEGIN
             Exponent:

             ;   Replace if one exists. Otherwise allow it anywhere
             ;   AFTER the decimal point
             IF CW_FIELD_EXT_EXPONENT( Value, Idx ) THEN BEGIN
               Selection   = [ Idx, 1 ]
               WIDGET_CONTROL, TextId, SET_TEXT_SELECT=Selection
             ENDIF ELSE BEGIN
               Idx = WHERE(BYTE(Value) EQ 46b, Count)
               IF Count EQ 1 THEN BEGIN
                 IF Selection[0] LE Idx[0] THEN RETURN
               ENDIF
             ENDELSE
             WIDGET_CONTROL, TextId, SET_VALUE=STRING(Ch), /USE_TEXT_SELECT
           END

           ELSE:   RETURN  ; Bad
         ENDCASE
       ENDELSE



       Altered = 1
       WIDGET_CONTROL, TextId, SET_TEXT_SELECT=[Selection[0]+1,0]
     END


     ;+=================================================================
     ;
     ;
     ; Format the text using the format string given.
     ;
     ; Don't do anything if a format string was not given or
     ; if it was empty.
     ;
     ;
     ;-=================================================================
     PRO CW_FIELD_EXT_FORMAT_TEXT, state

       IF state.format NE '' THEN BEGIN

         WIDGET_CONTROL, State.TextId, GET_VALUE = current_text

         CATCH, error
         IF (error NE 0) THEN BEGIN
           CATCH, /CANCEL
           MESSAGE, 'Format string is not valid:'+STRING(state.format)
           RETURN
         ENDIF

         current_text = STRING(FORMAT=state.format, current_text)
       
         WIDGET_CONTROL, State.TextId, SET_VALUE = current_text

       ENDIF
     END ; PRO CW_FIELD_EXT_FORMAT_TEXT


     ;+=================================================================
     ; If we get an event in a neumerical field we want to check to
     ; make sure that it is valid.
     ;
     ; Right now all we do here is check to make sure the value
     ; is not larger than the number of digits given in uvalue.max_size.
     ;
     ; This routine assume that CW_FIELD was used to create the field
     ; if that is not the case, this may not work properly.
     ;-=================================================================
     FUNCTION CW_FIELD_EXT_OVER_MAXLENGTH, event, state

       ; First check if characters are being added,
       ; If not, then there is nothing to do.
       IF event.type GT 1 THEN RETURN, 0

       ; Now check the length of the new string, ignore carrage returns.
       IF event.type EQ 0 THEN BEGIN

         IF (state.type NE 7) AND (Event.Ch EQ 10b) THEN BEGIN
           new_length = 0
         ENDIF ELSE BEGIN
           new_length = 1
         ENDELSE

       ENDIF ELSE BEGIN

         new_string = event.str
         IF state.type NE 7 THEN BEGIN
           ; Remove all spaces and carrage returns.
           new_string = STRJOIN(STRSPLIT(new_string, STRING(10B)+' ', /EXTRACT))
         ENDIF
         new_length = STRLEN(new_string)
         
         Chars = BYTE(new_string)
         FOR ii=0,N_ELEMENTS(Chars)-1 DO BEGIN
           IF Chars[ii] EQ 10b THEN new_length -= 1
         ENDFOR

       ENDELSE

       ; If there will be no new length (carrage returns or spaces) then return
       IF new_length EQ 0 THEN RETURN, 0


       ; Get the length of the current string.
       WIDGET_CONTROL, State.TextId, GET_VALUE = current_text
       current_text = current_text[0]

       IF state.type NE 7 THEN BEGIN
         ; Remove all spaces and carrage returns.
         current_text = STRJOIN(STRSPLIT(current_text, STRING(10B)+' ', /EXTRACT))
       ENDIF
       
       current_length = STRLEN(current_text)

       ; Now check the final length.
       RETURN, ((current_length + new_length) GT state.maxlength)


     END ;FUNCTION CW_FIELD_EXT_OVER_MAXLENGTH



     ;+=================================================================
     ;
     ; This is the main event handler for this compound widget.
     ;
     ; This handles any character deletes.
     ; If the type of this widget is string this function
     ;   handles updating the text widget, otherwise control
     ;   is handed off to one of the neumerical handlers.
     ;
     ;
     ;-=================================================================
     FUNCTION CW_FIELD_EXT_EVENT, Event

       COMPILE_OPT hidden

       StateHolder = WIDGET_INFO(Event.Handler, /CHILD)
       WIDGET_CONTROL, StateHolder, GET_UVALUE=State, /NO_COPY

       ;   At this point, we need to look at what kind of field
       ;   we have:
       Altered = 0

       ; If a max_size was given.  Then check to see if this event 
       ; will cause the text to exceed the maximum length.
       too_long = 0
       IF state.maxlength GT 0 THEN BEGIN
         too_long = CW_FIELD_EXT_OVER_MAXLENGTH(event, state)
       ENDIF


       ; If this event will not make the string too long then
       ; process the event.
       IF ~too_long THEN BEGIN
         CASE 1 OF
           ; Delete Text
           Event.Type EQ 2: BEGIN    
             
             IF Event.Length GT 0 THEN BEGIN ; Bug in widget
               WIDGET_CONTROL, State.TextId, $
                 SET_TEXT_SELECT=[Event.Offset,Event.Length]
               WIDGET_CONTROL, State.TextId, SET_VALUE='', /USE_TEXT_SELECT
               
               ;   See if user wants an update
               Altered = 1
             ENDIF
           END


           ; Not a String?
           State.Type NE 7: BEGIN  
     
             ; Insert Characters?
             IF Event.Type LE 1 THEN BEGIN       
             
               IF (State.Type EQ 4) OR (State.Type EQ 5) THEN $
                 Procedure='CW_FIELD_EXT_FLOAT' $
               ELSE Procedure='CW_FIELD_EXT_INT'
               
               Altered = 0
               IF Event.Type EQ 0 THEN BEGIN
                 CALL_PROCEDURE, Procedure, Event.Ch, State, Event, Altered
               ENDIF ELSE BEGIN
                 Chars   = BYTE(Event.Str)
                 FOR I=0,N_ELEMENTS(Chars)-1 DO $
                   CALL_PROCEDURE, Procedure, Chars[I], State, Event, Altered
               ENDELSE
             ENDIF
           END

           ; Is a string
           ELSE: BEGIN

             ; Insert Characters?
             IF Event.Type LE 1 THEN BEGIN 
               IF Event.Type EQ 0 THEN BEGIN
                 WIDGET_CONTROL, state.TextId, SET_VALUE=STRING(event.Ch), /USE_TEXT_SELECT
               ENDIF ELSE BEGIN
                 WIDGET_CONTROL, state.TextId, SET_VALUE=STRING(event.str), /USE_TEXT_SELECT
               ENDELSE
             ENDIF

             ;   All delete/add char events effect the contents of
             ;   a string. <CR> is considered special.
             IF Event.Type GE 0 AND Event.Type LE 2 THEN Altered = 1
             IF Event.Type EQ 0 THEN $
               Altered  = 1 + (Event.Ch EQ 10b)
             
           ENDELSE
         ENDCASE
       ENDIF
         
       ; Update the value stored in the state structure.
       IF altered GT 0 THEN BEGIN
         WIDGET_CONTROL, State.TextId, GET_VALUE=current_text
         state.value = CW_FIELD_EXT_VALUE(current_text, State.Type)
       ENDIF

       ; Check if this was a <CR>, if so allow format the displayed text.
       ; This won't do anything if the format string was not set.
       IF event.type EQ 0 THEN BEGIN
         IF event.ch EQ 10b THEN BEGIN
           CW_FIELD_EXT_FORMAT_TEXT, state
         ENDIF
       ENDIF

       Ret = 0

       ;   If the entry has been modified or <CR> was hit
       ;   And the user is interested in all event or
       ;   Just <CR> AND <CR> was the cause of update then
       ;   send it
       IF State.Update NE 0 AND $
         Altered GE State.Update THEN BEGIN

         Ret = {         $
                 ID: Event.Handler,  $
                 TOP: Event.Top,     $
                 HANDLER: 0L,        $
                 VALUE: state.value, $
                 TYPE: State.Type,   $
                 UPDATE: Altered - 1 $   ; 0=any,1=CR
               }
       ENDIF

       user_event_function = State.user_event_function
       update = State.Update

       ;   Restore our state structure
       WIDGET_CONTROL, StateHolder, SET_UVALUE=State, /NO_COPY
       if user_event_function eq '' then $
         return, ret $
       else begin
         IF update NE 0 AND $
           Altered GE update THEN BEGIN
           ; only call event handler if we are returning an event structure
           return, CALL_FUNCTION(user_event_function, ret)
         ENDIF ELSE return, ret
       endelse
     END


     FUNCTION CW_FIELD_EXT, Parent $

                            ,FLOATING=Float $
                            ,DOUBLE=Double $
                            ,INTEGER=Int $
                            ,LONG=Long $
                            ,STRING=String $

                            ,GET_STRING=get_string $

                            ,FORMAT=format $
                            ,MAXLENGTH=maxlength $

                            ,TITLE=Title $
                            ,LABEL_XSIZE=Label_Xsize $

                            ,VALUE=TextValueIn $
                            ,UVALUE=UValue $
                            ,UNAME=uname $

                            ,COLUMN=Column $
                            ,ROW=Row $
                            ,XSIZE=XSize $
                            ,YSIZE=YSize $

                            ,XPAD=xpad $
                            ,YPAD=ypad $

                            ,RETURN_EVENTS=ReturnEvents $
                            ,ALL_EVENTS=AllUpdates $
                            ,EVENT_FUNC = user_event_function $

                            ,FIELDFONT=FieldFont $
                            ,FONT=LabelFont $
                            ,FRAME=Frame $

                            ,NOEDIT=NoEdit $
                            ,TEXT_FRAME=Text_Frame $

                            ,TAB_MODE=tab_mode $
                            ,SENSITIVE=sensitive
       
       ;   FLOOR=vmin, CEILING=vmax

       ;   Examine our keyword list and set default values
       ;   for keywords that are not explicitly set.

       Column      = KEYWORD_SET(Column)
       Row         = 1 - Column
       AllEvents   = 1 - KEYWORD_SET(NoEdit)

       ; Enum Update { None, All, CRonly }
       Update      = 0
       IF KEYWORD_SET(AllUpdates) THEN Update  = 1
       IF KEYWORD_SET(ReturnEvents) THEN Update    = 2

       IF N_ELEMENTS(user_event_function) LE 0 THEN user_event_function = ''
       IF N_ELEMENTS(Title) EQ 0 THEN Title='Input Field:'
       TextValue = (N_ELEMENTS(TextValueIn) gt 0) ? TextValueIn : ''

       ; Convert non-string values to strings.
       if (SIZE(TextValue, /TNAME) ne 'STRING') then $
         TextValue = STRTRIM(TextValue,2)
       IF N_ELEMENTS(YSize) EQ 0 THEN YSize=1
       IF N_ELEMENTS(uname) EQ 0 THEN uname='CW_FIELD_EXT_UNAME'

       Type    = 7 ; string is default
       IF KEYWORD_SET(Float)  THEN Type = 4
       IF KEYWORD_SET(Double) THEN Type = 5
       IF KEYWORD_SET(Int)    THEN Type = 2
       IF KEYWORD_SET(Long)   THEN Type = 3

       ; Don't allow multiline non string widgets.
       IF (Type NE 7) THEN $
         YSize=1
       YSize = YSize > 1

       ; Make the default string
       IF N_ELEMENTS(format) EQ 0 THEN format = ''

       ; Set default sensitivity
       IF N_ELEMENTS(sensitive) EQ 0 THEN sensitive=1

       ; Set a default value for maxlength, any value less than 1 will be ignored.
       IF N_ELEMENTS(maxlength) EQ 0 THEN maxlength = -1

       ; By default we will return the value as the given type.
       IF N_ELEMENTS(get_string) EQ 0 THEN get_string = 0

       ; If maxlength was given, then set a default for xsize.
       IF N_ELEMENTS(xsize) EQ 0 AND maxlength GT 0 THEN BEGIN
         xsize = maxlength + 1
       ENDIF

       ;   Build Widget
       Base = WIDGET_BASE(Parent, UNAME=uname $
                          ,UVALUE=UValue $
                          ,ROW=Row $
                          ,COLUMN=Column $
                          ,EVENT_FUNC='CW_FIELD_EXT_EVENT' $
                          ,PRO_SET_VALUE='CW_FIELD_EXT_SET' $
                          ,FUNC_GET_VALUE='CW_FIELD_EXT_GET' $
                          ,FRAME=Frame $
                          ,SENSITIVE=sensitive $
                          ,XPAD=xpad $
                          ,YPAD=ypad)

       IF ( N_ELEMENTS(tab_mode) NE 0 ) THEN $
         WIDGET_CONTROL, Base, TAB_MODE = tab_mode

       IF (STRLEN(Title) GT 0) OR (N_ELEMENTS(label_xsize) GT 0) THEN BEGIN
         Label = WIDGET_LABEL(Base $
                              ,VALUE=Title $
                              ,FONT=LabelFont $
                              ,UNAME=uname+'_LABEL' $
                              ,XSIZE=label_xsize $
                              ,/ALIGN_RIGHT)
       ENDIF
       
       ; Never allow the text to be editable.
       Text = WIDGET_TEXT(Base, UNAME=uname+'_TEXT' $
                          ,VALUE=TextValue $
                          ,XSIZE=XSize $
                          ,YSIZE=YSize $
                          ,FONT=FieldFont $
                          ,ALL_EVENTS=AllEvents $
                          ,EDITABLE=0 $
                          ,FRAME=Text_Frame  $
                          )


       ; Save our internal state in the first child widget
       State   = { $
                   value:FIX(TextValue, TYPE=type) $
                   ,user_event_function:user_event_function $
                   ,textid:text $
                   ,title:title $
                   ,update:update $
                   ,type:type $
                   ,format:format $
                   ,maxlength:maxlength $
                   ,get_string:get_string $
                 }

       WIDGET_CONTROL, WIDGET_INFO(Base, /CHILD), SET_UVALUE=State, /NO_COPY
       RETURN, Base
     END
