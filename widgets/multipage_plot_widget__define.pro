

;+====================================================================== 
; 
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-07
;
; PURPOSE:
;   Create a widget that can display multiple plots.
;
;
;-======================================================================


     ;+=================================================================
     ;
     ; Initialize the [MULTIPAGE_PLOT_WIDGET] object. 
     ;
     ; KEYWORDS:
     ;   /SHOW
     ;       After initializing the object, immidiatly build and show
     ;       the gui.
     ;
     ;   DESTROY = bool
     ;       Default = 1
     ;       Whether or not to destroy the object the when the gui is 
     ;       closed.
     ;
     ;-=================================================================
     FUNCTION MULTIPAGE_PLOT_WIDGET::INIT, pagelist $
                                           ,SHOW=show $
                                           ,DESTROY_WITH_WIDGET=destroy_with_widget $
                                           ,XSIZE=xsize $
                                           ,YSIZE=ysize $
                                           ,NO_COPY=no_copy


       RESOLVE_ROUTINE, [ $
                          'superplot' $
                        ], /EITHER, /COMPILE_FULL_FILE, /NO_RECOMPILE


       self.debug = 1

       ; Setup the list that will hold the plot list for each page.
       self.page_list = OBJ_NEW('MIR_LIST_IDL7')

       ; Set the current plot to an invalid number
       self.current_page = -1

       ; Set wether or not to destroy the object when the widget is closed.
       MIR_DEFAULT, destroy_with_widget, 1
       self.destroy_with_widget = KEYWORD_SET(destroy_with_widget)

       ; Set the default plot size
       MIR_DEFAULT, xsize, 714
       MIR_DEFAULT, ysize, 924
       self.gui_param.plot_size_default = [xsize, ysize]

       IF ISA(pagelist) THEN BEGIN
         self->SET_PAGE_LIST, pagelist, NO_COPY=no_copy
       ENDIF

       IF KEYWORD_SET(show) THEN BEGIN
         self->SHOW
       ENDIF

       RETURN, 1

     END ; FUNCTION MULTIPAGE_PLOT_WIDGET::INIT


     ;+=================================================================
     ;
     ; Destroy the object.
     ;
     ;-=================================================================
     PRO MULTIPAGE_PLOT_WIDGET::DESTROY

       OBJ_DESTROY, self

     END ; PRO MULTIPAGE_PLOT_WIDGET::DESTROY


     ;+=================================================================
     ;
     ; Clean up on object destruction
     ;
     ;-=================================================================
     PRO MULTIPAGE_PLOT_WIDGET::CLEANUP

       self->DESTROY_GUI

       IF OBJ_VALID(self.page_list) THEN BEGIN
         OBJ_DESTROY, self.page_list
       ENDIF

     END ; PRO MULTIPAGE_PLOT_WIDGET::CLEANUP



     ;+=================================================================
     ;
     ; Show the window.
     ; 
     ; A page number (index starting from 1) can optionally be set.
     ;
     ;-=================================================================
     PRO MULTIPAGE_PLOT_WIDGET::SHOW, page_number

       IF N_ELEMENTS(page_number) NE 0 THEN BEGIN
         self->GO_TO_PAGE, page_number-1, /NO_UPDATE 
       ENDIF

       IF ~ WIDGET_INFO(self.gui_param.gui_id, /VALID_ID) THEN BEGIN
         self->BUILD_WIDGET_OBJECT
       ENDIF

       self->UPDATE_GUI

     END ; PRO MULTIPAGE_PLOT_WIDGET::SHOW



     ;+=================================================================
     ;
     ; Create the gui.
     ; For now just a single display windows and some buttons.
     ;
     ;-=================================================================
     PRO MULTIPAGE_PLOT_WIDGET::BUILD_WIDGET_OBJECT, DESTINATION=ui_destination_base $
                                                ,GROUP_LEADER=group_leader $
                                                ,TITLE=title


       uname = 'UI_BASE_'+OBJ_CLASS(self)
       uvalue = self
       frame = 1
       event_pro = 'WIDGET_OBJECT_EVENT'
       MIR_DEFAULT, title, 'Multipage Plot'


       ; There are two ways that the GUI can be built:
       ;   1. As part of the main BST_INSTRUMENTAL_FIT_GUI window.
       ;   2. In a separate window.
       ; 
       ; Here we take different action for each possibility.
       IF N_ELEMENTS(ui_destination_base) NE 0 THEN BEGIN

         ; Diable updating of gui while we add the new widget
         WIDGET_CONTROL, ui_destination_base, UPDATE=0


         ; Setup the base widget
         self.gui_param.gui_id = WIDGET_BASE( ui_destination_base $
                                              ,/COLUMN $
                                              ,UNAME=uname $
                                              ,UVALUE=uvalue $
                                              ,FRAME=frame $
                                              ,EVENT_PRO=event_pro $
                                              ,TITLE=title $
                                            )
       ENDIF ELSE BEGIN

         ; Setup the base widget
         self.gui_param.gui_id = WIDGET_BASE(/COLUMN $
                                             ,UNAME=uname $
                                             ,UVALUE=uvalue $
                                             ,FRAME=frame $
                                             ,EVENT_PRO=event_pro $
                                             ,TITLE=title $
                                             ,GROUP_LEADER=group_leader $
                                             ,/TLB_KILL_REQUEST_EVENTS $
                                             ,/TLB_SIZE_EVENTS $
                                             ,MAP=0 $
                                             ,MBAR=ui_menu_bar $
                                            )
         self.gui_param.ui_menu_bar = ui_menu_bar

         ; Realize the window, but don't allow updating yet.
         WIDGET_CONTROL, self.gui_param.gui_id, /REALIZE, UPDATE=0

         ; Regester with the event handler
         XMANAGER, 'MULTIPAGE_PLOT_WIDGET::BUILD_WIDGET_OBJECT', self.gui_param.gui_id $
           ,EVENT_HANDLER=event_pro, /NO_BLOCK

         ui_destination_base = self.gui_param.gui_id
       ENDELSE


       ; Now build up the actuall gui components.
       self->BUILD_GUI

       ; Reenable update of the gui now that everything is built.
       WIDGET_CONTROL, ui_destination_base, UPDATE=1, MAP=1



       ; Set up varibles for handling resize events
       ; ---------------------------------------------------------------
       ; Note this must be done after the window is mapped. 
       ;
       ; I want to resize the plot window whenever the main window is resized.
       ; To get the size right I want to know what is the current differance
       ; between the window size and the plot size.

       ; Get the state holder.
       state_holder = WIDGET_INFO(self.gui_param.gui_id, /CHILD)
       WIDGET_CONTROL, state_holder, GET_UVALUE=state

       window_geometry = WIDGET_INFO(self.gui_param.gui_id, /GEOMETRY)
       plot_geometry = WIDGET_INFO(state.id_struct.ui_plot, /GEOMETRY)

       self.gui_param.plot_size_reduction = $
          [window_geometry.scr_xsize - plot_geometry.scr_xsize $
           ,window_geometry.scr_ysize - plot_geometry.scr_ysize]

       
     END ; PRO MULTIPAGE_PLOT_WIDGET::BUILD_WIDGET_OBJECT



     ;+=================================================================
     ;
     ; Create a widget to view multiple plots.
     ;
     ;-=================================================================
     PRO MULTIPAGE_PLOT_WIDGET::BUILD_GUI
                                        
       ui_base = WIDGET_BASE(self.gui_param.gui_id, /COLUMN)

       ; Set up the plot display
       ; ---------------------------------------------------------------
       ui_base_top = WIDGET_BASE(ui_base, /ROW)
         ui_plot = WIDGET_DRAW(ui_base_top $
                               ,UNAME='ui_plot' $
                               ,FRAME=0 $
                               ,SCR_XSIZE=self.gui_param.plot_size_default[0] $
                               ,SCR_YSIZE=self.gui_param.plot_size_default[1])
         

       ; Set up the caption display
       ; ---------------------------------------------------------------
       ui_base_mid = WIDGET_BASE(ui_base, /ROW)
         ui_text_caption = WIDGET_TEXT(ui_base_mid $
                                       ,UNAME='ui_text_caption' $
                                       ,VALUE='' $
                                       ,SCR_XSIZE=self.gui_param.plot_size_default[0] $
                                       ,YSIZE=5 $
                                       ,/WRAP $
                                       ,/EDITABLE $
                                      )
                                       
                             

       ; Set up the controls
       ; ---------------------------------------------------------------        
       ui_base_bottom = WIDGET_BASE(ui_base, /ROW)

         ; First set up the previous and next buttons.
         ui_button_previous = WIDGET_BUTTON(ui_base_bottom $
                                            ,UNAME='ui_button_previous' $
                                            ,VALUE=' < ' $
                                            ,XSIZE=100)
         ui_button_next = WIDGET_BUTTON(ui_base_bottom $
                                        ,UNAME='ui_button_next' $
                                        ,VALUE=' > ' $
                                        ,XSIZE=100)

         ; Now set up a way to jump to given page and display the
         ; current page number.
         ui_base_page_display = WIDGET_BASE(ui_base_bottom, /ROW, FRAME=1)
           ui_field_page = CW_FIELD_EXT(ui_base_page_display $
                                        ,UNAME='ui_field_plot' $
                                        ,VALUE=0 $
                                        ,/LONG $
                                        ,XSIZE=4 $
                                        ,MAXLENGTH=4 $
                                        ,/RETURN_EVENTS)
           ui_label_page = WIDGET_LABEL(ui_base_page_display $
                                        ,UNAME='ui_label_page' $
                                        ,VALUE=' of  0 ' $
                                        ,XSIZE=40 $
                                       )

         ui_button_refresh = WIDGET_BUTTON(ui_base_bottom $
                                        ,UNAME='ui_button_refresh' $
                                        ,VALUE='Refresh' $
                                        ,XSIZE=100)

       ; Set up the menu bar
       ; ---------------------------------------------------------------
       IF WIDGET_INFO(self.gui_param.ui_menu_bar, /VALID_ID) THEN BEGIN
         ui_menu_output = WIDGET_BUTTON(self.gui_param.ui_menu_bar $
                                        ,UNAME='ui_menu_output' $
                                        ,VALUE='Output' $
                                        ,/MENU)
           ui_menu_page_to_pdf = WIDGET_BUTTON(ui_menu_output $
                                        ,UNAME='ui_menu_page_to_pdf' $
                                        ,VALUE='Save Page to PDF')

       END


       id_struct = { $
                     ui_base:ui_base $
                     ,ui_plot:ui_plot $
                     ,ui_text_caption:ui_text_caption $
                     ,ui_button_previous:ui_button_previous $
                     ,ui_button_next:ui_button_next $
                     ,ui_field_page:ui_field_page $
                     ,ui_label_page:ui_label_page $
                     ,ui_button_refresh:ui_button_refresh $

                     ,ui_menu_page_to_pdf:ui_menu_page_to_pdf $
                   }


       state = { $
                 id_struct:id_struct $
               }

       state_holder = WIDGET_INFO(self.gui_param.gui_id, /CHILD)
       WIDGET_CONTROL, state_holder, SET_UVALUE=state

     END ; PRO MULTIPAGE_PLOT_WIDGET ::BUILD_GUI



     ;+=================================================================
     ;
     ; This is the event manager for the MULTIPAGE_PLOT_WIDGET.
     ;
     ;-=================================================================
     PRO MULTIPAGE_PLOT_WIDGET::EVENT_HANDLER, event, EVENT_HANDLED=event_handled

       ; Establish catch block.
       CATCH, error                  
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL

         MESSAGE_SWITCH, STRING('Error caught in MULTIPAGE_PLOT_WIDGET::EVENT_HANDLER:') $
                         ,MESSAGE_PRO=self.message_pro
         MESSAGE_SWITCH, STRING('    ', !ERROR_STATE.MSG) $
                         ,MESSAGE_PRO=self.message_pro
         IF self.debug THEN MESSAGE, /REISSUE_LAST
       ENDIF  

      ; Handle kill events
       IF TAG_NAMES(event, /STRUCTURE_NAME) EQ 'WIDGET_KILL_REQUEST' THEN BEGIN
         self->KILL
         event_handled = 1
         RETURN
       ENDIF


       ; Get the state holder.
       state_holder = WIDGET_INFO(Event.Handler, /CHILD)
       WIDGET_CONTROL, state_holder, GET_UVALUE=state

       ; Start main event loop
       CASE event.id OF



         ; Main window events.
         ; -------------------------------------------------------------
         self.gui_param.gui_id: BEGIN
           self->RESIZE_EVENT_HANDLER, event          
         END



         ; Controls events.
         ; -------------------------------------------------------------
         state.id_struct.ui_button_previous: BEGIN
           self->GO_TO_PAGE, /PREVIOUS
         END

         state.id_struct.ui_button_next: BEGIN
           self->GO_TO_PAGE, /NEXT
         END

         state.id_struct.ui_field_page: BEGIN
           WIDGET_CONTROL, event.id, GET_VALUE=value
           ; Page numbers are indexed from zero but displayed from one.
           self->GO_TO_PAGE, value-1
         END

         state.id_struct.ui_button_refresh: BEGIN
           self->UPDATE_GUI
         END


         ; Menu events.
         ; -------------------------------------------------------------
         state.id_struct.ui_menu_page_to_pdf: BEGIN
           self->SAVE_CURRENT_PAGE_TO_PDF
         END

         ; Text events
         ; -------------------------------------------------------------
         ui_text_caption: BEGIN
         END


         ELSE: BEGIN
           uname = WIDGET_INFO(event.id, /UNAME)
           MESSAGE, STRING('No rule for: ', uname)
         ENDELSE
       ENDCASE

     END ; PRO MULTIPAGE_PLOT_WIDGET::EVENT_HANDLER



     ;+=================================================================
     ;
     ; Handle resize events for the MULTIPAGE_PLOT_WIDGET.
     ;
     ;-=================================================================
     PRO MULTIPAGE_PLOT_WIDGET::RESIZE_EVENT_HANDLER, event $
                                                      ,EVENT_HANDLED=event_handled

       ; Get the state holder.
       state_holder = WIDGET_INFO(Event.Handler, /CHILD)
       WIDGET_CONTROL, state_holder, GET_UVALUE=state, /NO_COPY

       ; Resize event.
       WIDGET_CONTROL, self.gui_param.gui_id, UPDATE=0
       WIDGET_CONTROL, state.id_struct.ui_plot $
                       ,SCR_XSIZE=event.x-self.gui_param.plot_size_reduction[0] $
                       ,SCR_YSIZE=event.y-self.gui_param.plot_size_reduction[1]

       WIDGET_CONTROL, state.id_struct.ui_text_caption $
                       ,SCR_XSIZE=event.x-self.gui_param.plot_size_reduction[0]
       WIDGET_CONTROL, self.gui_param.gui_id, UPDATE=1

       ; Save the state holder
       WIDGET_CONTROL, state_holder, SET_UVALUE=state, /NO_COPY


       self->UPDATE_PLOT


     END ;PRO MULTIPAGE_PLOT_WIDGET::RESIZE_EVENT_HANDLER



     ;+=================================================================
     ;
     ; Handle a kill request. 
     ; This happens when the main window is closed.
     ;
     ;-=================================================================
     PRO MULTIPAGE_PLOT_WIDGET::KILL

       ; If 'destroy_with_widget' is set the destroy the entire object.
       ; otherwise just destroy the gui.

       IF self.destroy_with_widget THEN BEGIN
         self->DESTROY
       ENDIF ELSE BEGIN
         self->DESTROY_GUI
       ENDELSE

     END ;PRO MULTIPAGE_PLOT_WIDGET::KILL





     ;+=================================================================
     ;
     ; Destroy the gui.
     ;
     ;-=================================================================
     PRO MULTIPAGE_PLOT_WIDGET::DESTROY_GUI

       ; Destroy all widgets
       IF WIDGET_INFO(self.gui_param.gui_id, /VALID_ID) THEN BEGIN
         WIDGET_CONTROL, self.gui_param.gui_id, /DESTROY
       ENDIF


     END ;PRO MULTIPAGE_PLOT_WIDGET::DESTROY_GUI



     ;+=================================================================
     ;
     ; This method will handle chossing the next plot to display.
     ;
     ;-=================================================================
     PRO MULTIPAGE_PLOT_WIDGET::GO_TO_PAGE, index $
                                            ,PREVIOUS=previous $
                                            ,NEXT=next $
                                            ,NO_UPDATE=no_update

       num_pages = self.page_list->N_ELEMENTS()

       ; First check if there are any plots to display
       IF num_pages EQ 0 THEN RETURN

       ; Find the plot to display.
       CASE 1 OF
         N_ELEMENTS(index) NE 0: BEGIN
           page_index = index
         END
         KEYWORD_SET(previous): BEGIN
           page_index = self.current_page - 1
         END
         KEYWORD_SET(next): BEGIN
           page_index = self.current_page + 1
         END
       ENDCASE

       self.current_page = page_index

       ; Make sure that the requested page index is in range.
       self->CHECK_CURRENT_PAGE
       
       IF ~ KEYWORD_SET(no_update) THEN BEGIN
         self->UPDATE_GUI
       ENDIF

     END ; PRO MULTIPAGE_PLOT_WIDGET::GO_TO_PAGE




     ;+=================================================================
     ;
     ; Update the gui with the current parameters.
     ;
     ;-=================================================================
     PRO MULTIPAGE_PLOT_WIDGET::UPDATE_GUI
       
       ; Update only if the gui is active.
       IF ~ WIDGET_INFO(self.gui_param.gui_id, /VALID_ID) THEN BEGIN
         RETURN
       ENDIF

       self->UPDATE_PLOT
       
       ; Get the state holder.
       state_holder = WIDGET_INFO(self.gui_param.gui_id, /CHILD)
       WIDGET_CONTROL, state_holder, GET_UVALUE=state, /NO_COPY

       ;ui_field_page
       WIDGET_CONTROL, state.id_struct.ui_field_page, SET_VALUE=self.current_page+1

       ;ui_label_page
       num_pages = self.page_list->N_ELEMENTS()
       label_string = STRING('of', num_pages, FORMAT='(1x,a0,i2,1x)')
       WIDGET_CONTROL, state.id_struct.ui_label_page, SET_VALUE=label_string

       ; Set the sensitivity of the buttons.
       WIDGET_CONTROL, state.id_struct.ui_button_previous, SENSITIVE=(self.current_page GT 0)
       next_sensitive = ((self.current_page LT num_pages-1) AND (num_pages GT 0))
       WIDGET_CONTROL, state.id_struct.ui_button_next, SENSITIVE=next_sensitive

       WIDGET_CONTROL, state_holder, SET_UVALUE=state, /NO_COPY

     END ; PRO MULTIPAGE_PLOT_WIDGET::UPDATE_GUI




     ;+=================================================================
     ;
     ; Check that the current page is in range.  If not adjust.
     ;
     ;-=================================================================
     PRO MULTIPAGE_PLOT_WIDGET::CHECK_CURRENT_PAGE

       num_pages = self.page_list->N_ELEMENTS()

       ; Make sure that the requested page index is in range.
       page_index = 0 > self.current_page
       page_index = num_pages-1 < page_index
         
       self.current_page = page_index

     END ;PRO MULTIPAGE_PLOT_WIDGET::CHECK_CURRENT_PAGE




     ;+=================================================================
     ;
     ; Update the plot.
     ;
     ;-=================================================================
     PRO MULTIPAGE_PLOT_WIDGET::UPDATE_PLOT
       
       ; Get the index of the plot window
       state_holder = WIDGET_INFO(self.gui_param.gui_id, /CHILD)
       WIDGET_CONTROL, state_holder, GET_UVALUE=state, /NO_COPY
       WIDGET_CONTROL, state.id_struct.ui_plot, GET_VALUE = window_index
       WIDGET_CONTROL, state_holder, SET_UVALUE=state, /NO_COPY


       ; Set the window
       WINDOWSET, window_index

       num_pages = self.page_list->N_ELEMENTS()
       
       IF num_pages EQ 0 THEN BEGIN
         ERASE
       ENDIF ELSE BEGIN

         self->CHECK_CURRENT_PAGE

         plot_list = self.page_list->GET(self.current_page)
         SUPERPLOT_MULTI, plot_list, DEBUG=self.debug
       ENDELSE
       
       ; Reset the window index
       WINDOWSET, /RESTORE

       self->UPDATE_CAPTION

     END ;PRO MULTIPAGE_PLOT_WIDGET::UPDATE_PLOT




     ;+=================================================================
     ;
     ; Update the caption.
     ;
     ;-=================================================================
     PRO MULTIPAGE_PLOT_WIDGET::UPDATE_CAPTION


       caption = ''

       self->CHECK_CURRENT_PAGE
       IF self.current_page GE 0 THEN BEGIN

         plot_list = self.page_list->GET(self.current_page)

         ; Get the caption.
         ; This only works if PLOTLIST objects were used.
         IF OBJ_VALID(plot_list) THEN BEGIN
           IF OBJ_CLASS(plot_list) EQ 'MIR_PLOTLIST' THEN BEGIN
             caption = CALL_METHOD('GET_CAPTION', plot_list)
           ENDIF
         ENDIF

       ENDIF

       state_holder = WIDGET_INFO(self.gui_param.gui_id, /CHILD)
       WIDGET_CONTROL, state_holder, GET_UVALUE=state, /NO_COPY
       WIDGET_CONTROL, state.id_struct.ui_text_caption $
                       , SET_VALUE = Caption
       WIDGET_CONTROL, state_holder, SET_UVALUE=state, /NO_COPY

     END ;PRO MULTIPAGE_PLOT_WIDGET::UPDATE_CAPTION



     ;+=================================================================
     ;
     ; Save the current page to a pdf.
     ;
     ;-=================================================================
     PRO MULTIPAGE_PLOT_WIDGET::SAVE_CURRENT_PAGE_TO_PDF

       ; Check if the current page is valid.
       self->CHECK_CURRENT_PAGE
       IF self.current_page LT 0 THEN BEGIN
         RETURN
       ENDIF

       ; Plot the current page to PDF.
       plot_list = self.page_list->GET(self.current_page)


; TEMPORARY ------------------------------------------------------------
; This need to be made into a plot option
       ;plot_list->SET_DEFAULTS, {thesis_yfactor:2}
; TEMPORARY ------------------------------------------------------------


       SUPERPLOT_MULTI, plot_list, DEBUG=self.debug, /PDF

     END ;PRO MULTIPAGE_PLOT_WIDGET::SAVE_CURRENT_PAGE_TO_PDF




     ;+=================================================================
     ;
     ; Add a page.
     ;
     ;-=================================================================
     PRO MULTIPAGE_PLOT_WIDGET::ADD_PAGE, plot_list, NO_COPY=no_copy

       IF KEYWORD_SET(no_copy) THEN BEGIN
         self.page_list->APPEND, plot_list
       ENDIF ELSE BEGIN
         self.page_list->APPEND, plot_list->GET_COPY()
       ENDELSE

       self->UPDATE_GUI

     END ;PRO MULTIPAGE_PLOT_WIDGET::ADD_PAGE



     ;+=================================================================
     ;
     ; Set the page list.
     ;
     ;-=================================================================
     PRO MULTIPAGE_PLOT_WIDGET::SET_PAGE_LIST, page_list, NO_COPY=no_copy

       OBJ_DESTROY, self.page_list

       IF KEYWORD_SET(no_copy) THEN BEGIN
         self.page_list = page_list
       ENDIF ELSE BEGIN
         self.page_list = page_list->GET_COPY(/COPYDATA)
       ENDELSE

       self->UPDATE_GUI

     END ;PRO MULTIPAGE_PLOT_WIDGET::SET_PAGE_LIST



     ;+=================================================================
     ;
     ; Get a copy of the page list.
     ;
     ;-=================================================================
     FUNCTION MULTIPAGE_PLOT_WIDGET::GET_PAGE_LIST, COPY=copy

       IF KEYWORD_SET(copy) THEN BEGIN
         RETURN, self.page_list->GET_COPY(/COPYDATA)
       ENDIF ELSE BEGIN
         RETURN, self.page_list
       ENDELSE

     END ;PRO MULTIPAGE_PLOT_WIDGET::SET_PAGE_LIST



     ;+=================================================================
     ;
     ; Define the class for the multipage plot widget.
     ;
     ;-=================================================================
     PRO MULTIPAGE_PLOT_WIDGET__DEFINE


       struct = { MULTIPAGE_PLOT_WIDGET $
                  ; This will be intialized as a 'LIST' object.
                  ; It will hold a list of plot lists.
                  ,page_list:OBJ_NEW() $
                  ,current_page:0 $
                  ,gui_param:{MULTIPAGE_PLOT_WIDGET__GUI_PARAM $
                              ,gui_id:0 $
                              ,ui_menu_bar:0 $
                              ,plot_size_default:[0,0] $
                              ,plot_size_reduction:[0,0] $
                             } $
                  ,destroy_with_widget:0 $
                  ,message_pro:'' $
                  ,debug:0 $
                  ,INHERITS OBJECT $
                }

     END ; PRO MULTIPAGE_PLOT_WIDGET__DEFINE
