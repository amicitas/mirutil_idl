


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-1
;
; PURPOSE:
;   Allow an object widget to us an internal method for the cleanup
;   handler.
;
;
;-======================================================================



     ;+=================================================================
     ;
     ; Event handler for object widegts
     ;
     ; Since IDL does not allow widget to call object methods, this
     ; is just a little wrapper to send the event to the object method.
     ; 
     ;
     ;-=================================================================
     PRO WIDGET_OBJECT_CLEANUP, id, DEBUG=debug

       ; Get a reference to the object from the uvalue.
       WIDGET_CONTROL, id, GET_UVALUE=object

       ; Call the object event handler.
       IF OBJ_VALID(object) THEN BEGIN
         object->CLEANUP_HANDLER
       ENDIF ELSE BEGIN
         MESSAGE, 'Object reference not valid.  Could not call cleanup handler.', /CONTINUE
       ENDELSE
       


       IF KEYWORD_SET(debug) THEN BEGIN
         PRINT, 'Event from: ' + OBJ_CLASS(object)
       ENDIF

     END ; PRO WIDGET_OBJECT_CLEANUP
