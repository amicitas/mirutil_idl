


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-07
;
; PURPOSE:
;   Allow an object widget to us an internal method for the event
;   handler.
;
;
;-======================================================================



     ;+=================================================================
     ;
     ; Event handler for object widegts
     ;
     ; Since IDL does not allow widget to call object methods, this
     ; is just a little wrapper to send the event to the object method.
     ; 
     ;
     ;-=================================================================
     PRO WIDGET_OBJECT_EVENT, event, DEBUG=debug

       ; Get a reference to the object from the uvalue.
       WIDGET_CONTROL, Event.Handler, GET_UVALUE=object

       ; Call the object event handler.
       object->EVENT_HANDLER, event


       IF KEYWORD_SET(debug) THEN BEGIN
         PRINT, 'Event from: ' + OBJ_CLASS(object)
       ENDIF

     END ; PRO WIDGET_OBJECT_EVENT
