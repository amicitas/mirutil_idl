

;+==============================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   npablant@pppl.gov
;   novimir.pablant@amicitas.com
;
; DATE:
;   2010-03
;
; VERSION:
;   1.0.0
;
; PURPOSE:
;   Find the distance between a line and a point in 3D.
;
; KEYWORDS:
;   /VECTOR
;     Return a vector from the point to the location of closest approtch.
;
;-==============================================================================


    FUNCTION MIR_LINE_DISTANCE_TO_POINT, line_1, line_2, point, VECTOR=vector

      IF KEYWORD_SET(vector) THEN BEGIN
        point_min = MIR_LINE_CLOSEST_TO_POINT(line_1, line_2, point)
        result = point_min - point 
      ENDIF ELSE BEGIN
        result = MIR_MAGNITUDE(CROSSP(point-line_1, point-line_2))/MIR_MAGNITUDE(line_2 - line_1)
      ENDELSE
      
      RETURN, result

    END ; FUNCTION MIR_LINE_DISTANCE_TO_POINT
