


;+======================================================================
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;   novimir.pablant@amicitas.com
;
; DATE:
;   2010-04
;
; VERSION:
;   1.0.0
;
; PURPOSE:
;   Find the standard deviation of an array of data with any linear
;   trend removed.
;
;   This will work exactly when the data to be examined is a
;   linear function with constant noise.
;
;   It will work reasonably well for higher order functions so long
;   and the change in function between sampling points is small
;   compared to the noise.
;   
;
;-======================================================================


     FUNCTION STDDEV_LINEAR_CORRECTED, data_in, calc_size

       num_data = N_ELEMENTS(data_in)

       MIR_DEFAULT, calc_size, num_data-1
       calc_size = calc_size < num_data-1


       ; Find the difference between sucessive data points.
       diff_data = (data_in - SHIFT(data_in, -1))[0:num_data-2]

       stddev = DBLARR(num_data)
       FOR ii=0,num_data-1 DO BEGIN
         CASE 1 OF
           (ii LE calc_size/2): BEGIN
             calc_start = 0
             calc_end = calc_size - 1
           END
           (ii GT num_data - calc_size/2 - 2): BEGIN
             calc_start = num_data - calc_size - 1
             calc_end = num_data - 2
           END
           ELSE: BEGIN
             calc_start = ii - CEIL(calc_size/2E)
             calc_end = ii + calc_size/2 -1
           ENDELSE
         ENDCASE

         ;PRINT, FORMAT='(i2, 2x, 3(2x, i2))', ii, [calc_start, calc_end], calc_end - calc_start + 1
         stddev[ii] = STDDEV(diff_data[calc_start:calc_end])/SQRT(2D)
       ENDFOR

       RETURN, stddev

     END ; FUNCTION STDDEV_LINEAR_CORRECTED
