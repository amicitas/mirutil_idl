FUNCTION ROW_REDUCE, array, $
                     UNIT=unit, $
                     DOUBLE=double, $
                     HELP=help, $
                     SIGFIG=sigfig

  IF KEYWORD_SET(help) THEN BEGIN
    PRINT, 'ROW_REDUCE(array [, /DOUBLE][, /UNIT][, /HELP]'
    PRINT, ''
    PRINT, ' THIS FUNCITON ROW REDUCES AN ARRAY BY PERFORMING ROW OPERATIONS'
    PRINT, ' THIS IS NOT AN EFFICIENT ALGORITHIM AND SHOULD NOT BE USED '
    PRINT, ' FOR LARGE ARRAYS.'
    PRINT, ''
    PRINT, ' THIS PROCEDURE ONLY WORKS FOR 2 DIMENTIONAL ARARYS'
    PRINT, ''
    PRINT, '--------'
    PRINT, 'INPUT:'
    PRINT, ' A 2 dimentional array'
    PRINT, ''
    PRINT, '--------'
    PRINT, 'RETURNS:'
    PRINT, ' A 2 dimentional array of type float or double'
    PRINT, ''
    PRINT, '--------'
    PRINT, 'OPTIONS:'
    PRINT, ''
    PRINT, '/UINT   -- OUTPUT THE RESULTNT ARRAY WITH THE PIVOTS SET TO 1'
    PRINT, '           THE DEFAULT IS TO NORMALIZE THE ROWS'
    PRINT, '/DOUBLE -- Perform calculations in double accuracy.'
    PRINT, '           This will always be done if input array is of type double'
    PRINT, ''
    PRINT, 'SIGFIG=int -- This options determines the number of significant'
    PRINT, '              that should be used in the calculations.'
    PRINT, '              This is more calculation intensive since the rows'
    PRINT, '              of the input matrix must be normalized before the calculations'
    PRINT, '              for this to make sence.'
    RETURN, 0
  ENDIF

  ; SAVE THE SIZE INFORMATION ABOUT THE ARRAY
  size_array = SIZE(array)

  IF size_array[0] NE 2 THEN BEGIN
    PRINT, 'ERROR IN row_reduce.pro'
    PRINT, 'Array must be of dimention 2'
    RETURN, 0
  ENDIF

  ; CREATE A COPY OF THE ARRAY AND CONVERT TO DOUBLE
  IF KEYWORD_SET(double) OR size_array[n_elements(size_array) - 2] EQ 5 THEN BEGIN
    a = DOUBLE(array)
    ten = 10d
    zero = 0D
  ENDIF ELSE BEGIN
    a = FLOAT(array)
    ten = 10e
    zero = 0e
  ENDELSE

  num_rows = size_array[2]
  num_cols = size_array[1]

  pivot_count = 0

  ; IF THE KEYWORD /SIGFIG IS USED THEN NORAMIZE THE ROWS OF THE
  ; INPUT MATIRX SO THAT WE CAN DEFINE AN ACCURACY
  IF KEYWORD_SET(sigfig) THEN BEGIN
    accuracy = ten^(-1*sigfig)
    FOR row = 0,num_rows-1 DO BEGIN
      row_magnitude = TOTAL(a[*,row]^2)
      IF row_magnitude NE 0 THEN BEGIN
        a[*,row] = a[*,row]/SQRT(row_magnitude)
      ENDIF
    ENDFOR
  ENDIF
  
  ; LOOP OVER THE COLUMNS, BUT NOT MORE THAN THE NUMBER OF ROWS
  FOR col=0,num_cols - 1 DO BEGIN
    ; FIRST CHECK IF THERE ARE ANY NON-ZERO NUMBERS IN THE FIRST
    ; COLUMN
    IF KEYWORD_SET(sigfig) THEN BEGIN
      where_nonzero = WHERE(ABS(a[col,*]) GT accuracy)
    ENDIF ELSE BEGIN
      where_nonzero = WHERE(a[col,*] NE 0)
    ENDELSE


    IF where_nonzero[0] NE -1 THEN BEGIN
      num_nonzero = N_ELEMENTS(where_nonzero)

      ; NOW WE NEED SEE IF ANY OF THESE NON ZERO NUMBERS ARE
      ; BELOW OR AT THE CURRENT PIVOT ROW
      where_pivot = WHERE(where_nonzero GE pivot_count)

      IF where_pivot[0] NE -1 THEN BEGIN

        ; CHECK IF THERE IS MORE THAN ONE NON ZERO VALUE IN THIS COLUMN
        IF num_nonzero NE 1 THEN BEGIN

          ; NOW DO ROW SUBTRACTION SO THAT ONLY 1 NON ZERO VALUE IS
          ; LEFT IN THIS COLUMN
          FOR i = num_nonzero-2, 0, -1 DO BEGIN
            a[*,where_nonzero[i]] = ( $
                                      a[*,where_nonzero[i]] - $
                                      a[*,where_nonzero[num_nonzero-1]] / $
                                      a[col,where_nonzero[num_nonzero-1]] * $
                                      a[col,where_nonzero[i]] $
                                    )
            
          ENDFOR

        ENDIF

        ; NOW REORDER THE ROWS
        ; MAKE SURE WE AR NOT TRYING TO MOVE THE ROW TO ITSELF
        IF where_nonzero[num_nonzero-1] GT pivot_count THEN BEGIN
          temp = a[*,pivot_count]
          a[*,pivot_count] = a[*,where_nonzero[num_nonzero-1]]
          a[*,where_nonzero[num_nonzero-1]] = temp
        ENDIF

        ; INCREMENT THE PIVOT COUNT NOW THAT WE ARE FINISHED WITH
        ; THIS COLUMN
        pivot_count = pivot_count + 1
      ENDIF
    ENDIF

  ENDFOR

  ; NOW WE WANT TO NORMALIZE THE OUTPUT
  FOR row = 0,num_rows-1 DO BEGIN
    IF KEYWORD_SET(UNIT) THEN BEGIN
      ; SET ALL THE PIVOTS TO ONE
      IF KEYWORD_SET(sigfig) THEN BEGIN
        where_nonzero = WHERE(ABS(a[row,*]) GT accuracy)
      ENDIF ELSE BEGIN
        where_nonzero = WHERE(a[row,*] NE 0)
      ENDELSE
      IF where_nonzero[0] NE -1 THEN BEGIN
        a[*,row] = a[*,row]/a[where_nonzero[0],row]
      ENDIF 
    ENDIF ELSE BEGIN
      row_magnitude = TOTAL(a[*,row]^2)
      IF KEYWORD_SET(sigfig) THEN BEGIN
        IF row_magnitude gt accuracy THEN BEGIN
          a[*,row] = a[*,row]/SQRT(row_magnitude)
        ENDIF
      ENDIF ELSE BEGIN
        IF row_magnitude NE 0 THEN BEGIN
          a[*,row] = a[*,row]/SQRT(row_magnitude)
        ENDIF
      ENDELSE
    ENDELSE
  ENDFOR

  ; NOW AS A LAST STEP IF /SIGFIG WAS USED THEN SET ALL OF
  ; THE APPROPRIATE ELEMENTS TO ZERO
  IF KEYWORD_SET(sigfig) THEN BEGIN
    where_nonzero = WHERE(ABS(a) LE accuracy)
    IF where_nonzero[0] NE -1 THEN BEGIN
      a[where_nonzero] = zero
    ENDIF
  ENDIF

  RETURN, a

END
