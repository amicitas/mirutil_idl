

;+==============================================================================
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
; PURPOSE:
;   An extended median calculation.
;
;-=============================================================================




     FUNCTION MIR_MEDIAN, array, level
       ON_ERROR, 2
       
       MIR_DEFAULT, level, 0.5

       IF level GT 1 OR level LT 0 THEN BEGIN
         MESSAGE, 'The given level must be between 0.0 and 1.0.'
       ENDIF

       w = WHERE(FINITE(array))
       
       num = N_ELEMENTS(array[w])

       level_index = ROUND((num-1)*level)

       RETURN, ((array[w])[SORT(array[w])])[level_index]

     END ; FUNCTION MIR_MEDIAN
