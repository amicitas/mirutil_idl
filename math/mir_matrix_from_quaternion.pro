
;+==============================================================================
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
; PURPOSE:
;   Return a rotation matrix from a Quaternion.
;
; DESCRIPTION:
;   See (2011-04-13): 
;   https://secure.wikimedia.org/wikipedia/en/wiki/Quaternions_and_spatial_rotation
;
;-==============================================================================



     FUNCTION MIR_MATRIX_FROM_QUATERNION, q

       m = [ [q[0]^2+q[1]^2-q[2]^2-q[3]^2, 2D*(q[1]*q[2]-q[0]*q[3]),    2D*(q[1]*q[3]+q[0]*q[2])] $
            ,[2D*(q[2]*q[1]+q[0]*q[3]),    q[0]^2-q[1]^2+q[2]^2-q[3]^2, 2D*(q[2]*q[3]-q[0]*q[1])] $
            ,[2D*(q[3]*q[1]-q[0]*q[2]),    2D*(q[3]*q[2]+q[0]*q[1]),    q[0]^2-q[1]^2-q[2]^2+q[3]^2] ]

       RETURN, m

     END ; FUNCTION MIR_MATRIX_FROM_QUATERNION
