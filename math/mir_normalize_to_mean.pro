

;+====================================================================== 
; 
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-05
;
; PURPOSE:
;   Normalize an array so that it's mean is 1.0
;
; TAGS:
;   EXCLUDE_FILE_HEADER
;
;
;-======================================================================



     ;+=================================================================
     ;
     ; Normalize an array so that it's mean is 1.0
     ;
     ;-=================================================================
     FUNCTION MIR_NORMALIZE_TO_MEAN, array
       
       RETURN, array/MEAN(array)
       
     END ;FUNCTION MIR_NORMALIZE_TO_MEAN
