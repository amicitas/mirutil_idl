

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;   amicitas@gmail.com
;
; DATE:
;   2010-02
;
; VERSION:
;   0.0.0
;
; PURPOSE:
;   Allow the IDL built in routine <NEWTON> be called with a fuction
;   method instead of a function.
;
;-======================================================================




     ;+=================================================================
     ; PURPOSE:
     ;   This is a wrapper for the function method given in
     ;   <OO_NEWTON> that <NEWTON> will use.
     ;
     ;-=================================================================
     FUNCTION OO_NEWTON_FUNCTION, x
       COMMON OO_NEWTON, c_parameters


       IF ~ IS_NULL(c_parameters.keywords) THEN BEGIN
         RETURN, CALL_METHOD(c_parameters.function_name $
                             ,c_parameters.object_reference $
                             ,x $
                             ,_EXTRA=c_parameters.keywords)
       ENDIF ELSE BEGIN
         ; Call the given method.
         RETURN, CALL_METHOD(c_parameters.function_name $
                             ,c_parameters.object_reference $
                             ,x)
       ENDELSE

     END ; FUNCTION OO_NEWTON_FUNCTION



     ;+=================================================================
     ; PURPOSE:
     ;   Call the built in IDL routine <NEWTON> but using a fuction
     ;   method instead of a function.
     ;
     ;-=================================================================
     FUNCTION OO_NEWTON, x $
                         ,OBJECT_REFERENCE=object_reference $
                         ,FUNCTION_NAME=function_name $
                         ,KEYWORDS=keywords $
                         ,_REF_EXTRA=extra
       COMPILE_OPT STRICTARR

       COMMON OO_NEWTON, c_parameters
       

       IF ~ ISA(function_name) THEN BEGIN
         MESSAGE, 'The name of the function to be evaluated must be given.'
       ENDIF


       IF ~ ISA(object_reference) THEN BEGIN
         ; No object reference was given. Just call <NEWTON> normally.
         RETURN, NEWTON(x, function_name, _STRICT_EXTRA=extra)
       ENDIF

       IF ~ OBJ_VALID(object_reference) THEN BEGIN
         MESSAGE, 'The given object reference is not valid.'
       ENDIF
       
       MIR_DEFAULT, keywords, {NULL}

       ; Fill the common block with the needed info.
       c_parameters = { $
                        object_reference:object_reference $
                        ,function_name:function_name $
                        ,keywords:keywords $
                      }

       RETURN, NEWTON(x, 'OO_NEWTON_FUNCTION', _STRICT_EXTRA=extra)
     END ; FUNCTION OO_NEWTON
