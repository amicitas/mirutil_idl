

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
; DATE:
;   2007-07
;
; VERSION:
;   0.0.0
;
; PURPOSE:
;   Truncate numbers to a given number of significant figures.   
;
;-======================================================================


     ;+=================================================================
     ;
     ; This is a program to truncate numbers to a given number of
     ; significant figures.
     ; 
     ; At this time this funciton does not check to see that the
     ; number of significant figures is realistic for the data type.
     ;
     ; PROGRAMING NOTES:
     ;   I tried two different methods of doing this:
     ;     1. Use ALOG10 to convert to an integer of the reqired length,
     ;        then convert back to the appropriate type.
     ;     2. Convert then nuber to a string, trucate the string, then
     ;        convert back to a number.
     ;
     ;   Method 1 was found to be significantly faster, so it is used
     ;   here.
     ;-=================================================================
     FUNCTION ROUND_SIGFIG, input, sigfig;, PRINT=print

       data_type = SIZE(input, /TYPE)

       ; USE ALOG10 AND THE SIGFIG TO CONVERT TO INTEGER OF THE CORRECT
       ; NUMBER OF SIGFIGS, THEN CONVERT BACK TO THE DESIRED DATA TYPE
       ;  time = SYSTIME(/sec)  

       ; NOW WE NEED TO CHECK IF LONG 64 IS NECESSARY
       ; OTHERWISE US LONG 32
       IF sigfig GT 9 THEN $
         L64 = 1 $
       ELSE $
         L64 = 0

       ; CONVERT THE NUMBER INTO AN INTEGER WITH THE 
       ; CORRECT NUMBER OF SIGNIFICANT FIGURES AND
       ; SAVE THE EXPONENT
       output = input

       ; WE NEED TO SET THE TYPE FOR THESE CALCULATIONS
       ten = FIX(10, TYPE=data_type)

       ; THIS CALCULATION CAN ONLY BE DONE WHERE
       ; THE INPUT IS NOT ZERO
       where_non_zero = where(input NE 0)

       IF where_non_zero[0] NE -1 THEN BEGIN
         ; IF THE DATA IS FLOAT OR DOUBLE
         IF (data_type EQ 4) OR (data_type EQ 5) THEN BEGIN
           exp = FLOOR( ALOG10(ABS(input[where_non_zero])))
           
           num_power = input[where_non_zero]/ten^(exp-sigfig+1)
           integer = ROUND(num_power, L64=L64)
           output[where_non_zero] = integer * ten^(exp-sigfig+1)
         ENDIF ELSE BEGIN
           ; IF THE NUMBER IS ALREADY AN INTEGER THEN WE NEED TO 
           ; DO THINGS SOMEWHAT DIFFERENTLY
           exp = FLOOR( ALOG10(ABS(input[where_non_zero])))
           
           ; WE DON'T WANT TO DO ANY ROUNDING IF WE ALREADY
           ; HAVE LESS THAN THE SPECIFIED NUMBER OF SIG FIGS
           where_to_round = WHERE(exp GE sigfig)
           IF where_to_round[0] NE -1 THEN BEGIN
             where_calculate = where_non_zero[where_to_round]
             ; HERE WE NEED TO CONVERT TO FLOTING POINT IN ORDER TO
             ; DO THE ROUNDING PROPERLY.
             IF sigfig GT 8 THEN $
               tenf = 10d $
             ELSE $
               tenf = 10e
             
             num_power = output[where_calculate]/tenf^(exp[where_to_round]-sigfig+1)
             integer = ROUND(num_power, L64=L64)
             output[where_calculate] = integer*ten^(exp[where_to_round]-sigfig+1)
           ENDIF
         ENDELSE
       ENDIF

       ;  PRINT, FORMAT = '(a0, e10.2)', 'runtime: ', SYSTIME(/sec) - time


       ;   IF KEYWORD_SET(print) THEN BEGIN
       ;     fmt= '(3e24.16)'
       ;     fmti = '(3i15)'
       ;     FOR i=0,N_ELEMENTS(input)-1 DO BEGIN
       ;       PRINT, FORMAT=fmti, input[i], output[i];, output3[i]
       ;     ENDFOR
       ;   ENDIF

       RETURN, output
     END ; FUNCTION ROUND_SIGFIG


     ;+=================================================================
     ;
     ; This is a program to truncate numbers to a given number of
     ; significant figures.
     ; 
     ; This uses a string manipulatin method to perform the trucating.
     ; [ROUND_SIGFIG] should be used instead as it runs much faster.
     ;
     ;
     ; WARNING:
     ;   This function is not complete.
     ;
     ;-=================================================================
     FUNCTION ROUND_SIGFIG_STRING_METHOD, input, sigfig, PRINT=print
       ; THIS IS A PROGRAM TO TRUNCATE NUMBERS TO A GIVEN NUMBER OF
       ; SIGNIFICANT FIGURES.
       ;
       ; AT THIS TIME THIS FUNCITON DOES NOT CHECK TO SEE THAT THE
       ; NUMBER OF SIGNIFICANT FIGURES IS REALISTIC FOR THE DATA TYPE

       data_type = SIZE(input, /TYPE)
       ; METHOD 1
       ; CONVERT TO STRING AND BACK
       
       fmt_e = '(e23.16)'
       string = STRING(FORMAT=fmt_e, input)
       output = FIX(STRMID(string, 0, sigfig+2) + STRMID(string,19), TYPE=data_type)

       RETURN, output
     END ;FUNCTION ROUND_SIGFIG_STRING_METHOD
