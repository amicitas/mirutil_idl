


PRO PERCENT_DIFF, a, b $
                  ,FIRST=first $
                  ,SECOND=second $
                  ,MEAN=mean

  diff = (a-b)
  CASE 1 OF
    KEYWORD_SET(first): BEGIN
      comp = a
      PRINT, 'Difference with respect to first value:'
    END
    KEYWORD_SET(second): BEGIN
      comp = b
      PRINT, 'Difference with respect to second value:'
    END
    ELSE: BEGIN
      comp = (a+b)/2
      PRINT, 'Difference with respect to mean:'
    END
  ENDCASE

  PRINT, diff/comp*100, FORMAT = '(f7.2, "%")'

END
