
;+==============================================================================
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
; PURPOSE:
;   Return a Quaternion from a rotation matrix.
;
; DESCRIPTION:
;   See (2011-04-13): 
;   https://secure.wikimedia.org/wikipedia/en/wiki/Quaternions_and_spatial_rotation
;
;-==============================================================================



     FUNCTION MIR_QUATERNION_FROM_MATRIX, matrix

       u = 0
       v = 1
       w = 2


       r = SQRT(1 + matrix[u,u] -  matrix[v,v] - matrix[w,w])

       q = MAKE_ARRAY(4, TYPE=SIZE(matrix, /TYPE), /NOZERO)

       ; Note that in IDL [a,b] denotes [column, row]
       q[0] = (matrix[v,w] - matrix[w,v])/(2*r)
       q[u+1] = r/2
       q[v+1] = (matrix[v,u] + matrix[u,v])/(2*r)
       q[w+1] = (matrix[u,w] + matrix[w,u])/(2*r)

       RETURN, q


     END ; FUNCTION MIR_QUATERNION_FROM_MATRIX
