

;+==============================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
; DATE:
;   2009-10
;
; VERSION:
;   0.0.0
;
; PURPOSE:
;   Find the angle between two vectors in 3 dimensions.
;
; DESCRIPTION:
;   When no additional keywords are given this will find the angle
;   between two 3D vectors.  This will always be an angle between
;   0 and pi, and will not contain any directional information unless
;   the /NORMAL_DIRECTION keyword is given.
;
; KEYWORDS:
;
;   NORMAL_DIRECTION=vector
;      If given, a normal vector will befined from the two input vectors. The 
;      angle returned will be the angle needed to rotate from input_1
;      to input_2 in the counter clockwise direction around this normal.
;
;      The direction of this normal vector will be determined so that the 
;      normal vector and the normal_direction vector are on the same
;      side of the plane defined by the input vectors.
;
;      It is expected that most of the time the normal direction will simply
;      be the actual normal vector, but this is not required.
;
;      NORMAL_DIRECTION cannot be in the plane defined by the input vectors.
;
;   /XY
;   /XZ
;   /YZ
;      Determine the angle between the projection of the two input
;      vectors onto the given plane.
;
;
;
;-==============================================================================


     FUNCTION MIR_VECTOR_ANGLE, input_1 $
                                ,input_2 $
                                ,NORMAL_DIRECTION=normal_direction $
                                ,XY=xy $
                                ,XZ=xz $
                                ,YZ=yz $
                                ,AXIS=normal


       CASE 1 OF
         KEYWORD_SET(xy): indexes = [0,1]
         KEYWORD_SET(xz): indexes = [0,2]
         KEYWORD_SET(yz): indexes = [1,2]
         ELSE:
       ENDCASE

       IF ISA(indexes) THEN BEGIN
         RETURN, ATAN(input_2[indexes[1]], input_2[indexes[0]]) $
                 - ATAN(input_1[indexes[1]], input_1[indexes[0]])
       ENDIF ELSE BEGIN
         
         normal = CROSSP(input_1, input_2)
         ; Check if the input vectors are parallel.
         IF TOTAL(normal) EQ 0.0 THEN BEGIN
           RETURN, 0.0
         ENDIF

         normal = MIR_NORMALIZE(normal)

         IF ISA(normal_direction) THEN BEGIN
           normal = MIR_NORMALIZE(CROSSP(input_1, input_2))
           dot_product = MIR_DOTP(normal, normal_direction)
           IF dot_product EQ 0 THEN BEGIN
             MESSAGE, 'NORMAL_DIRECTION is perpendicular to the normal vector.'
           ENDIF
           normal = normal * MIR_SIGN(dot_product)
         ENDIF

         ortho = MIR_NORMALIZE(CROSSP(normal, input_1))
         angle = ATAN(MIR_DOTP(ortho, input_2) $
                      ,MIR_DOTP(MIR_NORMALIZE(input_1), input_2))
         
         RETURN, angle

       ENDELSE


     END ; FUNCTION MIR_VECTOR_ANGLE
