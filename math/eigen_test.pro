PRO EIGEN_TEST
  ; A SMALL TEST PROGRAM TO TEST THE EIGENVALUE/VECTOR ROUTINES IN IDL

  ; LETS CREATE A LITTLE ARRAY FOR A TEST CASE
  a = 4D
  b = 1D

  eigen2 = TRANSPOSE([[0,a],[b,0]])

  eigen_val = LA_EIGENPROBLEM(eigen2, EIGENVECTORS=eigen_vec)

  print, eigen_val
  print, eigen_vec

  PRINT, 'HERE I AM USING THE ELMHES & HQR ROUTINES'

  eigen2_hes = ELMHES(eigen2,/DOUBLE)

  eigen_val = HQR(eigen2_hes, /DOUBLE)
  eigen_vec = EIGENVEC(eigen2, eigen_val)
  print, eigen_val
  print, eigen_vec

  STOP
END ;PRO EIGEN_TEST
