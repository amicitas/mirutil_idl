
;+=================================================================
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;
;   antoniuk@fusion.gat.com
;   amicitas@gmail.com
;
; DATE:
;   2009
;
; PURPOSE:
;   Evaluate a gaussian.
;
;   The gaussian is defined by:
;     amp * EXP( -1D*(x - loc)^2/wid^2 )
;
;
;  
;-=================================================================


     FUNCTION GAUSSIAN, x, input $
                        ,CUTOFF=do_cutoff

       ; Decide wich cutoff to use.
       IF KEYWORD_SET(do_cutoff) THEN BEGIN
         cutoff = 1D-16
       ENDIF ELSE BEGIN
         cutoff = 1D-308
       ENDELSE

   
       num_x = N_ELEMENTS(x)

       IF (input.amplitude EQ 0D) OR (input.width EQ 0D) THEN BEGIN
         RETURN, DBLARR(num_x)
       ENDIF

       ; Figure out where the value of the gaussian  will be below the cuttoff
       max_eval = SQRT(-1D * input.width^2 * ALOG(cutoff/ABS(input.amplitude)))
       w = WHERE(ABS(x - input.location) LE max_eval, num_w)

       ; To speed up calculation we avoid using the indexes from above
       ; if we know that all of the point are valid.
       CASE 1 OF
         (num_w EQ num_x): BEGIN
           ; All points will be above the cutoff
           gauss = input.amplitude * $
             EXP( -1D * ( x - input.location )^2 / input.width^2 )
         END

         (num_w EQ 0): BEGIN
           ; All points will be below the cutoff
           gauss = DBLARR(num_x)
         END

         ELSE: BEGIN
           ; Some points will be below the cutoff
           gauss = DBLARR(num_x)
           gauss[w] = input.amplitude * $
             EXP( -1D * ( x[w] - input.location )^2 / input.width^2 )
         END
       ENDCASE
       
       ; Clear underflow errors
       math_status = CHECK_MATH(MASK=(32+64))
       
       RETURN, gauss


     END ;FUNCTION GAUSSIAN
