

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
; DATE:
;   2009-10
;
; VERSION:
;   0.0.0
;
; PURPOSE:
;   Calculate the full width at percent maximum of the given spectrum.
;
; TAGS:
;   EXCLUDE_FILE_HEADER
;
;-======================================================================



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Calculate the full width at percent maximum of the given spectrum.
     ;
     ; TO DO:
     ;   I would like the option to fit the peak to a quadratic.
     ;     When doing this I should be able to set a relative or
     ;     absolute range.
     ;
     ;-=================================================================
     FUNCTION FWHM, x, y $
                    ,LEVEL=level $
                    ,HALF_WIDTHS=half_widths $
                    ,LOCATIONS=locations $
                    ,_REF_EXTRA=extra

       ; By default find the full width at half maximum
       MIR_DEFAULT, level, 0.5

       num_points = N_ELEMENTS(y)

       ; Find the maximum value.
       max = MAX(y, index)

       x_max = x[index]

       ; Interpolate to find the widths at half max.
       x_half_left = INTERPOL(x[0:index-1], y[0:index-1], level*max $
                                ,_STRICT_EXTRA=extra)
       x_half_right = INTERPOL(x[index+1:num_points-1], y[index+1:num_points-1], level*max $
                               ,_STRICT_EXTRA=extra)

       locations = [x_half_left, x_half_right]
       half_widths = [x_max - x_half_left, x_half_right - x_max]
       full_width = ABS(x_half_right - x_half_left)


       RETURN, full_width


      END ; FUNCTION FWHM
