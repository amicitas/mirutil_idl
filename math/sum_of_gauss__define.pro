


;+=================================================================
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;   mir@amicitas.com
;
; DATE:
;   2008-12
;
; PURPOSE:
;   A object to hold a sum of gaussians.
;
;
;
;  
;-=================================================================





; ======================================================================
; ======================================================================
; ######################################################################
;
; ANALYSIS ROUTINES
;   Routines to analyze the sum of gauss object.
;
; ######################################################################
; ======================================================================
; ======================================================================


     ;+=================================================================
     ; PURPOSE:
     ;   Evaluate the sum of gaussians.
     ;-=================================================================
     FUNCTION SUM_OF_GAUSS::EVALUATE, x, _REF_EXTRA=extra
       
       IF (SIZE(x, /DIM))[0] EQ 0 THEN BEGIN
         y = 0D
       ENDIF ELSE BEGIN
         y = DBLARR(N_ELEMENTS(x))
       ENDELSE

       FOR ii=0,self->N_ELEMENTS()-1 DO BEGIN
         y += (self->GET(ii))->EVALUATE(x, _STRICT_EXTRA=extra)
       ENDFOR
       
       RETURN, y

     END ;FUNCTION SUM_OF_GAUSS::EVALUATE



     ;+=================================================================
     ; PURPOSE:
     ;   Evaluate the sum of gaussians.
     ;-=================================================================
     FUNCTION SUM_OF_GAUSS::EVALUATE_DERIVATIVE, x, _REF_EXTRA=extra

       
       IF (SIZE(x, /DIM))[0] EQ 0 THEN BEGIN
         y = 0D
       ENDIF ELSE BEGIN
         y = DBLARR(N_ELEMENTS(x))
       ENDELSE

       FOR ii=0,self->N_ELEMENTS()-1 DO BEGIN
         y += (self->GET(ii))->EVALUATE_DERIVATIVE(x, _STRICT_EXTRA=extra)
       ENDFOR
       
       RETURN, y

     END ;FUNCTION SUM_OF_GAUSS::EVALUATE_DERIVATIVE



     ;+=================================================================
     ; PURPOSE:
     ;   Return the total intensity of the sum of gaussians.
     ;-=================================================================
     FUNCTION SUM_OF_GAUSS::INTENSITY

       intensity = 0D

       FOR ii=0,self->N_ELEMENTS()-1 DO BEGIN
         gauss = self->GET(ii)

         intensity += gauss->INTENSITY()
       ENDFOR

       RETURN, intensity

     END ;FUNCTION SUM_OF_GAUSS::INTENSITY



     ;+=================================================================
     ; PURPOSE:
     ;   Return the centroid of the gaussians.
     ;-=================================================================
     FUNCTION SUM_OF_GAUSS::CENTROID

       centroid_numerator = 0D
       centroid_denominator = 0D

       FOR ii=0,self->N_ELEMENTS()-1 DO BEGIN
         gauss = (self->GET(ii))->GET_STRUCT()
         centroid_numerator += gauss.amplitude * gauss.width * gauss.location
         centroid_denominator += gauss.amplitude * gauss.width
       ENDFOR

       centroid = centroid_numerator/centroid_denominator
       
       ; Check for 'divide by zero' or 'illegal operand' errors.
       math_error = CHECK_MATH(MASK=128+16)
       IF math_error NE 0 THEN BEGIN
         MESSAGE, 'Could not calculate centroid.', /CONTINUE
       ENDIF

       RETURN, centroid

     END ;FUNCTION SUM_OF_GAUSS::CENTROID



     ;+=================================================================
     ; PURPOSE:
     ;   Return SUM(ampiltude*width*location).
     ;   his value can be used in calculating the centroid
     ;-=================================================================
     FUNCTION SUM_OF_GAUSS::CENTROID_NEUMERATOR

       centroid_numerator = 0D

       FOR ii=0,self->N_ELEMENTS()-1 DO BEGIN
         gauss = (self->GET(ii))->GET_STRUCT()
         centroid_numerator += gauss.amplitude * gauss.width * gauss.location
       ENDFOR

       RETURN, centroid_neumerator

     END ;FUNCTION SUM_OF_GAUSS::CENTROID_NEUMERATOR



     ;+=================================================================
     ; PURPOSE:
     ;   Return SUM(ampiltude*width).
     ;   This value can be used in calculating the centroid.
     ;  
     ;-=================================================================
     FUNCTION SUM_OF_GAUSS::CENTROID_DENOMINATOR


       centroid_denominator = 0D

       FOR ii=0,self->N_ELEMENTS()-1 DO BEGIN
         gauss = (self->GET(ii))->GET_STRUCT()
         centroid_denominator += gauss.amplitude * gauss.width
       ENDFOR

       RETURN, centroid_denominator


     END ;FUNCTION SUM_OF_GAUSS::CENTROID_DENOMINATOR



     ;+=================================================================
     ; PURPOSE:
     ;   Estimate the location and extent of the peak of the
     ;   sum of gaussians and evaluate the spectrum over this range
     ;   
     ; DESCRIPTION:
     ;   I do not know an analytical way of finding the maxiumum
     ;   or fwhm of a peak made up of a sum of gaussians.  Accordingly
     ;   I use a neumerical method of finding these properties.
     ;
     ;   As part of the procedure that I am using I need to generate
     ;   a spectrum over the approximate peak locations to use
     ;   in generating the initial guesses.   This routine is 
     ;   used to estimate the peak extent and produce a spectrum
     ;   over this extent.  This is not the most efficent way to do
     ;   things but it works fairly well for most of my purposes.
     ;
     ;   This technique will not work in certain cases.  In particular
     ;   if there are multiple peaks, or if there is a very sharp peak
     ;   and a broad background the actuall peak may fall between the
     ;   the sampled points.
     ;  
     ;-=================================================================
     FUNCTION SUM_OF_GAUSS::GET_PEAK_SPECTRUM, cutoff, num_points
       COMPILE_OPT STRICTARR

       MIR_DEFAULT, cutoff, 0.2
       MIR_DEFAULT, num_points, 50

       peak_range = self->GET_PEAK_RANGE(cutoff)
       
 
       ; Make a guess from where the profile is maximum.
       ; To make the guess we evaluate the profile over the expected range
       ; and find the location of the maximum
       spectrum_x = DINDGEN(num_points)/(num_points-1) $
                    * (peak_range[1] - peak_range[0]) + peak_range[0]
       spectrum_y = self->EVALUATE(spectrum_x)


       RETURN, {y:spectrum_y $
                ,x:spectrum_x}

     END ; FUNCTION SUM_OF_GAUSS::GET_PEAK_SPECTRUM


     ;+=================================================================
     ; PURPOSE:
     ;   Return a range which will contain all points with values 
     ;   above a certain percentage of the amplitude of the largest 
     ;   gaussian in the sum of gauss object.
     ;
     ;   The returned range will in general will be too large, 
     ;   but will always return a range at least as big as needed 
     ;   to include points above the cutoff.
     ;-=================================================================
     FUNCTION SUM_OF_GAUSS::GET_PEAK_RANGE, cutoff

       MIR_DEFAULT, cutoff, 0.1

       ; First get all of the gaussians as an array of structures.
       gaussian_array = self->TO_ARRAY(/STRUCTURE)
       num_gauss = self->N_ELEMENTS()

       ; Find the gaussian with the maximum amplitude
       amplitude_max = MAX(gaussian_array[*].amplitude, amplitude_max_index)


       ; Adjust the cutoff to take into account that peaks overlap.
       ; Look at the worst case, where the peaks are lined up at the point
       ; where each gaussian is aplitude_max * cutoff.
       cutoff_adj = cutoff/(num_gauss)
       cutoff_value = amplitude_max * cutoff_adj

       ; Find which gaussians have amplitudes above the adjusted cutoff.
       where_gt_cutoff = WHERE(gaussian_array[*].amplitude GT cutoff_value)

       ; First find the range based on the gaussians with amplitudes above the cutoff.
       half_width = gaussian_array[where_gt_cutoff].width $
                    * SQRT(ALOG(gaussian_array[where_gt_cutoff].amplitude/cutoff_value))
       range_min = MIN(gaussian_array[where_gt_cutoff].location - half_width)
       range_max = MAX(gaussian_array[where_gt_cutoff].location + half_width)

       RETURN, [range_min, range_max]

     END ; FUNCTION GET_PEAK_RANGE



     ;+=================================================================
     ; PURPOSE:
     ;   Find the maximum value of the sum of gaussians.
     ;
     ; KEYWORDS:
     ;   LOCATION = named variable
     ;     The location where the maximum value is found will be
     ;     returned here.
     ;
     ;   PEAK_SPECTRUM = structure
     ;     As part of the calculation of the maximum location the
     ;     sum of gaussians need to be evaluated over a range
     ;     that contains the peak.
     ;
     ;     Since this spectrum is also needed when calculating
     ;     the full width of the gaussian, this keyword allows
     ;     this spectrum to be only computed once for both
     ;     calculations.
     ;    
     ;
     ;  
     ;-=================================================================
     FUNCTION SUM_OF_GAUSS::MAXIMUM, LOCATION=location_max $
                                     ,PEAK_SPECTRUM=peak_spectrum

       location_max = self->LOCATION_MAX(PEAK_SPECTRUM=peak_spectrum)
       value_max = self->EVALUATE(location_max)

       RETURN, value_max

     END ; FUNCTION SUM_OF_GAUSS::MAXIMUM



     ;+=================================================================
     ; PURPOSE:
     ;   Find the location where the sum of gaussians is maximum.
     ;
     ; KEYWORDS:
     ;   PEAK_SPECTRUM = structure
     ;     As part of the calculation of the maximum location the
     ;     sum of gaussians need to be evaluated over a range
     ;     that contains the peak.
     ;
     ;     Since this spectrum is also needed when calculating
     ;     the full width of the gaussian, this keyword allows
     ;     this spectrum to be only computed once for both
     ;     calculations.

     ;
     ;  
     ;-=================================================================
     FUNCTION SUM_OF_GAUSS::LOCATION_MAX, PEAK_SPECTRUM=peak_spectrum
       COMPILE_OPT STRICTARR

       IF ~ ISA(peak_spectrum) THEN BEGIN
         peak_spectrum = self->GET_PEAK_SPECTRUM(0.8, 20)
       ENDIF

       spectrum_max = MAX(peak_spectrum.y, spectrum_max_index)
       location_max_guess = peak_spectrum.x[spectrum_max_index]

       ; Estimate a reasonable step size.
       ; Here we assume that the guess of the peak extent gives us a
       ; reasonable scalelength for the peak.  We could also find
       ; this by looking at the widths of the gaussians.
       stepmax = (LAST(peak_spectrum.x) - peak_spectrum.x[0]) / N_ELEMENTS(peak_spectrum.x) / 5.0

       location_max = OO_NEWTON(location_max_guess $
                                ,OBJECT=self $
                                ,FUNCTION_NAME='EVALUATE_DERIVATIVE' $
                                ,/DOUBLE $
                                ,STEPMAX=stepmax)

       RETURN, location_max

     END ; FUNCTION SUM_OF_GAUSS::LOCATION_MAX



     ;+=================================================================
     ; PURPOSE:
     ;   Return the full width and fraction of max.
     ;
     ; DESCRIPTION:
     ;   I do not know how to do this analytically, so I will just
     ;   do this neumerically.
     ;
     ;
     ; KEYWORDS:
     ;   LOCATION_MAX = named variable
     ;     The location where the maximum value is found will be
     ;     returned here.
     ;
     ;   VAULE_MAX = named variable
     ;     The maximum value of the peak will be returned here.
     ;
     ;   PEAK_SPECTRUM = structure
     ;     As part of the calculation of the maximum location the
     ;     sum of gaussians need to be evaluated over a range
     ;     that contains the peak.
     ;
     ;     Since this spectrum is also needed when calculating
     ;     the full width of the gaussian, this keyword allows
     ;     this spectrum to be only computed once for both
     ;     calculations.
     ;  
     ;-=================================================================
     FUNCTION SUM_OF_GAUSS::FULL_WIDTH, fraction_max $
                                        ,RANGE=range $
                                        ,LOCATION_MAX=location_max $
                                        ,VALUE_MAX=value_max $
                                        ,PEAK_SPECTRUM=peak_spectrum
       COMPILE_OPT STRICTARR

       IF ~ ISA(peak_spectrum) THEN BEGIN
         peak_spectrum = self->GET_PEAK_SPECTRUM(fraction_max, 50)
       ENDIF

       num_points = N_ELEMENTS(peak_spectrum.y)

       value_max = self->MAXIMUM(LOCATION=location_max, PEAK_SPECTRUM=peak_spectrum)

       ; Make the inital guess for this level.
       where_gt_level = WHERE(peak_spectrum.y GT value_max*fraction_max, num_gt_level)
       
       ; Make the guess for the left and right sides
       x_left = peak_spectrum.x[0 > (where_gt_level[0]-1)]
       x_right = peak_spectrum.x[(num_points -1) < (where_gt_level[num_gt_level-1]+1)]
       
       ; I want to make sure that the step size will be less than the distance
       ; from the initial guess to the location_max.  stepmax is a scaling
       ; factor applied to the inital guess.
       stepleft = ABS(location_max - x_left)/8.0
       
       location_left = OO_NEWTON(x_left $
                                 ,OBJECT=self $
                                 ,FUNCTION_NAME='FULL_WIDTH_FUNCTION' $
                                 ,KEYWORDS={fraction_max:fraction_max $
                                            ,value_max:value_max} $
                                 ,/DOUBLE $
                                 ,STEPMAX=stepleft)

       stepright = ABS(x_right - location_max)/8.0

       location_right = OO_NEWTON(x_right $
                                  ,OBJECT=self $
                                  ,FUNCTION_NAME='FULL_WIDTH_FUNCTION' $
                                  ,KEYWORDS={fraction_max:fraction_max $
                                             ,value_max:value_max} $
                                  ,/DOUBLE $
                                  ,STEPMAX=stepright)
       
       range = [location_left, location_right]

       full_width = ABS(location_right - location_left)


       RETURN, full_width

     END ;FUNCTION SUM_OF_GAUSS::FULL_WIDTH




     ;+=================================================================
     ; PURPOSE:
     ;   This function will return the differece between the value
     ;   of the SUM_OF_GAUSS object at a given point and a fraction
     ;   of the maximum value.
     ;
     ;   This is to be used by <::FULL_WIDTH> as the function used
     ;   for the <NEWTON> procedure.
     ;
     ;  
     ;-=================================================================
     FUNCTION SUM_OF_GAUSS::FULL_WIDTH_FUNCTION, x $
                                                 ,FRACTION_MAX=fraction_max $
                                                 ,VALUE_MAX=value_max

       RETURN, self->EVALUATE(x) - value_max * fraction_max
     END ; FUNCTION SUM_OF_GAUSS::FULL_WIDTH_FUNCTION



; ======================================================================
; ======================================================================
; ######################################################################
;
; MANIPULATION ROUTINES
;   Routines to manipulate sum of gauss object.
;
; ######################################################################
; ======================================================================
; ======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Scale the sum of gaussians.
     ;
     ;   The scaleing is done to chage the width of the profile.
     ;   The relitive widths and spacings of the gaussians in the profile
     ;   are kept the same. 
     ;
     ;   The gaussian amplitude can be adjusted, and the gaussian 
     ;   can be shifted.
     ;
     ;   The amplitude is scaled to retain normalization.
     ;
     ;   amplitude   = amplitude * amplitude_scale / width_scale
     ;   width       = width * width_scale
     ;   location    = centroid + (location - centroid) * width_scale
     ;
     ;   Note: Negative scales are allowed.
     ;
     ; KEYWORDS:
     ;   CENTROID = float
     ;       If given then this value will be used instead of 
     ;       calculating the centroid.  If set to a named variable
     ;       the calculated centroid will be returned.
     ;
     ;       This is useful to avoid calculating the centroid twice
     ;       when both scaling and moving the sum of gaussians.
     ;-=================================================================
     PRO SUM_OF_GAUSS::SCALE, WIDTH=scale_width $
                              ,AMPLITUDE=scale_amp $
                              ,CENTROID=centroid

       ; If the centroid was not given, then calculate it.
       IF ~ ISA(centroid) THEN BEGIN
         centroid = self->CENTROID()
       ENDIF
       MIR_DEFAULT, scale_width, 1
       MIR_DEFAULT, scale_amp, 1

       IF scale_width EQ 0D THEN BEGIN
         FOR ii=0,self->N_ELEMENTS()-1 DO BEGIN
           gauss = self->GET(ii)
           gauss->SET_AMPLITUDE, !values.d_infinity
           gauss->SET_WIDTH, 0D
           gauss->SET_LOCATION, centroid
         ENDFOR
       ENDIF ELSE BEGIN
         FOR ii=0,self->N_ELEMENTS()-1 DO BEGIN
           gauss = self->GET(ii)
           gauss->SET_AMPLITUDE, gauss->AMPLITUDE() / ABS(scale_width) * scale_amp
           gauss->SET_WIDTH, gauss->WIDTH() * ABS(scale_width)
           gauss->SET_LOCATION, centroid + (gauss->LOCATION() - centroid) * scale_width
         ENDFOR
       ENDELSE

       ; We dont want to report underflow errors here.
       math_error = CHECK_MATH(MASK=32) 
       
     END ; PRO SUM_OF_GAUSS::SCALE



     ;+=================================================================
     ; PURPOSE:
     ;   Move the gaussians to be at the given centroid location.
     ;
     ; KEYWORDS:
     ;   CENTROID = float
     ;       If given then this value will be used instead of 
     ;       calculating the centroid.  If set to a named variable
     ;       the calculated centroid will be returned.
     ;
     ;       This is useful to avoid calculating the centroid twice
     ;       when both scaling and moving the sum of gaussians.
     ;-=================================================================
     PRO SUM_OF_GAUSS::MOVE, loc_in $
                             ,CENTROID=centroid

       ; If the centroid was not given, then calculate it.
       IF ~ ISA(centroid) THEN BEGIN
         centroid = self->CENTROID()
       ENDIF

       ; Turn the given location into a distance from the centroid.
       distance = loc_in - centroid
      
       self->SHIFT, distance
       
     END ; PRO SUM_OF_GAUSS::MOVE



     ;+=================================================================
     ; PURPOSE:
     ;   Move the gaussians by the given amount.
     ;
     ;-=================================================================
     PRO SUM_OF_GAUSS::SHIFT, distance

       FOR ii=0,self->N_ELEMENTS()-1 DO BEGIN
         gauss = self->GET(ii)
  
         gauss->SET_LOCATION, gauss->LOCATION() + distance
       ENDFOR

       
     END ; PRO SUM_OF_GAUSS::SHIFT



     ;+=================================================================
     ; PURPOSE:
     ;   Normalize the area of the gaussian to 1.
     ;
     ;-=================================================================
     PRO SUM_OF_GAUSS::NORMALIZE


       intensity = self->INTENSITY()

       FOR ii=0,self->N_ELEMENTS()-1 DO BEGIN
         gauss = self->GET(ii)

         gauss->SET_AMPLITUDE, gauss->AMPLITUDE() / intensity
       ENDFOR

     END ; PRO SUM_OF_GAUSS::NORMALIZE




; ======================================================================
; ======================================================================
; ######################################################################
;
; QBJECT ROUTINES
;   Routines handle the internal data of the SUM_OF_GAUSS object.
;
;   Many of these are reimplementations of the superclass
;   <SIMPLELIST::>
;
; ######################################################################
; ======================================================================
; ======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Set a specified gaussian to be equal to the given 
     ;   gaussian object.
     ;
     ;   Assume that the input will be a single gaussian object
     ;-=================================================================
     PRO SUM_OF_GAUSS::SET, index, gaussian, NO_COPY=no_copy
       
       self->CHECK_INDEX, index

       IF ~ KEYWORD_SET(no_copy) THEN BEGIN
         (self->GET(index))->COPY_FROM, gaussian, /RECURSIVE
       ENDIF ELSE BEGIN
         self->REMOVE, index
         self->SET, index, gaussain
       ENDELSE

     END ; PRO SUM_OF_GAUSS::SET



     ;+=================================================================
     ; PURPOSE:
     ;   Get an array of gaussian objects or structures.
     ;-=================================================================
     FUNCTION SUM_OF_GAUSS::TO_ARRAY, STRUCTURE=structure

       
       IF KEYWORD_SET(structure) THEN BEGIN
         list = OBJ_NEW('SIMPLELIST')
         FOR ii=0,self->N_ELEMENTS()-1 DO BEGIN
           list->APPEND, (self->GET(ii))->GET_STRUCT()
         ENDFOR
         array = list->TO_ARRAY()
         list->DESTROY
       ENDIF ELSE BEGIN
         array = self->SIMPLELIST::TO_ARRAY()
       ENDELSE

       
       RETURN, array

     END ;FUNCTION SUM_OF_GAUSS::TO_ARRAY



     ;+=================================================================
     ; PURPOSE:
     ;   Set the contents from an array of gaussians.
     ;
     ;   Assume that the input will be an array of gaussians structures
     ;-=================================================================
     PRO SUM_OF_GAUSS::FROM_ARRAY, gaussian_array, NO_COPY=no_copy
       

       num_gauss_new = N_ELEMENTS(gaussian_array)

       IF KEYWORD_SET(no_copy) THEN BEGIN

         ; Copy the gaussaian object references.
         self->REMOVE, /ALL
         self->SIMPLELIST::FROM_ARRAY, gaussaian_array

       ENDIF ELSE BEGIN

         ; Copy the gaussian object data.
         self->SET_NUM_GAUSS, num_gauss_new
         FOR ii=0,num_gauss_new-1 DO BEGIN
           (self->GET(ii))->COPY_FROM, gaussian_array[ii], /RECURSIVE
         ENDFOR


       ENDELSE

     END ; PRO SUM_OF_GAUSS::FROM_ARRAY



     ;+=================================================================
     ; PURPOSE:
     ;   Append the contents of an array of gaussians.
     ;
     ;   Assume that the input will be an array of gaussians structures
     ;-=================================================================
     PRO SUM_OF_GAUSS::JOIN, gaussian_array, NO_COPY=no_copy
       
       num_gauss_in = N_ELEMENTS(gaussian_array)
       num_gauss_old = self->N_ELEMENTS()

       IF KEYWORD_SET(no_copy) THEN BEGIN

         ; Copy the gaussaian object references.
         FOR ii=0, num_gauss_in-1 DO BEGIN
           self->SIMPLELIST::APPEND, gaussian_array[ii]
         ENDFOR

       ENDIF ELSE BEGIN

         ; Copy the gaussian object data.
         self->SET_NUM_GAUSS, num_gauss_old + num_gauss_in
         FOR ii=0, num_gauss_in-1 DO BEGIN
           (self->GET(ii+num_gauss_old))->COPY_FROM, gaussian_array[ii], /RECURSIVE
         ENDFOR
         
       ENDELSE

     END ; PRO SUM_OF_GAUSS::JOIN



     ;+=================================================================
     ; PURPOSE:
     ;   Copy the input object to the current object.
     ;
     ;   If recursive is set, the copy the gaussians across when
     ;   possible.
     ;   
     ;-=================================================================
     PRO SUM_OF_GAUSS::SET_NUM_GAUSS, num_gauss
       
       num_gauss_start =  self->N_ELEMENTS()

       CASE 1 OF
         (num_gauss LT num_gauss_start): BEGIN
           FOR ii=1,num_gauss_start - num_gauss DO BEGIN
             self->REMOVE, /LAST
           ENDFOR
         END
         (num_gauss GT num_gauss_start): BEGIN
           FOR ii=1,num_gauss - num_gauss_start DO BEGIN
             self->APPEND, OBJ_NEW('GAUSSIAN')
           ENDFOR
         END
         ELSE: ; Number of gauss is already correct.
       END

     END ;PRO SUM_OF_GAUSS::SET_NUM_GAUSS



     ;+=================================================================
     ; PURPOSE:
     ;   Copy the input object to the current object.
     ;
     ;   If recursive is set, the copy the gaussians across when
     ;   possible.
     ;   
     ;-=================================================================
     PRO SUM_OF_GAUSS::COPY_FROM, data, RECURSIVE=recursive
       
       IF KEYWORD_SET(recursive) THEN BEGIN

         ; Set the correct number of gaussaians, then copy the
         ; gaussians accross.
         num_gauss_new = data->N_ELEMENTS()
         self->SET_NUM_GAUSS,  num_gauss_new
         FOR ii=0, num_gauss_new-1 DO BEGIN
           (self->GET(ii))->COPY_FROM, data->GET(ii), RECURSIVE=recursive
         ENDFOR

       ENDIF ELSE BEGIN
         self->REMOVE, /ALL
         self->SIMPLELIST::COPY_FROM, data
       ENDELSE

     END ;PRO SUM_OF_GAUSS::COPY_FROM



     ;+=================================================================
     ; PURPOSE:
     ;  Reset all of the gaussians.
     ;-=================================================================
     PRO SUM_OF_GAUSS::RESET

       FOR ii=0,self->N_ELEMENTS()-1 DO BEGIN
         (self->GET(ii))->RESET
       ENDFOR

     END ;PRO SUM_OF_GAUSS::REMOVE



     ;+=================================================================
     ; PURPOSE:
     ;   Cleanup on destruction
     ;-=================================================================
     PRO SUM_OF_GAUSS::REMOVE, index, ALL=all

       IF KEYWORD_SET(all) THEN BEGIN
         FOR ii=0,self->N_ELEMENTS()-1 DO BEGIN
           (self->GET(ii))->DESTROY
         ENDFOR
       ENDIF ELSE BEGIN
         (self->GET(index))->DESTROY
       ENDELSE

       self->SIMPLELIST::REMOVE, index, ALL=all

     END ;PRO SUM_OF_GAUSS::REMOVE



     ;+=================================================================
     ; PURPOSE:
     ;   Cleanup on destruction
     ;-=================================================================
     PRO SUM_OF_GAUSS::DEALLOCATE, LEAVEDATA=leavedata

       IF ~ KEYWORD_SET(leavedata) THEN BEGIN
         self->REMOVE, /ALL
       ENDIF

       self->SIMPLELIST::DEALLOCATE

     END ;PRO SUM_OF_GAUSS::DEALLOCATE



     ;+=================================================================
     ; PURPOSE:
     ;   Define a class for a sum of gaussians.
     ;-=================================================================
     PRO SUM_OF_GAUSS__DEFINE

       struct = { SUM_OF_GAUSS $
                  ,INHERITS SIMPLELIST $
                }

     END ;PRO SUM_OF_GAUSS__DEFINE

