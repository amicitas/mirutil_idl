

;+====================================================================== 
; 
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-05
;
; PURPOSE:
;   Normalize a vector to have a magnitude of 1.0, or if desired
;   to the given magnitude.
;
; TAGS:
;   EXCLUDE_FILE_HEADER
;
;
;-======================================================================



     ;+=================================================================
     ;
     ; Normalize a vector to have a magnitude of 1.0, or if desired
     ;   to the given magnitude.
     ;
     ;-=================================================================
     FUNCTION MIR_NORMALIZE, vector, final_magnitude, DOUBLE=double, QUIET=quiet
       COMPILE_OPT STRICTARR
       ON_ERROR, 2

       vector_magnitude = MIR_MAGNITUDE(vector, DOUBLE=double)
       IF vector_magnitude EQ 0 THEN BEGIN
         IF KEYWORD_SET(quiet) THEN BEGIN
           RETURN, vector
         ENDIF ELSE BEGIN
           MESSAGE, 'Cannot normalize null vector.'
         ENDELSE
       ENDIF

       IF N_ELEMENTS(final_magnitude) EQ 0 THEN BEGIN
         RETURN, vector/vector_magnitude
       ENDIF ELSE BEGIN
         RETURN, vector/vector_magnitude*final_magnitude
       ENDELSE
       
     END ;FUNCTION MIR_NORMALIZE
