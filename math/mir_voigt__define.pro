
;+==============================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
; DATE:
;   2010-09
;
; PURPOSE:
;   A Voigt object.
;
; DESCRIPTION:
;   The Voigt profile is the result of a covolution of a Gaussian and
;   and a Lorentzian profile.
;
;   Gaussian:  
;       1D/sigma/SQRT(2*!pi) * EXP(-1D * x^2 / 2D / sigma^2)
;
;   Lorentzian:
;       gamma / (((x - x0)^2 + gamma^2)) * (1/!pi)
;
;   Voigt:
;       eta / sigma / !pi / SQRT(2*!pi) $
;         * INT( EXP(-1D*y^2)/(x0/SQRT(2)/sigma - y)^2 + eta^2 ) dy, -Inf, Inf)
;
;       where:
; 
;       eta = gamma/SQRT(2)/sigma
;       
;
;-==============================================================================

     ;+=================================================================
     ; PURPOSE:
     ;   Determine the half width at fraction of max.
     ;
     ;-=================================================================
     FUNCTION MIR_VOIGT::HWFM_FUNC, x

       RETURN, self.EVALUATE(x) - self.hwfm_level

     END ;FUNCTION MIR_VOIGT::HWFM_FUNC



     ;+=================================================================
     ; PURPOSE:
     ;   Determine the full width at fraction of max.
     ;
     ;-=================================================================
     FUNCTION MIR_VOIGT::FWFM, fraction

       ; First calculate the maximum value.
       max = self.EVALUATE(self.location)
       self.hwfm_level = max * fraction

       ; Calculate a guess.
       gauss_hwfm = SQRT(2D*ALOG(1D/fraction))*self.sigma

       ; Exact hwfm for the lorentzian.
       lorentz_hwfm = self.gamma*SQRT(1D/fraction - 1D)

       ; This will always be less than the actual hwfm.
       voigt_hwfm_min = SQRT(gauss_hwfm^2 + lorentz_hwfm^2)

       ; This will always be larger than the actual hwfm.
       voigt_hwfm_max = gauss_hwfm + lorentz_hwfm

       x = [voigt_hwfm_min  $
            ,voigt_hwfm_max $
            ,(voigt_hwfm_min + voigt_hwfm_max)/2D]

       hwfm = MIR_FX_ROOT(x, 'HWFM_FUNC', self)

       RETURN, hwfm*2

     END ;FUNCTION MIR_VOIGT::FWFM


     ;+=================================================================
     ; PURPOSE:
     ;   Evalutate the Voigt function but only where the result will 
     ;   be above a given cutoff.
     ; 
     ; DESCRIPTION:
     ;   In spectral fitting applications there will be many cases where
     ;   a Voigt profile will be evaluated at all points in the spectrum
     ;   but where values below a certain cutoff are not important.
     ;
     ;   This routine will only calculate the voigt profile where
     ;   the final value will be above a given cutoff.
     ;
     ;   In typical use where many points fall below the cutoff this
     ;   can speed up evaluation by > 70%.  In the worse case, where 
     ;   nearly all the points are above the cutoff, this routine will
     ;   take 10% longer than a simple Voigt evaluation.
     ;
     ;   Improvements of more that 70% are not easily obtainable given
     ;   the need to seach the given values for points above the
     ;   cutoff.  One could probably do better if this was programmed in
     ;   c and a CALL_EXTERNAL were used. <VOIGT> however is alread written
     ;   in c, so no improvement to that portion of the calculation is
     ;   likely possible.
     ;
     ;   Note:  As written this is much slower than direct evaluation
     ;          if sigma=0 (pure Lorentzian).
     ;   
     ;
     ; TEST RESULTS:
     ;   test_20110923, 1000, 1E5, CUTOFF=1E-04, SIGMA=1D, GAMMA=0.01D
     ;                 Y MIN:  3.2e-07
     ;        Y MIN w/CUTOFF:  2.1e-04
     ;
     ;   Minimum expected:       4.6%
     ;
     ;   EVALUATE_SIMPLE               : 16.74990 (100.0%)
     ;   EVALUATE, CUTOFF=1.0e-308     : 18.57056 (110.9%)
     ;   EVALUATE, CUTOFF=1.0e-04      : 4.70331 ( 28.1%)

     ;
     ;-=================================================================
     FUNCTION MIR_VOIGT::EVALUATE, x, CUTOFF=cutoff_value

       IF (self.intensity EQ 0D) OR (self.sigma EQ 0D AND self.gamma EQ 0D) THEN BEGIN
         IF (SIZE(x, /DIM))[0] EQ 0 THEN BEGIN
           y = 0D
         ENDIF ELSE BEGIN
           y = DBLARR(N_ELEMENTS(x))
         ENDELSE
         RETURN, y
       ENDIF


       IF ISA(cutoff_value) THEN BEGIN
         max = self.EVALUATE_SIMPLE(self.location)
         IF cutoff_value GT max THEN BEGIN
           count_above = 0
           count_below = N_ELEMENTS(x)
         ENDIF ELSE BEGIN
           fraction = cutoff_value/max
           gauss_hwfm = SQRT(2D*ALOG(1D/fraction))*self.sigma
           lorentz_hwfm = self.gamma*SQRT(1D/fraction - 1D)

           ; This will always be LARGER than the actual voigt_hwfm.
           voigt_hwfm = gauss_hwfm + lorentz_hwfm
           w_above = WHERE(ABS(x-self.location) LE voigt_hwfm $
                           ,count_above $
                           ,NCOMPLEMENT=count_below)
         ENDELSE

         CASE 1 OF
           count_below EQ 0: BEGIN
             ; All points will be above the cutoff
             y = self.EVALUATE_SIMPLE(x)
           END
           count_above EQ 0: BEGIN
             ; All points will be below the cuttoff.
             y = DBLARR(count_below)
           END
           ELSE: BEGIN
             ; Some points will be below the cutoff.
             y = DBLARR(count_above+count_below)
             y[w_above] = self.EVALUATE_SIMPLE(x[w_above])
           ENDELSE
         ENDCASE


       ENDIF ELSE BEGIN
         ; Don't do a cutoff.
         y = self.EVALUATE_SIMPLE(x)
       ENDELSE


       RETURN, y

     END ;FUNCTION MIR_VOIGT::EVALUATE



     ;+=================================================================
     ; PURPOSE:
     ;   Evalutate the Voigt function.
     ;
     ;-=================================================================
     FUNCTION MIR_VOIGT::EVALUATE_SIMPLE, x

       CASE 0D OF
         self.sigma: BEGIN
           ; Pure lorentzian
           y = self.intensity*self.gamma /  (((x - self.location)^2 + self.gamma^2)) * (1D/!dpi)
         END
         self.gamma: BEGIN
           ; Pure gaussian
           y = self.intensity/self.sigma/SQRT(2*!dpi) * EXP(-1D * (x-self.location)^2 / 2D / self.sigma^2)
         END
         ELSE: BEGIN
           u = (x-self.location)/SQRT(2)/self.sigma
           a = self.gamma/SQRT(2)/self.sigma
           y = VOIGT(a, u) / SQRT(2*!dpi)/self.sigma * self.intensity
         ENDELSE
       ENDCASE

       RETURN, y

     END ;FUNCTION MIR_VOIGT::EVALUATE_SIMPLE


     ;+=================================================================
     ; Return the intensity
     ;-=================================================================
     FUNCTION MIR_VOIGT::INTENSITY
       
       RETURN, self.intensity

     END ;FUNCTION MIR_VOIGT::INTENSITY


     ;+=================================================================
     ; Set the intensity
     ;-=================================================================
     PRO MIR_VOIGT::SET_INTENSITY, intensity
       
       self.intensity = intensity

     END ;PRO MIR_VOIGT::SET_INTENSITY


     ;+=================================================================
     ; Return the location
     ;-=================================================================
     FUNCTION MIR_VOIGT::LOCATION
       
       RETURN, self.location

     END ;FUNCTION MIR_VOIGT::LOCATION


     ;+=================================================================
     ; Set the location
     ;-=================================================================
     PRO MIR_VOIGT::SET_LOCATION, location
       
       self.location = location

     END ;PRO MIR_VOIGT::SET_LOCATION


     ;+=================================================================
     ; Return sigma
     ;-=================================================================
     FUNCTION MIR_VOIGT::SIGMA
       
       RETURN, self.sigma

     END ;FUNCTION MIR_VOIGT::SIGMA


     ;+=================================================================
     ; Set sigma
     ;-=================================================================
     PRO MIR_VOIGT::SET_SIGMA, sigma
       
       self.sigma = ABS(sigma)

     END ;PRO MIR_VOIGT::SET_SIGMA


     ;+=================================================================
     ; Return gamma
     ;-=================================================================
     FUNCTION MIR_VOIGT::GAMMA
       
       RETURN, self.gamma

     END ;FUNCTION MIR_VOIGT::GAMMA


     ;+=================================================================
     ; Set gamma
     ;-=================================================================
     PRO MIR_VOIGT::SET_GAMMA, gamma
       
       self.gamma = ABS(gamma)

     END ;PRO MIR_VOIGT::SET_GAMMA


     ;+=================================================================
     ; Reset the voigt to zero
     ;-=================================================================
     PRO MIR_VOIGT::RESET
       
       self.intensity = 0D
       self.location = 0D
       self.sigma = 0D
       self.gamma = 0D

     END ;PRO MIR_VOIGT::RESET



     ;+=================================================================
     ; Allow a way to set the values.
     ; 
     ; The input should be a structure with the following tags:
     ;  intensity
     ;  location
     ;  sigma
     ;  gamma
     ;-=================================================================
     PRO MIR_VOIGT::SET, voigt_in

       STRUCT_ASSIGN, voigt_in, self, /NOZERO

       self.sigma = ABS(self.sigma)
       self.gamma = ABS(self.gamma)

     END ;PRO MIR_VOIGT::SET



     ;+=================================================================
     ; PURPOSE:
     ;   Return a structure with the voigt parameters.
     ;
     ; DESCRIPTION:
     ;   By inheriting <OBJECT::> I end up with an extra propertry:
     ;   '_NULL'.  I do not want this returned when calling this
     ;   method.
     ;
     ;-=================================================================
     FUNCTION MIR_VOIGT::GET_STRUCT

       RETURN, {intensity:self.intensity $
                ,location:self.location $
                ,sigma:self.sigma $
                ,gamma:self.gamma $
               }

     END ;FUNCTION MIR_VOIGT::GET_STRUCT



     ;+=================================================================
     ; Initialize the object.
     ; 
     ; Optionally a structure may be given to set the voigt.
     ; This structure will be passed to MIR_VOIGT::SET
     ;-=================================================================
     FUNCTION MIR_VOIGT::INIT, voigt_in

       IF N_PARAMS() NE 0 THEN BEGIN
         self->SET, voigt_in
       ENDIF

       RETURN, 1

     END ;FUNCTION MIR_VOIGT::INIT


     ;+=================================================================
     ; Define a voigt object
     ;-=================================================================
     PRO MIR_VOIGT__DEFINE
       
       struct = { MIR_VOIGT $
                  ,intensity:0D $
                  ,location: 0D $
                  ,sigma:0D $
                  ,gamma:0D $

                  ,hwfm_level:0D $
                  ,INHERITS OBJECT $
                }
                 

     END ;PRO MIR_VOIGT__DEFINE
