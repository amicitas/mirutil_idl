


     ;+=================================================================
     ; Return the integrated intesity from a slice accross a circle.
     ; 
     ; If we had perfect focus this would be the intensity that would 
     ; would see at a particular row due to the fiber shapes.
     ;
     ; The returned intensity is normalized to a total intensity of one.
     ;-=================================================================
     FUNCTION INTENS_FROM_CIRCLE, y, r

       type = SIZE(y, /TYPE)
       dimension = SIZE(y, /DIM)
       
       normalization = !dpi * r^2

       IF dimension[0] EQ 0 THEN BEGIN
         result = FIX(0, TYPE=type)
       ENDIF ELSE BEGIN
         result = REPLICATE(FIX(0, TYPE=type), dimension)
       ENDELSE

       ; For any y values outisde of the circle radius
       ; the value is zero.
       where_lt_r = WHERE(ABS(y) LT r)
       IF where_lt_r[0] NE -1 THEN BEGIN
         result[where_lt_r] = 2*SQRT(r^2 - y[where_lt_r]^2)/normalization
       ENDIF

       RETURN, result

     END ;FUNCTION INTENS_FROM_CIRCLE
