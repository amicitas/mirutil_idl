

;+=================================================================
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;     antoniuk@fusion.gat.com
;     novimir.pablant@amicitas.com
;
; DATE:
;   2008-12-19
;
; PURPOSE:
;   A gaussian object.
;
;   The gaussian is defined by:
;     amp * EXP( -1D*(x - loc)^2/wid^2 )
;
;
;
;
;  
;-=================================================================




     ;+=================================================================
     ; Evaluate the gaussian
     ; \CUTOFF 
     ;   Only calculate where the answer will be greater than:
     ;   1E-16
     ;
     ; Otherwise only calculate where the answer will be greater than
     ;   1E-308
     ;
     ; Doing these cutoffs can speed up calculation by an order of
     ; magnitude if most of the points would have evaluated below the
     ; the cutoff.
     ;
     ; If all points need to calculated this can slow things down by 
     ; as much as 20% compared to a simple calculation
     ; (for 1000 points).
     ; 
     ;
     ;-=================================================================
     FUNCTION GAUSSIAN::EVALUATE, x $
                                  ,CUTOFF=do_cutoff

       ; Decide wich cutoff to use.
       IF KEYWORD_SET(do_cutoff) THEN BEGIN
         cutoff = 1D-16
       ENDIF ELSE BEGIN
         cutoff = 1D-308
       ENDELSE

   
       num_x = N_ELEMENTS(x)

       IF (self.amplitude EQ 0D) OR (self.width EQ 0D) THEN BEGIN
         IF (SIZE(x, /DIM))[0] EQ 0 THEN BEGIN
           gauss = 0D
         ENDIF ELSE BEGIN
           gauss = DBLARR(num_x)
         ENDELSE
         RETURN, gauss
       ENDIF

       ; Figure out where the value of the gaussian  will be below the cuttoff
       max_eval = SQRT(-1D * self.width^2 * ALOG(cutoff/ABS(self.amplitude)))
       w = WHERE(ABS(x - self.location) LE max_eval, num_w)

       ; To speed up calculation we avoid using the indexes from above
       ; if we know that all of the point are valid.
       CASE 1 OF
         (num_w EQ num_x): BEGIN
           ; All points will be above the cutoff
           gauss = self.amplitude * $
             EXP( -1D * ( x - self.location )^2 / self.width^2 )
         END

         (num_w EQ 0): BEGIN
           ; All points will be below the cutoff
           gauss = DBLARR(num_x)
         END

         ELSE: BEGIN
           ; Some points will be below the cutoff
           gauss = DBLARR(num_x)
           gauss[w] = self.amplitude * $
             EXP( -1D * ( x[w] - self.location )^2 / self.width^2 )
         END
       ENDCASE
       
       ; Clear underflow errors
       math_status = CHECK_MATH(MASK=(32+64))
       
       RETURN, gauss

     END ;FUNCTION GAUSSIAN::EVALUATE



     ;+=================================================================
     ; PURPOSE:
     ;   Evaluate the derivative of the gaussian.
     ;   y = df(x)/dx
     ;
     ;-=================================================================
     FUNCTION GAUSSIAN::EVALUATE_DERIVATIVE, x $
                                             ,CUTOFF=do_cutoff
       
       deriv = -1D * self.amplitude $
         / self.width^2 $
         * ( x - self.location ) $
         * EXP( -1D * ( x - self.location )^2 / self.width^2 )
       
       ; Clear underflow errors
       math_status = CHECK_MATH(MASK=(32+64))
       
       RETURN, deriv

     END ;FUNCTION GAUSSIAN::EVALUATE_DERIVATIVE



     ;+=================================================================
     ; Scale the gaussian.
     ;
     ; This is mainly for use with profiles made up of sum of gaussians.
     ;
     ; The scaleing is done to chage the width of the profile.
     ; The relitive widths and spacings of the gaussians in the profile
     ; are kept the same. 
     ;
     ; The amplitude is scaled to retain normalization.
     ;
     ; width**2       = width**2 * scale**2
     ; aplitude*width = amplitude * width
     ; shift          = shift * scale
     ;
     ; Note: Negative scales are allowed.
     ;-=================================================================
     PRO GAUSSIAN::SCALE, scale
       
       self.amplitude = self.amplitude / ABS(scale)
       self.width = self.width * ABS(scale)
       self.location = self.location * scale

     END ;PRO GAUSSIAN::SCALE



     ;+=================================================================
     ; Normalize the gaussian.
     ;-=================================================================
     PRO GAUSSIAN::NORMALIZE
       
       self.amplitude = 1D / ( self.width * SQRT(!DPI) )

     END ;PRO GAUSSIAN::NORMALIZE


     ;+=================================================================
     ; Return the intensity
     ;-=================================================================
     FUNCTION GAUSSIAN::INTENSITY
       
       RETURN, self.amplitude * self.width * SQRT(!DPI)

     END ;FUNCTION GAUSSIAN::INTENSITY


     ;+=================================================================
     ; Return the amplitude
     ;-=================================================================
     FUNCTION GAUSSIAN::AMPLITUDE
       
       RETURN, self.amplitude

     END ;FUNCTION GAUSSIAN::AMPLITUDE


     ;+=================================================================
     ; Return the width
     ;-=================================================================
     FUNCTION GAUSSIAN::WIDTH
       
       RETURN, self.width

     END ;FUNCTION GAUSSIAN::WIDTH


     ;+=================================================================
     ; Return the location
     ;-=================================================================
     FUNCTION GAUSSIAN::LOCATION
       
       RETURN, self.location

     END ;FUNCTION GAUSSIAN::LOCATION


     ;+=================================================================
     ; Set the amplitude
     ;-=================================================================
     PRO GAUSSIAN::SET_AMPLITUDE, amplitude
       
       self.amplitude = amplitude

     END ;PRO GAUSSIAN::SET_AMPLITUDE


     ;+=================================================================
     ; Set the width
     ;-=================================================================
     PRO GAUSSIAN::SET_WIDTH, width
       
       self.width = ABS(width)

     END ;PRO GAUSSIAN::SET_WIDTH


     ;+=================================================================
     ; Set the location
     ;-=================================================================
     PRO GAUSSIAN::SET_LOCATION, location
       
       self.location = location

     END ;PRO GAUSSIAN::SET_LOCATION


     ;+=================================================================
     ; Reset the gaussian to zero
     ;-=================================================================
     PRO GAUSSIAN::RESET
       
       self.amplitude = 0D
       self.width = 0D
       self.location = 0D

     END ;PRO GAUSSIAN::RESET



     ;+=================================================================
     ; Allow a way to set the values.
     ; 
     ; The input should be a structure with the following tags:
     ;  amplitude
     ;  width
     ;  location
     ;-=================================================================
     PRO GAUSSIAN::SET, gauss_in

       STRUCT_ASSIGN, gauss_in, self, /NOZERO
       self.width = ABS(self.width)

     END ;PRO GAUSSIAN::SET



     ;+=================================================================
     ; PURPOSE:
     ;   Return a structure with the gaussian parameters.
     ;
     ; DESCRIPTION:
     ;   By inheriting <OBJECT::> I end up with an extra propertry:
     ;   '_NULL'.  I do not want this returned when calling this
     ;   method.
     ;
     ;-=================================================================
     FUNCTION GAUSSIAN::GET_STRUCT

       RETURN, {amplitude:self.amplitude $
                ,width:self.width $
                ,location:self.location $
               }

     END ;FUNCTION GAUSSIAN::GET_STRUCT



     ;+=================================================================
     ; Initialize the object.
     ; 
     ; Optionally a structure may be given to set the gaussian.
     ; This structure will be passed to GAUSSIAN::SET
     ;-=================================================================
     FUNCTION GAUSSIAN::INIT, gauss_in

       IF N_PARAMS() NE 0 THEN BEGIN
         self->SET, gauss_in
       ENDIF

       RETURN, 1

     END ;FUNCTION GAUSSIAN::INIT



     ;+=================================================================
     ; Define a gaussian object
     ;-=================================================================
     PRO GAUSSIAN__DEFINE
       
       struct = { GAUSSIAN $
                  ,amplitude:0D $
                  ,width:0D $
                  ,location: 0D $
                  ,INHERITS OBJECT $
                }
                 

     END ;PRO GAUSSIAN__DEFINE
