
;+==============================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2013-11
;
; PURPOSE:
;   Determine the sign of a variable.
;
; DESCRIPTION:
;   This will only work for real finite numbers. 
;
;   Note that this method cannot distinguish between -0.0 AND 0.0.   
;
;
;-==============================================================================
     FUNCTION MIR_SIGN, value
       
       IF FINITE(value, /NAN) THEN BEGIN
         MESSAGE, 'Sign cannot be determined for values of NaN.'
       ENDIF

       IF value GE 0 THEN BEGIN
         RETURN, 1
       ENDIF ELSE BEGIN
         RETURN, -1
       ENDELSE

     END ; MIR_SIGN
