

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
; DATE:
;   2009-10
;
; VERSION:
;   0.0.0
;
; PURPOSE:
;   Calculate the full width at percent maximum of the given spectrum.
;   This routine attemps to make quadratic fits to the peak
;   and sides of the given spectrum.
;
;
;-======================================================================



     FUNCTION FWHM_FIT, x, y $
                        ,LEVEL=level $
                        ,HALF_WIDTHS=half_widths $
                        ,LOCATIONS=locations $
                        ,FIT_WINDOW=fit_window $
                        ,PLOT=plot $
                        ,STOP=stop


       ; By default find the full width at half maximum
       MIR_DEFAULT, level, 0.5

       num_points = N_ELEMENTS(y)

       ; Find the maximum value.
       max = MAX(y, max_index, MIN=min)
       x_max = x[max_index]
       mid = max * level

       ; For now set min to 0 untill I find a better way to do this
       min = 0D

       ; Get an approximaiton for the mid value.
       where_above_mid = WHERE(y GT mid, num_above_mid)

       half_index_est =  [where_above_mid[0], LAST(where_above_mid)]
       fwhm_est = x[half_index_est[1]] - x[half_index_est[0]]

       MIR_DEFAULT, fit_window, (half_index_est[1] - half_index_est[0])/3

       fit_window = fit_window > 8


       ; Fit the peak.
       coeff = POLY_FIT(x[max_index-fit_window/2:max_index+fit_window/2] $
                        ,y[max_index-fit_window/2:max_index+fit_window/2] $
                        ,2 $
                        ,YFIT=yfit_peak)

       x_max = -1D * coeff[1]/ 2D/coeff[2]
       max = coeff[0] + coeff[1] * x_max + (coeff[2])^2 * x_max
       mid = (max - min)/2 + min

       ; Fit the sides
       index = half_index_est[0]
       coeff_left = POLY_FIT(x[index-fit_window/4:index+fit_window/4] $
                        ,y[index-fit_window/4:index+fit_window/4] $
                        ,2 $
                        ,YFIT=yfit_left)
       coeff_left[0] -= mid
       x_half_left = (QUADRATIC_ROOT(coeff_left))[0]

       index = half_index_est[1]
       coeff_right = POLY_FIT(x[index-fit_window/4:index+fit_window/4] $
                        ,y[index-fit_window/4:index+fit_window/4] $
                        ,2 $
                        ,YFIT=yfit_right)
       coeff_right[0] -= mid
       x_half_right = (QUADRATIC_ROOT(coeff_right))[1]



       locations = [x_half_left, x_half_right]
       half_widths = [x_max - x_half_left, x_half_right - x_max]
       full_width = ABS(x_half_right - x_half_left)


       IF KEYWORD_SET(plot) THEN BEGIN
         SUPERPLOT, x, y
         SUPERPLOT, x[max_index-fit_window/2:max_index+fit_window/2], yfit_peak, COLOR=230, /OPLOT
         SUPERPLOT, x[half_index_est[0]-fit_window/4:half_index_est[0]+fit_window/4] $
                    ,yfit_left, COLOR=230, /OPLOT
         SUPERPLOT, x[half_index_est[1]-fit_window/4:half_index_est[1]+fit_window/4] $
                    , yfit_right, COLOR=230, /OPLOT
       ENDIF

       IF KEYWORD_SET(stop) THEN stop

       RETURN, full_width


      END ; FUNCTION FWHM_FIT
