


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
; DATE:
;   2009-10
;
; VERSION:
;   0.0.0
;
; PURPOSE:
;   Find the dot product of two vectors.
;
; TAGS:
;   EXCLUDE_FILE_HEADER
;
;-======================================================================


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Find the dot product
     ;
     ;-=================================================================
     FUNCTION MIR_DOTP, input_1, input_2

       result = MATRIX_MULTIPLY(input_1, input_2, /ATRANSPOSE)
       RETURN, result[0]

     END ; FUNCTION MIR_DOTP
