

;+====================================================================== 
; 
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-06
;
; PURPOSE:
;   Return the results from an FFT in a way that I understand.
;
;
;-======================================================================


     ;+=================================================================
     ;  
     ; PURPOSE:
     ;   Return the results from an FFT of a real array in a way that 
     ;   I understand.
     ;
     ; DESCRIPTION:
     ;   I wrote this program to examine the results of a Discrete
     ;   Fourier Transform of a real, one dimentional array.
     ;
     ;   In particular for the fourier transform of a real array
     ;   The result for negitive frequencys is the complex conjugate
     ;   of the result for positive freqencies. 
     ;   F(v) = F(v)*
     ;
     ;   In this case the magintude and phase of the result have 
     ;   physical meaning, and the original array can be constructed
     ;   as a sum of sinusiods using this magnitude and phase.
     ;
     ;   This program will return a structure that contains the the
     ;   following arrays:
     ;    fft      : The raw results from the FFT
     ;    frequency: The frequency at each point.
     ;    power    : The power at each point.
     ;    phase    : The phase of each point.    
     ;   
     ;   
     ;   This program also has the ablitiy to handle data that is 
     ;   not evenly spaced by interpolating it onto an even grid.
     ;
     ;   Also provided is the ability to plot the power or phase 
     ;   spectra.
     ;
     ;   When plotting,only positive frequencies are shown and the
     ;   power is multiplied by a factor of two.
     ;
     ;   An option exits to plot vs. period instead of frequency. 
     ;
     ;
     ;-=================================================================
     FUNCTION FOURIER, x_in, y_in $
                       ,INTERPOLATE=interpolate $
                       ,PLOT_CHECK=plot_check $
                       ,PLOT_POWER=plot_power $
                       ,PLOT_PHASE=plot_phase $
                       ,PERIOD=period $
                       ,_EXTRA=extra


       ; First check the dimensions of the input.
       IF SIZE(y_in, /N_DIMENSIONS) NE 1 THEN BEGIN
         MESSAGE, 'y must be a one dimensional array.'
       ENDIF

       ; Get the data type
       type_y = SIZE(y_in, /TYPE)
       dim_y = SIZE(y_in, /DIM)


       ; Copy the input arrays.
       y = y_in
       x = x_in

       dim = dim_y[0]



       ; Figure out the spacing.
       CASE SIZE(x_in, /N_DIMENSION) OF
      
         1: BEGIN
           ; If x is an array check if it has constant spacing.  
           spacing_array = (x_in - SHIFT(x_in, 1))[1:N_ELEMENTS(x_in)-1]
           spacing = MEAN(spacing_array)

           IF ~ ARRAY_EQUAL(spacing_array, spacing) THEN BEGIN

             IF N_ELEMENTS(interpolate) NE 0 THEN BEGIN
               PRINT, 'Interpolating to evenly space data.'
               IF interpolate NE 1 THEN BEGIN
                 dim = interpolate
               ENDIF

               ; Create the final x array
               x_max = MAX(x, MIN=x_min)
               x = INDGEN(dim, TYPE=type_y)/dim*(x_max-x_min+1) + x_min
               Y = INTERPOL(y_in, x_in, x)

               spacing = (x_max-x_min + 1) / dim

             ENDIF ELSE BEGIN
               MESSAGE, STRING( 'Spacing of x is not constant.' $
                                ,'Average spacing:', spacing $
                                ,'Max deviation:' $
                                ,MAX(ABS(spacing_array - spacing)) $
                                ,FORMAT='(a, 1x, a, 1x, f0, 1x, a, 1x, f0)' $
                              ), /INFORMATIONAL
             ENDELSE
           ENDIF
         END
         ELSE: MESSAGE, 'x must be a one dimensional array.'

       ENDCASE


       index_mid = dim/2

       is_odd = dim MOD 2
       num_negitive = index_mid-1+is_odd

       ; Create an array with the frequencies.

       ; Create the positive indexes
       frequency = INDGEN(dim, TYPE=type_y)
       
       ; Add the negitive indexes
       frequency[index_mid + 1] = index_mid + 1 - dim + INDGEN(num_negitive, TYPE=type_y)

       ; Convert to a frequency
       frequency = frequency/(dim * spacing)
     
       ; Do the FFT
       fft_result = FFT(y, _EXTRA=extra)


       ; Shift so that the most negitive frequency is plotted first
       frequency = SHIFT(frequency, num_negitive)
       fft_result = SHIFT(fft_result, num_negitive)

       ; find the power
       power = ABS(fft_result)

       ; Find the phase
       phase = ATAN(fft_result, /PHASE)


       result = { $
                  fft:fft_result $
                  ,frequency:frequency $
                  ,power:power $
                  ,phase:phase $
                }


       ; Find the positive frequencies
       where_positive = WHERE(frequency GE 0)

       IF KEYWORD_SET(plot_check) THEN BEGIN
         ; Plot Reconstruction
         y_fft = MAKE_ARRAY(dim, TYPE=type_y)
         FOR ii = 0, dim-1 DO BEGIN
           y_fft += result.power[ii]*COS((x-x_min)*2D*!dpi*result.frequency[ii] + result.phase[ii])
         ENDFOR

         PLOT, x_in, y_in $
           ,PSYM=-1 $
           ,_EXTRA=extra
         OPLOT, x, y $
           ,PSYM=-1 $
           ,COLOR=50, _EXTRA=extra
         OPLOT, x, y_fft $
           ,PSYM=-1 $
           ,COLOR=230 $
           ,_EXTRA=extra
       ENDIF

       ; Choose weather to plot vs. frequency or period.
       IF KEYWORD_SET(period) THEN BEGIN
         index = 1D/result.frequency[where_positive]
         xtitle = 'Period'
       ENDIF ELSE BEGIN
         index = result.frequency[where_positive]
         xtitle = 'Frequency'
       ENDELSE


       IF KEYWORD_SET(plot_power) THEN BEGIN
         SUPERPLOT, index, 2*result.power[where_positive] $
           ,SYMBOL_COLOR=230 $
           ,XTITLE=xtitle $
           ,YTITLE='Power' $
           ,_EXTRA=extra
       ENDIF


       IF KEYWORD_SET(plot_phase) THEN BEGIN
         SUPERPLOT, index, 2*result.phase[where_positive] $
           ,SYMBOL_COLOR=230 $
           ,XTITLE=xtitle $
           ,YTITLE='Phase' $
           ,_EXTRA=extra
       ENDIF


       RETURN, result
       


     END ;FUNCTION FOURIER
