

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;   novimir.pablant@amicitas.com
;
; DATE:
;   2010-03
;
; VERSION:
;   1.0.0
;
; PURPOSE:
;   Find the intersection of two lines as defined by a point and
;   a direction vector.
;
;-======================================================================


    FUNCTION VECTOR_INTERSECT_2D, point_1, vector_1, point_2, vector_2

      A = [[vector_1[0:1]],[vector_2[0:1]]]
      LUDC, A, index, /COLUMN, /DOUBLE
      t = LUSOL(A, index, point_2[0:1] - point_1[0:1], /COLUMN, /DOUBLE)

      result = point_1 + t[0] * vector_1

      RETURN, result

    END ; FUNCTION VECTOR_INTERSECT_2D
