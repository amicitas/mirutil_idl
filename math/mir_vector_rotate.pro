
;+==============================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
; DATE:
;   2010-09
;
; PURPOSE:
;   Rotate a vector around an arbitrary axis.
;
;-==============================================================================



     FUNCTION MIR_VECTOR_ROTATE, vector_in, axis_in, angle, INVERSE=inverse

       IF angle EQ 0.0 THEN BEGIN
         RETURN, vector_in
       ENDIF
       
       axis = MIR_NORMALIZE(axis_in)

       q0 = COS(angle/2D)
       q1 = SIN(angle/2D) * axis[0]
       q2 = SIN(angle/2D) * axis[1]
       q3 = SIN(angle/2D) * axis[2]

       Q = [ [q0^2 + q1^2 - q2^2 - q3^2,  2D*(q2*q1 + q0*q3),         2D*(q3*q1 - q0*q2)] $
            ,[2D*(q1*q2 - q0*q3),         q0^2 - q1^2 + q2^2 - q3^2,  2D*(q3*q2 + q0*q1)] $
            ,[2D*(q1*q3 + q0*q2),         2D*(q2*q3 - q0*q1),         q0^2 - q1^2 - q2^2 + q3^2] ]

       IF KEYWORD_SET(inverse) THEN BEGIN
         Q = TRANSPOSE(TEMPORARY(Q))
       ENDIF

       RETURN, Q # vector_in

     END ; FUNCTION MIR_VECTOR_ROTATE
