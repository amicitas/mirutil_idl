

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   npablant@pppl.gov
;   novimir.pablant@amicitas.com
;
; DATE:
;   2010-03
;
; VERSION:
;   1.0.0
;
; PURPOSE:
;   Find the closest approtch of a line to give point in 3D.
;
;-======================================================================


    FUNCTION MIR_LINE_CLOSEST_TO_POINT, line_1, line_2, point

      t = -1 * MIR_DOTP(line_1-point, line_2-line_1)/TOTAL((line_2 - line_1)^2)

      result = line_1 + (line_2 - line_1)*t

      RETURN, result

    END ; FUNCTION MIR_LINE_CLOSEST_TO_POINT
