

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;   novimir.pablant@amicitas.com
;
; DATE:
;   2010-03
;
; VERSION:
;   1.0.0
;
; PURPOSE:
;   Find the point at which a line defined by a point and a direction
;   has a given radius
;
;-======================================================================


    FUNCTION VECTOR_LINE_RADIUS_2D, point, vector, radius, DEBUG=debug, DOUBLE=double

      roots = QUADRATIC_ROOT( [ MIR_MAGNITUDE(point[0:1])^2 - radius^2 $
                                ,2D*(point[0]*vector[0] + point[1]*vector[1]) $
                                ,MIR_MAGNITUDE(vector[0:1])^2] $
                              ,/REAL $
                              ,DOUBLE=double $
                              ,DEBUG=debug)

      IF N_ELEMENTS(roots) EQ 2 THEN BEGIN
        result = DBLARR(N_ELEMENTS(vector), 2)
      ENDIF ELSE BEGIN
        result = DBLARR(N_ELEMENTS(vector))
      ENDELSE

      FOR ii=0,N_ELEMENTS(roots)-1 DO BEGIN
        result[*,ii] = point + roots[ii]*vector
      ENDFOR


      RETURN, result

    END ; FUNCTION VECTOR_LINE_RADIUS_2D
