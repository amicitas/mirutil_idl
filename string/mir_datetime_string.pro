
;+==============================================================================
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
;
; PURPOSE:
;   Return a standard datetime string.
;
;-=============================================================================

     ;+=========================================================================
     ; 
     ;
     ;-=========================================================================
     FUNCTION MIR_DATETIME_STRING, time_in $
                                   ,SECONDS=seconds $
                                   ,DATE=date_key $
                                   ,TIME=time_key

       date_key = KEYWORD_SET(date_key)
       time_key = KEYWORD_SET(time_key)
       IF ~ date_key AND ~ time_key THEN BEGIN
         date_key = 1
         time_key = 1
       ENDIF


       IF ~ ISA(time_in) THEN BEGIN
         time = SYSTIME(/JULIAN)
       ENDIF ELSE BEGIN
         time = time_in
         IF KEYWORD_SET(seconds) THEN BEGIN
           time = SYSTIME(time, /JULIAN)
         ENDIF
       ENDELSE
       


       string = ''
       IF date_key THEN BEGIN
         string +=  STRING(time, FORMAT='(C(CYI4,"-",CMOI2.2, "-", CDI2.2))')
       ENDIF

       IF time_key THEN BEGIN
         IF date_key THEN BEGIN
           string += ' '
         ENDIF

         string += STRING(time, FORMAT='(C(CHI2.2,":",CMI2.2,":",CSI2.2))')
       ENDIF

       RETURN, string
       
     END ; FUNCTION MIR_DATETIME_STRING
