
;+==============================================================================
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
;
; PURPOSE:
;   Return the correct format code for a given string.
;
;-=============================================================================

     ;+=========================================================================
     ; 
     ;
     ;-=========================================================================
     FUNCTION MIR_GET_FORMAT_CODE, type $
                                   ,F=f $
                                   ,E=e $
                                   ,G=g $
                                   ,I=i $
                                   ,O=o $
                                   ,Z=z
       
       CASE 1 OF
         KEYWORD_SET(f): float_code = 'f'
         KEYWORD_SET(e): float_code = 'e'
         KEYWORD_SET(g): float_code = 'g'
         ELSE: float_code = 'g'
       ENDCASE
       
       CASE 1 OF
         KEYWORD_SET(i): int_code = 'i'
         KEYWORD_SET(o): int_code = 'o'
         KEYWORD_SET(z): int_code = 'z'
         ELSE: int_code = 'i'
       ENDCASE

       CASE type OF
         2:  RETURN, int_code ; INT
         3:  RETURN, int_code ; LONG
         4:  RETURN, float_code ; FLOAT
         5:  RETURN, float_code ; DOUBLE
         7:  RETURN, 'a' ; STRING
         1:  RETURN, 'b' ; BYTE
         12: RETURN, int_code ; UINT
         13: RETURN, int_code ; ULONG
         14: RETURN, int_code ; LONG64
         15: RETURN, int_code ; ULING64


         0:  MESSAGE, 'Format undefined for type 0, UNDEFINED.'
         6:  MESSAGE, 'Format undefined for type 6, COMPLEX.'
         8:  MESSAGE, 'Format undefined for type 8, STRUCTURE.'
         9:  MESSAGE, 'Format undefined for type 9, DCOMPLEX.'
         10: MESSAGE, 'Format undefined for type 10, POINTER.'
         11: MESSAGE, 'Format undefined for type 11, OBJECT.'

         ELSE: MESSAGE, 'Format code not recognized.'
       ENDCASE
     END ; FUNCTION MIR_GET_FORMAT_CODE, type
