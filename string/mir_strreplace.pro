





;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-07
;
; PURPOSE:
;   Replaces a substring in a given string.
;
; TAGS:
;   EXCLUDE_FILE_HEADER
;
;-======================================================================


     ;+=================================================================
     ;
     ; Searches for a regular expression in a string, then replaces
     ; all occerances with a string.
     ;
     ;-=================================================================
     FUNCTION MIR_STRREPLACE, input_string, search_string, replace_string $
                              ,FOLD_CASE=fold_case $
                              ,REGEX=regex

       MIR_DEFAULT, regex, 1
       output_string = input_string

       FOR ii=0, N_ELEMENTS(output_string)-1 DO BEGIN
         output_string[ii] = STRJOIN(STRSPLIT(output_string[ii], search_string $
                                              ,REGEX=regex $
                                              ,FOLD_CASE=fold_case $
                                              ,/EXTRACT $
                                              ,/PRESERVE_NULL) $
                                     ,replace_string)
       ENDFOR


       RETURN, output_string

     END ; FUNCTION MIR_STRREPLACE
