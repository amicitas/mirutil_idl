
;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-07
;
; PURPOSE:
;   Converts standard escape characters to IDL equivalents.
;
;-======================================================================


     ;+=================================================================
     ;
     ; Searches for a regular expression in a string, then replaces
     ; all occerances with a string.
     ;
     ;
     ; To Do:
     ;   I should figure out how to make this work without REGEX
     ;   (For speed reasons).
     ;
     ;-=================================================================
     FUNCTION MIR_STRFROMESCAPE, input_string
  
       output_string = MIR_STRREPLACE(input_string, '\\n', STRING(10B), REGEX=1)

       RETURN, output_string

     END ; FUNCTION MIR_STRFROMESCAPE


     
