

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
; DATE:
;   2009-05-04
;
; PURPOSE:
;   Convert a string to title case.
;
; TAGS:
;   NO_INDEX, NO_CREATE_PAGE
;   
;-======================================================================

     ;+=================================================================
     ; PURPOSE:
     ;   Convert a string to title case.
     ;
     ; TAGS:
     ;   INDEX, CREATE_PAGE
     ;
     ;-=================================================================
     FUNCTION MIR_STRTITLECASE, string_in
       
       ; First lowcase
       string = STRLOWCASE(string_in)

       space_loc = STRSPLIT(string, ' ', COUNT=num_space)

       FOR ii=0,num_space-1 DO BEGIN
         STRPUT, string, STRUPCASE(STRMID(string, space_loc[ii], 1)), space_loc[ii]
       ENDFOR

       RETURN, string
     END ;FUNCTION MIR_STRTITLECASE
