

;+==============================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
; DATE:
;   2015-03-07
;
; PURPOSE:
;   Split a string but intelegently handle quoted text.
;
; SYNTAX:
;   Result = STRSPLIT( String [,Pattern] [,COUNT=variable] [,ESCAPE=string]
;            [,/REGEX] [,/FOLD_CASE]] [,/EXTRACT] [,LENGTH=variable]
;            [,/PRESERVE_NULL] [,/QUOTATIONS])
;
; KEYWORDS:
;   See the builtin STRSPLIT for details on standard keywords.
;
;   /QUOTATIONS:
;     Do not split any text surounted by quotation marks.
;
; DESCRIPTION:
;   This function works similarly to STRSPLIT however it has the option to
;   preserve any text that is surrounded by quotes as a single substring.
;
;   This will only work for well formated.
;
;   
;-==============================================================================

     FUNCTION MIR_STRSPLIT, string_in $
                            ,pattern $
                            ,QUOTATIONS=quotations $
                            ,EXTRACT=extract $
                            ,COUNT=count $
                            ,HELP=help $
                            , _REF_EXTRA=_extra

       IF KEYWORD_SET(help) THEN BEGIN & INFO & RETURN, !NULL & ENDIF
       
       IF ~KEYWORD_SET(quotations) THEN BEGIN
         ; Standard splitting.
         substrings = STRSPLIT(string_in, pattern, EXTRACT=extract, COUNT=count, _STRICT_EXTRA=_extra)

       ENDIF ELSE BEGIN
         ; Now deal with splitting while preserving quoted text.
         IF ~ISA(pattern) THEN BEGIN
           pattern = ' '
         ENDIF ELSE BEGIN
           ; I can't see a use case to make this all work with multiple splitting
           ; strings.  Doing so would make everything slower.
           IF STRLEN(pattern) GT 1 THEN BEGIN
             MESSAGE, 'Cannot use /QUOTATIONS with multiple splitting characters.'
           ENDIF
         ENDELSE
         
         substrings = STRSPLIT(string_in, /EXTRACT, _STRICT_EXTRA=_extra)

         ; We will save the new split strings here.
         substr_list = LIST()

         ; We only need to worry about one type of quote at a time here.
         in_quoted = 0
         index_quoted_start = -1
         FOR ii = 0,N_ELEMENTS(substrings)-1 DO BEGIN
           substr = substrings[ii]
           IF in_quoted THEN BEGIN
             substr_list[-1] += (pattern+substr)
             
             IF STRMID(substr, 0,1, /REVERSE_OFFSET) EQ qtype THEN BEGIN
               in_quoted = 0
               index_quoted_start = -1
             ENDIF
           ENDIF ELSE BEGIN
             substr_list.ADD, substr

             IF STRCMP(substr, "'", 1) OR STRCMP(substr, '"', 1) THEN BEGIN
               qtype = STRMID(substr, 0,1)
               ; Check if this substr is quoted by itself.
               IF ~(STRMID(substr, 0,1, /REVERSE_OFFSET) EQ qtype) THEN BEGIN
                 in_quoted = 1
                 index_quoted_start = ii
               ENDIF
             ENDIF
           ENDELSE
         ENDFOR

         IF in_quoted EQ 1 THEN BEGIN
           ; If we get here it means that we found an open quotation, but not a
           ; close quotation.  For now just quietly ignore the quotations
           ; altogether.
           _temp = substr_list.REMOVE(-1)
           substr_list.ADD, substrings[index_quoted_start:*], /EXTRACT
         ENDIF
         
         IF KEYWORD_SET(extract) THEN BEGIN
           substrings = substr_list.TOARRAY()
         ENDIF ELSE BEGIN
           substrings = LONARR(N_ELEMENTS(substr_list))
           count = STRLEN(substr_list[0])+1
           FOR ii=1,N_ELEMENTS(substr_list)-1 DO BEGIN
             substrings[ii] = count
             count += STRLEN(substr_list[ii])+1
           ENDFOR
         ENDELSE
       ENDELSE


       IF ARG_PRESENT(count) THEN BEGIN
         count = N_ELEMENTS(substrings)
       ENDIF
         
       RETURN, substrings

     END ;FUNCTION MIR_STRSPLIT
