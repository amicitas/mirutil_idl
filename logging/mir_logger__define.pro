



;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
; DATE:
;   2009-08
;
; VERSION:
;   0.0.0
;
; PURPOSE:
;   An object to handle logging to a file and to the terminal.
;
;-======================================================================



    
     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Initialize the LOG object
     ;
     ;-=================================================================
     FUNCTION MIR_LOGGER::INIT, filename, MAX_LENGTH=max_length
       
       status = self.MIR_HASH_OBJECT::INIT()
       
       ; Set the default flags.
       self['flags'] = MIR_HASH()
       (self['flags'])['debug'] = !NULL
       (self['flags'])['info'] = !NULL
       (self['flags'])['error'] = !NULL
       (self['flags'])['warning'] = !NULL
       (self['flags'])['critical'] = !NULL
       (self['flags'])['testing'] = !NULL

       self['options'] = MIR_HASH()
       (self['options'])['print_to_terminal'] = 1
       (self['options'])['print_to_file'] = 0
       
       
       self['filename'] = ''
       self['lun'] = !NULL
       self['open'] = 0
       
       MIR_DEFAULT, max_length, 5000
       self['max_length'] = max_length

       RETURN, status
     END ; FUNCTION MIR_LOGGER::INIT


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Set various flags.
     ;
     ;-=================================================================
     PRO MIR_LOGGER::SET_FLAGS, input
  
       IF ISA(input) THEN BEGIN
         self['flags'].JOIN, input
       ENDIF

     END ;PRO MIR_LOGGER::SET_FLAGS

    
     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Open the log file.
     ; 
     ;   Truncate the length if necessary.
     ;
     ;-=================================================================
     PRO MIR_LOGGER::OPEN, filename

       self['filename'] = filename

       IF self->IS_OPEN() THEN BEGIN
         MESSAGE, 'Log file is already open.'
       ENDIF


       IF FILE_TEST(self['filename']) THEN BEGIN

         ; If the log file already exists then truncate if necessary.
         self->TRUNCATE

         OPENU, lun, self['filename'], /GET_LUN, /APPEND
       ENDIF ELSE BEGIN
         OPENW, lun, self['filename'], /GET_LUN
       ENDELSE

       self['lun'] = lun
       self['open'] = 1

       ; Print some white space
       PRINTF, self['lun'], ''
       PRINTF, self['lun'], ''

       ; Print a header
       PRINTF, self['lun'], 'Log file opened.'
       PRINTF, self['lun'], self->GET_TIME_STRING()

     END ;PRO MIR_LOGGER::OPEN_LOG_FILE


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Close the logfile.
     ;
     ;-=================================================================
     PRO MIR_LOGGER::CLOSE

       FREE_LUN, self['lun']
       self['open'] = 0

     END ;PRO MIR_LOGGER::CLOSE


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Save the logfile.
     ;
     ;-=================================================================
     PRO MIR_LOGGER::SAVE

       IF ~ self->IS_OPEN() THEN BEGIN
         MESSAGE, 'Log file is not open.'
       ENDIF

       CLOSE, self['lun']
       OPENU, self['lun'], self['filename'], /APPEND


     END ;PRO MIR_LOGGER::SAVE

    
     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Truncate the length of the logfile.
     ;
     ; DESCRIPTION:
     ;
     ; I want to limit the size of the log file. I will always
     ; keep the entire log from the current session, but I want
     ; to truncate the log from the previous sessions.
     ;
     ;-=================================================================
     PRO MIR_LOGGER::TRUNCATE

       ; First count the number of lines.
       num_lines = FILE_LINES(self['filename'])

       ; If the file does note exceed the max length then continue.
       IF num_lines LE self['max_length'] THEN RETURN


       ; We need to truncate the file.
       buffer = STRARR(self['max_length'])

       OPENR, lun, self['filename'], /GET_LUN

       num_to_truncate = num_lines - self['max_length']

       line = ''
       count = 0
       buffer_count = 0
       WHILE ~ EOF(lun) DO BEGIN
         READF, lun, line
         count += 1
         IF count GT num_to_truncate THEN BEGIN
           buffer[buffer_count] = line
           buffer_count += 1
         ENDIF
       ENDWHILE

       ; Done reading
       CLOSE, lun
           
       ; Now write the truncated file.
       OPENW, lun, self['filename']
       FOR ii=0,self['max_length']-1 DO BEGIN
         PRINTF, lun, buffer[ii]
       ENDFOR

       FREE_LUN, lun

     END ; PRO MIR_LOGGER::TRUNCATE


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Check if the log file is currently open.
     ;
     ;   This will check both that the object thinks the file is open
     ;   and the the file is actually open.
     ;
     ;-=================================================================
     FUNCTION MIR_LOGGER::IS_OPEN
       is_open = 0

       IF self['open'] THEN BEGIN
         file_info = FSTAT(self['lun'])
         is_open = file_info.open
       ENDIF

       RETURN, is_open

     END ; MIR_LOGGER::IS_OPEN

     
     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return a string with the current date and time.
     ;
     ;-=================================================================
     FUNCTION MIR_LOGGER::GET_TIME_STRING, TIMESTAMP=timestamp, DATESTAMP=datestamp

       ; Get the current time.
       time_string = ''
       IF KEYWORD_SET(datestamp) THEN BEGIN
         time_string += STRING(SYSTIME(/JULIAN) $
                               ,FORMAT='(C(CYI4,"-",CMOI2.2, "-", CDI2.2)')
         IF KEYWORD_SET(timestamp) THEN time_string += ' '
       ENDIF
       IF KEYWORD_SET(timestamp) THEN BEGIN
         time_string += STRING(SYSTIME(/JULIAN) $
                              ,FORMAT='(C(CHI2.2,":",CMI2.2,":",CSI2.2))')
       ENDIF
       
       RETURN, time_string

     END ; MIR_LOGGER::GET_TIME_STRING

     
     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Log a message, or an array of messages to the logfile
     ;   and/or terminal.
     ;
     ;   This is the procedure that handles the actuall output.
     ;
     ;
     ;-=================================================================
     PRO MIR_LOGGER::PRINT_TO_TERMINAL, message
                   
       FOR ii=0,N_ELEMENTS(message)-1 DO BEGIN
         PRINT, message[ii]
       ENDFOR

     END ;PRO MIR_LOGGER::PRINT_TO_TERMINAL

     
     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Log a message, or an array of messages to the logfile
     ;   and/or terminal.
     ;
     ;   This is the procedure that handles the actuall output.
     ;
     ;
     ;-=================================================================
     PRO MIR_LOGGER::PRINT_TO_FILE, message
                   
       FOR ii=0,N_ELEMENTS(message)-1 DO BEGIN
         PRINT, message[ii]
       ENDFOR

     END ;PRO MIR_LOGGER::PRINT_TO_FILE

  
     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Log a message, or an array of messages to the logfile
     ;   and/or terminal.
     ;
     ;   This is the procedure that handles the actuall output.
     ;
     ;
     ;-=================================================================
     PRO MIR_LOGGER::DISPATCH, message
                   
       ; Print to the terminal.
       IF (self['options'])['print_to_terminal'] THEN BEGIN
         self.PRINT_TO_TERMINAL, message
       ENDIF
         
       ; Print to file.
       IF (self['options'])['print_to_file'] THEN BEGIN
         PRINTF, self['lun'], log_entry
       ENDIF

     END ;PRO MIR_LOGGER::DISPATCH

     
     ;+=================================================================
     ;
     ; PURPOSE:
     ;
     ;
     ;-=================================================================
     FUNCTION MIR_LOGGER::FORMAT_MESSAGE, message_in $
                                          ,level_string $
                                          ,TRACEBACK=key_traceback $
                                          ,LAST_MESSAGE=key_last_message $
                                          ,SCOPE_LEVEL=scope_level_in
                                          
  
       COMPILE_OPT STRICTARR
       COMMON MIR_LOGGER, c_options

       option_timestamp = c_options.timestamp
       option_datestamp = c_options.datestamp
       option_caller = c_options.caller

       ; It wolud be good to have some logic to deal with structures
       ; and hashes as well.
       IF ISA(message_in, /NUMBER) THEN BEGIN
         formatted = STRING(message_in, /IMPLIED_PRINT)
       ENDIF ELSE IF N_ELEMENTS(message_in) EQ 1 THEN BEGIN
         formatted = [message_in[0]]
       ENDIF ELSE BEGIN
         formatted = message_in
       ENDELSE

       add_prefix = 1
       
       IF KEYWORD_SET(key_last_message) THEN BEGIN
         HELP, OUTPUT=last_message, /LAST_MESSAGE
         last_message = MIR_STRREPLACE(STRJOIN(last_message), 'Execution halted at:', '          TRACEBACK:')
         last_message = '%' + STRSPLIT(last_message, '%', /EXTRACT)
         formatted = [formatted, last_message]
       ENDIF
       
       IF option_timestamp OR option_datestamp THEN BEGIN
         timestamp = self->GET_TIME_STRING(TIMESTAMP=option_timestamp, DATESTAMP=option_datestamp)
       ENDIF

       ; Update the scope.
       MIR_DEFAULT, scope_level_in, 0
       scope_level = scope_level_in + 1

       IF KEYWORD_SET(key_traceback) AND ~ KEYWORD_SET(key_last_message) THEN BEGIN
         HELP, /TRACEBACK, OUTPUT=traceback
         formatted = [formatted, '% TRACEBACK:', traceback[scope_level:*]]
       ENDIF
       
       IF option_caller THEN BEGIN
         scope = SCOPE_TRACEBACK(/STRUCTURE)
         scope_len = N_ELEMENTS(scope)
         caller = (scope[scope_len - scope_level - 1]).routine
       ENDIF

       
       IF add_prefix THEN BEGIN
         ; Create the prefix.
         prefix = '['
         IF ISA(timestamp) THEN BEGIN
           prefix += timestamp+' '
         ENDIF

         IF option_caller THEN BEGIN
           spacer = ':'
         ENDIF ELSE BEGIN
           spacer = ''
         ENDELSE
         
         prefix += STRING(STRUPCASE(level_string+spacer), FORMAT='(a-8)')
         
         IF option_caller THEN BEGIN
           prefix += STRING(caller, FORMAT='(a-30)')
         ENDIF
         prefix += '] '

         ; Add the prefix to each entry.
         FOR ii=0,N_ELEMENTS(formatted)-1 DO BEGIN
           formatted[ii] = prefix+formatted[ii]
         ENDFOR
       ENDIF

       RETURN, formatted

       
     END ; FUNCTION MIR_LOGGER::FORMAT_MESSAGE

     
     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Log a message, or an array of messages to the logfile.
     ;
     ; KEYWORDS:
     ;
     ;   TERMINAL = 0
     ;       Do not output to terminal.
     ;
     ;   FILE = 0
     ;       Do not output to the log file.
     ;
     ;   /NOLOG
     ;       No output to terminal or file.
     ;
     ;   /LOG
     ;       Output to terminal or file regardless of flags.
     ;
     ;       This is a way to force output while retaining formatting
     ;       appropriate to the given message type.
     ;
     ;   /DEBUG
     ;       Mark message as a debugging message. Will only
     ;       output if appropriate flags have been set.
     ;
     ;   /ERROR
     ;       Mark message as an error message. Will only
     ;       output if appropriate flags have been set. Additionally
     ;       traceback output will be output if flags have been set.
     ;
     ;   /SEVERE_ERROR
     ;       If this keyword is set, then message and trace back
     ;       will always be output regardless of flags.
     ;
     ;-=================================================================
     PRO MIR_LOGGER::LOG, message_in $
                          ,DEBUG=key_debug $
                          ,INFO=key_info $
                          ,WARNING=key_warning $
                          ,ERROR=key_error $
                          ,CRITICAL=key_critical $
                          ,TESTING=key_testing $

                          ,SET_LEVEL=set_level $
                          ,SET_DEBUG=set_debug $
                          ,SET_INFO=set_info $
                          ,SET_WARNING=set_warning $
                          ,SET_CRITICAL=set_critical $

                          ,LAST_MESSAGE=key_last_message $
                          ,TRACEBACK=key_traceback $

                          ,SAVE=key_save $
                          ,SET_OPTIONS=set_options $

                          ,SCOPE_LEVEL=scope_level_in $
                          ,HELP=help

       COMPILE_OPT STRICTARR
       COMMON MIR_LOGGER, c_options
       
       IF ISA((self['flags'])['debug']) THEN BEGIN
         debug = (self['flags'])['debug']
       ENDIF ELSE BEGIN
         debug = c_options.debug
       ENDELSE
       
       IF ISA((self['flags'])['info']) THEN BEGIN
         info = (self['flags'])['info']
       ENDIF ELSE BEGIN
         info = c_options.info
       ENDELSE
       
       IF ISA((self['flags'])['warning']) THEN BEGIN
         warning = (self['flags'])['warning']
       ENDIF ELSE BEGIN
         warning = c_options.warning
       ENDELSE      
       
       IF ISA((self['flags'])['error']) THEN BEGIN
         error = (self['flags'])['error']
       ENDIF ELSE BEGIN
         error = c_options.error
       ENDELSE
              
       IF ISA((self['flags'])['critical']) THEN BEGIN
         critical = (self['flags'])['critical']
       ENDIF ELSE BEGIN
         critical = c_options.critical
       ENDELSE
       
       IF ISA((self['flags'])['testing']) THEN BEGIN
         testing = (self['flags'])['testing']
       ENDIF ELSE BEGIN
         testing = c_options.testing
       ENDELSE

              
       ; If this was a setup call then return immediately.
       IF ISA(set_level) OR ISA(set_options) THEN RETURN

       ; If this was a save command the return immediately.
       ; This is temporary untill I implement saving of log files.
       if ISA(key_save) THEN RETURN

       ; Sed defaults for the scopeing.
       MIR_DEFAULT, scope_level_in, 0
       scope_level = scope_level_in + 1
       
       ; Set defaults for the traceback.
       IF KEYWORD_SET(key_error) THEN BEGIN
         MIR_DEFAULT, key_last_message, 1
       ENDIF

       ; If no message was given, then this call was just to set the logger up.
       IF ~ ISA(message_in) $
         AND ~ KEYWORD_SET(key_traceback) $
         AND ~ KEYWORD_SET(key_last_message) $
         AND ~ KEYWORD_SET(key_save) THEN BEGIN
         
         MESSAGE, 'Variable is undefined: MESSAGE.'
       ENDIF 


       ; Check the level of this message.
       CASE 1 OF
         KEYWORD_SET(key_testing): BEGIN
           IF ~ testing THEN RETURN
           level_string = 'testing'
         END
         KEYWORD_SET(key_critical): BEGIN
           IF ~ critical THEN RETURN
           level_string = 'critical'
         END
         KEYWORD_SET(key_error): BEGIN
           IF ~ error THEN RETURN
           level_string = 'error'
         END
         KEYWORD_SET(key_warning): BEGIN
           IF ~ warning THEN RETURN
           level_string = 'warning'
         END
         KEYWORD_SET(key_info): BEGIN
           IF ~ info THEN RETURN
           level_string = 'info'
         END
         KEYWORD_SET(key_debug): BEGIN
           IF ~ debug THEN RETURN
           level_string = 'debug'
         END
         ELSE: BEGIN
           info = 1
           IF ~ info THEN RETURN
           level_string = 'info'
         ENDELSE
       ENDCASE

       formatted = self.FORMAT_MESSAGE(message_in $
                                       ,level_string $
                                       ,TRACEBACK=key_traceback $
                                       ,LAST_MESSAGE=key_last_message $
                                       ,SCOPE_LEVEL=scope_level)
       self.DISPATCH, formatted

     END ;PRO MIR_LOGGER::LOG




     ;+=================================================================
     ;
     ; PURPOSE:
     ;   An object to handle logging to a file and to the terminal.
     ;
     ;-=================================================================
     PRO MIR_LOGGER__DEFINE
       
       struct = {MIR_LOGGER $
                 ,INHERITS MIR_HASH_OBJECT $
                }
     END ; PRO MIR_LOGGER__DEFINE
