


 
     ;+=================================================================
     ; PURPOSE:
     ;  A routine to allow passing of mesages to a procedure given
     ;  at runtime by the user.
     ; 
     ;-=================================================================
     PRO MESSAGE_SWITCH, message_array $
                         ,MESSAGE_PROCEDURE=message_pro $
                         ,MESSAGE_OBJECT=object_ref $
                         ,_REF_EXTRA=extra

       IF KEYWORD_SET(message_pro) THEN BEGIN

         IF SIZE(message_pro, /TYPE) EQ 7 THEN BEGIN
           IF ISA(object_ref) THEN BEGIN
             CALL_METHOD, message_pro, object_ref, message_array, _STRICT_EXTRA=extra
           ENDIF ELSE BEGIN
             CALL_PROCEDURE, message_pro, message_array, _STRICT_EXTRA=extra
           ENDELSE
           RETURN
         ENDIF

       ENDIF

       ; By default just display the message.
       FOR ii=0,N_ELEMENTS(message_array)-1 DO BEGIN
         PRINT, message_array[ii]
       ENDFOR

     END; PRO MESSAGE_SWITCH
