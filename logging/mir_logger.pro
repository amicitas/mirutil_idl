

     


;+==============================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
; DATE:
;   2011-07
;
; PURPOSE:
;   Provide a global way to control debugging output.
;
; DESCRIPTION:
;   This routine provides a way to globally control debugging output. This is
;   especially usefull when writting small scripts and stand along programs.
;   This should generally be avoided when writing complex applications as
;   no fine grained control is possible.
;
; KEYWORDS:
;   /DEBUG
;   /INFO
;   /WARNING
;   /ERROR
;   /CRITICAL
;   /TESTING
;
;   /SET_LEVEL
;   /SET_DEBUG
;   /SET_INFO
;   /SET_WARNING
;   /SET_CRITICAL
;
;   /LAST_MESSAGE
;   /TRACEBACK
;                     
;   /SAVE
;   SET_OPTIONS = struct
;                     
;   SCOPE_LEVEL = int
;     Set the scope from which the logging function is being called.
;     This can be used when a wrapper of MIR_LOGGER is used so that the
;     calling routine is still named correctly.
;
;   HELP=help
;-==============================================================================




     PRO MIR_LOGGER, message_in $

                     ,DEBUG=key_debug $
                     ,INFO=key_info $
                     ,WARNING=key_warning $
                     ,ERROR=key_error $
                     ,CRITICAL=key_critical $
                     ,TESTING=key_testing $

                     ,SET_LEVEL=set_level $
                     ,SET_DEBUG=set_debug $
                     ,SET_INFO=set_info $
                     ,SET_WARNING=set_warning $
                     ,SET_CRITICAL=set_critical $

                     ,LAST_MESSAGE=key_last_message $
                     ,TRACEBACK=key_traceback $
                     
                     ,SAVE=key_save $
                     ,SET_OPTIONS=set_options $
                     
                     ,SCOPE_LEVEL=scope_level_in $
                     
                     ,HELP=help

       
       COMPILE_OPT STRICTARR
       COMMON MIR_LOGGER, c_options

       IF KEYWORD_SET(help) THEN BEGIN
         INFO
         RETURN
       ENDIF


       ; Setup the options.
       IF ~ ISA(c_options) THEN BEGIN
         c_options = {debug:1 $
                      ,info:1 $
                      ,warning:1 $
                      ,error:1 $
                      ,critical:1 $
                      ,testing:1 $
                      
                      ,timestamp:1 $
                      ,datestamp:0 $
                      ,caller:1 $
                      ,type:0 $
                      ,object:OBJ_NEW('MIR_LOGGER') $
                     }
       ENDIF

       IF ~ ISA(set_level) THEN BEGIN
         IF KEYWORD_SET(set_debug) THEN set_level = 'debug'
         IF KEYWORD_SET(set_info) THEN set_level = 'info'
         IF KEYWORD_SET(set_warning) THEN set_level = 'warning'
         IF KEYWORD_SET(set_error) THEN set_level = 'error'
         IF KEYWORD_SET(set_critical) THEN set_level = 'critical'
       ENDIF

       IF ISA(set_level) THEN BEGIN             
         CASE set_level OF
           'debug': BEGIN
             level_options = {debug:1 $
                              ,info:1 $
                              ,warning:1 $
                              ,error:1 $
                              ,critical:1 $
                              ,testing:1}
           END
           'info': BEGIN
             level_options = {debug:0 $
                              ,info:1 $
                              ,warning:1 $
                              ,error:1 $
                              ,critical:1 $
                              ,testing:1}
           END
           'warning': BEGIN
             level_options = {debug:0 $
                              ,info:0 $
                              ,warning:1 $
                              ,error:1 $
                              ,critical:1 $
                              ,testing:1}
           END
           'error': BEGIN
             level_options = {debug:0 $
                              ,info:0 $
                              ,warning:0 $
                              ,error:1 $
                              ,critical:1 $
                              ,testing:1}
           END
           'critical': BEGIN
             level_options = {debug:0 $
                              ,info:0 $
                              ,warning:0 $
                              ,error:0 $
                              ,critical:1 $
                              ,testing:1}
           END
           'testing': BEGIN
             level_options = {debug:1 $
                              ,info:1 $
                              ,warning:1 $
                              ,error:1 $
                              ,critical:1 $
                              ,testing:1}
           END
           ELSE: MESSAGE, 'Unknown logging level: '+level
         ENDCASE

         STRUCT_ASSIGN, level_options, c_options, /NOZERO
       ENDIF

       IF ISA(set_options) THEN BEGIN
         STRUCT_ASSIGN, set_options, c_options, /NOZERO
       ENDIF
       
       ; If this was a setup call then return immediately.
       IF ISA(set_level) OR ISA(set_options) THEN RETURN
       
       ; Parse keywords

       MIR_DEFAULT, scope_level_in, 0
       scope_level = scope_level_in + 1
       
       ; Set defaults for the traceback.
       IF KEYWORD_SET(key_error) THEN BEGIN
         MIR_DEFAULT, key_last_message, 1
       ENDIF

       IF ~ ISA(message_in) $
         AND ~ KEYWORD_SET(key_traceback) $
         AND ~ KEYWORD_SET(key_last_message) $
         AND ~ KEYWORD_SET(key_save) THEN BEGIN
         
         MESSAGE, 'Variable is undefined: MESSAGE.'
       ENDIF
       
       IF ~ ISA(message_in) THEN message_in = !NULL

       ; Now pass everything to the object.
       c_options.object.LOG, $
         message_in $
         ,DEBUG=key_debug $
         ,INFO=key_info $
         ,WARNING=key_warning $
         ,ERROR=key_error $
         ,CRITICAL=key_critical $

         ,SET_LEVEL=set_level $
         ,SET_DEBUG=set_debug $
         ,SET_INFO=set_info $
         ,SET_WARNING=set_warning $
         ,SET_CRITICAL=set_critical $
         
         ,LAST_MESSAGE=key_last_message $
         ,TRACEBACK=key_traceback $

         ,SAVE=key_save $
         ,SET_OPTIONS=set_options $
         
         ,SCOPE_LEVEL=scope_level $
         
         ,HELP=help

     END ; MIR_LOGGER
