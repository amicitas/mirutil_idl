




;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-07
;
; PURPOSE:
;   Define a dictionary object.
;
; DESCRIPTION:
;   Defineds a dictionary object in which each entry is given by a
;   key - value pair.
;
;   The value can be any kind of data, and different datay types may
;   be mixed.
;
;   The key must be given as a string.
;
;
;   The order of the entries are preserved.  This allows entries to
;   be acesssed either by key or by index.
;
;
; MAIN METHODS:
;   SET
;   GET
;   REMOVE
;   HAS_KEY
;
;   GET_BY_INDEX
;   REMOVE_BY_INDEX
;
; 
;
;
;-======================================================================





     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Add an entry to the dictionary.
     ;
     ; INPUTS:
     ;   key
     ;       A string containing the key for the entry.
     ;       Keys are case insensitive.
     ;
     ;   data
     ;       The data for the entry.
     ;
     ; KEYWORDS:
     ;
     ;   OVERWRITE = bool
     ;       (default 1)
     ;       If set to 0 then overwriting of existing entries is
     ;       disabled.
     ; 
     ;
     ;-=================================================================
     PRO MIR_DICTIONARY::SET, key_in, data, OVERWRITE=overwrite
       
       IF N_ELEMENTS(overwrite) EQ 0 THEN overwrite = 1

       ; First find out if this key already exists
       index = self->GET_INDEX(key_in)

       IF index NE -1 THEN BEGIN
         IF ~ KEYWORD_SET(overwrite) THEN BEGIN
           MESSAGE, 'Key already exists in dictionary.'
         ENDIF

         ; Key exists, overwrite the data.

         ; First remove the existing data.
         self->DEALLOCATE_BY_INDEX, index

         ; Now add the new data
         (*self.ptr_data_array)[index] = PTR_NEW(data)

       ENDIF ELSE BEGIN
         ; Key does not exist, so add it.
         IF self.case_sensitive THEN BEGIN
           key = key_in
         ENDIF ELSE BEGIN
           key = STRUPCASE(key_in)
         ENDELSE

 
         IF self.num_entries EQ 0 THEN BEGIN
           self.ptr_key_array = PTR_NEW([key])

           self.ptr_data_array = PTR_NEW(PTRARR(1))
           (*self.ptr_data_array)[0] = PTR_NEW(data)
         ENDIF ELSE BEGIN
           
           ; Add the key.
           *self.ptr_key_array = [*self.ptr_key_array, key]

           ; Add the data
           *self.ptr_data_array = [*self.ptr_data_array, PTR_NEW(data)]
         ENDELSE

         ; Increment the number of entries.
         self.num_entries += 1
       ENDELSE
         

     END ; PRO MIR_DICTIONARY::SET



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Get the data associated with the given key.
     ;
     ;-=================================================================
     FUNCTION MIR_DICTIONARY::GET, key
  
       ; First find out if this key already exists
       index = self->GET_INDEX(key)

       IF index NE -1 THEN BEGIN
         ; Key exists, return the data
         
         RETURN, SELF->GET_BY_INDEX(index)

       ENDIF ELSE BEGIN
         ; Key does not exist. Write an error.
         ON_ERROR, 2
         MESSAGE, 'Key not found.'
       ENDELSE
         

     END ; FUNCTION MIR_DICTIONARY::GET




     ;+=================================================================
     ; PURPOSE:
     ;   Get the data at the given index.
     ;
     ; INPUTS:
     ;   index
     ;       The index for which to return the data.
     ;
     ;   key (optional)
     ;       The key at the given index.
     ;
     ; KEYWORDS:
     ;
     ;   /LAST
     ;       Get the data from the last entry.
     ;
     ;       NOTE: When data is overwritten (using SET, /OVERWRITE)
     ;       the position of the data is not changed.
     ;
     ;-=================================================================
     FUNCTION MIR_DICTIONARY::GET_BY_INDEX, index, key, LAST=last

       IF KEYWORD_SET(last) THEN BEGIN
         index = self->NUM_ENTRIES() - 1
       ENDIF

       IF ARG_PRESENT(key) THEN BEGIN
         key = (*self.ptr_key_array)[index]
       ENDIF

       ; Return the data at the given index
       RETURN, *(*self.ptr_data_array)[index]
         

     END ; FUNCTION MIR_DICTIONARY::GET_BY_INDEX




     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return the key at the given index.
     ;
     ; INPUTS:
     ;   index
     ;       The index for which to return the key.
     ;
     ;   data (optional)
     ;       The data at the given index.
     ;
     ; KEYWORDS:
     ;
     ;   /LAST
     ;       Get the key from the last entry.
     ;
     ;       NOTE: When data is overwritten (using SET, /OVERWRITE)
     ;       the position of the data is not changed.
     ;
     ;-=================================================================
     FUNCTION MIR_DICTIONARY::GET_KEY_BY_INDEX, index, data, LAST=last

       IF KEYWORD_SET(last) THEN BEGIN
         index = self->NUM_ENTRIES() - 1
       ENDIF

       IF ARG_PRESENT(data) THEN BEGIN
         ; Return the data at the given index
         data =  *(*self.ptr_data_array)[index]
       ENDIF
         
       ; Return the key at the given index.
       RETURN, (*self.ptr_key_array)[index]

     END ; FUNCTION MIR_DICTIONARY::GET_KEY_BY_INDEX




     ;+=================================================================
     ;
     ; Remove a key-data pair.
     ;
     ;
     ; KEYWORDS:
     ;   /ALL
     ;       Remove all entries.
     ;
     ;   /HEAP_FREE
     ;       Call [HEAP_FREE] on the entry before removing it.
     ;       This will recursively destroy all pointers and objects
     ;       referenced by the data associated with the given key.
     ;
     ;       See [HEAP_FREE] for more information.
     ;
     ;   /OBJ_FREE
     ;       Call [OBJ_FREE] on the entry before removing it.
     ;       Like /HEAP_FREE but only destroys objects.
     ;
     ;   /PTR_FREE
     ;       Call [PTR_FREE] on the entry before removing it.
     ;       Line /HEAP_FREE but only frees pointers.
     ;
     ;-=================================================================
     PRO MIR_DICTIONARY::REMOVE, key $
                             ,ALL=all $
                             ,_REF_EXTRA=extra

       IF KEYWORD_SET(all) THEN BEGIN
         FOR ii=0,self->NUM_ENTRIES()-1 DO BEGIN
           ; I am removing the entries starting at the end.
           ; I could just as well remove them starting at the beginning.
           self->REMOVE_BY_INDEX, /LAST, _STRICT_EXTRA=extra
         ENDFOR
       ENDIF ELSE BEGIN
         IF ~KEYWORD_SET(last) THEN BEGIN
           ; First find out if this key already exists
           index = self->GET_INDEX(key)
         ENDIF

         IF index NE -1 THEN BEGIN
           self->REMOVE_BY_INDEX, index, _STRICT_EXTRA=extra
         ENDIF
       ENDELSE

     END ; PRO MIR_DICTIONARY::REMOVE



     ;+=================================================================
     ;
     ; Remove a key / data pair given an index
     ;
     ; KEYWORDS:
     ;   /ALL
     ;       Remove all entries.
     ;
     ;   /LAST
     ;       Remove the last entry.
     ;
     ;       NOTE: When data is overwritten (using SET, /OVERWRITE)
     ;       the position of the data is not changed.
     ;
     ;   /HEAP_FREE
     ;       Call [HEAP_FREE] on the entry before removing it.
     ;       This will recursively destroy all pointers and objects
     ;       referenced by the data at the given index.
     ;
     ;       See [HEAP_FREE] for more information.
     ;
     ;   /OBJ_FREE
     ;       Call [OBJ_FREE] on the entry before removing it.
     ;       Like /HEAP_FREE but only destroys objects.
     ;
     ;   /PTR_FREE
     ;       Call [PTR_FREE] on the entry before removing it.
     ;       Line /HEAP_FREE but only frees pointers.
     ;
     ;-=================================================================
     PRO MIR_DICTIONARY::REMOVE_BY_INDEX, index $
                                      ,KEY=key $
                                      ,LAST=last $
                                      ,_REF_EXTRA=extra

       IF KEYWORD_SET(last) THEN BEGIN
         index = self->NUM_ENTRIES() - 1
       ENDIF

       IF ARG_PRESENT(key) THEN BEGIN
         key = (*self.ptr_key_array)[index]
       ENDIF


       self->DEALLOCATE_BY_INDEX, index, _STRICT_EXTRA=extra

       CASE 1 OF
         self.num_entries EQ 1: BEGIN
           self->DEALLOCATE
         END
         index EQ 0: BEGIN
           *self.ptr_key_array = (*self.ptr_key_array)[1:self.num_entries-1]
           *self.ptr_data_array = (*self.ptr_data_array)[1:self.num_entries-1]
           self.num_entries -= 1
         END
         index EQ self.num_entries-1: BEGIN
           *self.ptr_key_array = (*self.ptr_key_array)[0:self.num_entries-2]
           *self.ptr_data_array = (*self.ptr_data_array)[0:self.num_entries-2]
           self.num_entries -= 1
         END
         ELSE: BEGIN
           *self.ptr_key_array = $
              [(*self.ptr_key_array)[0:index-1], (*self.ptr_key_array)[index+1:self.num_entries-1]]
           *self.ptr_data_array = $
              [(*self.ptr_data_array)[0:index-1], (*self.ptr_data_array)[index+1:self.num_entries-1]]
           self.num_entries -= 1
         END
       ENDCASE

     END ;MIR_DICTIONARY::REMOVE_BY_INDEX



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return an array containing all of the keys.
     ;
     ;-=================================================================
     FUNCTION MIR_DICTIONARY::GET_KEYS
       

       IF self.num_entries GT 0 THEN BEGIN
         
         RETURN, *self.ptr_key_array

       ENDIF ELSE BEGIN

         ON_ERROR, 2
         MESSAGE, 'Dictionary is empty.'
       ENDELSE
         

     END ; FUNCTION MIR_DICTIONARY::GET_KEYS




     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Check if a given key is in the dictionary. If so return 1
     ;   otherwise return 0.
     ; 
     ; INPUTS:
     ;   key
     ;       The key to check for
     ;
     ;   data (optional)
     ;       The data associate with the given key, if the key is found.
     ;
     ; KEYWORDS:
     ;   INDEX = named variable
     ;       Return the index at which the given key was found.
     ;
     ;-=================================================================
     FUNCTION MIR_DICTIONARY::HAS_KEY, key, data, INDEX=index
       
       ; First find out if this key already exists
       index = self->GET_INDEX(key)

       IF index EQ -1 THEN BEGIN
         RETURN, 0
       ENDIF ELSE BEGIN

         IF ARG_PRESENT(data) THEN BEGIN
           data = self->GET_BY_INDEX(index)
         ENDIF

         RETURN, 1
       ENDELSE
         

     END ; FUNCTION MIR_DICTIONARY::GET_KEYS



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Searches the keys and returns the index of the first match.
     ;
     ;   If no matches are found, then -1 is returned.
     ;
     ; PROGRAMING NOTES:
     ;   I did a speed study on various algorithims for searching
     ;   an array of strings.  The IDL overhead on any loops
     ;   far outweighed any algorithm considerations.  As such,
     ;   this is the fastest way to do this.
     ;
     ;
     ;-=================================================================
     FUNCTION MIR_DICTIONARY::GET_INDEX, key

       ; Don't search if the dictionary is empty.
       IF self.num_entries EQ 0 THEN BEGIN
         RETURN, -1
       ENDIF

       w = WHERE(STRCMP(key, *self.ptr_key_array, FOLD_CASE=~self.case_sensitive))

       RETURN, w[0]

     END ; FUNCTION MIR_DICTIONARY::GET_INDEX



     ;+=================================================================
     ;
     ; Return the number of entries in the dictionary.
     ;
     ;-=================================================================
     FUNCTION MIR_DICTIONARY::NUM_ENTRIES
       

       RETURN, self.num_entries
         

     END ; FUNCTION MIR_DICTIONARY::NUM_ENTRIES



     ;+=================================================================
     ; PURPOSE:
     ;   Return the contents of the dictionary as a structure.
     ;
     ;-=================================================================
     FUNCTION MIR_DICTIONARY::TO_STRUCTURE
       

       FOR ii=0,self.num_entries-1 DO BEGIN
         struct = BUILD_STRUCTURE((*self.ptr_key_array)[ii] $
                                  ,*(*self.ptr_data_array)[ii] $
                                  ,STRUCT=struct)
       ENDFOR
         
       RETURN, struct

     END ; FUNCTION MIR_DICTIONARY::TO_STRUCTURE



     ;+=================================================================
     ; PURPOSE:
     ;   Set the contents of the dictionary from a structure.
     ;
     ;-=================================================================
     PRO MIR_DICTIONARY::FROM_STRUCTURE, struct, OVERWRITE=overwrite
       
       IF self.num_entries NE 0 AND ~KEYWORD_SET(overwrite) THEN BEGIN
         MESSAGE, 'Dictionary is not empty.  Use /OVERWRITE to overwrite'
       ENDIF

       tags = TAG_NAMES(struct)
       FOR ii=0,N_TAGS(struct)-1 DO BEGIN
         self->SET, tags[ii], struct.(ii)
       ENDFOR

     END ; FUNCTION MIR_DICTIONARY::FROM_STRUCTURE



     ;+=================================================================
     ;
     ; Return a copy of the dictionary in a new object.
     ;
     ;-=================================================================
     FUNCTION MIR_DICTIONARY::GET_COPY, RECURSIVE=recursive
       
       recursive = KEYWORD_SET(recursive)

       copy = self->OBJECT::GET_COPY()
       copy.ptr_key_array = PTR_NEW(*self.ptr_key_array)
       copy.ptr_data_array = PTR_NEW(*self.ptr_data_array)

       FOR ii=0,self->NUM_ENTRIES() - 1 DO BEGIN
         IF recursive AND OBJ_VALID(*(*self.ptr_data_array)[ii]) THEN BEGIN
           (*copy.ptr_data_array)[ii] = PTR_NEW((*(*self.ptr_data_array)[ii])->GET_COPY(/RECURSIVE))
         ENDIF ELSE BEGIN
           (*copy.ptr_data_array)[ii] = PTR_NEW(*(*self.ptr_data_array)[ii])
         ENDELSE
       ENDFOR
       
       RETURN, copy
         

     END ; FUNCTION MIR_DICTIONARY::GET_COPY()


     ;+=================================================================
     ; PURPOSE:
     ;   Deallocate all internal data pointers and arrays.
     ;
     ; KEYWORDS:
     ;   /HEAP_FREE
     ;       Call [HEAP_FREE] on each entry before removing it.
     ;       This will destroy all objects and pointers in the 
     ;       dictionary.
     ;
     ;   /OBJ_FREE
     ;       Call [OBJ_FREE] on each entry before removing it.
     ;       This will destroy all objects in the dictionary.
     ;
     ;   /PTR_FREE
     ;       Call [PTR_FREE] on each entry before removing it.
     ;       This will destroy all pointers in the dictionary.
     ;
     ;-=================================================================
     PRO MIR_DICTIONARY::DEALLOCATE, _REF_EXTRA=extra

       PTR_FREE, self.ptr_key_array

       IF PTR_VALID(self.ptr_data_array) THEN BEGIN
         FOR ii=0,self->num_entries()-1 DO BEGIN
           self->DEALLOCATE_BY_INDEX, ii, _STRICT_EXTRA=extra
         ENDFOR

         PTR_FREE, self.ptr_data_array
       ENDIF

       self.num_entries=0

     END ; PRO MIR_DICTIONARY::DEALLOCATE




     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Deallocate the data at the given index.
     ;
     ;   This method can be reimplemented by derived classes to take
     ;   additional action when data is removed.
     ; 
     ;
     ; KEYWORDS:
     ;   /HEAP_FREE
     ;       Call [HEAP_FREE] on the entry before removing it.
     ;       This will recursively destroy all pointers and objects
     ;       referenced by the data at the given index.
     ;
     ;       See [HEAP_FREE] for more information.
     ;
     ;   /OBJ_FREE
     ;       Call [OBJ_FREE] on the entry before removing it.
     ;       Like /HEAP_FREE but only destroys objects.
     ;
     ;   /PTR_FREE
     ;       Call [PTR_FREE] on the entry before removing it.
     ;       Line /HEAP_FREE but only frees pointers.
     ;
     ;-=================================================================
     PRO MIR_DICTIONARY::DEALLOCATE_BY_INDEX, index $
                                          ,HEAP_FREE=heap_free $
                                          ,OBJ_FREE=obj_free $
                                          ,PTR_FREE=ptr_free

       heap_free = KEYWORD_SET(heap_free)
       obj_free = KEYWORD_SET(obj_free)
       ptr_free = KEYWORD_SET(ptr_free)


       
       ; Free heap variables, if desired.
       IF heap_free OR obj_free OR ptr_free THEN BEGIN
         IF heap_free THEN BEGIN
           obj_free = 1
           ptr_free = 1
         ENDIF
         HEAP_FREE, *(*self.ptr_data_array)[index], OBJ=obj_free, PTR=ptr_free
       ENDIF

       ; Now free the data pointer.
       PTR_FREE, (*self.ptr_data_array)[index]

     END ; PRO MIR_DICTIONARY::DEALLOCATE_BY_INDEX



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Cleanup on destruction.
     ;
     ; KEYWORDS:
     ;   /HEAP_FREE
     ;       Call [HEAP_FREE] on each entry before removing it.
     ;
     ;       This will recursivly destroy all objects and pointers
     ;       referenced by the data in the dictionary.
     ;
     ;       See [HEAP_FREE] for more information.
     ;
     ;   /OBJ_FREE
     ;       Call [OBJ_FREE] on each entry before removing it.
     ;       Like /HEAP_FREE but only destroys objects.
     ;
     ;   /PTR_FREE
     ;       Call [PTR_FREE] on each entry before removing it.
     ;       Line /HEAP_FREE but only frees pointers.
     ;
     ;-=================================================================
     PRO MIR_DICTIONARY::CLEANUP, _REF_EXTRA=extra

       self->DEALLOCATE, _STRICT_EXTRA=extra

     END ; PRO MIR_DICTIONARY::CLEANUP




     ;+=================================================================
     ;
     ; Initialize the object.
     ;
     ;-=================================================================
     FUNCTION MIR_DICTIONARY::INIT, CASE_SENSITIVE=case_sensitive

       self.case_sensitive = KEYWORD_SET(case_sensitive)
       self.num_entries = 0
       
       RETURN, 1

     END ; FUNCTION MIR_DICTIONARY::INIT




     ;+=================================================================
     ;
     ; Object defition for the dictionary class
     ;
     ;-=================================================================
     PRO MIR_DICTIONARY__DEFINE

       struct = {MIR_DICTIONARY $
                 ,case_sensitive:0 $
                 ,num_entries:0 $
                 ,ptr_key_array:PTR_NEW() $
                 ,ptr_data_array:PTR_NEW() $
                 ,INHERITS OBJECT $
                }

     END ;PRO MIR_DICTIONARY__DEFINE
