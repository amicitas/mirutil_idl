


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-06
;
; PURPOSE:
;   Define a NULL object/structure.
;
; TAGS:
;   NOPAGE
;-======================================================================




     ;+=================================================================
     ;
     ; PURPOSE:
     ;   This is the definiton for the standard NULL class.
     ;
     ;   This object/structure allows standard way to return an 
     ;   'empty' structure.
     ;
     ;   This definition is  also usefull in defining objects with
     ;   no properties.
     ;
     ; DESCRIPTION:
     ;   The null structure has one property, 'null', which is an
     ;   integer.  This can be used to check if a given structure
     ;   is the NULL structure.  Doing provides a 30% speed increase
     ;   over checking the structure name.
     ;
     ; TAGS:
     ;   MAKEPAGE
     ;-=================================================================
     PRO NULL__DEFINE

      struct ={ NULL $
                ,null:0 $
              }

     END ;PRO NULL__DEFINE
