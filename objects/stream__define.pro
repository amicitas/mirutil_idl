


;+======================================================================
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;
; DATE:
;   2009-09
;
; PURPOSE:
;   Create an object to act as a generalized data stream.
;
;-======================================================================





     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Clean up the file stream.
     ;
     ;   The standard action is to close the stream.
     ;
     ;
     ;-=================================================================
     PRO STREAM::CLEANUP

       self->CLOSE

     END ;PRO STREAM::CLEANUP


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Open the stream
     ;
     ;
     ;-=================================================================
     PRO STREAM::OPEN

       ; This method must be reimplemented by any derived classes.

     END ;PRO STREAM::OPEN


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Close the stream
     ;
     ;
     ;-=================================================================
     PRO STREAM::CLOSE

       ; This method must be reimplemented by any derived classes.

     END ;PRO STREAM::CLOSE


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return TRUE if the end of the stream has been reached.
     ;   Otherwise reurn false.
     ;
     ;
     ;-=================================================================
     FUNCTION STREAM::END_OF_STREAM

       MESSAGE, 'Not implemented.'

     END ;FUNCTION STREAM::END_OF_STREAM



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return TRUE if stream is open.
     ;   Otherwise reurn false.
     ;
     ;
     ;-=================================================================
     FUNCTION STREAM::IS_OPEN

       MESSAGE, 'Not implemented.'

     END ;FUNCTION STREAM::IS_OPEN



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   This object will allow arbitrary data streams to be handeled
     ;   with a uniform interface and persistant data.
     ;
     ;
     ;-=================================================================
     PRO STREAM__DEFINE

       struct = { STREAM $
                  ,INHERITS OBJECT $
                }

     END ;PRO STREAM__DEFINE
