

;+==============================================================================
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
; PURPOSE:
;   Define a default object.
;
;   This is an extention of the IDL standard object.  
;
;   This object is meant to eventually replace <OBJECT::> which was needed
;   for IDL before version 8.0.
;
;-=============================================================================





     ;+=================================================================
     ; PURPOSE:
     ;   Define a default <::GETPROPERTY> method.
     ;
     ;-=================================================================
     FUNCTION MIR_OBJECT::GETPROPERTY, _REF_EXTRA=extra
       
       STOP

       RETURN, !NULL

     END ; FUNCTION FUNCTION MIR_OBJECT::GETPROPERTY

     ;+=========================================================================
     ;
     ; PURPOSE:
     ;   Setup the MIR_OBJECT object
     ;
     ;
     ;-=========================================================================
     PRO MIR_OBJECT__DEFINE

       struct = { MIR_OBJECT $
                  ,INHERITS IDL_OBJECT $
                }

     END ; PRO MIR_OBJECT
