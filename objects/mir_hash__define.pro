
;+==============================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;     antoniuk@fusion.gat.com
;     novimir.pablant@amicitas.com
;
; DATE:
;   2011-09
;
; PURPOSE:
;   Create a custom version of the HASH object.  
; 
;-==============================================================================



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return the number of elements in the hash object.
     ;
     ;-=================================================================
     FUNCTION MIR_HASH::N_ELEMENTS

       RETURN, N_ELEMENTS(self)

     END ; FUNCTION MIR_HASH::N_ELEMENTS

     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return a copy of this HASH with a new object reference.
     ;
     ;-=================================================================
     FUNCTION MIR_HASH::COPY

       RETURN, self[*]

     END ; FUNCTION MIR_HASH::COPY

     ;+=========================================================================
     ;
     ; PURPOSE:
     ;   Add the entries from a hash to this hash inplace
     ;
     ; DESCRIPTOIN:
     ;   This will add the entries from the input has o the current
     ;   hash.  This will be done in place.
     ;
     ;   To craete a new HASH from the concactination of two HASHes
     ;   use this instead:
     ;     hash3 = hash1 + hash2
     ;
     ; KEYWORDS:
     ;   /UPCASE
     ;     Using this key will ensure that all keys from the source HASH
     ;     will be converted to uppercase before being added to the destination
     ;     hash.
     ;
     ;-=========================================================================
     PRO MIR_HASH::JOIN, new_hash, UPCASE=upcase

       upcase = KEYWORD_SET(upcase)
       
       FOREACH value, new_hash, key DO BEGIN
         IF upcase THEN key = STRUPCASE(key)
         self[key] = value
       ENDFOREACH

     END ; PRO MIR_HASH::JOIN


     ;+=========================================================================
     ; PURPOSE:
     ;   
     ;
     ;-=========================================================================
     PRO MIR_HASH__DEFINE
       
       struct = {MIR_HASH $
                 ,INHERITS HASH $
                }

     END ;PRO MIR_HASH__DEFINE
