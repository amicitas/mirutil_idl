


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;   novimir.pablant@amicitas.com
;
; DATE:
;   2010-03
;
; VERSION:
;   1.0.0
;
; PURPOSE:
;   Check if the given object is derived from them the given superclass.
;
;   This is similar to <OBJ_ISA()> however it will accept an object
;   name as well as an instantiated object.
;
; DESCRIPTION:
;   This works very similarly to <OBJ_SUPERCLASS()>.
;
; KEYWORDS:
;   /RESOLVE
;       If set, an attempt will be made to resolve all of the 
;       superclasses, up untill a match is found.
;       
;       This will be done using <RESOLVE_ROUTINE>.  This can 
;       pontentially be very slow, and will not work if the needed
;       classes are not resolvable using IDL's aoutomatic resolving
;       convensions.
;
;       This is never needed if an instantiated object is given
;       instead of an object name.
;
;
;-======================================================================



     FUNCTION OBJ_IS_DERIVED, object_in, superclass_in, COUNT=count, RESOLVE=resolve
       
       CATCH, error
       IF error NE 0 THEN BEGIN
         CATCH, /CANCEL
         MESSAGE, /REISSUE
       ENDIF

       CASE 1 OF
         ISA(object_in) AND OBJ_VALID(object_in): object_name = OBJ_CLASS(object_in)
         ISA(object_in) AND MIR_IS_STRING(object_in): BEGIN
           object_name = object_in
           IF KEYWORD_SET(resolve) THEN BEGIN
             RESOLVE_ROUTINE, STRLOWCASE(object_name)+'__define', /NO_RECOMPILE, /COMPILE_FULL_FILE
           ENDIF
         END
         ELSE: MESSAGE, 'No valid object or object name given.'
       ENDCASE

       superclass_name = STRUPCASE(superclass_in)

       direct_superclasses = OBJ_CLASS(object_name, COUNT=count, /SUPERCLASS)
       IF count EQ 0 THEN RETURN, 0

       w = WHERE(direct_superclasses EQ superclass_in, count_match)
       IF count_match NE 0 THEN RETURN, 1

       match = 0
       FOR ii=0,count-1 DO BEGIN
         match = OBJ_IS_DERIVED(direct_superclasses[ii], superclass_in, RESOLVE=resolve)
         IF match THEN BREAK
       ENDFOR

       RETURN, match

     END ;FUNCTION OBJ_IS_DERIVED
