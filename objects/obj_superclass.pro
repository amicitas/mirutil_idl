


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
; DATE:
;   2009-05-18
;
; VERSION:
;   1.0.0
;
; PURPOSE:
;   Find all of the superclasses for a given obejct.
;
;   This is similar to OBJ_CLASS(/SUPERCLASS) however it will decend
;   the tree to find all inherited classes.
;
;   WARNING: The ordering of the superclasses may NOT match the
;            the IDL rules of INHERITANCE.  I have not checked.
;
; KEYWORDS:
;   /RESOLVE
;       If set, an attempt will be made to resolve all of the 
;       superclasses.
;       
;       This will be done using <RESOLVE_ROUTINE>.  This can 
;       pontentially be very slow, and will not work if the needed
;       classes are not resolvable using IDL's aoutomatic resolving
;       convensions.
;
;       This is never needed if an instantiated object is given
;       instead of an object name.
;
;-======================================================================



     FUNCTION OBJ_SUPERCLASS, object, COUNT=count, RESOLVE=resolve
       
       CATCH, error
       IF error NE 0 THEN BEGIN
         CATCH, /CANCEL
         MESSAGE, /REISSUE
       ENDIF

       IF KEYWORD_SET(resolve) THEN BEGIN
         IF MIR_IS_STRING(object) THEN BEGIN
           RESOLVE_ROUTINE, STRLOWCASE(object)+'__define', /NO_RECOMPILE, /COMPILE_FULL_FILE
         ENDIF
       ENDIF

       direct_superclasses = OBJ_CLASS(object, COUNT=count, /SUPERCLASS)

       FOR ii=0,count-1 DO BEGIN
         superclasses = BUILD_ARRAY(superclasses, direct_superclasses[ii], /NO_COPY)

         next_superclasses = OBJ_SUPERCLASS(direct_superclasses[ii], COUNT=next_count, RESOLVE=resolve)
         IF next_count NE 0 THEN BEGIN
           superclasses = BUILD_ARRAY(superclasses, next_superclasses, /NO_COPY)
           count += next_count
         ENDIF
       ENDFOR

       IF ISA(superclasses) THEN BEGIN
         RETURN, superclasses
       ENDIF ELSE BEGIN
         RETURN, ''
       ENDELSE

     END ;FUNCTION OBJ_SUPERCLASS
