
;+==============================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;     antoniuk@fusion.gat.com
;     novimir.pablant@amicitas.com
;
; DATE:
;   2011-04
;
; PURPOSE:
;   Create a base class for a 'HASH_OBJECT'.  
;
; DESCRIPTION:
;   I don't like that parameter data in a typical IDL object are fixed and 
;   strongly typed.  This is another way to create an IDL object without those
;   limitiations.
;
;   Unfortunatly the HASH implementation in IDL is slow.  So this does not
;   work when speed is an issue.
; 
;-==============================================================================







     ;+=================================================================
     ;
     ; Get a copy of this object.
     ;
     ;-=================================================================
     FUNCTION MIR_HASH_OBJECT::COPY

       new_hash = HASH(self.KEYS(), self.VALUES())

       RETURN, new_hash

     END ; FUNCTION MIR_HASH_OBJECT::COPY

     
     ;+=================================================================
     ;
     ; Set the data in the structure.
     ;
     ;-=================================================================
     PRO MIR_HASH_OBJECT::SET_STRUCT, struct

       ; Copy the data to the object
       STRUCT_ASSIGN, struct, self, /NOZERO


     END ;PRO MIR_HASH_OBJECT::SET_STRUCT


     ;+=================================================================
     ;
     ; This method will return a structure with all of the 
     ; data contained in this object
     ;
     ;-=================================================================
     FUNCTION MIR_HASH_OBJECT::GET_STRUCT

       ; Create a new structure.
       struct = CREATE_STRUCT(NAME=OBJ_CLASS(self))

       ; Copy the data to the new structure
       STRUCT_ASSIGN, self, struct

       RETURN, struct

     END ;FUNCTION MIR_HASH_OBJECT::GET_STRUCT

  

     ;+=========================================================================
     ; PURPOSE:
     ;   
     ;
     ;-=========================================================================
     PRO MIR_HASH_OBJECT__DEFINE
       

       struct = {MIR_HASH_OBJECT $
                 ,INHERITS HASH $
                }

     END ;PRO MIR_HASH_OBJECT__DEFINE
