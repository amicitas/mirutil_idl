


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-06
;
; PURPOSE:
;   Here a standard object is defined.
;
;   This standard object defines several methods that allow access
;   to the object properties.  
;
;   In addition it adds methods to copy the object.
;
;-======================================================================








     ;+=================================================================
     ;
     ; This method will return the property specifed by the input
     ; string.
     ;
     ; WARNING:    This is a slow way to do things.
     ;             Consider using COPY_TO instead.
     ;
     ;-=================================================================
     FUNCTION OBJECT::GET_PROPERTY, name

       ; Separate the name by '.'
       name_array = STRSPLIT(STRUPCASE(name), '.', /EXTRACT)
       
       current = self->GET_STRUCT()
       num_levels = N_ELEMENTS(name_array)

       FOR ii=0,num_levels-1 DO BEGIN
         tags = TAG_NAMES(current)
         tag_index = WHERE(tags EQ name_array[ii], count)

         IF count EQ 0 THEN BEGIN
           MESSAGE, 'Property not found: '+STRUPCASE(name)
         ENDIF

         current = current.(tag_index[0])
       ENDFOR

       RETURN, current

     END ;FUNCTION OBJECT::GET_PROPERTY




     ;+=================================================================
     ;
     ; This method will set property specifed by the input
     ; string to the given value.
     ;
     ; WARNING:    This is a slow way to do things.
     ;             Consider using COPY_FROM instead.
     ;
     ;-=================================================================
     PRO OBJECT::SET_PROPERTY, name, value

       ; Separate the name by '.'
       name_array = STRSPLIT(STRUPCASE(name), '.', /EXTRACT)
       
       num_levels = N_ELEMENTS(name_array)

       ; Now build up the structure
       struct = CREATE_STRUCT(name_array[num_levels-1], value)

       IF num_levels GT 1 THEN BEGIN
         FOR ii=num_levels-2,0,-1 DO BEGIN
           struct = CREATE_STRUCT(name_array[ii], struct)
         ENDFOR
       ENDIF

       self->COPY_FROM, struct, /RELAXED

     END ;PRO OBJECT::SET_PROPERTY




     ;+=================================================================
     ;
     ; PURPOSE:
     ;   This method copies properties from the input structure/object
     ;   to this object.
     ;
     ;   NOTE: Copying from an object will ony work if the input object
     ;         has the method GET_STRUCT().
     ;
     ; KEYWORDS:
     ;   /RECURSIVE
     ;       Will loop though all of the properties.  If one of the
     ;       properties contains an object, the object will be copied
     ;       recursively.
     ;
     ;       With out this keyword the object reference will be copied.
     ;
     ;   /RELAXED
     ;       Will perform the copy using relaxed structure assignment.
     ; 
     ;       This keyword cannot be used with /RECURSIVE.
     ;       
     ;   /ZERO
     ;       When doing relaxed copying, this keyword will zero out
     ;       and properties in this object that are not found
     ;       in the input structure/object.
     ; 
     ;   /VERBOSE
     ;       When doing relaxed copying, this keyword will cause
     ;       messages to be printed with there is a mispatch in
     ;       properties.
     ;
     ;-=================================================================
     PRO OBJECT::COPY_FROM, data $
                            ,RECURSIVE=recursive $
                            ,RELAXED=relaxed $
                            ,ZERO=zero $
                            ,VERBOSE=verbose
      
       recursive = KEYWORD_SET(recursive)

       IF KEYWORD_SET(relaxed) THEN BEGIN

         IF KEYWORD_SET(recursive) THEN BEGIN
           MESSAGE, 'Can not perform a recursive copy with relaxed definitions.'
         ENDIF

         STRUCT_ASSIGN, data, self, NOZERO=~KEYWORD_SET(zero), VERBOSE=verbose

       ENDIF ELSE BEGIN

         FOR ii=0,self->N_TAGS()-1 DO BEGIN
           IF OBJ_VALID(self.(ii)[0]) AND recursive THEN BEGIN
             self.(ii)->COPY_FROM, data.(ii), /RECURSIVE
           ENDIF ELSE BEGIN
             self.(ii) = data.(ii)
           ENDELSE
         ENDFOR

       ENDELSE

     END ;PRO OBJECT::COPY_FROM




     ;+=================================================================
     ;
     ; PURPOSE:
     ;   This assign one structure to another recursively.
     ;
     ;-=================================================================
     PRO OBJECT::STRUCT_ASSIGN

       ; The idea here is to eventually create a truely recursive 
       ; way to copy structures.

       ; The current COPY_FROM method will not work correctly if
       ; there are objects inside structures.

     END ;PRO OBJECT::STRUCT_ASSIGN




     ;+=================================================================
     ;
     ; This method will return a copy of the object in a new 
     ; instance.
     ;
     ;-=================================================================
     FUNCTION OBJECT::GET_COPY, RECURSIVE=recursive

       ; Create a new object
       new_copy = OBJ_NEW(OBJ_CLASS(self))

       ; Copy the data to the new object.
       new_copy->COPY_FROM, self, RECURSIVE=recursive

       RETURN, new_copy

     END ;PRO OBJECT::GET_COPY



     ;+=================================================================
     ;
     ; Set the data in the structure.
     ;
     ;-=================================================================
     PRO OBJECT::SET_STRUCT, struct

       ; Copy the data to the object
       STRUCT_ASSIGN, struct, self, /NOZERO


     END ;PRO OBJECT::SET_STRUCT


     ;+=================================================================
     ;
     ; This method will return a structure with all of the 
     ; data contained in this object
     ;
     ;-=================================================================
     FUNCTION OBJECT::GET_STRUCT

       ; Create a new structure.
       struct = CREATE_STRUCT(NAME=OBJ_CLASS(self))

       ; Copy the data to the new structure
       STRUCT_ASSIGN, self, struct

       RETURN, struct

     END ;FUNCTION OBJECT::GET_STRUCT




     ;+=================================================================
     ; PURPOSE:
     ;   Return a structure containing the data in this structure.
     ;   If this structure contains any objects, the structure for
     ;   those object will be used in place of the object reference.
     ;
     ; PROGRAMING NOTES:
     ;   This may be a slow process.
     ;
     ;-=================================================================
     FUNCTION OBJECT::GET_STRUCT_RECURSIVE

       num_tags = self->N_TAGS()
       tags = self->TAG_NAMES()

       FOR ii=0,num_tags-1 DO BEGIN
         IF SIZE(self.(ii), /TYPE) EQ 11 THEN BEGIN
           output = BUILD_STRUCTURE(tags[ii] $
                                    ,self.(ii)->GET_STRUCT_RECURSIVE() $
                                    ,STRUCTURE=output)
         ENDIF ELSE BEGIN
           output = BUILD_STRUCTURE(tags[ii] $
                                    ,self.(ii) $
                                    ,STRUCTURE=output)
         ENDELSE
       ENDFOR

       RETURN, output

     END ;FUNCTION OBJECT::GET_STRUCT_RECURSIVE




     ;+=================================================================
     ; PURPOSE:
     ;   Return the number of tags in the internal structure.
     ;
     ;
     ;
     ;-=================================================================
     FUNCTION OBJECT::N_TAGS

       RETURN, N_TAGS(CREATE_STRUCT(NAME=OBJ_CLASS(self)))

     END ;FUNCTION OBJECT::N_TAGS




     ;+=================================================================
     ; PURPOSE:
     ;   Return the property names from the internal structure.
     ;
     ;-=================================================================
     FUNCTION OBJECT::TAG_NAMES

       RETURN, TAG_NAMES(CREATE_STRUCT(NAME=OBJ_CLASS(self)))

     END ;FUNCTION OBJECT::N_TAGS




     ;+=================================================================
     ;
     ; PURPOSE:
     ;   This method prints the properties of the object to the
     ;   terminal.
     ;
     ; KEYWORDS:
     ;   This will accept all keywords valid for [PRINT_STRUCTURE].
     ;
     ;-=================================================================
     PRO OBJECT::PRINT_STRUCT, _EXTRA=extra
       
       PRINT_STRUCTURE, self->GET_STRUCT(), _EXTRA=extra

     END ;PRO OBJECT::PRINT_STRUCT




     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Destroy the object.
     ;
     ;   This can be used instead of OBJ_DESTROY
     ;
     ;-=================================================================
     PRO OBJECT::DESTROY, _REF_EXTRA=extra

       IF N_ELEMENTS(extra) NE 0 THEN BEGIN
         OBJ_DESTROY, self, _STRICT_EXTRA=extra
       ENDIF ELSE BEGIN
         OBJ_DESTROY, self
       ENDELSE

     END ;PRO OBJECT::DESTROY




     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Cleanup on object destruction.
     ;
     ;-=================================================================
     PRO OBJECT::CLEANUP, _NULL=_null
     END ;PRO OBJECT::CLEANUP




     ;+=================================================================
     ;
     ; This is the definiton for the standard object class.
     ;
     ;-=================================================================
     PRO OBJECT__DEFINE

      struct ={ OBJECT $
                ,INHERITS NULL $
              }

     END ;PRO OBJECT__DEFINE
