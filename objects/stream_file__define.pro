


;+======================================================================
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;
; DATE:
;   2009-09
;
; PURPOSE:
;   Create an object to use as a file stream.
;
;-======================================================================




     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Initialize the file stream
     ;
     ;
     ;-=================================================================
     FUNCTION STREAM_FILE::INIT, filepath, VERBOSE=verbose

       self.filepath = filepath
       self.lun = -999

       RETURN, 1

     END ;FUNCTION STREAM_FILE::INIT


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Open the stream
     ;
     ;
     ;-=================================================================
     PRO STREAM_FILE::OPEN

       IF self->IS_OPEN() THEN MESSAGE, 'File stream is already open.'
       
       GET_LUN, lun
       self.lun = lun
       OPENR, self.lun, self.filepath

       IF self.verbose THEN BEGIN
         PRINT, 'Opening file stream: ', self.filepath
       ENDIF

     END ;PRO STREAM_FILE::OPEN


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Close the stream
     ;
     ;
     ;-=================================================================
     PRO STREAM_FILE::CLOSE

       IF self->IS_OPEN() THEN BEGIN
         FREE_LUN, self.lun
         self.lun = -999
       ENDIF

     END ;PRO STREAM_FILE::CLOSE


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return TRUE if stream is open.
     ;   Otherwise reurn false.
     ;
     ;
     ;-=================================================================
     FUNCTION STREAM_FILE::IS_OPEN

       RETURN, (self.lun NE -999)

     END ;FUNCTION STREAM_FILE::IS_OPEN


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return TRUE if the end of the stream has been reached.
     ;   Otherwise reurn false.
     ;
     ;
     ;-=================================================================
     FUNCTION STREAM_FILE::END_OF_STREAM

       IF ~ self->IS_OPEN() THEN MESSAGE, 'File stream is not open.'
       
       RETURN, EOF(self.lun)

     END ;FUNCTION STREAM_FILE::END_OF_STREAM



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Read the next line from the file stream.
     ;
     ;
     ;-=================================================================
     FUNCTION STREAM_FILE::READ

       IF ~ self->IS_OPEN() THEN MESSAGE, 'File stream is not open.'

       line = ''
       READF, self.lun, line

       RETURN, line

     END ;FUNCTION STREAM_FILE::IS_OPEN



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   This object will allow file streams to be handeled
     ;   using the standard stream interface.
     ;
     ;
     ;-=================================================================
     PRO STREAM_FILE__DEFINE

       struct = { STREAM_FILE $
                  ,filepath:'' $
                  ,lun:0L $
                  ,verbose:0 $
                  ,INHERITS STREAM $
                }

     END ;PRO STREAM_FILE__DEFINE
