




;+======================================================================
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-07
;
; PURPOSE:
;   Define a MIR_LIST_IDL7 object.
;
;   This list holds a list of data as an array of pointers.
;   This allows the list to contain data of different types.
;
; PROGRAMMING NOTES:
;   This object should not be used along with LIST and HASH objects
;   from IDL 8.0.  In particular, RECURSIVE copying will not work if
;   an <::MIR_LIST_IDL7> object contains <::LIST> or <::HASH> elements. 
;   
;
;-======================================================================



     ;+=================================================================
     ; 
     ; This will return the pointer to the data at a given index.
     ;
     ;-=================================================================
     FUNCTION MIR_LIST_IDL7::GET_POINTER, index, _REF_EXTRA=extra

       RETURN, self->SIMPLELIST::GET(index, _STRICT_EXTRA=extra)

     END ; FUNCTION MIR_LIST_IDL7::GET_POINTER


     ;+=================================================================
     ; 
     ; This will return the pointer to the data at a given index.
     ;
     ;-=================================================================
     FUNCTION MIR_LIST_IDL7::GET_POINTER_ARRAY, _REF_EXTRA=extra

       RETURN, self->SIMPLELIST::TO_ARRAY()

     END ; FUNCTION MIR_LIST_IDL7::GET_POINTER_ARRAY



     ;+=================================================================
     ; 
     ; This will return the data at the given index.
     ;
     ; If multiple indexes are requested or /ALL is set,
     ; then this routine will attempt to return an array with all the
     ; data.  This will only work if all the data is of the same type.
     ;
     ;-=================================================================
     FUNCTION MIR_LIST_IDL7::GET, index, _REF_EXTRA=extra

       ; First get the pointers.
       ptr = self->GET_POINTER(index, _STRICT_EXTRA=extra)

       RETURN, *ptr
      
     END ; FUNCTION MIR_LIST_IDL7::GET




     ;+=================================================================
     ; 
     ; Add data to the list.
     ; 
     ;-=================================================================
     PRO MIR_LIST_IDL7::APPEND, data, NO_COPY=no_copy

       ; Create a new pointer to the data.
       ptr_data = PTR_NEW(data, NO_COPY=no_copy)

       ; Add the pointer to the list.
       self->SIMPLELIST::APPEND, ptr_data
       
     END ;PRO MIR_LIST_IDL7::SAVE




     ;+=================================================================
     ; 
     ; Set the data at a given element.
     ;
     ; If the given element has data, reuse the pointer.
     ; If not create a new one.
     ;
     ; 
     ;-=================================================================
     PRO MIR_LIST_IDL7::SET, index, data, NO_COPY=no_copy

       ; First get pointers to the existing data.
       ptr_data = self->GET_POINTER(index)

       ; Create a pointer to the new data
       IF PTR_VALID(ptr_data) THEN BEGIN
         IF KEYWORD_SET(NO_COPY) THEN BEGIN
           *ptr_data = TEMPORARY(data)
         ENDIF ELSE BEGIN
           *ptr_data = data
         ENDELSE
       ENDIF ELSE BEGIN
         self->SIMPLELIST::SET, index, PTR_NEW(data, NO_COPY=no_copy)
       ENDELSE
       
     END ;PRO MIR_LIST_IDL7::SET


     ;+=================================================================
     ; Alow the object to be set from an array.
     ;-=================================================================
     PRO MIR_LIST_IDL7::FROM_ARRAY, array

       ; Create a pointer array
       ptr_array = PTRARR(N_ELEMENTS(array))
       FOR ii=0,N_ELEMENTS(ptr_array)-1 DO BEGIN
         ptr_array[ii] = PTR_NEW(array[ii])
       ENDFOR

       self->SIMPLELIST::FROM_ARRAY, ptr_array

     END ;PRO MIR_LIST_IDL7::FROM_ARRAY



     ;+=================================================================
     ; PURPOSE:
     ;   Return and array with the data in the list.
     ;   This will fail if all of the data is not of the same type.
     ;
     ;-=================================================================
     FUNCTION MIR_LIST_IDL7::TO_ARRAY


       ; First get the pointers.
       ptr_array = self->GET_POINTER_ARRAY(_STRICT_EXTRA=extra)

       ; Now try to create an array with the results
       type = SIZE(*ptr_array[0], /TYPE)
       dim = SIZE(*ptr_array[0], /DIMENSIONS)
       IF dim[0] GT 0 THEN BEGIN
         array_size = [self.num_elements,dim]
       ENDIF ELSE BEGIN
         array_size = self.num_elements
       ENDELSE

       ; Create and fill an array of results.
       ; Here we have to handle things differently for structures and other data types.
       IF type EQ 8 THEN BEGIN

         ; For structures we handle things differently depending on whether the
         ; structures were named or not.
         structure_name = TAG_NAMES((*ptr_array[0])[0], /STRUCTURE_NAME)

         IF structure_name NE '' THEN BEGIN
           output = REPLICATE(CREATE_STRUCT(structure_name), array_size)
           FOR ii=0,self.num_elements-1 DO BEGIN
             IF TAG_NAMES(*ptr_array[ii], /STRUCTURE_NAME) NE structure_name THEN BEGIN
               MESSAGE, 'Named structures are not all the same type.'
             ENDIF
             output[ii,*] = *ptr_array[ii]
           ENDFOR
         ENDIF ELSE BEGIN
           tags = TAG_NAMES(*ptr_array[0])
           output = REPLICATE((*ptr_array[0])[0], array_size)
           FOR ii=0,self.num_elements-1 DO BEGIN
             IF ~ARRAY_EQUAL(TAG_NAMES(*ptr_array[ii]), tags) THEN BEGIN
               MESSAGE, 'Structure tags do not match.'
             ENDIF
             output[ii,*] = *ptr_array[ii]
           ENDFOR
         ENDELSE
         
       ENDIF ELSE BEGIN

         output = MAKE_ARRAY(array_size, TYPE=type, /NOZERO)
         FOR ii=0,self.num_elements-1 DO BEGIN
           IF SIZE(*ptr_array[ii], /TYPE) NE type THEN BEGIN
             MESSAGE, 'Data is not all of the same type.'
           ENDIF
           output[ii,*] = *ptr_array[ii]
         ENDFOR
       ENDELSE

          

       RETURN, output

     END ; FUNCTION MIR_LIST_IDL7::TO_ARRAY



     ;+=================================================================
     ; PURPOSE:
     ;   Return a structure with the data in the list.
     ;
     ;-=================================================================
     FUNCTION MIR_LIST_IDL7::TO_STRUCTURE


       ; First get the pointers.
       ptr_array = self->GET_POINTER_ARRAY(_STRICT_EXTRA=extra)

       FOR ii = 0, N_ELEMENTS(ptr_array)-1 DO BEGIN
         tag = '_'+STRING(ii, '(i0)')
         output = BUILD_STRUCTURE(tag, *ptr_array[ii], output)
       ENDFOR          

       RETURN, output

     END ; FUNCTION MIR_LIST_IDL7::TO_STRUCTURE




     ;+=================================================================
     ;
     ; Delete the data at the given index.
     ;
     ; If /ALL is given then all data will be deleted.
     ;
     ; 
     ;-=================================================================
     PRO MIR_LIST_IDL7::REMOVE, index $

                       ,LAST=last $

                       ,HEAP_FREE=heap_free $
                       ,OBJ_FREE=obj_free $
                       ,PTR_FREE=ptr_free $

                       ,_REF_EXTRA=extra
       
       IF self.num_elements EQ 0 THEN BEGIN
         RETURN
       ENDIF

       ; Extract the elements of the list that we are going to remove.
       IF N_PARAMS() EQ 1 OR KEYWORD_SET(last) THEN BEGIN
         deleted_rows = self->GET_POINTER(index, LAST=last, _EXTRA=extra)
       ENDIF ELSE BEGIN
         deleted_rows = self->GET_POINTER_ARRAY(_EXTRA=extra)
       ENDELSE

       heap_free = KEYWORD_SET(heap_free)
       obj_free = KEYWORD_SET(obj_free)
       ptr_free = KEYWORD_SET(ptr_free)

       IF heap_free OR obj_free OR ptr_free THEN BEGIN
         IF heap_free THEN BEGIN
           obj_free = 1
           ptr_free = 1
         ENDIF

         ; If the user wants we can destroy whatever the list was pointing to.
         FOR ii=0,N_ELEMENTS(deleted_rows)-1 DO BEGIN
           HEAP_FREE, *deleted_rows[ii], OBJ=obj_free, PTR=ptr_free
         ENDFOR
       ENDIF

       ; The elements that we extracted are pointers,
       ; so we need to free them here.
       PTR_FREE, deleted_rows

       ; Now call the super method
       self->SIMPLELIST::REMOVE, index, _STRICT_EXTRA=extra


     END ;PRO MIR_LIST_IDL7::REMOVE



     ;+=================================================================
     ; 
     ; Add the contents of one list or array to the end of this list.
     ;
     ; This method just checks if the input was an object or not
     ; then sends the input to JOIN_LIST or JOIN_ARRAY.
     ; 
     ;-=================================================================
     PRO MIR_LIST_IDL7::JOIN, input, NO_COPY=no_copy

       CASE SIZE(input, /TYPE) OF
         11: self->JOIN_LIST, input, NO_COPY=no_copy
         ELSE: self->JOIN_ARRAY, input, NO_COPY=no_copy
       ENDCASE
       
     END ;PRO MIR_LIST_IDL7::JOIN



     ;+=================================================================
     ; 
     ; Add the contents of one list to the end of another.
     ; 
     ;-=================================================================
     PRO MIR_LIST_IDL7::JOIN_LIST, list_in, NO_COPY=no_copy

       num_in = list_in->N_ELEMENTS()
       FOR ii=0,num_in-1 DO BEGIN

         ; Get the pointer to the data, or make a
         ; pointer to a copy of the data.
         IF KEYWORD_SET(no_copy) THEN BEGIN
           ptr_data = list_in->GET_POINTER(ii)
         ENDIF ELSE BEGIN
           ptr_data = PTR_NEW(list_in->GET(ii))
         ENDELSE

         ; Add the pointer to the list.
         self->SIMPLELIST::APPEND, ptr_data
       ENDFOR
       
     END ;PRO MIR_LIST_IDL7::JOIN_LIST



     ;+=================================================================
     ; 
     ; Add the contents of an array to the end of the list.
     ; This will add each element in the array to the list.
     ;
     ; This is different from APPEND which would add the entire
     ; array as one element in the list.
     ; 
     ;-=================================================================
     PRO MIR_LIST_IDL7::JOIN_ARRAY, array, NO_COPY=no_copy

       num_in = N_ELEMENTS(array)
       FOR ii=0,num_in-1 DO BEGIN

         self->APPEND, array[ii], NO_COPY=no_copy

       ENDFOR
       
     END ;PRO MIR_LIST_IDL7::JOIN_ARRAY



     ;+=================================================================
     ; 
     ; Reorder the contents of the list.
     ;
     ; (A list object is actually good for this since a list only
     ;  holds pointers to the data. yeay!)
     ;
     ; NOTE: This does not check the order array.
     ; 
     ;-=================================================================
     PRO MIR_LIST_IDL7::REORDER, order
       IF N_ELEMENTS(order) NE self->N_ELEMENTS() THEN BEGIN
         MESSAGE, 'Size of order array does not match the size of the list.'
       ENDIF

       self.ptr_array = TEMPORARY(self.ptr_array[order])
       
     END ;PRO MIR_LIST_IDL7::REORDER



     ;+=================================================================
     ; PURPOSE:
     ;   Return a copy of the list.
     ;
     ; KEYWORDS:
     ;   /COPYDATA
     ;       If set, any objects contained in the list are copied, 
     ;       rather than simply copying the object references.
     ;
     ;   /RECURSIVE
     ;       If set, any objects contained in the list are copied
     ;       recursively.  That is <::GET_COPY(/RECURSIVE)> is called
     ;       on each object and the result saved in the new list copy.
     ;
     ;       /RECURSIVE impiles /COPYDATA.
     ;
     ; PROGRAMMING NOTES:
     ;   This routine will not work correctly for recursive copying if
     ;   LIST or HASH objects from IDL 8.0 are used.
     ;
     ;-=================================================================
     FUNCTION MIR_LIST_IDL7::GET_COPY, COPYDATA=copydata, RECURSIVE=recursive

       copydata = KEYWORD_SET(copydata)
       recursive = KEYWORD_SET(recursive)

       ; Create a new object
       new_copy = OBJ_NEW(OBJ_CLASS(self))

       new_copy.num_elements = self.num_elements

       IF PTR_VALID(self.ptr_array) THEN BEGIN
         ; Make an new pointer to a pointer array for the data.
         new_copy.ptr_array = PTR_NEW(PTRARR(self.num_elements))

         ; Fill the pointer array with copies of the data.
         FOR ii_list=0,self.num_elements-1 DO BEGIN

           data = self->GET(ii_list)

           IF OBJ_VALID(data[0]) AND (copydata OR recursive) THEN BEGIN
             num_elements_data = N_ELEMENTS(data)
             FOR ii_data=0,num_elements_data-1 DO BEGIN
               data[ii_data] = data[ii_data]->GET_COPY(RECURSIVE=recursive)
             ENDFOR
           ENDIF

           new_copy->SET, ii_list, data

         ENDFOR
       ENDIF

       RETURN, new_copy
  

     END ;FUNCTION SIMPLELIST::GET_COPY



     ;+=================================================================
     ; 
     ; Clean up when the object is destroyed.
     ; Delete all pointers and the list object.
     ;
     ;-=================================================================
     PRO MIR_LIST_IDL7::CLEANUP, _REF_EXTRA=extra

       CATCH, error
       IF ~ error THEN BEGIN
         self->REMOVE, /ALL $
                       ,_STRICT_EXTRA=extra
       ENDIF ELSE BEGIN
         CATCH, /CANCEL
         HELP, /LAST_MESSAGE
       ENDELSE

     END ; PRO MIR_LIST_IDL7::CLEANUP



     ;+=================================================================
     ;
     ; Initialize the object
     ;
     ;-=================================================================
     FUNCTION MIR_LIST_IDL7::INIT, input

       RETURN, self->SIMPLELIST::INIT(input)

     END ; FUNCTION MIR_LIST_IDL7::INIT

    
     ;+=================================================================
     ; 
     ; Define the LIST object.
     ;
     ; This list hold a list of data as an array of pointers.
     ; This allows the list to contain data of different types.
     ;
     ; NOTE:  If the speed of conversion between the list object and
     ;        an array is an issue, a simplelist should be used
     ;        instead.
     ;
     ;-=================================================================
     PRO MIR_LIST_IDL7__DEFINE
       
       struct = {MIR_LIST_IDL7 $
                 ,INHERITS SIMPLELIST $
                }

     END ;PRO MIR_LIST_IDL7__DEFINE
