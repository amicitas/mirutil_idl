

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-07
;
; PURPOSE:
;   Define a simplelist object.  
;
;   The simplelist object basically acts an array with the following
;   properties:
;     It can be empty.
;     Elements can be added or removed at any location.
;
; TO DO:
;   At this point I make a number of calls to SIMPLELIST::FROM_ARRAY.
;   This is not really a good way to do things as it prevents
;   reimplementation of FROM_ARRAY.  A better way is to have two
;   methods, FROM_ARRAY, and SET_INTERNAL_ARRAYS (or something.)
;
;-======================================================================



     ;+=================================================================
     ; Remove an value
     ; If an index is not given then the last item will be removed
     ;-=================================================================
     PRO SIMPLELIST::REMOVE, index_in, ALL=all, LAST=last

       IF N_PARAMS() EQ 0 OR KEYWORD_SET(all) THEN BEGIN
         self->SIMPLELIST::DEALLOCATE
         RETURN
       ENDIF
         
         
       IF KEYWORD_SET(last) THEN BEGIN
         index = self->N_ELEMENTS()-1
       ENDIF ELSE BEGIN
         index = index_in
       ENDELSE
       
       self->CHECK_INDEX, index

       IF N_ELEMENTS(index) EQ 1 THEN BEGIN
         index = [index, index]
       ENDIF ELSE BEGIN
         index = index
       ENDELSE

       CASE 1 OF
         self.num_elements EQ index[1]-index[0]+1: BEGIN
           self->SIMPLELIST::DEALLOCATE
           self.num_elements = 0
         END
         index[0] EQ 0: BEGIN
           new_array = (*self.ptr_array)[index[1]+1:self.num_elements-1]
           self->SIMPLELIST::FROM_ARRAY, new_array
         END
         index[1] EQ self.num_elements-1: BEGIN
           new_array = (*self.ptr_array)[0:index[0]-1]
           self->SIMPLELIST::FROM_ARRAY, new_array
         END
         ELSE: BEGIN
           new_array = [(*self.ptr_array)[0:index[0]-1] $
                        ,(*self.ptr_array)[index[1]+1:self.num_elements-1]]
           self->SIMPLELIST::FROM_ARRAY, new_array
         ENDELSE
       ENDCASE
 

     END



     ;+=================================================================
     ; Insert a value at the given index.
     ; Will instert before the given index.
     ;-=================================================================
     PRO SIMPLELIST::INSERT, index, value

       self->CHECK_DIMENSIONS, value

       IF (index GT self.num_elements) $
         OR (index LT 0) THEN BEGIN
         MESSAGE, 'Index is out of range.'
       ENDIF

       CASE 1 OF
         index EQ 0: BEGIN
           new_array = [value, (*self.ptr_array)]
         END
         index EQ self.num_elements: BEGIN
           new_array = [(*self.ptr_array), value] 
         END
         ELSE: BEGIN
           new_array = [(*self.ptr_array)[0:index-1] $
                        ,value $
                        ,(*self.ptr_array)[index:self.num_elements-1]]
         ENDELSE
       ENDCASE

       self->SIMPLELIST::FROM_ARRAY, new_array

     END



     ;+=================================================================
     ; Add a value to the end.
     ;-=================================================================
     PRO SIMPLELIST::APPEND, value, NO_COPY=no_copy


       self->CHECK_DIMENSIONS, value
         
       IF self.num_elements EQ 0 THEN BEGIN
         IF KEYWORD_SET(no_copy) THEN BEGIN
           new_array = TEMPORARY(value)
         ENDIF ELSE BEGIN
           new_array = value
         ENDELSE
       ENDIF ELSE BEGIN
         IF KEYWORD_SET(no_copy) THEN BEGIN
           new_array = [TEMPORARY(*self.ptr_array), TEMPORARY(value)]
         ENDIF ELSE BEGIN
           new_array = [TEMPORARY(*self.ptr_array), value]
         ENDELSE
       ENDELSE

       self->SIMPLELIST::FROM_ARRAY, new_array, /NO_COPY

     END



     ;+=================================================================
     ; Return the number of elements
     ;-=================================================================
     FUNCTION SIMPLELIST::N_ELEMENTS

       RETURN, self.num_elements

     END



     ;+=================================================================
     ; Check that an input array is one dimensional
     ;-=================================================================
     PRO SIMPLELIST::CHECK_DIMENSIONS, input

       IF SIZE(input, /N_DIMENSIONS) GT 1 THEN BEGIN
         MESSAGE, 'LIST objects are 1 dimentional.'
       ENDIF

     END



     ;+=================================================================
     ; Check the range of the index
     ;-=================================================================
     PRO SIMPLELIST::CHECK_INDEX, index
       ON_ERROR, 2

       CASE N_ELEMENTS(index) OF
         1: BEGIN
           IF (index GT self.num_elements-1) $
             OR (index LT 0) THEN BEGIN
             MESSAGE, 'Index is out of range.'
           ENDIF
         END
         2: BEGIN
           IF (index[1] LT index[0]) $
             OR (index[0] LT 0) $
             OR (index[1] GT self.num_elements-1) $
             THEN BEGIN
             MESSAGE, 'Invalid index range.'
           ENDIF
         END
         ELSE: BEGIN
           MESSAGE, 'Invalid index input.'
         ENDELSE
       ENDCASE

     END



     ;+=================================================================
     ; Alow the object to be set from an array.
     ;-=================================================================
     PRO SIMPLELIST::SET, index, array

       self->CHECK_DIMENSIONS, array
       self->CHECK_INDEX, index

       CASE N_ELEMENTS(index) OF
         1: BEGIN
           IF N_ELEMENTS(array) EQ 1 THEN BEGIN
             (*self.ptr_array)[index] = array
           ENDIF ELSE BEGIN
             MESSAGE, 'Too many values given.'
           ENDELSE
         END
         2: BEGIN
           IF N_ELEMENTS(array) EQ index[1]-index[0]+1 THEN BEGIN
             (*self.ptr_array)[index[0]:index[1]] = array
           ENDIF ELSE BEGIN
             MESSAGE, 'Number of values does not match index range.'
           ENDELSE
         END
       ENDCASE

     END



     ;+=================================================================
     ; PURPOSE:
     ;   Return elements of the simplelist as an array.
     ;
     ;-=================================================================
     FUNCTION SIMPLELIST::GET, index, LAST=last

       CATCH, error
       IF error NE 0 THEN BEGIN
         CATCH, /CANCEL

         ON_ERROR, 2

         IF self.num_elements EQ 0 THEN BEGIN
           MESSAGE, 'LIST object is empty.'
         ENDIF

         self->CHECK_INDEX, index

         MESSAGE, /REISSUE
       ENDIF

       IF KEYWORD_SET(last) THEN BEGIN
         RETURN, (*self.ptr_array)[self->N_ELEMENTS()-1]
       ENDIF

       RETURN, (*self.ptr_array)[index]

     END ; FUNCTION SIMPLELIST::GET



     ;+=================================================================
     ; PURPOSE:
     ;   Allow the list to be set from an array.
     ;-=================================================================
     PRO SIMPLELIST::FROM_ARRAY, array $
                                 ,NO_CHECK=no_check $
                                 ,NO_COPY=no_copy

       IF ~ KEYWORD_SET(no_check) THEN BEGIN
         self->CHECK_DIMENSIONS, array
       ENDIF

       self->SIMPLELIST::DEALLOCATE
       
       self.ptr_array = PTR_NEW(array, NO_COPY=no_copy)
       self.num_elements = N_ELEMENTS(*self.ptr_array)

     END ;PRO SIMPLELIST::FROM_ARRAY



     ;+=================================================================
     ; PURPOSE:
     ;   Add each element of an array to the list.
     ;-=================================================================
     PRO SIMPLELIST::JOIN, array $
                           ,NO_CHECK=no_check $
                           ,NO_COPY=no_copy

       IF self.num_elements EQ 0 THEN BEGIN
         self->SIMPLELIST::FROM_ARRAY, array $
                                       ,NO_CHECK=no_check $
                                       ,NO_COPY=no_copy
         RETURN
       ENDIF



       IF ~ KEYWORD_SET(no_check) THEN BEGIN
         self->CHECK_DIMENSIONS, array
       ENDIF

       IF KEYWORD_SET(no_copy) THEN BEGIN
         new_array = [TEMPORARY(*self.ptr_array), TEMPORARY(array)]
       ENDIF ELSE BEGIN
         new_array = [TEMPORARY(*self.ptr_array), array]
       ENDELSE

       self->SIMPLELIST::FROM_ARRAY, new_array, /NO_CHECK, /NO_COPY

     END ;PRO SIMPLELIST::JOIN



     ;+=================================================================
     ; PURPOSE:
     ;   Return the simplelist as an array.
     ;
     ;-=================================================================
     FUNCTION SIMPLELIST::TO_ARRAY

       IF self.N_ELEMENTS() GT 0 THEN BEGIN
         RETURN, (*self.ptr_array)
       ENDIF ELSE BEGIN
         RETURN, []
       ENDELSE

     END ;FUNCTION SIMPLELIST::TO_ARRAY



     ;+=================================================================
     ; PURPOSE:
     ;   Copy the data from another simplelist.
     ;-=================================================================
     PRO SIMPLELIST::COPY_FROM, data

       self->SIMPLELIST::DEALLOCATE

       IF ~ PTR_VALID(data.ptr_array) THEN RETURN

       self->SIMPLELIST::FROM_ARRAY, *data.ptr_array
       self.num_elements = N_ELEMENTS(*self.ptr_array)


     END  ;  PRO SIMPLELIST::COPY_FROM



     ;+=================================================================
     ; Deallocate the array
     ;-=================================================================
     PRO SIMPLELIST::DEALLOCATE, _NULL=null

       PTR_FREE, self.ptr_array
       self.num_elements=0

     END



     ;+=================================================================
     ; Cleanup on destruction.
     ;-=================================================================
     PRO SIMPLELIST::CLEANUP, _REF_EXTRA=extra

       self->DEALLOCATE, _STRICT_EXTRA=extra

     END



     ;+=================================================================
     ; Initialize the object
     ;-=================================================================
     FUNCTION SIMPLELIST::INIT, num_elements, _EXTRA=extra

       self.num_elements = 0

       IF N_ELEMENTS(num_elements) GT 0 THEN BEGIN
         self->CHECK_DIMENSIONS, num_elements

         ptr_array = PTR_NEW(MAKE_ARRAY(num_elements, _EXTRA=extra))
         self.num_elements = num_elements

       ENDIF ELSE BEGIN
         self.num_elements = 0
       ENDELSE
           
       RETURN, 1

     END ; FUNCTION SIMPLELIST::INIT



     ;+=================================================================
     ; PURPOSE:
     ;   Object definition for the SIMPLELIST object.
     ;
     ; DESCRIPTION:
     ;   This works by pointing to an array that contains the data.
     ;
     ;-=================================================================
     PRO SIMPLELIST__DEFINE
       
       struct = { SIMPLELIST $
                  ,ptr_array:PTR_NEW() $
                  ,num_elements:0L $
                  ,INHERITS OBJECT $
                }
                 
     END ;PRO SIMPLELIST__DEFINE
