
;+==============================================================================
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
; PURPOSE:
;   A more object oriented version of the LIST object.
;
; DESCRIPTION:
;   IDL 8.0 introduced a <LIST::> object (yeay!).  This object though is very 
;   limited and is expected to be used in a procedural manner (boo).
;
;   This <MIR_LIST::> object is intended to generalize the list object.
;
;-=============================================================================



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Remove items from the list.
     ;  
     ;   IDL is terrible, I wish I was programing in Python.
     ;
     ;   When calling ::REMOVE, /ALL on an empty list, IDL throws up an
     ;   error about not having a pointer.
     ;
     ;   The ::REMOVE() method however works fine 
     ;
     ;-=================================================================
     PRO MIR_LIST::REMOVE, indices, ALL=all

       IF KEYWORD_SET(all) THEN BEGIN
         IF self.N_ELEMENTS() EQ 0 THEN BEGIN
           RETURN
         ENDIF
       ENDIF

       self.LIST::REMOVE, indices, ALL=all
     END ;  PRO MIR_LIST::REMOVE, indicies, ALL=all



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return the number of elements in the list.
     ;
     ;-=================================================================
     FUNCTION MIR_LIST::N_ELEMENTS

       RETURN, N_ELEMENTS(self)

     END ; FUNCTION MIR_LIST::N_ELEMENTS



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Creates a list object from a structure.
     ;   Tag name information will be lost in this conversion.
     ;-=================================================================
     PRO MIR_LIST::FROM_STRUCTURE, structure

       FOR ii=0,N_TAGS(structure)-1 DO BEGIN
         self.ADD, structure.(ii)
       ENDFOR

     END ;  PRO MIR_LIST::FROM_STRUCTURE



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Define the <MIR_LIST::> object.
     ;
     ;-=================================================================
     PRO MIR_LIST__DEFINE

       struct = {MIR_LIST $
                 ,INHERITS LIST}

     END ; PRO MIR_LIST__DEFINE
