

;+==============================================================================
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
; PURPOSE:
;   IDL 8.0 now defines a <HASH::> object.  Here I extend that object to
;   create an ordered hash object.
;
; DESCRIPTION:
;   The goal here is to retain the speed of the <HASH::> object for key 
;   lookups while also allowing ordered retrieval.
;
;   NOTE: This might be really slow compared to a normal <HASH::> object
;         I have not tested it.
;   
;
;-=============================================================================





     ;+=================================================================
     ; PURPOSE:
     ;   Initialize the object
     ;
     ;   As far as I know there is no way to do parameter inheritance
     ;   for positional parameters.  For this reason I cannot allow
     ;   keys value pairs to be given as positional parameters.
     ;
     ;-=================================================================
     FUNCTION MIR_ORDERED_HASH::INIT, arg1, arg2, _REF_EXTRA=extra
      
       self.index = LIST()

       IF ISA(arg1) THEN BEGIN
         IF ISA(arg1, 'struct') THEN BEGIN
           self.index.ADD, TAG_NAMES(arg1), /EXTRACT
         ENDIF ELSE BEGIN
           self.index.ADD, arg1, /EXTRACT
         ENDELSE
       ENDIF

       status = self.HASH::INIT(arg1, arg2, _EXTRA=extra)

       RETURN, status

     END ; FUNCTION MIR_ORDERED_HASH::INIT


     ;+=================================================================
     ; PURPOSE:
     ;   Return an array containing the keys.
     ;
     ;-=================================================================
     FUNCTION MIR_ORDERED_HASH::keys
      
       RETURN, self.index

     END ; FUNCTION MIR_ORDERED_HASH::keys



     ;+=================================================================
     ; PURPOSE:
     ;   Return an array containing the values.
     ;
     ;-=================================================================
     FUNCTION MIR_ORDERED_HASH::values

       output = LIST()
       FOR ii=0,self.n_Elements()-1 DO BEGIN
         output.add, self[(self.index)[ii]]
       ENDFOR

       RETURN, output

     END ; FUNCTION MIR_ORDERED_HASH::values



     ;+=================================================================
     ; PURPOSE:
     ;   Return an structure containing all of the HASH elements.
     ;
     ;-=================================================================
     FUNCTION MIR_ORDERED_HASH::toStruct, RECURSIVE=recursive
      
       keys = self.keys()
       values = self.values()

       struct = {}
       FOR ii=0, self.n_Elements()-1 DO BEGIN
         struct = CREATE_STRUCT(struct, keys[ii], values[ii])
       ENDFOR

       RETURN, struct

     END ; FUNCTION MIR_ORDERED_HASH::toStruct



     ;+=================================================================
     ; PURPOSE:
     ;   Return the number of elements in the hash
     ;
     ;-=================================================================
     FUNCTION MIR_ORDERED_HASH::n_Elements

       RETURN, N_ELEMENTS(self)

     END ; FUNCTION MIR_ORDERED_HASH::n_Elements



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Reimplement the method for brackets, [], on the left hand
     ;   side of the equals sign.
     ;
     ;-=================================================================
     PRO MIR_ORDERED_HASH::_overloadBracketsLeftSide, objref $
                                                      ,value $
                                                      ,isrange $
                                                      ,subscript1 $
                                                      ,subscript2 $
                                                      ,subscript3 $
                                                      ,subscript4 $
                                                      ,subscript5 $
                                                      ,subscript6 $
                                                      ,subscript7 $
                                                      ,subscript8

       IF ~ self.HASKEY(subscript1) THEN BEGIN
         self.index.add, subscript1
       ENDIF

       self.HASH::_overloadBracketsLeftSide, objref $
          ,value $
          ,isrange $
          ,subscript1 $
          ,subscript2 $
          ,subscript3 $
          ,subscript4 $
          ,subscript5 $
          ,subscript6 $
          ,subscript7 $
          ,subscript8          

       
     END ;MIR_ORDERED_HASH::_overloadBracketsLeftSide



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Reimplement the method for brackets, [], on the left hand
     ;   side of the equals sign.
     ;
     ;-=================================================================
     FUNCTION MIR_ORDERED_HASH::_overloadBracketsRightSide, isrange $
                                                            ,subscript1 $
                                                            ,subscript2 $
                                                            ,subscript3 $
                                                            ,subscript4 $
                                                            ,subscript5 $
                                                            ,subscript6 $
                                                            ,subscript7 $
                                                            ,subscript8
       ON_ERROR, 2

       IF ISA(subscript1, 'string') THEN BEGIN
         RETURN, self.HASH::_overloadBracketsRightSide( isrange $
                                                        ,subscript1 $
                                                        ,subscript2 $
                                                        ,subscript3 $
                                                        ,subscript4 $
                                                        ,subscript5 $
                                                        ,subscript6 $
                                                        ,subscript7 $
                                                        ,subscript8 )    
       ENDIF ELSE BEGIN
         keys = self.index._overloadBracketsRightSide( isrange $
                                                       ,subscript1 $
                                                       ,subscript2 $
                                                       ,subscript3 $
                                                       ,subscript4 $
                                                       ,subscript5 $
                                                       ,subscript6 $
                                                       ,subscript7 $
                                                       ,subscript8 )

         RETURN, self[keys]
       ENDELSE
       
     END ;MIR_ORDERED_HASH::_overloadBracketsLeftSide



     ;+=========================================================================
     ;
     ; PURPOSE:
     ;   Setup the MIR_ORDERED_HASH object
     ;
     ;
     ;-=========================================================================
     PRO MIR_ORDERED_HASH__DEFINE

       struct = { MIR_ORDERED_HASH $
                  ,index:LIST() $
                  ,INHERITS HASH $
                }

     END ; PRO MIR_PLOT_WINDOW
