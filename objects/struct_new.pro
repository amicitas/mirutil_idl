

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-07
;
; PURPOSE:
;   Retrive an initialized structure.
;
; TAGS:
;   EXCLUDE_FILE_HEADER
;
;-======================================================================


     ;+=================================================================
     ;
     ;
     ; PURPOSE:
     ;   Retrive an initialized structure.
     ;
     ; DESCRIPTION:
     ;   IDL does not have the ability to initialize named structures.
     ;
     ;   While in some cases this can be dealt with by using anonymous
     ;   arrays, or by using a function to fill the defaults, I
     ;   want a way that is compatable with object definitions.
     ;
     ;   What this function will do is to initialize an object,
     ;   extract its structure, delete the object and return the
     ;   structrue.
     ;
     ;   NOTE: This will only work for object that inherit from 
     ;         'OBJECT'
     ;
     ;
     ;-=================================================================
     FUNCTION STRUCT_NEW, object_name

       obj = OBJ_NEW(object_name)

       struct = obj->GET_STRUCT()
       
       OBJ_DELETE, obj

       RETURN, struct

     END ;FUNCTION STRUCT_NEW
