


;+======================================================================
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;
; DATE:
;   2009-09
;
; PURPOSE:
;   Create an object to use as a file stream.
;
;-======================================================================




     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Initialize the file stream
     ;
     ;
     ;-=================================================================
     FUNCTION STREAM_ARRAY::INIT, array

       self.ptr_array = PTR_NEW(array)
       self.n_elements = N_ELEMENTS(array)
       self.index = -1

       RETURN, 1

     END ;FUNCTION STREAM_ARRAY::INIT


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Open the stream
     ;
     ;
     ;-=================================================================
     PRO STREAM_ARRAY::OPEN

       IF self->IS_OPEN() THEN MESSAGE, 'Array stream is already open.'
       
       self.index = 0

     END ;PRO STREAM_ARRAY::OPEN


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Close the stream
     ;
     ;
     ;-=================================================================
     PRO STREAM_ARRAY::CLOSE

       self.index = -1

     END ;PRO STREAM_ARRAY::CLOSE


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return TRUE if stream is open.
     ;   Otherwise reurn false.
     ;
     ;
     ;-=================================================================
     FUNCTION STREAM_ARRAY::IS_OPEN

       RETURN, (self.index NE -1)

     END ;FUNCTION STREAM_ARRAY::IS_OPEN


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return TRUE if the end of the stream has been reached.
     ;   Otherwise reurn false.
     ;
     ;
     ;-=================================================================
     FUNCTION STREAM_ARRAY::END_OF_STREAM

       IF ~ self->IS_OPEN() THEN MESSAGE, 'Array stream is not open.'
       
       RETURN, (self.index GE self.n_elements)

     END ;FUNCTION STREAM_ARRAY::END_OF_STREAM



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Read the next line from the file stream.
     ;
     ;
     ;-=================================================================
     FUNCTION STREAM_ARRAY::READ

       IF ~ self->IS_OPEN() THEN MESSAGE, 'Array stream is not open.'

       result = (*self.ptr_array)[self.index]
       self.index += 1
       RETURN, TEMPORARY(result)

     END ;FUNCTION STREAM_ARRAY::IS_OPEN



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   This object will allow arbitrary data streams to be handeled
     ;   with a uniform interface and persistant data.
     ;
     ;
     ;-=================================================================
     PRO STREAM_ARRAY__DEFINE

       struct = { STREAM_ARRAY $
                  ,ptr_array:PTR_NEW() $
                  ,n_elements:0L $
                  ,index:-1 $
                  ,INHERITS STREAM $
                }

     END ;PRO STREAM_ARRAY__DEFINE
