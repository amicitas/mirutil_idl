


PRO TEST_N_ELEMENTS_0, KEYWORD=keyword
  test = 0
  IF N_ELEMENTS(keyword) NE 0 THEN BEGIN
    test = 1
  ENDIF ELSE BEGIN
    test = 0
  ENDELSE
END

PRO TEST_N_ELEMENTS_1, KEYWORD=keyword
  test = 0
  IF N_ELEMENTS(keyword) NE 0 THEN BEGIN
    test = 1
  ENDIF ELSE BEGIN
    test = 0
  ENDELSE
END


PRO TEST_ISA_0, KEYWORD=keyword
  test = 0
  IF ISA(keyword) THEN BEGIN
    test = 1
  ENDIF ELSE BEGIN
    test = 0
  ENDELSE
END

PRO TEST_ISA_1, KEYWORD=keyword
  test = 0
  IF ISA(keyword) THEN BEGIN
    test = 1
  ENDIF ELSE BEGIN
    test = 0
  ENDELSE
END

PRO TEST_KEYWORD_SET_0, KEYWORD=keyword
  test = 0
  IF KEYWORD_SET(keyword) THEN BEGIN
    test = 1
  ENDIF ELSE BEGIN
    test = 0
  ENDELSE
END

PRO TEST_KEYWORD_SET_1, KEYWORD=keyword
  test = 0
  IF KEYWORD_SET(keyword) THEN BEGIN
    test = 1
  ENDIF ELSE BEGIN
    test = 0
  ENDELSE
END


PRO TEST_EMPTY_0, KEYWORD=keyword
  test = 0
  IF 0 THEN BEGIN
    test = 1
  ENDIF ELSE BEGIN
    test = 0
  ENDELSE
END

PRO TEST_EMPTY_1, KEYWORD=keyword
  test = 0
  IF 1 THEN BEGIN
    test = 1
  ENDIF ELSE BEGIN
    test = 0
  ENDELSE
END

PRO TEST_BLANK_0
  test = 0
  IF 0 THEN BEGIN
    test = 1
  ENDIF ELSE BEGIN
    test = 0
  ENDELSE
END

PRO TEST_BLANK_1
  test = 0
  IF 1 THEN BEGIN
    test = 1
  ENDIF ELSE BEGIN
    test = 0
  ENDELSE
END


PRO TEST_20110902, num, PROFILE=profile

  MIR_DEFAULT, num, 1E6
  num = LONG(num)

  IF KEYWORD_SET(profile) THEN MIR_PROFILER, /START

  time_last = SYSTIME(/SECONDS)
  format = '(a-20,": ", f0.4)'


  FOR ii=0L,num DO BEGIN
    TEST_N_ELEMENTS_0
  ENDFOR
  FOR ii=0L,num DO BEGIN
    TEST_N_ELEMENTS_1, /KEYWORD
  ENDFOR

  time = SYSTIME(/SECONDS)
  PRINT, FORMAT=format, 'TEST_N_ELEMENTS', time - time_last
  time_last = time


  FOR ii=0L,num DO BEGIN
    TEST_ISA_0
  ENDFOR
  FOR ii=0L,num DO BEGIN
    TEST_ISA_1, /KEYWORD
  ENDFOR

  time = SYSTIME(/SECONDS)
  PRINT, FORMAT=format, 'TEST_ISA', time - time_last
  time_last = time

  
  FOR ii=0L,num DO BEGIN
    TEST_KEYWORD_SET_0
  ENDFOR
  FOR ii=0L,num DO BEGIN
    TEST_KEYWORD_SET_1, /KEYWORD
  ENDFOR

  time = SYSTIME(/SECONDS)
  PRINT, FORMAT=format, 'TEST_KEYWORD_SET', time - time_last
  time_last = time

  
  FOR ii=0L,num DO BEGIN
    TEST_EMPTY_0
  ENDFOR
  FOR ii=0L,num DO BEGIN
    TEST_EMPTY_1, /KEYWORD
  ENDFOR

  time = SYSTIME(/SECONDS)
  PRINT, FORMAT=format, 'TEST_EMPTY', time - time_last
  time_last = time


  
  FOR ii=0L,num DO BEGIN
    TEST_BLANK_0
  ENDFOR
  FOR ii=0L,num DO BEGIN
    TEST_BLANK_1
  ENDFOR

  time = SYSTIME(/SECONDS)
  PRINT, FORMAT=format, 'TEST_BLANK', time - time_last
  time_last = time


    IF KEYWORD_SET(profile) THEN MIR_PROFILER, /REPORT

END

