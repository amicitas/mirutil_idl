

;+==============================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
; DATE:
;   2012-11-14
;
; PURPOSE:
;   Evaluation of Voigt profiles is very slow.  

;   For fitting of the Ar16+  spectrum, which has many sattelite lines, voigt 
;   evalutaion ends up taking a long time, making analysis a very slow process.
;
;   In my current fitting model all of the satellite lines have the same
;   natural line width.  Takeing advatage of this, I had the idea that I could
;   potentially speed up my fitting process by evaluating the voigt profile
;   for the satellites lines once, then use interpolation to construct the
;   spectrum.
;
;   This routine is mean to test what amount of performance improment might be
;   possible using this strategy.
;
;
; RESULTS:
;   num_cycles:       100000
;   
;   Module                              Count      Time (s)      Avg. (s)
;   Voigt Evaluation                        1      6.265657      6.265657
;   Interpolation double resolution         1      2.153752      2.153752
;   Interpolation                           1      1.977271      1.977271

;
;   From these results it looks like the maximum improvement from this
;   strategy would be a factor of ~3.  This is a significant improvement, but 
;   not the sort of large gain I was hoping for. 
;
;   When using a cuttof value, the performance gain will be even less.
;
;
;   Note:  I also explored doing the voigt calculate in single precision and
;          found no performance difference.
;
;-==============================================================================



     
     PRO TEST_20121114, STOP=stop


       ;cutoff = 1E-4

       voigt = OBJ_NEW('MIR_VOIGT', {intensity:1.0, location:0.0, sigma:1.0, gamma:0.1}) 

       x = (FINDGEN(200)-100.)/3.
       x_new = x+0.015
       
      
       num_cycles = 10000
       
       MIR_TIMER, /RESET

       MIR_TIMER, 'Voigt Evaluation', /START
       FOR ii=0,num_cycles-1 DO BEGIN
         y_new_voigt = voigt.EVALUATE(x_new, CUTOFF=cutoff)
       ENDFOR
       MIR_TIMER, 'Voigt Evaluation', /STOP
       
       

       MIR_TIMER, 'Interpolation', /START
       y = voigt.EVALUATE(x, CUTOFF=cutoff)

       FOR ii=0,num_cycles-1 DO BEGIN
         y_new_interpol = INTERPOL(y, x, x_new)
       ENDFOR
       MIR_TIMER, 'Interpolation', /STOP
       
       

       MIR_TIMER, 'Interpolation double resolution', /START

       x_half = (x + (SHIFT(x, 1) - x)/2.)[0:-2]
       x_double = [x, x_half]
       x_double = x_double[SORT(x_double)]

       y_double = voigt.EVALUATE(x_double, CUTOFF=cutoff)

       FOR ii=0,num_cycles-1 DO BEGIN
         y_new_interpol_double = INTERPOL(y_double, x_double, x_new)
       ENDFOR
       MIR_TIMER, 'Interpolation double resolution', /STOP



       WINDOWSET, 0
       SUPERPLOT, x_new, y_new_voigt
       SUPERPLOT, x_new, y_new_interpol, /OPLOT, COLOR='red'
       SUPERPLOT, x_new, y_new_interpol_double, /OPLOT, COLOR='blue'

       WINDOWSET, 1
       SUPERPLOT, x_new, y_new_voigt - y_new_interpol, COLOR='red'
       SUPERPLOT, x_new, y_new_voigt - y_new_interpol_double, /OPLOT, COLOR='blue'


       IF KEYWORD_SET(cutoff) THEN BEGIN
         PRINT, 'cutoff: ', cutoff
       ENDIF
       PRINT, 'num_cycles: ', num_cycles

       MIR_TIMER, /REPORT


       IF KEYWORD_SET(stop) THEN STOP
       
     END ; PRO TEST_20121114, STOP=stop

