


PRO TEST_20110923, num_points $
                   ,num_iter $
                   ,CUTOFF=cutoff $
                   ,PROFILE=profile $
                   ,SIGMA=sigma $
                   ,GAMMA=gamma

  MIR_DEFAULT, num_points, 1e3
  MIR_DEFAULT, num_iter, 1e3
  num_iter = LONG(num_iter)
  num_points = LONG(num_points)

  MIR_DEFAULT, sigma, 1D
  MIR_DEFAULT, gamma, 1D

  v = MIR_VOIGT({intensity:1D, location:0D, sigma:sigma, gamma:gamma})
  MIR_DEFAULT, cutoff, 1e-3

  x_index = (DINDGEN(num_points)/(num_points-1D)*2D - 1D)*100.

  y_simple = v.EVALUATE_SIMPLE(x_index)
  SUPERPLOT, x_index, y_simple, PSYM=0, /YLOG, YRANGE=[MIN(y_simple) > 1D-308, MAX(y_simple)]

  y = v.EVALUATE(x_index, CUTOFF=cutoff)
  w = WHERE( y NE 0D, count)
  SUPERPLOT, x_index[w], y[w], PSYM=0, /OPLOT, COLOR=MIR_COLOR('red'), /YLOG

  r = CHECK_MATH(/PRINT)
  PRINT, 'Y MIN:', MIN(y_simple), FORMAT='(a20, 2x, e7.1)'
  PRINT, 'Y MIN w/CUTOFF:', MIN(y[w]), FORMAT='(a20, 2x, e7.1)'
  PRINT, ''
  PRINT, 'Minimum expected:', FLOAT(count)/num_points*100, FORMAT='(a-20, 2x, f5.1,"%")'
  PRINT, ''

  IF KEYWORD_SET(profile) THEN MIR_PROFILER, /START

  format = '(a-30,": ", f0.5, " (", f5.1,"%)")'


  r = CHECK_MATH(/PRINT)


  time_last = SYSTIME(/SECONDS)
  FOR ii=0L,num_iter-1 DO BEGIN
    result_simple = v.EVALUATE_SIMPLE(x_index)
  ENDFOR

  time = SYSTIME(/SECONDS)
  r = CHECK_MATH(/PRINT)
  PRINT, FORMAT=format, 'EVALUATE_SIMPLE', time - time_last, 100.
  simple_runtime = time - time_last





  time_last = SYSTIME(/SECONDS)
  FOR ii=0L,num_iter-1 DO BEGIN
    result_maxcut = v.EVALUATE(x_index, CUTOFF=1D-308)
  ENDFOR

  time = SYSTIME(/SECONDS)
  r = CHECK_MATH(/PRINT)
  PRINT, FORMAT=format, 'EVALUATE, CUTOFF=1.0e-308', time - time_last $
         ,(time-time_last)/simple_runtime*100





  time_last = SYSTIME(/SECONDS)
  FOR ii=0L,num_iter-1 DO BEGIN
    result_cutoff = v.EVALUATE(x_index, CUTOFF=cutoff)
  ENDFOR

  time = SYSTIME(/SECONDS)
  r = CHECK_MATH(/PRINT)
  PRINT, FORMAT=format, 'EVALUATE, CUTOFF='+STRING(cutoff, FORMAT='(e7.1)'), time - time_last $
         ,(time-time_last)/simple_runtime*100

  IF KEYWORD_SET(profile) THEN MIR_PROFILER, /REPORT

END

