

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
; DATE:
;   2009-05-04
;
; PURPOSE:
;   Extract and display help information on IDL routines.
;   
; DESCRIPTION:
;   This set of routines is meant to extract documentation 
;   from routine and file headers.
;
;   It is meant to be used along with INFO to:
;     1. Display help information.
;
;     2. Generate HTML help files.
;
;   In general none of these routines should be called directly,
;   instead INFO should be used.
;
;   This system is meant to be a replacement for the DOC_LIBRARY
;   and MK_HTML_HELP built in routines.

; SOURCE CODE MARKUP:
;
;   Marking a Documentation Block:
;     To start a documentation block use:
;       ;+
;     to end the block use:
;       ;- 
;     White space before the first ';' will be ignored.  Anything after
;     the ';+' or ';-' directives will be ignored.
;
;     The lines containing those directives will be not be included 
;     in the documentation.
;
;     The first documentation block in a file will be attributed to
;     the file.  All other documentation blocks will be attributed
;     to the routine following the block.
;
;   Headers:
;     Inside the documentation block headers can be specifed.
;     Headers must be in all caps and be immediatly followed by a colon.
;     White space before and after the first consectutive semi-colons
;     will be ignored. 
;
;     In the examples below the additonal semi-colon was used to cause 
;     INFO not to parse the text as a header.
;
;     For example a valid header is:
;     ;   RETURN VALUE:
;     ;;  RETURN VALUE:
;
;     These are not valid headers and will not be parsed as such.
;     ;   RETURN VALUE
;     ;   Return Value:
;     ; ; RETURN VALUE:
;     ;   RERURN VALUE :
;     ;   RETURN VALUE: some_stuff
;
;   Standard Headers:
;     While any headers are allowed, certain standard headers are
;     recomended.  These have been chosen to closly match the standard
;     IDL help files.
;
;     The standard headers will be dispalyed first, in the order
;     described below.  All user headers will be displayed
;     afterwards in the order they appear in the source.
;
;     Some of these standard headers will be treated specially.
;
;     PURPOSE
;         When exporting to HTML, information under this header
;         will be used as the summary information.  This information
;         will be displayed directly after the title and will not
;         include a header.
;
;         Synonyms: BRIEF, SUMMARY
;
;     SYNTAX
;         If not present, this header will automatically generated.
;         When exporting to HTML the information under this header
;         is syntax highlighted.  No information execept the 
;         calling sequence should be provided under this header.
;
;         Synonyms: CALLING SEQUENCE
;
;     RETURN VALUE
;         
;         Synonyms: OUTPUTS
;
;     ARGUMENTS
;         When exporting to HTML highligting will be done based on
;         indentation.  
;
;         Any of the following will be highligted if found at the
;         minimum indentation of the header:
;           single words (underscores may be used)
;           Two words separated by an equals sign
;           A single word preceded by '/'
;           
;         (in), (out) or (inout) may be placed after the argument name.
;
;         Words to be highlighted must not be ajacent to another line
;         at the same indentation level.
;
;         Example:
;
;         This stuff will not be highliged since it has multiple words.
;
;         x_values
;           The index values. ('x_values' will be highlighed)
;
;         Y_VALUES = array (in)
;           The function values. ('Y_VALUES = array' will be highlighed)
;
;          /Z_VALUES
;             Here 'Z_VALUES' will not be highligted since it is 
;             not at the minimum indetation for this header.
;
;         a_values
;         b_values
;            Neither 'a_values' nor 'b_values' will be highlighted
;            since they are ajacent to a line with the same indentation.
;
;
;         Synonyms: INPUTS
;     
;     OPTIONAL ARGUMENTS
;         Same highlighting scheme as in ARGUMENTS.
;
;         Synonyms: OPTIONAL INPUTS
;
;     KEYWORDS
;         Same highlighting scheme as in ARGUMETNTS.
;
;         Synonyms: KEYWORD PARAMETERS
;
;     OPTIONAL KEYWORDS
;         Same highlighting scheme as in ARGUMETNTS.
;
;         Synonyms: OPTIONAL KEYWORD PARAMETERS
;
;     COMMON BLOCKS
;         If not provided, this header will be generated.
;
;     TAGS
;         Tags are used to send directives to INFO and INFO_MAKE_HTML.
;         These can be used to build up selective sets of
;         documentation.  Any tags may be used and passed to the
;         info routines.  
;
;         Multiple tags should be separated by a comma (',').
;
;         The standard tags are:
;
;         USE_FILE_HEADER
;           The documentation for this routine is contained in the
;           fileheader.
;
;         EXCLUDE_FILE_HEADER
;           Used for HTML processing.  Do not generate a page for the
;           file, only for the individual routines.
;
;         INTERNAL
;           This is an internal routine, should not be directly called.
;
;         USER
;           This routine is directly callable by a user.
;
;         EXCLUDE
;           Exclude this routine from processing.
;
;         KEEP_HTML
;           In general html tags will be processed as html.  If
;           this tag is present, they will be left in the documentation
;           as is.
;
;     CHANGE LOG
;         A change log can be kept under this header.
;         It will not generally be displayed by the info routines.
;
;     TO DO:
;         A list of desired changes can be kept under this header.
;         It will not generally be displayed by the info routines. 
;
;    
;
;
;   HTML Markup:     
;     HTML markup may be used in the source markup.
;
;     Unless the /KEEP_HTML option is given to the INFO routines, any 
;     HTML tags will be converted into their ascii equivalents.
;
;     The use of HTML in the code comments is discouraged as it make
;     reading the code comments more difficult.
;
;
;   Links:
;     Links to other pages can be specifed using the following syntax:
;     [FILE:info.pro]
;     [FUNCTION:INFO_FORMAT_HTML]
;     [INFO_FORMAT_HTML]
;     [PRO:INFO]
;     [MIR_DICTIONARY::]
;     [MIR_DICTIONARY::SET]
;
;     The type (FILE, PRO, or FUNCTION) must be in all caps.
;     The routine/file name does not need to be.
;
;     To go to a routine in a particualar file you can string
;     the specifers together.
;     [FILE:info.pro PRO:INFO]
;
;     At this time links are not made in the HTML documentation.
;     Hopfully this will be added in the future.
;         
;         
;
; TO DO:
;   Add an option to pick up all comments that are outside of
;     any routines, even if they are not marked ;s documentation blocks.
;   
;     To do this I would want to pick up anything that starts with ';'.
;     If I run into something that doesn't start with ';' then reset
;     the docstring.  If I run into a PRO or FUNCTION then add
;     the docstring.     
;
; TAGS:
;   INDEX, INTERNAL
;-======================================================================

     ;+=================================================================
     ; Get the default structure to be used for the documentation.
     ;
     ; The main reason to do this is to set a default ordering
     ; of the standard headers.  This will allow automatically
     ; generated documentaiton to be added at the correct location.
     ;
     ; Any headers defined in the documentation blocks that are not
     ; defined here will be added in the order that they are found.
     ;
     ;-=================================================================
     FUNCTION INFO_PARSER::GET_DEFAULT_STRUCTURE
       doc_struct = { $
                      name:'' $
                      ,type:'' $
                      ,source_file:'' $
                      ,tags:'' $
                      ,purpose:'' $
                      ,syntax:'' $
                      ,return_value:'' $
                      ,arguments:'' $
                      ,optional_arguments:'' $
                      ,keywords:'' $
                      ,common_blocks:'' $
                      ,optional_keywords:'' $
                    }

       RETURN, doc_struct
     
     END ;FUNCTION INFO_PARSER::GET_DEFAULT_STRUCTURE

     ;+=================================================================
     ; PURPOSE:
     ;   This function Extracts documentation of routines from the 
     ;   source files. It looks for documentation blocks and attaches 
     ;   them to the apropriate routine definitions.
     ;
     ; ARGUMENTS:
     ;   routine_name
     ;       If given then the documentation for that given routine
     ;       will be returned.
     ;
     ;       If not given then the FILENAME keyword is required.
     ;
     ; KEYWORDS:
     ;   FILENAME = string
     ;       The source file to extract the documentation from.
     ;      
     ; DESCRIPTION:
     ;   The routine name will need to have already been compliled
     ;   unless FILENAME is given, or the routine can be automatically
     ;   compiled (is defined in 'routine_name.pro').
     ;
     ;   If FILENAME is passed in it will be used, otherwise the actual
     ;   source file will be used.
     ;
     ;-=================================================================
     FUNCTION INFO_PARSER::EXTRACT, routine_name $
                                     ,FILENAME = filename $
                                     ,FULL_FILE = full_file $
                                     ,FILE_ONLY = file_only $
                                     ,IS_FUNCTION=is_func $
                                     ,IS_PROCEDURE=is_proc $
                                     ,EITHER=either
       COMPILE_OPT STRICTARR

       ; Check that there were inputs
       IF N_ELEMENTS(filename) EQ 0 AND N_PARAMS() EQ 0 THEN BEGIN
         MESSAGE, 'Either the routine name, or the source filename must be given.'
         RETURN, 0
       ENDIF

       IF N_ELEMENTS(filename) EQ 0 THEN BEGIN
         filename = self.ROUTINE_GET_SOURCEFILE(routine_name $
                                                 ,IS_FUNCTION=is_func $
                                                 ,IS_PROCEDURE=is_proc $
                                                 ,EITHER=either)
       ENDIF


       ; Extract all of the documentaion from the source file.
       source_docs = self.EXTRACT_FROM_SOURCE(filename)
       
       RETURN, source_docs


     END ;FUNCTION INFO_PARSER::EXTRACT


     ;+=================================================================
     ; Extract all of the documentation blocks and routine names 
     ; in an IDL source file.  The documentation blocks will be 
     ; associated with the appropriate routines.
     ;
     ; This will also extract any COMMON blocks
     ;
     ;-=================================================================
     FUNCTION INFO_PARSER::EXTRACT_FROM_SOURCE, filename

       ; Check that the file exists
       IF NOT FILE_TEST(filename) THEN BEGIN
         MESSAGE, STRING('Source file not found: ', filename)
       ENDIF

       file_tail = FILE_BASENAME(filename)

       ; Open the file and start parsing
       OPENR, lun, filename, /GET_LUN

       ; Set up some variable that will be used
       ; to keep track of where we are in the parsing
       routine_found = 0
       file_header_found = 0
       in_documentation_block = 0
       num_entries = 0

       line_raw = ''
       current_documentation = ''
       common_blocks = ''

       entry_struct = {type:'' $
                       ,name:'' $
                       ,source_file:'' $
                       ,common_blocks:'' $
                       ,documentation:'' $
                      }


       ; Start the read loop.  
       ; This will loop over all the lines in the input file.
       WHILE NOT EOF(lun) DO BEGIN
         READF, lun, line_raw

         ; Remove leading and trailing whitespace
         line = STRTRIM(line_raw, 2)
         
         ; NOTE:  The order of doing this is important.
         ;        If we are in a documentation block, and the end is detected
         ;        by finding a line that does not start with ';', then we want
         ;        to test the current line for being a routine definition
         ;        before reading the next line.
         IF in_documentation_block THEN BEGIN
           ; We are currently in a documenation block
           doc_end_found = 0

           ; Check for the end doc block directive
           IF STRCMP(line, ';-', 2) THEN BEGIN
             doc_end_found = 1
           ENDIF ELSE BEGIN
             ;   Once in a documentation block, if a line is encountered that
             ;   does not start with ';' and is not blank, then the documentation
             ;   block should be considered ended. 
             ;   (remember that we already striped whitespace.)
             ;
             ;   This will prevent typos from making everything into documetation.
             IF ~ (STRCMP(line, ';', 1) OR (line EQ '')) THEN BEGIN
               doc_end_found = 1
             ENDIF
           ENDELSE

           IF doc_end_found THEN BEGIN
             in_documentation_block = 0 
             
             ; If this is the first documentation block then add it as the file doc.
             IF ~ file_header_found THEN BEGIN
               type = 'FILE'
               name = file_tail
               current_struct = entry_struct

               current_struct.type = type
               current_struct.name = name
               current_struct.source_file = filename
               current_struct.documentation = current_documentation

               full_docs = BUILD_ARRAY(full_docs, current_struct, /NO_COPY)
               num_entries += 1
               current_documentation = ''
               file_header_found = 1
             ENDIF
           ENDIF ELSE BEGIN
             ; This line is part of the documentation block

             ; Strip any ;'s that are at the begining of the line.
             string = (STREGEX(line, '^;*(.*)', /EXTRACT, /SUBEXPR))[1]
             current_documentation += string + STRING(10B)
           ENDELSE
         ENDIF

         IF ~ in_documentation_block THEN BEGIN
           ; Look for the open directive
           IF STRCMP(line, ';+', 2) THEN BEGIN
             in_documentation_block = 1 
             CONTINUE
           ENDIF

           ; Look for a function or procedure.
           ; Note the extra space after 'PRO' or 'FUCTION'.
           IF STRCMP(STRUPCASE(line), 'PRO ', 4) THEN BEGIN
             type = 'PRO'
             name = (STREGEX(line, '^PRO ([[:alnum:]_:]+)', /EXTRACT, /SUBEXPR, /FOLD_CASE))[1]
             routine_found = 1
           ENDIF
           IF STRCMP(STRUPCASE(line), 'FUNCTION ', 9) THEN BEGIN
             type = 'FUNCTION'
             name = (STREGEX(line, '^FUNCTION ([[:alnum:]_:]+)', /EXTRACT, /SUBEXPR, /FOLD_CASE))[1]
             routine_found = 1
           ENDIF

           ; A routine was found, associate the current documentation to this routine.
           IF routine_found THEN BEGIN
             current_struct = entry_struct

             current_struct.type = STRUPCASE(type)
             current_struct.name = STRUPCASE(name)
             current_struct.source_file = filename
             current_struct.documentation = current_documentation

             full_docs = BUILD_ARRAY(full_docs, current_struct, /NO_COPY)
             num_entries += 1

             current_documentation = ''
             routine_found = 0
             CONTINUE
           ENDIF

           ; Now look for common blocks. And add them to the last entry.
           IF STRCMP(STRUPCASE(line), 'COMMON ', 7) THEN BEGIN
             name = (STREGEX(line, '^COMMON ([[:alnum:]_]+)', /EXTRACT, /SUBEXPR, /FOLD_CASE))[1]
             ; Add to the last entry.
             IF full_docs[num_entries-1].common_blocks EQ '' THEN BEGIN
               common_blocks = STRUPCASE(name)
             ENDIF ELSE BEGIN
               common_blocks = STRJOIN([full_docs[num_entries-1].common_blocks $
                                        ,STRUPCASE(name)], ', ')
             ENDELSE

             full_docs[num_entries-1].common_blocks = common_blocks
           ENDIF
         ENDIF
       ENDWHILE

       FREE_LUN, lun

       RETURN, full_docs

     END ;FUNCTION INFO_PARSER::EXTRACT_FROM_SOURCE



     ;+=================================================================
     ; Parse all the entries in the raw documentation.
     ;
     ;-=================================================================
     FUNCTION INFO_PARSER::PARSE_DOCUMENTATION, docs_raw

       output_list = MIR_LIST()

       FOR ii = 0, N_ELEMENTS(docs_raw)-1 DO BEGIN
         output_list.ADD, self.PARSE_ENTRY(docs_raw[ii])
       ENDFOR

       RETURN, output_list

     END ; FUNCTION INFO_PARSER::PARSE_DOCUMENTATION, doc_raw


     ;+=================================================================
     ; Parse a structure produced by INFO_PARSER::EXTRACT_FROM_SOURCE().
     ; 
     ; This will return a structure with all of the headings.
     ;
     ; This will grab the default headers form 
     ; INFO_PARSER::GET_DEFAULT_STRUCTURE and add them to the output.
     ;
     ; User headers will be added after these defaults.
     ;
     ; User headers will be checked, and when possible converted into 
     ; the standard headers.
     ;
     ;-=================================================================
     FUNCTION INFO_PARSER::PARSE_ENTRY, source_struct

       ; Add the default headers to the output structure.
       out_struct = self.GET_DEFAULT_STRUCTURE()

       ; First add the name, type, and source_file tags to the output.
       out_struct.name = source_struct.name
       out_struct.type = source_struct.type
       out_struct.source_file = source_struct.source_file

       ; Do not add the common blocks as we first need to see if that 
       ; was provided by the user.

       ; Now split the documentation block into an array of strings.
       string_array = STRSPLIT(source_struct.documentation $
                               ,STRING(10B) $
                               ,/EXTRACT $
                               ,/PRESERVE_NULL)


       ; If documentation is empty, then skip processing.
       IF ARRAY_EQUAL(string_array, '') THEN BEGIN
         out_struct.tags = '_EMPTY'
         out_struct.common_blocks = source_struct.common_blocks
         RETURN, out_struct
       ENDIF


       desc_start = 0
       description = ''
       header = ''

       desc_tag_used = 0
       
       ; Loop over all the lines.
       FOR ii=0,N_ELEMENTS(string_array) DO BEGIN
         
         IF ii LT N_ELEMENTS(string_array) THEN BEGIN
           ; Only match upercase letters, numbers, space and underscore.
           header_match = $
             (STREGEX(string_array[ii], '^ *([0-9A-Z_ ]+): *$', /EXTRACT, /SUBEXPR))[1]
         ENDIF ELSE BEGIN
           ; This is for the end of the array.
           header_match = 'END'
         ENDELSE
           

         IF header_match NE '' THEN BEGIN
           empty_desc = 1
           ; Check if this is the begining of the string, or if there
           ; were two headers in a row
           IF ii GT desc_start THEN BEGIN
             ; Now check that the description was not empty.
             IF ~ ARRAY_EQUAL(string_array[desc_start:ii-1], '') THEN BEGIN
               empty_desc = 0
             ENDIF
           ENDIF

           IF NOT empty_desc THEN BEGIN
             IF header EQ '' THEN BEGIN
               ; The header description is still empty.
               ; Add the information to the 'DESCRIPTION' tag.
               header = 'DESCRIPTION'
           
             ENDIF
           
             ; Check the current header, fix if neccessary
             header = self.PARSE_CHECK_HEADER(header, SKIP=skip)

             IF ~ skip THEN BEGIN
               ; Now check if the given tags has already been used, if so
               ; append.
               tags = TAG_NAMES(out_struct)
               w = WHERE(tags EQ header)
               IF w NE -1 THEN BEGIN
                 description = out_struct.(w)
                 
                 ; If the last description did not end in two newlines,
                 ; then add one (it will always end with at least 1 newline.)
                 ;IF STRMID(description,1,2, /REVERSE) NE STRING(10B)+STRING(10B) THEN BEGIN
                 ;  description += STRING(10B)
                 ;ENDIF
                 
               ENDIF ELSE BEGIN
                 description = ''
               ENDELSE
               
               ; Turn the array back into a single string
               ; Dont add any extra newlines at the begining or end.
               newlines = ''
               added_text = 0
               FOR jj=desc_start, ii-1 DO BEGIN
                 IF string_array[jj] EQ '' THEN BEGIN
                   ; Don't add newlines that come before any text.
                   IF added_text THEN BEGIN
                     newlines += STRING(10B)
                   ENDIF
                 ENDIF ELSE BEGIN
                   description += newlines + string_array[jj] +  STRING(10B)
                   newlines = ''
                   added_text = 1
                 ENDELSE
               ENDFOR
               
               ; Add the tag to the ouput structure.
               out_struct = BUILD_STRUCTURE(header, description, STRUCT=out_struct, /OVERWRITE)
             ENDIF
           ENDIF ; IF NOT skip
             

           header = header_match
           desc_start = ii+1
         ENDIF
       ENDFOR

       ; Now add the common blocks if the user did not specify.
       IF out_struct.common_blocks EQ '' THEN BEGIN
         out_struct.common_blocks = source_struct.common_blocks
       ENDIF

       RETURN, out_struct
           
             
     END ;FUNCTION INFO_PARSER::PARSE_ENTRY



     ;+=================================================================
     ; PURPOSE:
     ;   If documentation for one routine should come frome somewhere
     ;   else (say the file header), then do that here.
     ;
     ;   This can either be specifically requested using tags, or done
     ;   by default in certain cases (empty documentation for the 
     ;   first or main routine).
     ;
     ;
     ; PROGRAMMING NOTES:
     ;   This needs to be done done before <::GENERATE_DOCUMENTATION>
     ;   so that I can tell when there are empty doc entries.
     ;-=================================================================
     PRO INFO_PARSER::LINK_DOCUMENTATION, doc_list

       FOR ii=0,N_ELEMENTS(doc_list)-1 DO BEGIN
         entry = doc_list[ii]

         ; For now, nothing to do for the file entry.
         IF entry.type EQ 'FILE' THEN CONTINUE

         IF self.HASTAG(entry, ['_EMPTY', 'USE_FILE_HEADER']) THEN BEGIN
           doc_list[ii] = self.ENTRY_COPY(doc_list[0], doc_list[ii])
         ENDIF

       ENDFOR
       

     END ; PRO INFO_PARSER::LINK_DOCUMENTATION


     ;+=================================================================
     ; Check to see if an entry has a given tag.
     ;
     ;-=================================================================
     FUNCTION INFO_PARSER::HASTAG, entry, tags
       entry_tags = STRUPCASE(STRTRIM(STRSPLIT(entry.tags, ',', /EXTRACT), 2))


       FOR ii=0,N_ELEMENTS(tags)-1 DO BEGIN
         w = WHERE(entry_tags EQ STRUPCASE(tags[ii]), count)
         IF count NE 0 THEN BEGIN
           RETURN, 1
         ENDIF
       ENDFOR

       RETURN, 0
     END ;  FUNCTION INFO_PARSER::HASTAG

     ;+=================================================================
     ; Intelegently copy stuff from one entry to another. 
     ;
     ; At this point I am only using this to copy stuff from the
     ; file header into the routine info.
     ;
     ;-=================================================================
     FUNCTION INFO_PARSER::ENTRY_COPY, source, old_entry

       new_entry = source

       new_entry.name = old_entry.name
       new_entry.type = old_entry.type

       RETURN, new_entry

     END ; FUNCTION INFO_PARSER::ENTRY_COPY

     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Checks the headers and fixes if necessary.
     ;
     ;   Also checks for headers to skip entirely.
     ;
     ; DESCRIPTION:
     ;   The main purpose here is to allow the user to to use a number
     ;   of differnt headers for each section.  We want the final
     ;   output however to always be uniform.  
     ;
     ;   In particular the headers uesed in the IDL help files: 
     ;     ('SYNTAX', 'RETURN VALUE', 'ARGUMENTS', 'KEYWORDS') 
     ;   do not match those recommended by DOC_LIBRARY:
     ;     ('CALLING SEQUENCE', 'OUTPUTS', 'INPUTS'
     ;      ,'KEYWORD PARAMETERS')
     ;
     ;   This function will covert the DOC_LIBRARY headers, or any
     ;   other allowable headers, into standard help file headers.
     ;
     ;   It will also look for common typeos or abrreviations and 
     ;   convert those as well ('ARGS', 'OUTPUT'). 
     ;
     ;-=================================================================
     FUNCTION INFO_PARSER::PARSE_CHECK_HEADER, header, SKIP=skip

       skip = 0
       CASE header OF
         'CALLING SEQUENCE':            header_out = 'SYNTAX'

         'OUTPUTS':                     header_out = 'RETURN VALUE'
         'OUTPUT':                      header_out = 'RETURN VALUE'
         'OPTIONAL OUTPUTS':            header_out = 'RETURN VALUE'
         'OUT':                         header_out = 'RETURN VALUE'
         'RETURNS':                     header_out = 'RETURN VALUE' 

         'INPUTS':                      header_out = 'ARGUMENTS'
         'INPUT':                       header_out = 'ARGUMENTS'
         'IN':                          header_out = 'ARGUMENTS'
         'ARG':                         header_out = 'ARGUMENTS'
         'ARGS':                        header_out = 'ARGUMENTS'

         'OPTIONAL INPUTS':             header_out = 'OPTIONAL ARGUMENTS'
         'OPTIONAL INPUT':              header_out = 'OPTIONAL ARGUMENTS' 

         'KEYWORD PARAMETERS':          header_out = 'KEYWORDS'
         'KEYWORD ARGUMENTS':           header_out = 'KEYWORDS'

         'OPTIONAL KEYWORD PARAMETERS': header_out = 'OPTIONAL KEYWORDS'
         'OPTIONAL KEYWORD ARGUMENTS':  header_out = 'OPTIONAL KEYWORDS'

         'BREIF':                       header_out = 'PURPOSE'
         'SUMMARY':                     header_out = 'PURPOSE'

         'COMMON':                      header_out = 'COMMON BLOCKS'
         
         'NAME': BEGIN
           skip = 1
           header_out = header
         END

         ELSE:                          header_out = header
       ENDCASE

       RETURN, header_out

     END ;FUNCTION INFO_PARSER::PARSE_CHECK_HEADER


     ;+=================================================================
     ;
     ; This will automatically generate documentation for certain
     ; headers not found in the user supplied documentation.
     ;
     ; Note: Some genereration is done during the source extraction
     ;       (see [INFO_PARSER::EXTRACT_FROM_SOURCE]).  In particular
     ;       COMMON blocks are dealt with there.
     ;
     ;       This routine deals with generation done after all parsing
     ;       is complete.
     ;
     ; Documentation generated:
     ;   SYNTAX
     ;-=================================================================
     FUNCTION INFO_PARSER::GENERATE_DOCUMENTATION, doc_struct

       newline = STRING(10B)

       ; Set the maximum line length, in characters
       max_len = 78
       max_indent = 30
       standard_indent = 4

       ; First check if the syntax has already been supplied.
       IF doc_struct.syntax NE '' THEN BEGIN
         RETURN, doc_struct
       ENDIF

       ; Check to make sure that this is a routine (not a file)
       IF doc_struct.type EQ 'FILE' THEN BEGIN
         RETURN, doc_struct
       ENDIF

       is_func = (doc_struct.type EQ 'FUNCTION')
       

       ; Get the routine information
       param = ROUTINE_INFO(doc_struct.name, FUNCTIONS=is_func, /PARAMETERS)

       syntax = ''

       ; Setup for the correct function type.
       IF is_func THEN BEGIN
         syntax += 'Result = ' + doc_struct.name + '('
       ENDIF ELSE BEGIN
         syntax += doc_struct.name

         ; Dont add a comma unless there are arguments.
         IF param.num_args + param.num_kw_args GT 0 THEN BEGIN
           syntax += ', '
         ENDIF

       ENDELSE

       ; I want a cutoff on the indent.
       indent_len = STRLEN(syntax)
       IF indent_len GT max_indent THEN BEGIN
         indent_len = standard_indent
       ENDIF

       fmt_indent = '('+STRING(indent_len, FORMAT='(i0)')+'x, a0)'

       ; Now add the arguments
       current_len = STRLEN(syntax)

       ; No commas for the first argument.
       comma = ''
       comma_space = ''
       comma_len = 0
       comma_space_len = 0

       IF param.num_args GT 0 THEN BEGIN
         FOR ii=0,param.num_args-1 DO BEGIN
           arg = STRLOWCASE(param.args[ii])
           arg_len = STRLEN(arg)
           IF current_len + arg_len + comma_space_len GT max_len THEN BEGIN
             ;start a new line.
             syntax += newline + STRING('', FORMAT=fmt_indent) + comma + arg
             current_len = arg_len + indent_len + comma_len
           ENDIF ELSE BEGIN
             syntax += comma_space + arg
             current_len += arg_len + comma_space_len
           ENDELSE

           ; This is to make sure that after the first argument, commas get inserted.
           IF ii EQ 0 THEN BEGIN
             comma=','
             comma_space=', '
             comma_len = 1
             comma_space_len = 2
           ENDIF
         ENDFOR
       ENDIF

       ; Now add the keywords
       IF param.num_kw_args GT 0 THEN BEGIN
         FOR ii=0,param.num_kw_args-1 DO BEGIN
           ;arg = STRUPCASE(param.kw_args[ii])+'='+STRLOWCASE(param.kw_args[ii])
           arg = STRUPCASE(param.kw_args[ii])+'='+'?'
           arg_len = STRLEN(arg)
           
           IF current_len + arg_len +comma_space_len GT max_len THEN BEGIN
             ;start a new line.
             syntax += newline + STRING('', FORMAT=fmt_indent) + comma + arg
             current_len = arg_len + indent_len + comma_len
           ENDIF ELSE BEGIN
             syntax += comma_space + arg
             current_len += arg_len + comma_space_len
           ENDELSE

           ; This is to make sure that after the first argument, commas get inserted.
           IF ii EQ 0 THEN BEGIN
             comma=','
             comma_space=', '
             comma_len = 1
             comma_space_len = 2
           ENDIF
         ENDFOR
       ENDIF

       ; If this is a function then close the parenthesis
       IF is_func THEN BEGIN
         syntax += ')'
       ENDIF

       ; Add the syntax to the doc structure
       doc_struct.syntax = syntax

       RETURN, doc_struct
       
     END ;FUNCTION INFO_PARSER::GENERATE_DOCUMENTATION
     

     ;+=================================================================
     ; Find the source file for a given routine.
     ;
     ; If no keywords are set, and '()' is found at the end of the
     ; routine name, then the routine will be assumed to be a funciton.
     ; Otherwise the routine will be assumed to be a procedure.
     ;-=================================================================
     FUNCTION INFO_PARSER::ROUTINE_GET_SOURCEFILE, routine_name $
                                                   ,IS_FUNCTION=is_func $
                                                   ,IS_PROCEDURE=is_proc $
                                                   ,EITHER=either

         ; Remove whitespace and parenthesis from the routine name.
         routine = self.CLEAN_ROUTINE_NAME(routine_name, HAS_PAREN=has_paren)


         ; If no keywords were passed then guess the type.
         IF NOT KEYWORD_SET(is_func) $
           AND NOT KEYWORD_SET(is_proc) $
           AND NOT KEYWORD_SET(either) THEN BEGIN
           
           IF has_paren THEN BEGIN
             is_func = 1
           ENDIF ELSE BEGIN
             is_proc=1
           ENDELSE

         ENDIF

         ; Set the proper keywords to be compatable with RESOLVE_ROUTINE.
         IF KEYWORD_SET(is_func) AND KEYWORD_SET(is_proc) THEN BEGIN
           either = 1
           is_func = 0
           is_proc = 0
         ENDIF

         ; First check if the routine has been compiled
         is_compiled = self.ROUTINE_IS_COMPLILED(routine, IS_FUNCTION=is_func, IS_PROCEDURE=is_proc)

         ; If the routine is not already complied, then attempt to 
         ; automatically compile it.
         IF NOT is_compiled THEN BEGIN

           ; Set up a catch block for a clearer error.
           CATCH, error_status
           IF error_status NE 0 THEN BEGIN
             CATCH, /CANCEL
             fmt= '(a0, a0)'
             MESSAGE, STRING(routine, '.pro not found. Automatic compilation failed.' $
                             ,FORMAT=format)
           ENDIF
           RESOLVE_ROUTINE, routine, IS_FUNCTION=is_function, EITHER=either

         ENDIF

         ; At this point we know the routine is compiled.
         ; Now we need to find out whether it is a procedure or a function.
         IF KEYWORD_SET(either) THEN BEGIN
           is_proc = self.ROUTINE_IS_COMPLILED(routine, /IS_PROCEDURE)
           IF NOT is_proc THEN BEGIN
             is_func = 1
           ENDIF
         ENDIF
         
         ; Find the source file.
         source = ROUTINE_INFO(routine, /SOURCE, FUNCTIONS=is_func)

         ; Return just the file name.
         RETURN, source.(1)

     END ;FUNCTION INFO_PARSER::ROUTINE_GET_SOURCEFILE


     ;+=================================================================
     ; PURPOSE:
     ;   Checks to see if the given rountine is complied.
     ;
     ; DESCRIPTION:
     ;   <ROUTINE_INFO()> will also return unresolved routines 
     ;   in the routine list.  We look for these by checking if
     ;   a source file is returned.
     ;-=================================================================
     FUNCTION INFO_PARSER::ROUTINE_IS_COMPLILED, routine_name $
                                                 ,IS_FUNCTION=func $
                                                 ,IS_PROCEDURE=proc $
                                                 ,EITHER=either

       ; Remove whitespace and parenthesis from the routine name.
       routine = self.CLEAN_ROUTINE_NAME(routine_name, HAS_PAREN=has_paren)

       ; If no keywords were passed then guess the type.
       IF NOT KEYWORD_SET(func) $
         AND NOT KEYWORD_SET(proc) $
         AND NOT KEYWORD_SET(either) THEN BEGIN

         IF has_paren THEN BEGIN
           func = 1
         ENDIF ELSE BEGIN
           proc = 1
         ENDELSE

       ENDIF

       ; If /EITHER was set then seach for either type.
       IF KEYWORD_SET(either) THEN BEGIN
         proc=1
         func=1
       ENDIF

       ; First check procedures.
       IF KEYWORD_SET(proc) THEN BEGIN
         all_proc = ROUTINE_INFO(/SOURCE)
         w = WHERE(all_proc.name EQ routine, count)
         IF count GT 0 THEN BEGIN
           IF all_proc[w].path NE '' THEN BEGIN
             RETURN, 1
           ENDIF ELSE BEGIN
             RETURN, 0
           ENDELSE
         ENDIF
       ENDIF

       ; First check procedures.
       IF KEYWORD_SET(func) THEN BEGIN
         all_func = ROUTINE_INFO(/FUNCTIONS, /SOURCE)
         w = WHERE(all_func.name EQ routine, count)
         IF COUNT GT 0 THEN BEGIN
           IF all_func[w].path NE '' THEN BEGIN
             RETURN, 1
           ENDIF ELSE BEGIN
             RETURN, 0
           ENDELSE
         ENDIF
       ENDIF
       
       ; Nothing was found.
       RETURN, 0

     END ;FUNCTION INFO_PARSER::ROUTINE_IS_COMPLILED


     ;+=================================================================
     ; Removes whitespace and parenthesis from a string.
     ;
     ; If '()' is found in the string then 1 will be returned in the
     ; HAS_PARENTHESIS keyword.
     ;-=================================================================
     FUNCTION INFO_PARSER::CLEAN_ROUTINE_NAME, routine_name, HAS_PARENTHESIS=has_paren

       ; Make a local copy of the routine name.
       routine = routine_name

       ; Remove any whitespace an upcase
       routine = STRUPCASE(STRCOMPRESS(routine, /REMOVE_ALL))

       ; First check if '()' was passed in.
       ; If so strip it and mark it found.
       has_paren = 0
       paren = STRPOS(routine, '()', /REVERSE_SEARCH)
       IF paren NE -1 THEN BEGIN
         has_paren = 1
         routine = STRMID(routine, 0, paren)
       ENDIF

       RETURN, routine

     END ;FUNCTION INFO_PARSER::CLEAN_ROUTINE_NAME



     ;+=================================================================
     ; PURPOSE:
     ;   This function extracts and generates documentation of routines 
     ;   from the source files. 
     ;
     ; OPTIONAL INPUTS:
     ;   routine
     ;       If given, the documentation for that given routine will 
     ;       be returned.
     ;
     ;       If not given then the FILENAME keyword is required.
     ;
     ;       The routine name will need to have already been compliled
     ;       unless either the FILENAME is given, or the routine can be
     ;       automatically compiled (is defined in 'routine_name.pro').
     ;
     ; KEYWORDS:
     ;   FILENAME = string
     ;       The source file to extract the documentation from.
     ;       
     ;       This keyword is required if a routine name is not given.
     ;
     ;       If this keyword is not set, then the actual source file
     ;       for the given routine is used.
     ;
     ;   /FULL_FILE
     ;       Will extract all documentation from the source file.
     ;
     ;       This keyword is implied if a routine name is not given.
     ;
     ;
     ; DESCRIPTION:
     ;
     ;   This routine will first extract the documentation blocks
     ;   from the comments in the source code of the given routine.
     ;
     ;   It will then parse thouse documentation blocks and build
     ;   a documentation structure.  
     ;
     ;   It will then generate portions of the documentation if
     ;   they were not given.
     ;
     ;
     ;   For information on the syntax for writing documentation blocks
     ;   into your IDL source code, see the documentation associated
     ;   with this file:
     ;     INFO, 'DOCUMENTATION', /FILE
     ;
     ;
     ; PROGRAMMING NOTES
     ;
     ;   I don't really like how this is done here.  It would be
     ;   better to use a list or a dictionaly than all this structure 
     ;   manipulation.  That would of course break this routine on
     ;   IDL versions less than 8.0.
     ;
     ;-=================================================================
     FUNCTION INFO_PARSER::GET_INFO, routine_in $
                                     ,FILENAME = filename $
                                     ,FULL_FILE = full_file $
                                     ,FILE_ONLY = file_only $
                                     ,IS_FUNCTION = is_func $
                                     ,IS_PROCEDURE = is_proc $
                                     ,EITHER = either $
                                     ,HELP = help
       
       IF KEYWORD_SET(help) THEN BEGIN
         INFO
         RETURN, 0
       ENDIF
       
       ; Get the cleaned up routine name.
       routine = self.CLEAN_ROUTINE_NAME(routine_in)

       ; Parse the *.pro file and extract the documentation found.
       docs_raw = self.EXTRACT(routine $
                                ,FILENAME=filename $
                                ,FULL_FILE=full_file $
                                ,FILE_ONLY=file_only $
                                ,IS_FUNCTION=is_func $
                                ,IS_PROCEDURE=is_proc $
                                ,EITHER=either)


       output_list = self.PARSE_DOCUMENTATION(docs_raw)
       
       ; Link the documentation.
       self.LINK_DOCUMENTATION, output_list


       ; Generate documentation for anything not input by the user.
       FOR ii=0,N_ELEMENTS(output_list)-1 DO BEGIN
         output_list[ii] = self.GENERATE_DOCUMENTATION(output_list[ii])
       ENDFOR



       ; -----------------------------------------------------------------------
       ; Choose what to return.

       CASE 1 OF
         KEYWORD_SET(full_file): BEGIN
           RETURN, output_list
         END
         KEYWORD_SET(file_only): BEGIN
           RETURN, output_list[0]
         END
         ELSE: BEGIN

           ; If no routine name was given, then /FULL_FILE is assumed.
           IF ~ ISA(routine) THEN BEGIN
             RETURN, output_list
           END
           
           w = WHERE(docs_raw.name EQ routine, count)
           
           IF count EQ 0 THEN BEGIN
             MESSAGE, 'Routine: "' + routine + '" not found in source file: "'+ filename+'"'
           ENDIF
                           
           RETURN,  output_list[w[0]]
         ENDELSE
       ENDCASE


     END ;FUNCTION INFO_PARSER::GET_INFO



     ;+=================================================================
     ; PURPOSE:
     ;   Define the INFO_PARSER object.
     ;-=================================================================
     PRO INFO_PARSER__DEFINE

       struct = {INFO_PARSER $
                 ,INHERITS OBJECT}
       
     END ; PRO INFO_PARSER__DEFINE
