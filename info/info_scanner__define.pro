
;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;   amicitas@gmail.com
;
; DATE:
;   2010-02
;
; PURPOSE:
;   Scan a directory and excract all routine names.
;-======================================================================


     ;+=================================================================
     ; PURPOSE:
     ;   Initialize INFO_SCANNER object.
     ;-=================================================================
     FUNCTION INFO_SCANNER::INIT

       self.routines = OBJ_NEW('MIR_DICTIONARY')
       self.files = OBJ_NEW('MIR_DICTIONARY', /CASE_SENSITIVE)

       RETURN, 1
     END ; FUNCTION INFO_SCANNER::INIT



     ;+=================================================================
     ; PURPOSE:
     ;   Initialize INFO_SCANNER object.
     ;-=================================================================
     PRO INFO_SCANNER::CLEANUP

       IF OBJ_VALID(self.routines) THEN BEGIN
         self.routines->DESTROY
       ENDIF

       IF OBJ_VALID(self.files) THEN BEGIN
         self.files->DESTROY
       ENDIF

     END ; PRO INFO_SCANNER::CLEANUP



     ;+=================================================================
     ; PURPOSE:
     ;   Add all the routines and files from a given directory
     ;   to the scanner object
     ;-=================================================================
     PRO INFO_SCANNER::SCAN_DIRECTORY, path $
                                      ,RECURSIVE=recursive

       ; Scan though the directories backwards.  
       ; This will ensure that if a routine/file is found
       ; multiple times, they will be overwriten in the right order.
       FOR ii_dir = N_ELEMENTS(path)-1, 0, -1 DO BEGIN

         ; Get a list of all the *.pro files in the given path.
         ; Do this recursivly if desired.
         IF KEYWORD_SET(recursive) THEN BEGIN
           files = FILE_SEARCH(path[ii_dir], '*.pro')
         ENDIF ELSE BEGIN
           files = FILE_SEARCH(MIR_PATH_JOIN([path[ii_dir], '*.pro']))
         ENDELSE

         IF files[0] NE '' THEN BEGIN

           CATCH, error
           IF error NE 0 THEN BEGIN
             CATCH, /CANCEL
             MESSAGE, 'Could not scan file: ' + files[0], /CONTINUE
           ENDIF
             
           FOR ii_file=0,N_ELEMENTS(files)-1 DO BEGIN
             self->ADD_FILE, files[ii_file]
             self->SCAN_FILE, files[ii_file]
           ENDFOR
         ENDIF

       ENDFOR


     END ; PRO INFO_SCANNER::SCAN_DIRECTORY



     ;+=================================================================
     ; PURPOSE:
     ;   Scan a file, extract all of the routine names and add
     ;   them to the routines dictionary
     ;-=================================================================
     PRO INFO_SCANNER::SCAN_FILE, filepath


       routine_list = OBJ_NEW('SIMPLELIST')

       ; Open the file and start parsing
       OPENR, lun, filepath, /GET_LUN

       line_raw = ''

       WHILE NOT EOF(lun) DO BEGIN
         READF, lun, line_raw

         ; Remove leading whitespace
         line = STRUPCASE(STRTRIM(line_raw, 1))

         found_pro = 0
         found_function = 0

         ; Check if this line defines a procedure or a function.
         CASE 1 OF
           STRCMP(line, 'PRO ', 4): BEGIN
             found_pro = 1
             name = (STREGEX(line, '^PRO ([[:alnum:]_:]+)', /EXTRACT, /SUBEXPR))[1]
           END
           STRCMP(line, 'FUNCTION ', 9): BEGIN
             found_function = 1
             name = (STREGEX(line, '^FUNCTION ([[:alnum:]_:]+)', /EXTRACT, /SUBEXPR))[1]
             name = name+'()'
           END
           ELSE:
         ENDCASE

         IF ~ found_pro AND ~ found_function THEN CONTINUE

         routine_list->APPEND, name
           
       ENDWHILE


       FOR ii = 0, routine_list->N_ELEMENTS()-1 DO BEGIN
         name = routine_list->GET(ii)

         IF self.routines->HAS_KEY(name, old_file) THEN BEGIN
          MESSAGE, 'The routine ' + name $
                   + ' is already in routine dictionary.' $
                    ,/INFORMATIONAL
          
          PRINT, '  Old file: ', old_file
          PRINT, '  New_file: ', filepath
         ENDIF

         self.routines->SET, name, filepath, /OVERWRITE
       ENDFOR


       FREE_LUN, lun
       routine_list->DESTROY


     END ; PRO INFO_SCANNER::SCAN_FILE



     ;+=================================================================
     ; PURPOSE:
     ;   Scan a file, extract all of the routine names and add
     ;   them to the routines dictionary
     ;-=================================================================
     PRO INFO_SCANNER::ADD_FILE, filepath

       file_tail = FILE_BASENAME(filepath)

       IF self.files->HAS_KEY(file_tail) THEN BEGIN
         MESSAGE, 'The file ' + file_tail $
                  + ' is already in file dictionary.' $
                  ,/INFORMATIONAL
       ENDIF

       self.files->SET, file_tail, filepath, /OVERWRITE

     END ; INFO_SCANNER::ADD_FILE, filepath



     ;+=================================================================
     ; PURPOSE:
     ;   Return the routines dictionary.
     ;-=================================================================
     FUNCTION INFO_SCANNER::GET_ROUTINES

       RETURN, self.routines

     END ; FUNCTION INFO_SCANNER::GET_ROUTINES



     ;+=================================================================
     ; PURPOSE:
     ;   Return the files dictionary.
     ;-=================================================================
     FUNCTION INFO_SCANNER::GET_FILES

       RETURN, self.files

     END ; FUNCTION INFO_SCANNER::GET_FILES



     ;+=================================================================
     ; PURPOSE:
     ;   Define the INFO_SCANNER object.
     ;-=================================================================
     PRO INFO_SCANNER__DEFINE

       struct = {INFO_SCANNER $
                 ,routines:OBJ_NEW() $
                 ,files:OBJ_NEW() $
                 ,INHERITS OBJECT}
       
     END ; PRO INFO_SCANNER__DEFINE
