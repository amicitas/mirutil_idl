

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;   amicitas@gmail.com
;
; DATE:
;   2009-05-07
;
; VERSION:
;   0.0.0
;
; PURPOSE:
;   Create HTML documentation for user IDL routines.
;   
; DESCRIPTION:
;   The basic parsing of the source files is done by:
;     <INFO_PARSER::>
;
;   Here the output from that package is formated and displayed.
;
; TO DO:
;   Deal with tags.
;
;   Allow for links in the documentation blocks.
;
;-======================================================================




     ;+=================================================================
     ; PURPOSE:
     ;   Create an HTML document from the documentation structure
     ;   returned by:
     ;     <INFO_PARSER::>
     ;
     ; DESCRIPTION:
     ;   This routine will take an structure of documentation structures
     ;   from <INFO_PARSER::> and format them into a single string
     ;   with html markup.
     ; 
     ;   Note that these will be displayed in the order given.
     ;   The ordering of the various tags should be handled in
     ;   <INFO_PARSER::>.
     ;-=================================================================
     FUNCTION INFO_FORMAT_HTML, doc_struct
       COMPILE_OPT STRICTARR
       
       doc_struct_size = N_TAGS(doc_struct)
       doc_tags = TAG_NAMES(doc_struct)

       ; Create a list object to hold the html document.
       html_list = OBJ_NEW('MIR_LIST_IDL7')

       doc_string = ''

       ; Most headers will just be added to the HTML document
       ; in the order they are found.  
       ; Certain headers we will deal with manually.
       manual_headers = ['TYPE', 'NAME', 'TAGS', 'SOURCE_FILE', 'PURPOSE', 'CHANGE_LOG', 'TO_DO']

       ; Add some empty space
       html_list->APPEND, ''
       
       ; First add the doctype.
       html_list->APPEND, HTML_DOCTYPE()

       ; Add some empty space
       html_list->APPEND, ''

       ; First open the html.
       html_list->APPEND, HTML_TAG_OPEN('html')

       ; Add some empty space
       html_list->APPEND, ''

       ; Add the header.
       html_list->APPEND, HTML_HEAD(TITLE=doc_struct.(0).name, STYLESHEET='info_make_html.css')

       ; Add some empty space
       html_list->APPEND, ''

       ; Start the body
       html_list->APPEND, HTML_TAG_OPEN('body', ATTRIBUTES=['text="#000000"', 'bgcolor="#FFFFFF"'])

       ; Add some empty space
       html_list->APPEND, ''

       ; If this is a full file, then add a different header.
       ; 
       ; Don't do this if this is only the file header.
       IF doc_struct.(0).type EQ 'FILE' AND doc_struct_size GT 1 THEN BEGIN
         ; Add the page title
         html_list->APPEND, HTML_TAG('h1', doc_struct.(0).name)

         ; Add a table of contents
         ;html_list->APPEND, INFO_HTML_FILE_INDEX
         
         ; Add a horizontal line
         html_list->APPEND, HTML_HR()

         ; Add some empty space
         html_list->APPEND, ''

         ; Add some empty space
         html_list->APPEND, ''
       ENDIF


       ; Now start going through all of the records in the structure.
       ; ---------------------------------------------------------------
       FOR ii=0,doc_struct_size-1 DO BEGIN 
         tags = TAG_NAMES(doc_struct.(ii))
         num_tags = N_TAGS(doc_struct.(ii))

         ; Add a link: <a name="name"></a>
         html_list->APPEND, HTML_TAG('a', '', ATTRIBUTES=['name="'+doc_tags[ii]+'"'])

         ; Add the title.
         ; Add '()' to the end of the name if this is a function
         name = doc_struct.(ii).name
         IF doc_struct.(ii).type EQ 'FUNCTION' THEN BEGIN
           name += '()'
         ENDIF
         html_list->APPEND, HTML_TAG('h2', name)

         ; Add some empty space
         html_list->APPEND, ''
         
         ; Add the index
         ;html_list->APPEND, INFO_HTML_ROUTINE_INDEX()

         ; Add the purpose
         html_list->APPEND, HTML_TAG('p', doc_struct.(ii).purpose)


         ; Now loop through all the rest
         FOR jj=0,num_tags-1 DO BEGIN

           header = tags[jj]
           ; Skip the manual headers (TAGS, TYPE, NAME, etc.).
           ; These are defined at the beginning of this routine.
           IF (where(manual_headers EQ header))[0] NE -1 THEN CONTINUE

           
           current_doc = doc_struct.(ii).(jj)
           
           ; Skip any empty headers
           IF current_doc NE '' THEN BEGIN

             ; Add some empty space
             html_list->APPEND, ''

             ; Since the header comes from structure tag names,
             ; Convert any underscores in the tag name to spaces
             header = STRJOIN(STRSPLIT(header, '_', /PRESERVE_NULL, /EXTRACT), ' ')

             ; First add a link: <a name="name"></a>
             link_name = doc_tags[ii]+'_'+tags[jj]
             html_list->APPEND, HTML_TAG('a', '', ATTRIBUTES=['name="'+link_name+'"'])

             ; Now add the header
             html_list->APPEND, HTML_TAG('h3', MIR_STRTITLECASE(header))
             
             ; Now add the documentation.
             ; For now all documentation will be preformatted.
             ;
             ; In the future I want to parse some of headers, 
             ; (such as arguments, or keywords)


             ; Add some empty space
             html_list->APPEND, ''

             html_list->APPEND, HTML_TAG('pre', current_doc)
             
           ENDIF
         ENDFOR
         
         ; Add some empty space
         html_list->APPEND, ''


         ; Add a source section if this is either the last record, or
         ; if this is a record of type 'FILE'.
         IF (doc_struct.(ii).type EQ 'FILE') OR (ii EQ doc_struct_size-1) THEN BEGIN

           IF (ii EQ doc_struct_size-1) THEN BEGIN
             ; Add some empty space
             html_list->APPEND, ''
 
             ;Add a horizontal rule after each routine.
             html_list->APPEND, HTML_HR()


             ; Add some empty space
             html_list->APPEND, ''
           ENDIF
             
           ; Add a section about the source
           ; ---------------------------------------------------------------

           ; First add a link: <a name="name"></a>
           link_name = 'SOURCE_FILE'
           html_list->APPEND, HTML_TAG('a', '', ATTRIBUTES=['name="'+link_name+'"'])
           
           ; Now add the header
           html_list->APPEND, HTML_TAG('h3', MIR_STRTITLECASE('SOURCE FILE'))


           ; Add a description
           desc = [ 'These routines are written in the IDL Language.' $
                    + '  The source code can be found in the file:' $
                    ,HTML_TAG('font', html_br()+doc_struct.(0).source_file, ATTRIBUTES='size=+1')]

           html_list->JOIN, HTML_TAG('p', desc)

         ENDIF

         ; add a horizontal rule
         html_list->APPEND, HTML_HR()
         ; add a horizontal rule
         html_list->APPEND, HTML_HR()

       ENDFOR



       ; Add a section about the generation.
       ; ---------------------------------------------------------------
       desc = ['This help file was automatically generated from comments' $
               +' in the source files by the INFO package.' $
               ,html_br() + 'The INFO package was written by Novimir Antoniuk Pablant']
       desc = HTML_TAG('font', desc, ATTRIBUTES='size=-1')
       html_list->JOIN, HTML_TAG('p', desc)


       

       ; Close the html page
       ; ---------------------------------------------------------------

       ; Add some empty space
       html_list->APPEND, ''

       ; Close the body
       html_list->APPEND, HTML_TAG_CLOSE('body')
       
       ; Close the html
       html_list->APPEND, HTML_TAG_CLOSE('html')


       ; Extract the array from the list objcet
       html_array = html_list->TO_ARRAY()

       OBJ_DESTROY, html_list

       RETURN, html_array
       
     END ;FUNCTION INFO_FORMAT_HTML


     
     ;+=================================================================
     ; PURPOSE:
     ;   Return an automatically generated filename for a given
     ;   documentation structure.
     ;
     ;-=================================================================
     FUNCTION INFO_FORMAT_GET_HTML_FILENAME, doc_struct
       doc_tags = TAG_NAMES(doc_struct)
       RETURN, STRLOWCASE(doc_tags[0])+'.html'
     END ;FUNCTION INFO_FORMAT_GET_HTML_FILENAME


     ;+=================================================================
     ; PURPOSE:
     ;   Generate a home page for the help files.
     ;
     ;-=================================================================
     FUNCTION INFO_FORMAT_HOME, doc_struct
       COMPILE_OPT STRICTARR


       doc_struct_size = N_TAGS(doc_struct)
       doc_tags = TAG_NAMES(doc_struct)

       ; Create a list object to hold the html document.
       home_list = OBJ_NEW('MIR_LIST_IDL7')

       ; Add some empty space
       home_list->APPEND, ''
       
       ; First add the doctype.
       home_list->APPEND, HTML_DOCTYPE()

       ; Add some empty space
       home_list->APPEND, ''

       ; First open the html.
       home_list->APPEND, HTML_TAG_OPEN('html')

       ; Add some empty space
       home_list->APPEND, ''

       ; Add the header.
       home_list->APPEND, HTML_HEAD(TITLE=doc_struct.(0).name, STYLESHEET='info_make_html.css')

       ; Add some empty space
       home_list->APPEND, ''

       ; Start the body
       home_list->APPEND, HTML_TAG_OPEN('body', ATTRIBUTES=['text="#000000"', 'bgcolor="#FFFFFF"'])

       ; Add some empty space
       home_list->APPEND, ''

       ; Add a title
       home_list->APPEND, HTML_TAG('h1', 'Help generated by INFO_HTML.')


       ; Add some empty space
       home_list->APPEND, ''

       ; Add some empty space
       home_list->APPEND, ''


       ; add a horizontal rule
       home_list->APPEND, HTML_HR()


       ; Add a section about the generation.
       ; ---------------------------------------------------------------
       desc = ['This help file was automatically generated from comments' $
               +' in the source files by the INFO package.' $
               ,html_br() + 'The INFO package was written by Novimir Antoniuk Pablant']
       desc = HTML_TAG('font', desc, ATTRIBUTES='size=-1')
       home_list->APPEND, HTML_TAG('p', desc)

       

       ; Close the html page
       ; ---------------------------------------------------------------

       ; Add some empty space
       home_list->APPEND, ''

       ; Close the body
       home_list->APPEND, HTML_TAG_CLOSE('body')
       
       ; Close the html
       home_list->APPEND, HTML_TAG_CLOSE('html')


       ; Extract the array from the list objcet
       home_array = home_list->TO_ARRAY()

       OBJ_DESTROY, home_list

       RETURN, home_array

     END ;FUNCTION INFO_FORMAT_HOME


     ;+=================================================================
     ; Brings up a dialog to ask if a file should be overwritten
     ;-=================================================================
     PRO INFO_FILE_WRITE, string_array $
                          ,FILENAME=filename $
                          ,OVERWRITE_ALL=overwrite_all

       file_tail = FILE_BASENAME(filename)
       
       ; Check if the file exists
       IF FILE_TEST(filename) AND (~ KEYWORD_SET(overwrite_all)) THEN BEGIN
         overwrite = DIALOG_MESSAGE(['The file:',file_tail,'Already exists.', '', 'Overwrite?'] $
                                    ,/QUESTION, /DEFAULT_NO)
         IF overwrite EQ 'No' THEN RETURN
       ENDIF

       ; Open the file for writing
       OPENW, lun, filename, /GET_LUN

       FOR ii=0,N_ELEMENTS(string_array)-1 DO BEGIN
         PRINTF, lun, string_array[ii]
       ENDFOR

       FREE_LUN, lun


       PRINT, 'File saved to: ', filename
 

     END ;PRO INFO_FILE_WRITE



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   The INFO_HTML function will generate HTML documentation from
     ;   formatted comment blocks in IDL source files.
     ;
     ; DESCRIPTION:
     ;   This function will provide HTML  documentation, as generated
     ;   from comments in the source files.
     ; 
     ;   Eventually it will also produce the indexes needed to display
     ;   the html files in the IDL help browser.
     ;
     ; SYNTAX:
     ;   INFO_HTML, routine[,PATH=string][,OUTPUT_FILE='string']
     ;       [,/FILE_HEADER][,/FULL_FILE]
     ;       [,/IS_FUNCTION][,/IS_PROCEDURE][,/EITHER]
     ;       [,/HELP][,/DEBUG]
     ;
     ; ARGUMENTS:
     ;   routine
     ;     A string containing the name of the routine for which 
     ;     documentation will be returned.
     ;
     ;     Optionally '()' can be added after the routine name to
     ;     tell INFO the the named routine is a function.  This
     ;     is equivalent to setting /IS_FUNCTION.
     ;     Example:  INFO, 'BUILD_STRUCTURE()'
     ;
     ; KEYWORDS:
     ;   PATH=string
     ;     The path to which the html file will be saved.
     ;     If not given, the file will be save in the current path.
     ;
     ;   OUTPUT_FILE=string
     ;     This keyword can be set to save the output to a
     ;     specific file.
     ;
     ;     By default filenames will be automatically generated.
     ;
     ;   /FILE_HEADER
     ;     Will print the header documentation for the source file 
     ;     in which the routine was defined.
     ;   /FULL_FILE
     ;     Will print all documentation from the source file in which
     ;     the routine was defined.  This will print out the file 
     ;     header documentation as well as the documentation for every
     ;     routine defined in the file.
     ;
     ;   /IS_FUNCITION
     ;     Tells INFO that the named routine is a function.
     ;   /IS_PROCEDURE
     ;     Tells INFO that the named routine is a procedure.
     ;   /EITHER
     ;     INFO will look for either fuctions or procedures that match
     ;     the given routine name.  Procedures will be searched first.
     ;     This is the default.
     ;
     ;   /HELP
     ;     Print this documentation to the terminal.
     ;   /DEBUG
     ;     Show traceback when an error is encountered.
     ;
     ;-=================================================================
     PRO INFO_HTML, name $
                    ,PATH=path $
                    ,OUTPUT_FILE=output_file $
                    ,OVERWRITE_ALL=overwrite_all $

                    ,IS_FUNCTION=is_func $
                    ,IS_PROCEDURE=is_proc $
                    ,EITHER=either $
                    
                    ,FULL_FILE=full_file $
                    ,FILE_HEADER=file_only $
                    
                    ,TAGS_EXCLUDE=tags_exclude $
                    ,TAGS_INCLUDE=tags_include $
                    ,TAGS_CREATE_PAGE=tags_create_page $
                    
                    ,IDL_ASSISTANT=idl_assistant $
                    
                    ,DEBUG=debug $
                    ,HELP = help

       COMPILE_OPT STRICTARR


       RESOLVE_ROUTINE, [ $
                          'html_utilities' $
                          ,'info_adp' $
                        ], /EITHER, /COMPILE_FULL_FILE, /NO_RECOMPILE


       IF KEYWORD_SET(help) THEN BEGIN
         INFO
         RETURN
       ENDIF

       ; Setup a catch block for errors.
       CATCH, error
       IF (error NE 0) THEN BEGIN
         CATCH, /CANCEL
         PRINT, 'No INFO available for: ', name
         IF KEYWORD_SET(debug) THEN BEGIN
           MESSAGE, /REISSUE
         ENDIF
         RETURN
       ENDIF

       IF N_PARAMS() EQ 0 THEN BEGIN
         MESSAGE, 'A routine or filename is required.'
       ENDIF ELSE BEGIN
         ; A routine/file name was given.
  
         ; Set the default to /EITHER
         IF NOT KEYWORD_SET(is_func) XOR KEYWORD_SET(is_proc) THEN BEGIN
           either = 1
           is_func = 0
           is_proc = 0
         ENDIF

       ENDELSE

       parser = OBJ_NEW('INFO_PARSER')
       doc_struct = parser->GET_INFO(name $
                                     ,IS_FUNCTION=is_func $
                                     ,IS_PROCEDURE=is_proc $
                                     ,EITHER=either $
                                     ,FULL_FILE=full_file $
                                     ,FILE_ONLY=file_only $
                                    )
       parser->DESTROY

       ; Format the doc_structure into an html page.
       html_array = INFO_FORMAT_HTML(doc_struct)

       ; Generate a filename
       MIR_DEFAULT, output_file, INFO_FORMAT_GET_HTML_FILENAME(doc_struct)
       MIR_DEFAULT, path, ''

       
       filename = MIR_PATH_JOIN([path,output_file])

       INFO_FILE_WRITE, html_array, FILENAME=filename, OVERWRITE_ALL=overwrite_all

       ; Generate files for the help system
       IF KEYWORD_SET(idl_assistant) THEN BEGIN

         adp_array = INFO_FORMAT_ADP_PROFILE(doc_struct)
         adp_profile_array = INFO_BUILD_ADP_PROFILE(adp_array)
         
         MIR_DEFAULT, adp_output_file, 'info_make_html.adp'

         filename = MIR_PATH_JOIN([path,adp_output_file])
         INFO_FILE_WRITE, adp_profile_array, FILENAME=filename, OVERWRITE_ALL=overwrite_all


         home_array = INFO_FORMAT_HOME(doc_struct)
         MIR_DEFAULT, home_output_file, 'home.html'
         
         filename = MIR_PATH_JOIN([path, home_output_file])
         INFO_FILE_WRITE, home_array, FILENAME=filename, OVERWRITE_ALL=overwrite_all

         ; Print a note to the user
         PRINT, ''
         PRINT, 'These help files may be accessed using the following command:'
         PRINT, 'ONLINE_HELP, BOOK="', MIR_PATH_JOIN([path,adp_output_file]), '", /FULL_PATH'
       ENDIF


     END ;PRO INFO_HTML
