





     ;+=================================================================
     ; PURPOSE:
     ;   Return a list of all of the methods defined for a given
     ;   class name or object instance.
     ;
     ;-=================================================================
     FUNCTION OBJECT_INFO, object_in, ALL=all, METHODS=methods

       CASE 1 OF

         ; String
         MIR_IS_STRING(object_in): BEGIN
           class_name = object_in
         END

         ELSE: BEGIN
           IF OBJ_VALID(object_in) THEN BEGIN
             class_name = OBJ_CLASS(object_in)
           ENDIF ELSE BEGIN
             MESSAGE, 'Input must be either an object instance or a class name.'
           ENDELSE
         ENDELSE

       ENDCASE
       

       ; To make sure that the class and any subclassses  have been resolved,
       ; we instantiate a structure.
       ; This will not resolve methods found in separate files.
       struct = CREATE_STRUCT(NAME=class_name)

       super_classes = OBJ_SUPERCLASS(class_name)


       ; Get an array with all of the currently resolved routines.
       procedures = ROUTINE_INFO()
       functions = ROUTINE_INFO(/FUNCTION)
       functions += '()'
       routines = [procedures, functions]
       routines = routines[SORT(routines)]

       class_string = class_name + '::'
       w = WHERE(STRCMP(class_string, routines, STRLEN(class_string)))
       IF W[0] NE -1 THEN BEGIN
         methods_full = routines[w]
       ENDIF


       ; Extract only the method names.
       class_methods = STRMID(methods_full, STRLEN(class_string))


       ; Now loop though the superclasses
       FOR ii=0,N_ELEMENTS(super_classes)-1 DO BEGIN
         ; First find all of the superclass routines.
         class_string = super_classes[ii] + '::'
         w = WHERE(STRCMP(class_string, routines, STRLEN(class_string)))
         IF w[0] EQ -1 THEN CONTINUE

         superclass_routines = routines[w]
         superclass_methods = STRMID(superclass_routines, STRLEN(class_string))


         ; Unless /ALL was set we only want to add the methods
         ; that have not been reimplemented.
         IF KEYWORD_SET(all) THEN BEGIN
           methods_full = [methods_full, superclass_routines]
           class_methods = [class_methods, superclass_methods]
         ENDIF ELSE BEGIN

           is_new = INTARR(N_ELEMENTS(superclass_routines))
           FOR jj=0,N_ELEMENTS(superclass_routines)-1 DO BEGIN
             
             IF ARRAY_EQUAL(class_methods EQ superclass_methods[jj], 0) THEN BEGIN
               is_new[jj] = 1
             ENDIF
           ENDFOR

           w_new = WHERE(is_new)
           IF w_new[0] EQ -1 THEN CONTINUE

           methods_full = [methods_full, superclass_routines[w_new]]
           class_methods = [class_methods, superclass_methods[w_new]]
         ENDELSE
       
       ENDFOR


       ; Determine if we need to return the full routine names 
       ; or just the method names.
       IF KEYWORD_SET(methods) THEN BEGIN
         class_methods = class_methods[SORT(class_methods)]
         RETURN, class_methods
       ENDIF ELSE BEGIN
         RETURN, methods_full
       ENDELSE

     END ;FUNCTION OBJECT_INFO
