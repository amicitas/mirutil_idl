

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
; DATE:
;   2009-06
;
; VERSION:
;   0.0.0
;
; PURPOSE:
;   Print the contents of a structure to the terminal
;   
;
;
;-======================================================================


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Write the output from a nested structure to a terminal
     ;
     ; DESCRIPTION:
     ;   This procedure will take a structure and display the
     ;   contents to a terminal.
     ;
     ;   This is just a wrapper for LABEL_DATA_FORMAT_WRITE with
     ;   some default options set.
     ;
     ;
     ; EXAMPLE:
     ;   For an input structure:
     ;     struct = { some_numbers:[1, 2] $
     ;                ,nested:{numbers:[3, 4]} $
     ;              }
     ;
     ;   The following file will be displayed:
     ;
     ;     SOME_NUMBERS    1               2
     ;     NESTED          NUMBERS         3               4
     ;
     ;
     ;
     ; TAGS:
     ;   MAIN
     ;-=================================================================
     PRO PRINT_STRUCTURE, input_structure $
                          ,WIDTH=width $
                          ,LABEL_WIDTH=label_width $
                          ,RECORD_WIDTH=record_width $
                          ,_EXTRA=extra

       ; ---------------------------------------------------------------
       ; Set some defaults
       IF N_ELEMENTS(width) NE 0 THEN BEGIN
         IF N_ELEMENTS(label_width) EQ 0 THEN BEGIN
           label_width = width
         ENDIF
         IF N_ELEMENTS(record_width) EQ 0 THEN BEGIN
           record_width = width
         ENDIF
       ENDIF

       default_width = 20
       MIR_DEFAULT, label_width, 20
       MIR_DEFAULT, record_width, 20
       
       LABEL_DATA_FORMAT_WRITE, input_structure $
                                ,WIDTH=width $
                                ,LABEL_WIDTH=label_width $
                                ,RECORD_WIDTH=record_width $
                                ,_EXTRA=extra


     END ; PRO PRINT_STRUCTURE
