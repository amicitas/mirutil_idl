


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-09
;
; PURPOSE:
;   Returns the given tag from the structure.
;
; TAGS:
;   EXCLUDE_FILE_HEADER
;
;-======================================================================


     ;+=================================================================
     ;
     ; Checks whether or not a structure has a given tag.
     ;
     ;-=================================================================
     FUNCTION GET_TAG, structure, tag
       ON_ERROR, 2
  
       w = WHERE(STRCMP(TAG_NAMES(structure), tag, /FOLD_CASE), count)
       IF count EQ 0 THEN BEGIN
         MESSAGE, 'Structure does not contain tag.'
       ENDIF
       
       RETURN, structure.(w[0])

     END ; FUNCTION GET_TAG
