


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
; DATE:
;   2009-12
;
; VERSION:
;   0.0.0
;
; PURPOSE:
;   Does the same thing as STRUCT_ASSIGN_FIX but maybe without
;   segmentation faults.
;
;-======================================================================



     ;+=================================================================
     ;
     ;
     ;-=================================================================
     PRO STRUCT_ASSIGN_FIX::STRUCT_ASSIGN_FIX, source, dest $
                                           ,NOZERO=nozero $
                                           ,VERBOSE=verbose
         

       IF OBJ_VALID(source) THEN BEGIN
         tags_source = source->TAG_NAMES()
       ENDIF ELSE BEGIN
         tags_source = TAG_NAMES(source)
       ENDELSE

       IF OBJ_VALID(dest) THEN BEGIN
         tags_dest = dest->TAG_NAMES()
       ENDIF ELSE BEGIN
         tags_dest = TAG_NAMES(dest)
       ENDELSE

       FOR ii=0,N_ELEMENTS(tags_dest)-1 DO BEGIN
         w = WHERE(tags_source EQ tags_dest[ii])

         IF w[0] NE -1 THEN BEGIN
           IF MIR_IS_STRUCT(dest.(ii)) THEN BEGIN
             dest_temp = dest.(ii)
             self->STRUCT_ASSIGN_FIX, source.(w[0]), dest_temp, NOZERO=nozero, VERBOSE=verbose
             dest.(ii) = TEMPORARY(dest_temp)
           ENDIF ELSE BEGIN
             dest.(ii) = source.(w[0])
           ENDELSE
         ENDIF
           
       ENDFOR


       
     END ;STRUCT_ASSIGN_FIX::STRUCT_ASSIGN_FIX



     ;+=================================================================
     ;
     ;
     ;-=================================================================
     PRO STRUCT_ASSIGN_FIX__DEFINE

       struct = {STRUCT_ASSIGN_FIX $
                 ,INHERITS NULL}
       
     END ;PRO STRUCT_ASSIGN_FIX__DEFINE



     ;+=================================================================
     ;
     ;
     ;-=================================================================
     PRO STRUCT_ASSIGN_FIX, source, dest $
                        ,NOZERO=nozero $
                        ,VERBOSE=verbose


       IF ~ KEYWORD_SET(nozero) THEN BEGIN
         MESSAGE, /CONTINUE, 'STRUCT_ASSIGN_FIX reqires /NOZERO.'
         STRUCT_ASSIGN_FIX, source, dest $
                        ,NOZERO=nozero $
                        ,VERBOSE=verbose
         RETURN
       ENDIF


       IF OBJ_VALID(dest) THEN BEGIN
         obj = OBJ_NEW('STRUCT_ASSIGN_FIX')
         obj->STRUCT_ASSIGN_FIX, source, dest $
                             ,NOZERO=nozero $
                             ,VERBOSE=verbose
         OBJ_DESTROY, obj
         RETURN
       ENDIF
         


       tags_source = TAG_NAMES(source)
       tags_dest = TAG_NAMES(dest)

       FOR ii=0,N_TAGS(dest)-1 DO BEGIN
         w = WHERE(tags_source EQ tags_dest[ii])

         IF w[0] NE -1 THEN BEGIN
           IF MIR_IS_STRUCT(dest.(ii)) THEN BEGIN
             dest_temp = dest.(ii)
             STRUCT_ASSIGN_FIX, source.(w[0]), dest_temp, NOZERO=nozero, VERBOSE=verbose
             dest.(ii) = TEMPORARY(dest_temp)
           ENDIF ELSE BEGIN
             dest.(ii) = source.(w[0])
           ENDELSE
         ENDIF
           
       ENDFOR

     END ;PRO STRUCT_ASSIGN_FIX
