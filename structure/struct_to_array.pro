     ; =========================================================================
     ; This functions takes a structure and a tag name and attempts
     ; to return an array.  The input structure is assumed to contain
     ; number of identical data structures.  The output array
     ; will have dimentions of the data in the structure + 1.
     ;
     ; INPUT:
     ;   struct = { $
     ;              data01:{a, b, c}, $
     ;              data02:{a, b, c}, $
     ;              . . .
     ;              dataNN:{a, b, c} $
     ;            }
     ;
     ;
     ; OUTPUT:
     ;  array = [ $
     ;            struct.data01.a, $
     ;            struct.data02.a, $
     ;            . . ., $
     ;            struct.dataNN.a $
     ;          ]
     ;
     FUNCTION STRUCT_TO_ARRAY, struct, TAG_NAME=tag_name_input

       IF KEYWORD_SET(tag_name_input) THEN BEGIN
         IF SIZE(tag_name_input, /TYPE) NE 7 THEN BEGIN
           PRINT, 'STRUCT_TO_ARARY: tag_name must be a string'
           RETURN, [0]
         ENDIF ELSE BEGIN
           tag_name = STRTRIM(STRUPCASE(tag_name_input),2)
         ENDELSE
       ENDIF

       IF SIZE(struct, /TYPE) NE 8 THEN BEGIN
         PRINT, 'STRUCT_TO_ARRAY: Input must be a stucture.'
         RETURN, [0]
       ENDIF

       num_tags = N_TAGS(struct)

       IF KEYWORD_SET(tag_name_input) THEN BEGIN
         IF SIZE(struct.(0), /TYPE) NE 8 THEN BEGIN
           PRINT, 'STRUT_TO_ARRAY: Nested input struture expected.'
           RETURN, [0]
         ENDIF
         ; Check that the tag name exists
         tag_name_array = TAG_NAMES(struct.(0))
         tag_number = WHERE(tag_name_array EQ tag_name)
         IF tag_number[0] EQ -1 THEN BEGIN
           PRINT, 'STRUCT_TO_ARRAY: Tag name: ', tag_name, ' not found.'
           STOP
         ENDIF

         value_size = SIZE(struct.(0).(tag_number))
         value_type = SIZE(struct.(0).(tag_number), /TYPE)
       ENDIF ELSE BEGIN
         value_size = SIZE(struct.(0))
         value_type = SIZE(struct.(0), /TYPE)
       ENDELSE


       ; We want one more dimension that is currently present
       dimensions = intarr(value_size[0]+1)
       ; First dimension is the new one
       dimensions[0] = num_tags
       ; Now fill the rest
       IF value_size[0] GE 1 THEN BEGIN
         dimensions[1:value_size[0]] = value_size[1:value_size[0]]
       ENDIF

       ; make the output array
       IF value_type NE 8 THEN BEGIN
         output_array = MAKE_ARRAY(dimensions, TYPE=value_type)
       ENDIF ELSE BEGIN
         IF KEYWORD_SET(tag_name_input) THEN BEGIN
           output_array = REPLICATE(struct.(0).(tag_number), dimensions)
         ENDIF ELSE BEGIN
           output_array = REPLICATE(struct.(0), dimensions)          
         ENDELSE
       ENDELSE

       ; Now fill the array from the structure
       IF KEYWORD_SET(tag_name_input) THEN BEGIN
         FOR i = 0, num_tags-1 DO BEGIN
           output_array[i,*] = struct.(i).(tag_number)
         ENDFOR
       ENDIF ELSE BEGIN
         FOR i = 0, num_tags-1 DO BEGIN
           output_array[i,*] = struct.(i)
         ENDFOR
       ENDELSE

       RETURN, output_array
     END ;FUNCTION STRUCT_TO_ARRAY
