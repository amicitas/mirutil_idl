




;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-09
;
; PURPOSE:
;   Return the index in a structure where a given tag is found.
;
; TAGS:
;   EXCLUDE_FILE_HEADER
;
;-======================================================================


     ;+=================================================================
     ;
     ; Return the index in a structure where a given tag is found.
     ;
     ;-=================================================================
     FUNCTION TAG_INDEX, structure, tag
       ON_ERROR, 2
       
       w = WHERE(STRCMP(TAG_NAMES(structure), tag, /FOLD_CASE), count)

       RETURN, w[0]

     END ; FUNCTION TAG_INDEX
