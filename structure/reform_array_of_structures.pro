


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
; DATE:
;   2009-09
;
; VERSION:
;   0.0.0
;
; PURPOSE:
;   Convert an array of structures into a structure of arrays.
;
; TAGS:
;   EXCLUDE_FILE_HEADER
;
;-======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;    Convert an array of structures into a structure of arrays.
     ;   
     ;-=================================================================
     FUNCTION REFORM_ARRAY_OF_STRUCTURES, array_of_struct $
                                          ,SINGLE=single

       tags = TAG_NAMES(array_of_struct[0])
       num_tags = N_TAGS(array_of_struct[0])


       IF ISA(single) THEN BEGIN
         do_single = 1
         single = STRUPCASE(single)
       ENDIF ELSE BEGIN
         do_single = 0
       ENDELSE


       ; Copy the components into a structure.
       FOR ii=0,num_tags-1 DO BEGIN

         IF do_single THEN BEGIN
           IF (WHERE(single EQ tags[ii]))[0] NE -1 THEN BEGIN
             struct_of_array = BUILD_STRUCTURE(tags[ii], array_of_struct[0].(ii), struct_of_array)
             CONTINUE
           ENDIF
         ENDIF
         struct_of_array = BUILD_STRUCTURE(tags[ii], array_of_struct.(ii), struct_of_array)
       ENDFOR

       RETURN, struct_of_array

     END ;PRO REFORM_ARRAY_OF_STRUCTURES
