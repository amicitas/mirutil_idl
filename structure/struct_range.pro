





;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
; DATE:
;   2009-09
;
; VERSION:
;   0.0.0
;
; PURPOSE:
;   For a structure of arrays, find the max and min.
;
; TAGS:
;   EXCLUDE_FILE_HEADER
;
;-======================================================================





     ;+=================================================================
     ; PURPOSE:
     ;   For a structure of arrays, find the max and min.
     ;
     ; INPUTS:
     ;   struct
     ;       A structure of arrays for which the range is to be found.
     ;
     ;   indexes (optional)
     ;       A array specifing which elements to inculde in the
     ;       in the search.  If this array is given, then ALL arrays
     ;       in the structure must be less than the maximum index
     ;       given in indexes.
     ;     
     ;   
     ;-=================================================================
     FUNCTION STRUCT_RANGE, struct, indexes

       range = [0E,0E]

       IF ISA(indexes) THEN BEGIN
         max = MAX(struct.(0)[indexes], MIN=min)
       ENDIF ELSE BEGIN
         max = MAX(struct.(0), MIN=min)
       ENDELSE

       range = [min, max]

       FOR ii=1,N_TAGS(struct)-1 DO BEGIN
         IF ISA(indexes) THEN BEGIN
           max = MAX(struct.(ii)[indexes], MIN=min)
         ENDIF ELSE BEGIN
           max = MAX(struct.(ii), MIN=min)
         ENDELSE

         range[1] = range[1] > max
         range[0] = range[0] < min
       ENDFOR
       

       RETURN, range
  
     END ;FUNCTION STRUCT_RANGE
