



;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
;   antoniuk@fusion.gat.com
;   amicitas@gmail.com
;
; DATE:
;   2009-12
;
; VERSION:
;   0.0.0
;
; PURPOSE:
;   Take a structure, that may be named or contain named structures
;   and convert it into an anonymous structure.
;
; DESCRIPTION:
;   This routine will recursivly convert named structures into
;   anonymous structure.  It is useful when saving the structure
;   to an IDL save file.  By forcing the structure to be
;   anonymous, one can be sure that when the structure is restored
;   it will be exactly how it was saved, even if the structure
;   definitions have changed.
;
;
;
;   In addition there is a bug in IDL 6.2 that causes restoration of 
;   structures using [RESTORE, /RELAXED_STRUCTURE_ASSIGNMENT] to cause 
;   segmentation faults if names structures are present and
;   different from their current definitions.
;
;   This routine is meant to avoid the problem by converting
;   named structures into anonymous structures.
;
;   
;
;-======================================================================




     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Convert named structures into anonymous structures.
     ;
     ; TAGS:
     ;   DOCS_FROM_FILE_HEADER
     ;
     ;-=================================================================
     FUNCTION STRUCT_ANONYMOUS_SINGLE, struct_in
       
       tags = TAG_NAMES(struct_in)

       FOR ii=0,N_TAGS(struct_in)-1 DO BEGIN
         IF MIR_IS_STRUCT(struct_in.(ii)) THEN BEGIN
           struct_out = BUILD_STRUCTURE(tags[ii], STRUCT_ANONYMOUS(struct_in.(ii)), STRUCT=struct_out)
         ENDIF ELSE BEGIN
           struct_out = BUILD_STRUCTURE(tags[ii], struct_in.(ii), STRUCT=struct_out)
         ENDELSE
       ENDFOR

       RETURN, struct_out


     END ;FUNCTION STRUCT_ANONYMOUS_SINGLE



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Convert named structures or arrays of structures 
     ;   into anonymous structures (or arrays of).
     ;
     ;
     ;-=================================================================
     FUNCTION STRUCT_ANONYMOUS, struct_in

       dim = SIZE(struct_in, /DIM)

       IF dim[0] LE 1 THEN BEGIN

         RETURN, STRUCT_ANONYMOUS_SINGLE(struct_in)

       ENDIF ELSE BEGIN

         list = OBJ_NEW('SIMPLELIST')
         FOR ii=0,N_ELEMENTS(struct_in)-1 DO BEGIN
           list->APPEND, STRUCT_ANONYMOUS(struct_in[ii])
         ENDFOR
         array = list->TO_ARRAY()
         list->DESTROY

         RETURN, array

       ENDELSE
       

     END ;FUNCTION STRUCT_ANONYMOUS
