





;+==============================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
; DATE:
;   2010-09
;
; PURPOSE:
;   Fill a 1D array from a structure.
;   
;   It is assumed that there are the same number of elements in array
;   to be filled as in the structure.
;   
; DESCRIPTION:
;   This is very similar to <FILL_INDEX_STRUCTURE>, and will fill using the
;   same ordering.
;
;-==============================================================================


     FUNCTION STRUCTURE_FILL_ARRAY, struct, array_in, INDEX=index, NO_COPY=no_copy


       IF KEYWORD_SET(no_copy) THEN BEGIN
         array = TEMPORARY(array_in)
       ENDIF ELSE BEGIN
         array = array_in
       ENDELSE
       
       num_tags = N_TAGS(struct)

       MIR_DEFAULT, index, 0

       FOR ii=0,num_tags-1 DO BEGIN

         IF SIZE(struct.(ii), /TYPE) EQ 8 THEN BEGIN
           array = STRUCTURE_FILL_ARRAY(struct.(ii), array, INDEX=index, /NO_COPY)
         ENDIF ELSE BEGIN
           num_elements = N_ELEMENTS(struct.(ii))
           array[index:index+num_elements-1] = struct.(ii)
           index += num_elements
         ENDELSE

       ENDFOR
  
       RETURN, array

     END ;FUNCTION STRUCTURE_FILL_ARRAY
