





;+==============================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
; DATE:
;   2010-09
;
; PURPOSE:
;   Fill a structure given an 1D array.
;   
;   It is assumed that there are the same number of elements in structure
;   to be filled as in the array.
;   
; DESCRIPTION:
;   This is very similar to <FILL_INDEX_STRUCTURE>, and will fill using the
;   same ordering.
;
;-==============================================================================


     FUNCTION STRUCTURE_FILL_FROM_ARRAY, struct_in, array, INDEX=index, NO_COPY=no_copy


       IF KEYWORD_SET(no_copy) THEN BEGIN
         struct = TEMPORARY(struct_in)
       ENDIF ELSE BEGIN
         struct = struct_in
       ENDELSE
       
       num_tags = N_TAGS(struct)

       MIR_DEFAULT, index, 0

       FOR ii=0,num_tags-1 DO BEGIN

         IF SIZE(struct.(ii), /TYPE) EQ 8 THEN BEGIN
           struct.(ii) = STRUCTURE_FILL_FROM_ARRAY(struct.(ii), array, INDEX=index, /NO_COPY)
         ENDIF ELSE BEGIN
           num_elements = N_ELEMENTS(struct.(ii))
           struct.(ii) = array[index:index+num_elements-1]
           index += num_elements
         ENDELSE

       ENDFOR
  
       RETURN, struct

     END ;FUNCTION STRUCTURE_FILL_FROM_ARRAY
