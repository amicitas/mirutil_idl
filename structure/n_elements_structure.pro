





;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-11
;
; TAGS:
;   EXCLUDE_FILE_HEADER
;   
;
;-======================================================================




     ;+=================================================================
     ; PURPOSE:
     ;   Count the total number of data elements in a give structure.
     ;   This is done recursively.
     ;
     ;-=================================================================
     FUNCTION N_ELEMENTS_STRUCTURE, struct

       
       num_tags = N_TAGS(struct)

       count = 0

       FOR ii=0,num_tags-1 DO BEGIN
         IF SIZE(struct.(ii), /TYPE) EQ 8 THEN BEGIN
           count += N_ELEMENTS_STRUCTURE(struct.(ii))
         ENDIF ELSE BEGIN
           count += N_ELEMENTS(struct.(ii))
         ENDELSE
       ENDFOR
  
       RETURN, count

     END ;FUNCTION N_ELEMENTS_STRUCTURE
