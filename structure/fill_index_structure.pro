





;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-11
;
; TAGS:
;   EXCLUDE_FILE_HEADER
;   
;
;-======================================================================




     ;+=================================================================
     ; PURPOSE:
     ;   Fill a structure with indexes for each data item.
     ;   This is done recursively.
     ;
     ;-=================================================================
     FUNCTION FILL_INDEX_STRUCTURE, struct_in, index, NO_COPY=no_copy


       IF KEYWORD_SET(no_copy) THEN BEGIN
         struct = TEMPORARY(struct_in)
       ENDIF ELSE BEGIN
         struct = struct_in
       ENDELSE
       
       num_tags = N_TAGS(struct)

       MIR_DEFAULT, index, 0

       FOR ii=0,num_tags-1 DO BEGIN

         IF SIZE(struct.(ii), /TYPE) EQ 8 THEN BEGIN
           struct.(ii) = FILL_INDEX_STRUCTURE(struct.(ii), index, /NO_COPY)
         ENDIF ELSE BEGIN
           num_elements = N_ELEMENTS(struct.(ii))
           struct.(ii) = FINDGEN(num_elements) + index
           index += num_elements
         ENDELSE

       ENDFOR
  
       RETURN, struct

     END ;FUNCTION FILL_INDEX_STRUCTURE
