
;+======================================================================
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;   novimir.pablant@amicitas.com
;
; DATE:
;   2016-03
;
; VERSION:
;   1.0.0
;
; PURPOSE:
;   Read a complete CERDATA calibration file and return the data
;   in an ORDEREDHASH object.
;
;   CERDATA calibration files use the LABEL-DATA format.  
;   Calibrations are associated with shots.
;
; PROGRAMING NOTES:
;   To read cerdata files <LABEL_DATA_FORMAT_READ()> is used with
;   <STREAM_FILE_CERDATA::>.  
;
;   <STREAM_FILE_CERDATA::> will determine the available shot ranges 
;   and send them one by one to <LABEL_DATA_FORMAT_READ()>.  The
;   output from the label-data format reader will then be examined
;   here in <MIR_READ_CERDATA()>.
;
;
;-======================================================================


     FUNCTION MIR_READ_CERDATA_FILE, filepath

       shot_type = 'SHOT'

       ; Create a new output object
       output = ORDEREDHASH()
       
       ; Create a new CERDATA file stream.
       cerdata_stream = OBJ_NEW('STREAM_FILE_CERDATA', filepath, shot_type, /VERBOSE)

       ; Open the CERDATA file.
       cerdata_stream->OPEN_FILE

       ; If no name is given then return all data for the first
       ; valid shot range.
       IF ISA(name) THEN BEGIN
         find_name = 1
       ENDIF ELSE BEGIN
         find_name = 0
       ENDELSE

       ; Start the read loop.  Read untill we have reached the end of the file.
       WHILE (~ cerdata_stream->END_OF_FILE()) DO BEGIN

         ; Read the first shot found
         current_data = LABEL_DATA_FORMAT_READ(cerdata_stream, _STRICT_EXTRA=extra)
         ; Check to see if any data was found
         IF TAG_NAMES(current_data, /STRUCTURE_NAME) EQ 'NULL' THEN BEGIN
           CONTINUE
         ENDIF

         ; Check to see if the requested data was found.
         IF HAS_TAG(current_data, shot_type, INDEX=index) THEN BEGIN
           shot_start = current_data.(index)
         ENDIF ELSE BEGIN
           MIR_LOGGER, /TESTING, 'How did I get here?'
           CONTINUE
         ENDELSE

         output[shot_start] = current_data

       ENDWHILE

       OBJ_DESTROY, cerdata_stream

       RETURN, output
         
     END ;FUNCTION MIR_READ_CERDATA
