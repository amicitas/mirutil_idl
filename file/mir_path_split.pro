

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
; DATE:
;   2009-05-04
;
; PURPOSE:
;   Retrive various portions of a filepath.
;   
;
; TAGS:
;   EXCLUDE_FILE_HEADER
;-======================================================================

     ;+=================================================================
     ; PURPOSE:
     ;   Retrive various portions of a filepath.
     ;
     ;
     ; TAGS:
     ;   INDEX, CREATE_PAGE
     ;-=================================================================
     FUNCTION MIR_PATH_SPLIT, filepath

       sep = PATH_SEP()

       last_char = STRMID(filepath, 0, 1, /REVERSE)
       IF (last_char EQ sep) THEN BEGIN
         path = STRMID(filepath, 0, STRLEN(filepath)-1)
         filename = ''
         filehead = ''
         filetail = ''

       ENDIF ELSE BEGIN

         path = STRMID(filepath, 0, STRPOS(filepath, sep, /reverse_search)+1)
         filename = FILE_BASENAME(filepath)
         
         dot_pos = STRPOS(filename, '.', /REVERSE_SEARCH)
         IF dot_pos NE -1 THEN BEGIN
           filehead = STRMID(filename, 0, dot_pos)
           filetail = STRMID(filename, dot_pos+1)
         ENDIF ELSE BEGIN
           filehead = ''
           filetail = ''
         ENDELSE

       ENDELSE

       RETURN, {filepath:filepath $
                ,filename:filename $
                ,filehead:filehead $
                ,filetail:filetail $
                ,path:path $
               }

     END ;FUNCTION MIR_PATH_SPLIT
