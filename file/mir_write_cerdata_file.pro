
;+======================================================================
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;   novimir.pablant@amicitas.com
;
; DATE:
;   2016-03
;
; VERSION:
;   1.0.0
;
; PURPOSE:
;   Read a complete CERDATA calibration file and return the data
;   in an ORDEREDHASH object.
;
;   CERDATA calibration files use the LABEL-DATA format.  
;   Calibrations are associated with shots.
;
; PROGRAMING NOTES:
;   To read cerdata files <LABEL_DATA_FORMAT_READ()> is used with
;   <STREAM_FILE_CERDATA::>.  
;
;   <STREAM_FILE_CERDATA::> will determine the available shot ranges 
;   and send them one by one to <LABEL_DATA_FORMAT_READ()>.  The
;   output from the label-data format reader will then be examined
;   here in <MIR_READ_CERDATA()>.
;
;
;-======================================================================


     PRO MIR_WRITE_CERDATA_FILE, filepath $
                                 ,cerdata_hash $
                                 ,OVERWRITE=overwrite

       IF ~ KEYWORD_SET(overwrite) THEN BEGIN
         IF FILE_TEST(filepath) THEN BEGIN
           PRINT, 'File: ',+filepath
           PRINT, 'Already exists. Overwrite?'
           answer = ''
           READ, FORMAT='(a0)', answer
           answer = STRTRIM(answer,1)
           IF STRPOS(STRLOWCASE(answer), 'y') NE 0 $
             AND STRPOS(answer, '1') NE 0 THEN BEGIN
             PRINT, 'Returning . . .'
             RETURN
           ENDIF

         ENDIF
       ENDIF
       

       ; Clear the file
       OPENW, lun_write, filepath, /GET_LUN
       FREE_LUN, lun_write

       shots = cerdata_hash.KEYS()
       index_sort = REVERSE(SORT(shots.TOARRAY()))
     
       FOR ii = 0, N_ELEMENTS(cerdata_hash)-1 DO BEGIN
         value = cerdata_hash[shots(index_sort[ii])]
         
         LABEL_DATA_FORMAT_WRITE, value $
                                  ,FILENAME=filepath $
                                  ,/APPEND $
                                  ,LABEL_WIDTH=25 $
                                  ,/DOUBLE $
                                  ,/QUIET

         OPENW, lun_write, filepath, /APPEND
         PRINTF, lun_write, STRING(10B)
         PRINTF, lun_write, STRING(10B)
         FREE_LUN, lun_write
       END

       MIR_LOGGER, /INFO, STRING("Wrote cerdata file: ", filepath)
         
     END ;FUNCTION MIR_WRITE_CERDATA
