
;+==============================================================================
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
;
; PURPOSE:
;   Manipulate cerdata config structures and files.
;
;-==============================================================================



     ;+=========================================================================
     ; PURPOSE:
     ;   
     ;
     ;-=========================================================================
     FUNCTION MIR_CERDATA::INIT

       self['is_loaded'] = false
       self['filepath'] = ''

       RETURN, 1
     END ; FUNCTION MIR_CERDATA::INIT

     
     ;+=========================================================================
     ; PURPOSE:
     ;   
     ;
     ;-=========================================================================
     PRO MIR_CERDATA::READ_CERDATA_FILE, filepath

       self['filepath'] = filepath
       self['cerdata'] = MIR_READ_CERDATA_FILE(filepath)
       self['is_loaded'] = 1
       
     END ; PRO MIR_CERDATA::READ_CERDATA_FILE

     
     ;+=========================================================================
     ; PURPOSE:
     ;   
     ;
     ;-=========================================================================
     PRO MIR_CERDATA::READ_CERDATA_FILE, filepath

       MIR_WRITE_CERDATA_FILE(self['cerdata'])
       
     END ; PRO MIR_CERDATA::READ_CERDATA_FILE

     
     ;+=========================================================================
     ; PURPOSE:
     ;   
     ;
     ;-=========================================================================
     PRO MIR_CERDATA__DEFINE
       
       struct = {MIR_CERDATA $
                 ,INHERITS MIR_HASH_OBJECT $
                }

     END ;MIR_CERDATA__DEFINE
