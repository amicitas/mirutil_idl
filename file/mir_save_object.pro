


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
; DATE:
;   2009-09
;
; VERSION:
;   0.0.0
;
; PURPOSE:
;   Create a savefile containing a single variable.
;
; DESCRIPTION:
;   It is often convenient to save a single variable into an
;   IDL save file.  To restore the variable however one must know the 
;   name of the variable that was saved.
;
;   This function, along with [MIR_RESTORE_OBJECT]  automates the process 
;   so that a variable can be retrived without knowing its name 
;   beforehand and without causing any conflicts with existing 
;   variables.
;
;
;-======================================================================



     PRO MIR_SAVE_OBJECT, object $
                        ,QUIET=quiet $
                        ,FILENAME=filename $
                        ,HELP=help $
                        ,_REF_EXTRA=extra

       IF KEYWORD_SET(help) THEN BEGIN & INFO & RETURN & ENDIF
       
       ON_ERROR, 2

       CATCH, error
       IF error EQ 0 THEN BEGIN
         ; First rename the object.
         __mir_save_object = TEMPORARY(object)

         ; Now save the file.
         SAVE, __mir_save_object, FILENAME=filename , _STRICT_EXTRA=extra

         object = TEMPORARY(__mir_save_object)

         IF ISA(filename) AND ~KEYWORD_SET(quiet) THEN BEGIN
           PRINT, 'Object saved to: '
           PRINT, '  '+filename
         ENDIF

       ENDIF ELSE BEGIN
         CATCH, /CANCEL
         IF ISA(__mir_save_object) THEN BEGIN
           object = TEMPORARY(__mir_save_object)
         ENDIF
         MESSAGE, /REISSUE
       ENDELSE

     END ;FUNCTION MIR_SAVE_OBJECT
