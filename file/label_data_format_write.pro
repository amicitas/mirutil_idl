

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
; DATE:
;   2010-03
;
; VERSION:
;   1.0.0
;
; PURPOSE:
;   Write files formatted using the label-data format.
;   
; DESCRIPTION:
;
; TO DO:
;   Generalize routines to use DATA_STREAM objects.  
;   The output can then be directed to places other than a file or 
;   the terminal.  See <LABEL_DATA_FORMAT_READ>, where this is already 
;   done.
;
;   Define better error handling for unknown data types.  
;   For the time being an error is simply printed to the terminal.
;
;-======================================================================




     ;+=================================================================
     ; PURPOSE:
     ;   Write the output from a structure, with lables, to a file.
     ;
     ; DESCRIPTION:
     ;   See the description and example for [LABEL_DATA_FORMAT_WRITE]
     ;
     ;
     ; This is a recursive routine.
     ;-=================================================================
     PRO LABEL_DATA_FORMAT_WRITE_STRUCTURE, input_structure $
                                            ,lun_write $
                                            ,LABELS=labels_in $
                                            ,DOUBLE=double $
                                            ,LABEL_WIDTH=label_width $
                                            ,RECORD_WIDTH=record_width
       
       MIR_DEFAULT, record_width, 0
       MIR_DEFAULT, label_width, 0

       ; If no record width is given then set a default width for
       ; floating point records.
       ;
       ; Do not allow the float width to be less than 7.
       IF record_width EQ 0 THEN BEGIN
         IF KEYWORD_SET(double) THEN BEGIN
           float_width = 23
         ENDIF ELSE BEGIN
           float_width = 13
         ENDELSE
       ENDIF ELSE BEGIN
         float_width = record_width > 7
       ENDELSE
       
       num_labels = N_ELEMENTS(labels_in)
       
       ; Remove any leading '__' from the labels.
       FOR ii=0,num_labels-1 DO BEGIN
         IF STRCMP('__',labels_in[ii],2) THEN BEGIN
           labels_in[ii] = STRMID(labels_in[ii], 2)
         ENDIF
       ENDFOR

       fmt_write_start = STRING(num_labels+1, label_width, FORMAT='("(",i0,"(a-",i0,", 4x),")')

       ; Extract the tags from the input source.
       tags = TAG_NAMES(input_structure)
       num_tags = N_TAGS(input_structure)

       ; Print out the header comments if they exist.
       w = WHERE(tags EQ '_HEADER_COMMENTS', count_w)
       IF count_w GT 0 THEN BEGIN
         PRINTF, lun_write, input_structure.(w[0])
       ENDIF
       
       FOR ii=0,num_tags-1 DO BEGIN

         ; Define some error handling.
         CATCH, error
         IF error NE 0 THEN BEGIN
           CATCH, /CANCEL
           HELP, OUTPUT=last_message, /LAST_MESSAGE
           PRINT, last_message[0]
           CONTINUE
         ENDIF

         ; Skip the header comments.
         IF tags[ii] EQ '_HEADER_COMMENTS' THEN CONTINUE
         
         ; If this record is a structure, then add to the label list and 
         ; call this procedure again.
         IF (N_TAGS(input_structure.(ii)) NE 0) THEN BEGIN
           labels = BUILD_ARRAY(labels_in, tags[ii])
           LABEL_DATA_FORMAT_WRITE_STRUCTURE, input_structure.(ii), lun_write $
             ,LABELS=labels $
             ,DOUBLE=double $
             ,LABEL_WIDTH=label_width $
             ,RECORD_WIDTH=record_width
         ENDIF ELSE BEGIN
           
           ; Found data.  Write it out.
           num_data = N_ELEMENTS(input_structure.(ii))
           type_data = SIZE(input_structure.(ii), /TYPE)

           ; Take the data type and create a format code.
           CASE 1 OF
             ((WHERE(type_data EQ [2,3,12,13,14,15]))[0] NE -1): BEGIN
               ; Move the integers over by one space so that they allign with
               ; the first digit of the float entries.
               fmt_data = STRING(record_width-1 > 0, FORMAT='("1x, i-",i0)')
             END
             ((WHERE(type_data EQ [4,5]))[0] NE -1): BEGIN
               ; Set up the format code for floating point records.
               num_after_dot = float_width - 7
               fmt_data = STRING(float_width, num_after_dot, FORMAT='("e",i0,".",i0)')
             END
             (type_data EQ 7): BEGIN
               fmt_data = STRING(record_width-1 > 0, FORMAT='("1x, a-",i0)')
             END
             ELSE: MESSAGE, 'Unknown data type.'
           ENDCASE

           ; Put together the format code for the labels with a 
           ; format code for the data.
           fmt_write = fmt_write_start + STRING(num_data, fmt_data, FORMAT=('(i0,"(",a0,", 4x))")'))
           ;PRINT, fmt_write

           IF num_labels GT 0 THEN BEGIN
             PRINTF, lun_write, FORMAT=fmt_write, labels_in, tags[ii], input_structure.(ii)
           ENDIF ELSE BEGIN
             PRINTF, lun_write, FORMAT=fmt_write, tags[ii], input_structure.(ii)
           ENDELSE

         ENDELSE
       ENDFOR
           

     END ;PRO LABEL_DATA_FORMAT_WRITE_STRUCTURE


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Write the output from a nested structure to a file using
     ;   the label-data format.
     ;
     ; DESCRIPTION:
     ;   This procedure takes a filename and a data structure
     ;   from.  It will open the file and write the fit results 
     ;   to a file.
     ;
     ;   The format will be to start each line with the tag name
     ;   in the output structure.  The data will then follow.
     ;
     ;   Nested tags will have all tags placed before the data.
     ;
     ;   Tags starting with '_' will be printed without the leading
     ;   '_'.  This is useful in making labels that start with a number.
     ;
     ;
     ; EXAMPLE:
     ;   For an input structure:
     ;     struct = { some_numbers:[1, 2] $
     ;                ,nested:{numbers:[3, 4]} $
     ;              }
     ;
     ;   The following file will be written:
     ;
     ;     SOME_NUMBERS    1               2
     ;     NESTED          NUMBERS         3               4
     ;
     ;
     ;
     ; TAGS:
     ;   MAIN
     ;-=================================================================
     PRO LABEL_DATA_FORMAT_WRITE, input_structure $

                                  ,TERMINAL=terminal $

                                  ,FILENAME=filename_in $
                                  ,PATH=path $
                                  ,OVERWRITE=overwrite $
                                  ,APPEND=append $

                                  ,HEADER=header $

                                  ,DOUBLE=double $
                                  ,WIDTH=width $
                                  ,LABEL_WIDTH=label_width $
                                  ,RECORD_WIDTH=record_width $

                                  ,DEBUG=debug $
                                  ,QUIET=quiet
       
       FORWARD_FUNCTION BST_CHORD_NAME_PARSE


       ; ---------------------------------------------------------------
       ; Set some defaults
       IF N_ELEMENTS(width) NE 0 THEN BEGIN
         IF N_ELEMENTS(label_width) EQ 0 THEN BEGIN
           label_width = width
         ENDIF
         IF N_ELEMENTS(record_width) EQ 0 THEN BEGIN
           record_width = width
         ENDIF
       ENDIF
    
       ; ---------------------------------------------------------------
       ; Check to see if printing to a file is requested.
       CASE 1 OF
         KEYWORD_SET(debug): BEGIN
           terminal = 1
         END
         KEYWORD_SET(terminal): BEGIN
           terminal = 1
         END
         (N_ELEMENTS(filename_in) EQ 0): BEGIN
           terminal = 1
         END
         (filename_in EQ ''): BEGIN
           terminal = 1
         END
         ELSE: BEGIN
           terminal = 0
         ENDELSE
       ENDCASE
         

       ; ---------------------------------------------------------------
       ; If outputing to file, then join the file to the path and
       ; check for file existance.
       IF ~ terminal THEN BEGIN
         ; Join the file to the path, if necessary.
         IF KEYWORD_SET(path) THEN BEGIN
           filename = MIR_PATH_JOIN([path, filename_in])
         ENDIF ELSE BEGIN
           filename = filename_in
         ENDELSE
         
         ; First check if the file already exists
         IF ~ KEYWORD_SET(overwrite) AND ~ KEYWORD_SET(append) THEN BEGIN
           IF FILE_TEST(filename) THEN BEGIN
             PRINT, 'File: ',+filename
             PRINT, 'Already exists. Overwrite?'
             answer = ''
             READ, FORMAT='(a0)', answer
             answer = STRTRIM(answer,1)
             IF STRPOS(STRLOWCASE(answer), 'y') NE 0 $
               AND STRPOS(answer, '1') NE 0 THEN BEGIN
               PRINT, 'Returning . . .'
               RETURN
             ENDIF
           ENDIF
         ENDIF
       ENDIF
       



       ; ---------------------------------------------------------------
       ; Open the file

       ; Open file
       IF terminal THEN BEGIN
         lun_write = -1
       ENDIF ELSE BEGIN
         OPENW, lun_write, filename, /GET_LUN, APPEND=append
       ENDELSE

       ; ---------------------------------------------------------------
       ; Start Writing to file


       ; Write out the header
       IF N_ELEMENTS(header) NE 0 THEN BEGIN
         FOR ii=0,N_ELEMENTS(header)-1 DO BEGIN
           PRINTF, lun_write, FORMAT='("; ",a0)', header[ii]
         ENDFOR
       ENDIF

       ; Start writing tags to file.
       ; NOTE: This is a recursive routine that will print a nested structure.
       LABEL_DATA_FORMAT_WRITE_STRUCTURE, input_structure, lun_write  $
         ,DOUBLE=double $
         ,LABEL_WIDTH=label_width $
         ,RECORD_WIDTH=record_width


       ; ---------------------------------------------------------------
       ; Close the file

       ; Close file
       IF ~ KEYWORD_SET(terminal) THEN BEGIN
         FREE_LUN, lun_write
         IF ~KEYWORD_SET(quiet) THEN BEGIN
           MIR_LOGGER, /DEBUG, 'Data written to: ', filename
         ENDIF
       ENDIF

     END ; PRO LABEL_DATA_FORMAT_WRITE
