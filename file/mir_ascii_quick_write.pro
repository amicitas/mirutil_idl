


;+==============================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
; DATE:
;   2010-09
;
; PURPOSE:
;   Quickly write delimiated ascii files.
;
;-==============================================================================


     PRO MIR_ASCII_QUICK_WRITE, data $
                                ,filepath $
                                ,COMMENT_SYMBOL=comment_symbol $
                                ,SEPARATOR=separator $
                                ,WIDTHS=widths $
                                ,FORMAT=format_in $
                                ,DEBUG=debug
       COMPILE_OPT STRICTARR

       IF ISA(separator) THEN MESSAGE, 'SEPARATOR keyword not yet implemented.'

       MIR_DEFAULT, comment_symbol, ';'
       MIR_DEFAULT, separator, ' '
       MIR_DEFAULT, preserve_null, 1
       MIR_DEFAULT, widths, 0

       num_entries = N_ELEMENTS(data)
       num_tags = N_TAGS(data[0])
       tagnames = TAG_NAMES(data[0])

       ; Extract the data types.
       data_types = INTARR(num_tags)
       FOR ii=0,num_tags-1 DO BEGIN
         data_types[ii] = SIZE((data[0]).(ii), /TYPE)
       ENDFOR


       ; Generate an array with the field widths.
       IF N_ELEMENTS(widths) LT num_tags THEN BEGIN
         f_w = REPLICATE(STRING(widths[0], FORMAT='(i0)'), num_tags)
       ENDIF ELSE BEGIN
         f_w = STRARR(num_tags)
         FOR ii_tag=0, num_tags-1 DO BEGIN
           f_w[ii_tag] = STRING(widths[ii_tag], FORMAT='(i0)')
         ENDFOR
       ENDELSE


       ; Generate an array with the format codes.
       CASE N_ELEMENTS(format_in) OF
         0: BEGIN
           f_format = STRARR(num_tags)
           FOR ii_tag=0, num_tags-1 DO BEGIN
             f_code = MIR_GET_FORMAT_CODE(data_types[ii_tag], /E)
             f_format[ii_tag] = f_code + f_w[ii_tag]
           ENDFOR
         END
         1: BEGIN
           f_format =  REPLICATE(format_in[0], num_tags)
         END
         ELSE: BEGIN
           f_format = format_in
         ENDELSE
       ENDCASE




       ; Open the file
       OPENW, lun, filepath, /GET_LUN



       ; First write the header.
       header_string = ''
       FOR ii_tag=0, num_tags-1 DO BEGIN
         format = '(2x, a'+f_w[ii_tag]+')'
         header_string += STRING(tagnames[ii_tag], FORMAT=format)
       ENDFOR
       STRPUT, header_string, comment_symbol, 0
       PRINTF, lun, header_string

       ; Now write the data.
       FOR ii_entry=0, num_entries-1 DO BEGIN
         line = ''
         entry = data[ii_entry]

         FOR ii_tag=0, num_tags-1 DO BEGIN
           line += STRING(entry.(ii_tag), FORMAT='(2x,'+f_format[ii_tag]+')')
         ENDFOR
         PRINTF, lun, line
       ENDFOR


       FREE_LUN, lun


     END ;PRO MIR_ASCII_QUICK_READ
