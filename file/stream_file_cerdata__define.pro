
;+======================================================================
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;   novimir.pablant@amicitas.com
;
; DATE:
;   2010-03
;
; VERSION:
;   1.0.0
;
; PURPOSE:
;   Stream a CERDATA calibration file (*.cerdata).  
; 
;   This stream is mean to be used by <MIR_READ_CERDATA()> as 
;   the stream for <LABEL_DATA_FORMAT_READ()>.
;
;   Rather than steaming the full file, the file will be broken up
;   into the necessary shot ranges and each shot range streamed 
;   separatly.  To do this we do some read buffering so that we can
;   determine when the currently read shot range ends.
;
;-======================================================================




     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Initialize the cerdata file stream.
     ;
     ;
     ;-=================================================================
     FUNCTION STREAM_FILE_CERDATA::INIT, filepath, shot_type, VERBOSE=verbose

       self.stream_file = OBJ_NEW('STREAM_FILE', filepath, VERBOSE=verbose)

       MIR_DEFAULT, shot_type, 'SHOT'
       self.shot_type = shot_type

       self.line_buffer = LIST()
       
       RETURN, 1

     END ;FUNCTION STREAM_FILE_CERDATA::INIT



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Cleanup on object destruction.
     ;
     ;
     ;-=================================================================
     PRO STREAM_FILE_CERDATA::CLEANUP

       OBJ_DESTROY, self.stream_file

       self->STREAM::CLEANUP

     END ;PRO STREAM_FILE_CERDATA::CLEANUP



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Open the *.cerdata file.
     ;
     ;
     ;-=================================================================
     PRO STREAM_FILE_CERDATA::OPEN_FILE
  
       ; Reset the line buffer.
       self.line_buffer = LIST()
       
       self.stream_file->OPEN

     END ;PRO STREAM_FILE_CERDATA::OPEN_FILE



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Close the *.cerdata file.
     ;
     ;
     ;-=================================================================
     PRO STREAM_FILE_CERDATA::CLOSE_FILE

       self.stream_file->CLOSE

       ; Reset the line buffer.
       self.line_buffer = LIST()

     END ;PRO STREAM_FILE_CERDATA::CLOSE_FILE



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Open the stream
     ;
     ;
     ;-=================================================================
     PRO STREAM_FILE_CERDATA::OPEN

       IF self->IS_OPEN() THEN MESSAGE, 'Cerdata stream is already open.'

       ; Set the end_of_shot flag to zero.
       self.end_of_shot = 0
       self.data_found = 0
       
       ; Set the is_open flag
       self.is_open = 1


     END ;PRO STREAM_FILE_CERDATA::OPEN


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Close the stream
     ;
     ;
     ;-=================================================================
     PRO STREAM_FILE_CERDATA::CLOSE

       self.is_open = 0

     END ;PRO STREAM_FILE_CERDATA::CLOSE


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return TRUE if stream is open.
     ;   Otherwise reurn false.
     ;
     ;
     ;-=================================================================
     FUNCTION STREAM_FILE_CERDATA::IS_OPEN

       RETURN, self.is_open

     END ;FUNCTION STREAM_FILE_CERDATA::IS_OPEN


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return TRUE if the end of the stream has been reached.
     ;   Otherwise reurn false.
     ;
     ;
     ;-=================================================================
     FUNCTION STREAM_FILE_CERDATA::END_OF_STREAM

       IF ~ self->IS_OPEN() THEN MESSAGE, 'Cerdata stream is not open.'

       ; End of stream means 2 things:
       ;   1. The next line contains the 'SHOT' tag.
       ;   2. The end of the file has been reached.

       ; First check for end of file.
       IF self->END_OF_FILE() THEN BEGIN
         RETURN, 1
       ENDIF

       ; Now check for end of shot
       IF self->END_OF_SHOT() THEN BEGIN
         RETURN, 1
       ENDIF


       RETURN, 0


     END ;FUNCTION STREAM_FILE_CERDATA::END_OF_STREAM



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return TRUE if the end of the cerdata file has been reached.
     ;   Otherwise reurn false.
     ;
     ;
     ;-=================================================================
     FUNCTION STREAM_FILE_CERDATA::END_OF_FILE

       IF self.LINE_BUFFER_AVAILABLE() THEN RETURN, 0

       RETURN, self.end_of_file

     END ;FUNCTION STREAM_FILE_CERDATA::END_OF_FILE



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return TRUE if the end of the shot has been reached.
     ;   Otherwise reurn false.
     ;
     ;
     ;-=================================================================
     FUNCTION STREAM_FILE_CERDATA::END_OF_SHOT

       RETURN, self.end_of_shot

     END ;FUNCTION STREAM_FILE_CERDATA::END_OF_SHOT



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Check the lines in the line buffer to see if it contains the
     ;   SHOT tag.  If so set the end_of_shot flag.
     ;
     ;   Note: this will not check if the buffer only contains comments
     ;   or empty lines.
     ;
     ;
     ;-=================================================================
     PRO STREAM_FILE_CERDATA::CHECK_END_OF_SHOT

       IF ~ self.LINE_BUFFER_AVAILABLE() THEN MESSAGE, "Line buffer is empty."

       ; We always fill the line buffer untill the first non-comment line.
       ; So here we want to read the last line (the only non-comment line).
       line = STRUPCASE(STRTRIM(self.line_buffer[-1], 1))

       ; First check if the shot header of the given type is found.
       IF STRMATCH(line, self.shot_type+' *') THEN BEGIN
         self.end_of_shot = 1
         RETURN
       ENDIF

       ; If data has been found for this shot range, then check for any shot header.
       ; If no data is been found, check if there is data.
       IF self.data_found THEN BEGIN
         IF (STRMATCH(line, 'SHOT *') OR STRMATCH(line, 'CALIBRATION_SHOT *')) THEN BEGIN
           self.end_of_shot = 1
         ENDIF
       ENDIF ELSE BEGIN
         IF (~ STRMATCH(line, 'SHOT *')) $
            AND (~ STRMATCH(line, 'SHOT_END *')) $
            AND (~ STRMATCH(line, 'CALIBRATION_SHOT *')) $
            AND (~ STRMATCH(line, 'CALIBRATION_SHOT_END *')) $
            AND (~ STRMATCH(line, 'DATE *')) $
         THEN BEGIN
           
           self.data_found = 1
         ENDIF
       ENDELSE

     END ;PRO STREAM_FILE_CERDATA::PARSE_CURRENT_LINE


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Check if the given string start with the ';' or '!' character.
     ;
     ;
     ;-=================================================================
     FUNCTION STREAM_FILE_CERDATA::IS_COMMENT, line_in

       RETURN, (STRCMP(line_in, ';', 1) OR STRCMP(line_in, '!', 1))

     END ;FUCTION STREAM_FILE_CERDATA::IS_COMMENT

     
     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Check if the given string is empty.
     ;
     ;
     ;-=================================================================
     FUNCTION STREAM_FILE_CERDATA::IS_EMPTY, line_in

       RETURN, (STRTRIM(line_in, 2) EQ '')

     END ;FUCTION STREAM_FILE_CERDATA::IS_EMPTY

     
     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return 1 if lines are available in the line buffer.
     ;
     ;-=================================================================
     FUNCTION STREAM_FILE_CERDATA::LINE_BUFFER_AVAILABLE
  
       RETURN, (N_ELEMENTS(self.line_buffer) GT 0)
       
     END ; FUNCTION STREAM_FILE_CERDATA::LINE_BUFFER_AVAILABLE

     
     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Read the next line from the CERDATA file stream.
     ;
     ;
     ;-=================================================================
     FUNCTION STREAM_FILE_CERDATA::READ

       IF ~ self->IS_OPEN() THEN MESSAGE, 'Cerdata stream is not open.'

       ; Read into the buffer if needed.      
       IF ~ self.LINE_BUFFER_AVAILABLE() THEN BEGIN
         self.READ_UNTIL_NON_EMPTY
       ENDIF
       
       ; Get the next line.
       line_out = self.LINE_BUFFER.REMOVE(0)

       IF ~ self.LINE_BUFFER_AVAILABLE() THEN BEGIN       
         ; Buffer the next line.
         IF self.stream_file->END_OF_STREAM() THEN BEGIN
           self.end_of_file = 1
         ENDIF ELSE BEGIN
           self.READ_UNTIL_NON_EMPTY

           IF ~ self.end_of_shot THEN BEGIN
             ; Check for end of shot.
             self->CHECK_END_OF_SHOT
           ENDIF
         ENDELSE
       ENDIF

       RETURN, line_out
      

     END ;FUNCTION STREAM_FILE_CERDATA::READ


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Add lines to the line buffer until a non-empty line is found
     ;   in the file stream.
     ;
     ;   If no non-empty lines are found this will set the end_of_shot
     ;   flag.
     ;
     ; PRGRAMMING NOTES:
     ;   This routine will not set the end_of_file flag. Even if the
     ;   end of file is reached.  This is necessary to ensure that
     ;   the line buffering in <::READ> works correctly.
     ;
     ;   Accordingly this rouine should never be called directly.
     ;
     ;
     ;-=================================================================
     PRO STREAM_FILE_CERDATA::READ_UNTIL_NON_EMPTY

       IF ~ self->IS_OPEN() THEN MESSAGE, 'Cerdata stream is not open.'

       ; Get the next non-empty line.
       ; Take the data from the buffer if available.
       WHILE ~ self.stream_file->END_OF_STREAM() DO BEGIN
         line_out =  self.stream_file->READ()
         self.line_buffer.ADD, line_out

         IF ~self->IS_EMPTY(line_out) AND ~self.IS_COMMENT(line_out) THEN RETURN
       ENDWHILE

       ; If we get here in means we did not find any additional data.
       self.end_of_shot = 1

     END ;PRO STREAM_FILE_CERDATA::READ_UNTIL_NON_EMPTY


     
     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Read the next line from the file stream thas is not a comment
     ;   or an empty string.
     ;
     ;   This will return an empty string if the end of file is
     ;   encountered.
     ;
     ; PRGRAMMING NOTES:
     ;   This routine will not set the end_of_file flag. Even if the
     ;   end of file is reached.  This is necessary to ensure that
     ;   the line buffering in <::READ> works correctly.
     ;
     ;   Accordingly this rouine should never be called directly.
     ;
     ;
     ;-=================================================================
     FUNCTION STREAM_FILE_CERDATA::READ_NON_EMPTY

       IF ~ self->IS_OPEN() THEN MESSAGE, 'Cerdata stream is not open.'

       ; Get the next non-empty line.
       ; Take the data from the buffer if available.
       WHILE ~ self.stream_file->END_OF_STREAM() DO BEGIN
         line_out =  self.stream_file->READ()

         IF ~self->IS_EMPTY(line_out) AND ~self.IS_COMMENT() THEN BREAK
       ENDWHILE

       IF self.stream_file->END_OF_STREAM() THEN BEGIN
         IF self->IS_EMPTY(line_out) OR self.IS_COMMENT() THEN BEGIN
           line_out = ''
         ENDIF
       ENDIF

       RETURN, line_out

     END ;FUNCTION STREAM_FILE_CERDATA::READ_NON_EMPTY



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   This object will handle a CERDATA file stream.
     ;
     ;   This stream can be used in conjunction with
     ;   <LABEL_DATA_FORMAT_READ.()> to read in *.cerdata files.
     ;
     ;   The special point about this stream is that it will
     ;   break the input file into sections defined by the
     ;   SHOT and CALIBRATION_SHOT labels, and send each section as 
     ;   separate stream.
     ;
     ;
     ;-=================================================================
     PRO STREAM_FILE_CERDATA__DEFINE

       struct = { STREAM_FILE_CERDATA $
                  ,line_buffer:OBJ_NEW() $
                  
                  ,end_of_file:0 $
                  ,end_of_shot:0 $
                  ,is_open:0 $
                  ,stream_file:OBJ_NEW() $
                  ,shot_type:'' $

                  ,data_found:0 $
                  ,INHERITS STREAM $
                }

     END ;PRO STREAM_FILE_CERDATA__DEFINE
