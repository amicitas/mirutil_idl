


;+==============================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
; DATE:
;   2011-09
;
; PURPOSE:
;   Quickly write netcdf files from IDL structures or hash objects.
;
; DESCRIPTION:
;   For now this only reads a flat object.   In the future however this will
;   also be able to read in nested objects.
;
;   Also for now I assume double precision variables.
;
;-==============================================================================


     PRO MIR_NETCDF_QUICK_WRITE, data_in, filepath
       COMPILE_OPT STRICTARR

       CASE 1 OF
         ISA(data_in, 'STRUCT'): BEGIN
           hash = MIR_HASH()
           ; Initializing <HASH::> with a structure does not work.
           ; (stupid idl).  So do this manually.
           tags = TAG_NAMES(data_in)
           FOR ii=0,N_TAGS(data_in)-1 DO BEGIN
             hash[tags[ii]] = data_in.(ii)
           ENDFOR
         END
         ISA(data_in, 'HASH'): hash = data_in
         ELSE: MESSAGE, 'Input must be a structure or a hash object.'
       ENDCASE

       ; Create a netCDF file.
       id_root = NCDF_CREATE(filepath, /CLOBBER, /NETCDF4)

       dim_id_index = 0
       FOREACH value, hash, key DO BEGIN

         ; Create the variable.
         ndims = (SIZE(value))[0]
         IF ndims GT 0 THEN BEGIN

           ; Create dimensions.
           dim_ids = LONARR(ndims, /NOZERO)
           FOR ii=0,ndims-1 DO BEGIN
             dim_ids[ii] = NCDF_DIMDEF(id_root, STRING(dim_id_index, FORMAT='(i0)'), /UNLIMITED)
             dim_id_index += 1
           ENDFOR

           id_var = NCDF_VARDEF(id_root, key, dim_ids, /DOUBLE)
         ENDIF ELSE BEGIN
           id_var = NCDF_VARDEF(id_root, key, /DOUBLE)
         ENDELSE

         ; Add the variable data
         NCDF_VARPUT, id_root, id_var, value

       ENDFOREACH

       NCDF_CLOSE, id_root

     END ;PRO MIR_NETCDF_QUICK_WRITE
