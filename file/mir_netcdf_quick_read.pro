


;+==============================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
; DATE:
;   2011-09
;
; PURPOSE:
;   Quickly read netcdf files into IDL hash objects.
;
; DESCRIPTION:
;   For now this only reads a flat file.   In the future however this will
;   also be able to read in nested files.
;
;-==============================================================================


     FUNCTION MIR_NETCDF_QUICK_READ, filepath, STRUCTURE=structure
       COMPILE_OPT STRICTARR

       output = MIR_HASH()

       root_id = NCDF_OPEN(filepath)
       root_info = NCDF_INQUIRE(root_id)
       root_var_ids = NCDF_VARIDSINQ(root_id)


       FOR ii=0,root_info.nvars-1 DO BEGIN
         var_id = root_var_ids[ii]

         var_info = NCDF_VARINQ(root_id, var_id)
         NCDF_VARGET, root_id, var_id, var_data
         
         output[var_info.name] = TEMPORARY(var_data) 
       ENDFOR

       NCDF_CLOSE, root_id

       IF KEYWORD_SET(structure) THEN BEGIN
         output = output.ToStruct()
       ENDIF

       RETURN, output
     END ;FUNCTION MIR_NETCDF_QUICK_READ
