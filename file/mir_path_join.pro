

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
; DATE:
;   2009-05-04
;
; PURPOSE:
;   Inteligently join path names.
;   
;-======================================================================

     FUNCTION MIR_PATH_JOIN, path_array_in, UNIX=unix

       where_not_null = WHERE(path_array_in NE '')
       IF where_not_null[0] EQ -1 THEN BEGIN
         RETURN, ''
       ENDIF

       path_array = path_array_in(where_not_null)

       num_path = N_ELEMENTS(path_array)

       ; If there is only one element, then just return it.
       IF num_path EQ 1 THEN BEGIN
         RETURN, path_array[0]
       ENDIF

       ; Strip any trailing /'s
       FOR ii=0,num_path-2 DO BEGIN
         last_char = STRMID(path_array[ii], 0, 1, /REVERSE)
         IF (last_char EQ '/') OR (last_char EQ '\') THEN BEGIN
           path_array[ii] = STRMID(path_array[ii], 0, STRLEN(path_array[ii])-1)
         ENDIF
       ENDFOR

       ; Check for any leading /'s
       FOR ii=1,num_path-1 DO BEGIN
         IF STRCMP(path_array[ii], '/', 1) OR STRCMP(path_array[ii], '\', 1) THEN BEGIN
           MESSAGE, 'A path other than the first is an absolute path.'
         ENDIF
       ENDFOR

       path = STRJOIN(path_array, PATH_SEP())

       IF KEYWORD_SET(unix) THEN BEGIN
         sep = '/'
       ENDIF ELSE BEGIN
         sep = PATH_SEP()
       ENDELSE

       IF sep EQ '/' THEN BEGIN
         wrong_sep = '\'
       ENDIF ELSE BEGIN
         wrong_sep = '/'
       ENDELSE

       path = STRJOIN(STRSPLIT(path, wrong_sep, /PRESERVE, /EXTRACT), sep)

       RETURN, path

     END ; FUNCTION MIR_PATH_JOIN
