

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
; DATE:
;   2013-12-19
;
; PURPOSE:
;   Create the directory for the given filename.
;   If a directory is given then make the directory.
;-======================================================================

     PRO MIR_MAKE_PATH, filepath, QUIET=quiet
       
       path_info = MIR_PATH_SPLIT(filepath)

       IF ~ FILE_TEST(path_info.path) THEN BEGIN
         IF ~ KEYWORD_SET(quiet) THEN BEGIN
           MIR_LOGGER, /INFO, 'Creating directory: '+path_info.path
         ENDIF
         FILE_MKDIR, path_info.path
       ENDIF

     END ;PRO MIR_MAKE_PATH
