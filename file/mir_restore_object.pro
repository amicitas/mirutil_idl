


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
; DATE:
;   2011-09
;
; PURPOSE:
;   Return the contents of a savefile created with MIR_SAVE_OBJECT.
;
; DESCRIPTION:
;   It is often convenient to save a single variable into an
;   IDL save file.  To restore the variable however one must know the 
;   name of the variable that was saved.
;
;   This function, along with <MIR_SAVE_OBJECT> automates the process 
;   so that a variable can be retrived without knowing its name 
;   beforehand and without causing any conflicts with existing 
;   variables.
; 
;
;
;-======================================================================



     FUNCTION MIR_RESTORE_OBJECT, filename $
                                  ,VERBOSE=verbose $
                                  ,_REF_EXTRA=extra
       ON_ERROR, 2

       CATCH, error
       IF error NE 0 THEN BEGIN
         CATCH, /CANCEL
         MIR_LOGGER, /ERROR, /LAST_MESSAGE
         MESSAGE, 'Error restoring save object.'
       ENDIF

       IF KEYWORD_SET(verbose) THEN BEGIN
         MIR_LOGGER, 'Restoring object from: '+filename, /INFO
       ENDIF

       IF ISA(filename) THEN BEGIN
         obj_save = OBJ_NEW('IDL_SAVEFILE', filename, _STRICT_EXTRA=extra)
       ENDIF ELSE BEGIN
         obj_save = OBJ_NEW('IDL_SAVEFILE', _STRICT_EXTRA=extra)
       ENDELSE


       variables = obj_save->NAMES()
       CASE variables[0] OF

         '__MIR_SAVE_OBJECT': BEGIN
           obj_save->RESTORE, '__MIR_SAVE_OBJECT'
           RETURN, __mir_save_object
         END

         ; I changed the name of the internal save variable on 2011-09-03.
         ; To retain compatability with old SAVE_OBJECT files, also restore
         ; objects using the old name.
         '__SAVE_OBJECT': BEGIN
           obj_save->RESTORE, '__SAVE_OBJECT'
           RETURN, __save_object
         END

         ELSE: MESSAGE, 'Save file was not created using MIR_SAVE_OBJECT.'
       ENDCASE

     END ;FUNCTION MIR_RESTORE_OBJECT
