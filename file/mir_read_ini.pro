; Copyright (c) 2004, Forschungszentrum Juelich GmbH ICG
; All rights reserved.
; Unauthorized reproduction prohibited.
; This software may be used, copied, or redistributed as long as it is not
; sold and this copyright notice is reproduced on each copy made.  This
; routine is provided as is without any express or implied warranties
; whatsoever.
;
;+==============================================================================
; NAME:
;   MIR_READ_INI
;
; PURPOSE:
;   This function reads ini files and returns a structure of the values. 
;   The structure is nested by the section where the value are found.
;
; DESCRIPTION:
;   This is a modification of the the routine <READ_INI> from the ICG idl 
;   library.
;
;   Requires several functions from the ICG library (with renaming).
;
; CALLING SEQUENCE:
;   result = ICG_READ_INI(file,[section=section])
;
; INPUTS:
;   file: the file to read from, one or more files
;
; KEYWORD PARAMETERS:
;   SECTIONS = [string]
;      A string array containing the sections to be read.
;      If only one section is needed a scalar string can be given.
;
;   IGNORED = [string]
;      A string array containing the sections to be ignored.
;      If only one section is ignored a scalar string can be given.
;
;   COMMENT_CHARACTER = string
;      (default = ';#')
;      A character, or set of characters, that will be used to denote
;      comments.
;
;   PREFIX_CHARACTER = string
;      (default = '')
;      An optional required prefix string expected before every line
;      to be read. This can be useful for example when reading a configuration
;      that is stored in the comments of a data file.
;
;   /STRICT
;      If enabled, only well formed data will be allowed. Poorly formed
;      data will cause an exception.  Otherwise a best-effort attempt
;      will be made to understand the data in every case.
;      At the moment this only applies to quoted strings.
;
; EXAMPLE:
;   Sections are indicated by a name in brackets ([]).
;   They are written in uppercases. Variables are written in lowercases.
;
;   If these lines are saved to a file named file.ini:
;     [TEST]
;     a=1
;     b=1.2
;
;   result=icg_read_ini('file.ini')
;   help,result,/str
;     ** Structure <843ef3c>, 1 tags, length=8, data length=8, refs=1:
;     TEST            STRUCT    -> <Anonymous> Array[1]
;
;   help,result.test,/str
;     ** Structure <841a58c>, 2 tags, length=8, data length=8, refs=2:
;     A               LONG                 1
;     B               FLOAT           1.20000
;
; AUTHOR:
;   R.Bauer
;
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
; MODIFICATION HISTORY:
;  2004-12-12: Written by:  R.Bauer
;
;  2006-07-10:RB    bug fixed section parameters compared always uppercase.
;  2011-08-01:novi  Added option to specify a line prefix character.
;-==============================================================================
     
     FUNCTION MIR_READ_INI, file $
                            ,SECTIONS=section_in $
                            ,IGNORED=ignored_in $
                            ,COMMENT_CHARACTER=comment_char $
                            ,PREFIX_CHARACTER=prefix_char $
                            ,STRICT=strict $
                            ,DEBUG=debug
        
       MIR_DEFAULT, comment_char, ';#'
       MIR_DEFAULT, prefix_char,  ''

       IF ISA(section_in) THEN sections = STRUPCASE(section_in)
       IF ISA(ignored_in) THEN ignored = STRUPCASE(ignored_in)

       num_sections = N_ELEMENTS(sections)

       txt=''
       value=0
       n_file=N_ELEMENTS(file)
       FOR ii=0,n_file-1 DO BEGIN
         current_section = ''
         sections_found = 0

         IF KEYWORD_SET(debug) THEN BEGIN
           MIR_LOGGER, /DEBUG, 'Reading file: '+file[ii]
         ENDIF
         
         IF ~FILE_TEST(file[ii]) THEN BEGIN
           MESSAGE, 'Could not open file: '+file[ii]
         ENDIF
           
         OPENR,lun,/GET_LUN,file[ii]
         line_counter=0
         WHILE ~ EOF(lun) DO BEGIN

           CATCH, error
           IF error NE 0 THEN BEGIN
             CATCH, /CANCEL
             MIR_LOGGER, /ERROR, ['Could not parse line in section:  '+current_section $
                                  ,STRING(line_counter, FORMAT='(2x, i7,": ")')+txt] $
                         ,LAST_MESSAGE=debug
             CONTINUE
           ENDIF

           READF,lun,txt
           line_counter=line_counter+1
           txt = STRTRIM(txt,2)

           skip_line = 0
           is_section_head = 0

           ; -------------------------------------------------------------------
           ; Check if this is a valid line.
           IF prefix_char NE '' THEN BEGIN
             IF STRPOS(txt, prefix_char) NE 0 THEN skip_line = 1

             IF ~ skip_line THEN BEGIN
               txt = STRMID(txt,1)
               txt = STRTRIM(txt,1)
             ENDIF
           ENDIF

           IF STRLEN(txt) EQ 0 THEN  skip_line = 1

           ; Deal with comment characters.
           IF STRLEN(comment_char) GT 0 THEN BEGIN
             FOR ii_char=0,STRLEN(comment_char)-1 DO BEGIN
               char = STRMID(comment_char, ii_char, 1)
               IF STRPOS(txt, char) EQ 0 THEN  skip_line = 1

               ; remove comments after the values.
               IF STRPOS(txt, char) GT 0 THEN BEGIN
                 txt = (STRSPLIT(txt,char,/EXTRACT))[0]
               ENDIF
             ENDFOR
           ENDIF

           is_section_head = (STRPOS(txt,'[') EQ 0)


           ; -------------------------------------------------------------------
           ; Begin parsing valid lines.
           IF is_section_head THEN BEGIN
             IF ISA(sstruct) THEN BEGIN
               IF ISA(sections) THEN BEGIN
                 IF ICG_IS_ELEMENT_OF(current_section, sections) THEN BEGIN
                   sections_found++
                 ENDIF ELSE BEGIN
                   ignore_section = 1
                 ENDELSE
               ENDIF
               IF ~ ignore_section THEN BEGIN
                 struct = BUILD_STRUCTURE(current_section, TEMPORARY(sstruct), struct)
               ENDIF

               IF ISA(sections) THEN BEGIN
                 ; If we have found all of the requested sections then stop reading the file. 
                 IF sections_found EQ num_sections THEN BREAK
               ENDIF

             ENDIF
           ENDIF


           ; Don't parse this line if invalid.
           IF skip_line THEN CONTINUE

           IF is_section_head THEN BEGIN
             current_section = STRUPCASE(ICG_WITHIN_BRACKETS(txt))
             IF ISA(ignored) THEN BEGIN
               ignore_section = ICG_IS_ELEMENT_OF(current_section, ignored)
             ENDIF ELSE BEGIN
               ignore_section = 0
             ENDELSE

           ENDIF ELSE BEGIN
             IF ~ ignore_section THEN BEGIN 
               TV = ICG_STR_SEP2(txt,'=')
               value = MIR_VALUE_FROM_STRING(tv[1], info, STRICT=strict)
               
               sstruct = BUILD_STRUCTURE(STRTRIM(tv[0],2), value, sstruct)
             ENDIF
           ENDELSE

         ENDWHILE

         ; If we reached the end of the file then add in the final entries.
         IF  EOF(lun) THEN BEGIN
           IF ISA(sstruct) THEN BEGIN
             IF ~ ignore_section THEN BEGIN
               struct = BUILD_STRUCTURE(current_section, TEMPORARY(sstruct), struct)
             ENDIF
           ENDIF
         ENDIF

       
         FREE_LUN,lun
         IF ii GT 0 THEN BEGIN
           result = [result, TEMPORARY(struct)] 
         ENDIF ELSE BEGIN
           result=TEMPORARY(struct)
         ENDELSE

       ENDFOR
       RETURN, result

     END ; FUNCTION MIR_READ_INI
     
