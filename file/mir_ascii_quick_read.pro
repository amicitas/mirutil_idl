


;+==============================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
; DATE:
;   2010-09
;
; PURPOSE:
;   Quickly read delimiated ascii files.
;
;-==============================================================================


     FUNCTION MIR_ASCII_QUICK_READ, filepath $
                                    ,FIELD_NAMES=field_names $
                                    ,FIELD_TYPES=field_types $

                                    ,COMMENT_SYMBOL=comment_symbol $
                                    ,SEPARATOR=separator $
                                    ,PRESERVE_NULL=preserve_null $
                                    ,NUM_HEADER_LINES=num_header_lines $

                                    ,HASH=hash $

                                    ,VERBOSE=verbose $
                                    ,HELP=help $
                                    ,DEBUG=debug

       IF KEYWORD_SET(help) THEN BEGIN
         INFO
         RETURN, !null
       ENDIF
       MIR_TIMER, /START

       
       MIR_DEFAULT, comment_symbol, ''
       MIR_DEFAULT, separator, ' '
       MIR_DEFAULT, preserve_null, 0
       MIR_DEFAULT, hash, 0
       MIR_DEFAULT, num_header_lines, 0

       MIR_LOGGER, /INFO, 'Reading ASCII data from: '+filepath

       ; Get the number of comment symbols.
       num_comment_symbols = N_ELEMENTS(comment_symbol)
       comment_symbol_len = STRLEN(comment_symbol)

       ; Open the file
       OPENR, lun, filepath, /GET_LUN
       line = ''

       ; Skip any header lines.
       FOR ii=0,num_header_lines-1 DO BEGIN
         READF, lun, line
       ENDFOR
       ; Save the location after the header.
       POINT_LUN, -1*lun, start_position


       ; Find the first row of data and count the total number of lines.
       found_first_line = 0
       num_lines = 0L

       WHILE ~ EOF(lun) DO BEGIN
         READF, lun, line
         line = STRTRIM(line, 2)

         IF line EQ '' THEN CONTINUE

         skip_line = 0
         FOR ii_sym=0, num_comment_symbols-1 DO BEGIN
           IF  comment_symbol_len[ii_sym] GT 0 THEN BEGIN
             IF STRCMP(line, comment_symbol[ii_sym], comment_symbol_len[ii_sym]) THEN skip_line=1
           ENDIF
         ENDFOR
         IF skip_line THEN CONTINUE

         num_lines += 1

         IF ~ found_first_line THEN BEGIN
           first_line = line
           found_first_line = 1
         ENDIF

       ENDWHILE

       IF ~ found_first_line THEN BEGIN
         MESSAGE, /INFO, 'File contains no data.'
         RETURN, !null
       ENDIF

       ; We have found the first line.
       line_array = STRSPLIT(first_line, separator, /EXTRACT, PRESERVE_NULL=preserve_null)
       line_array = STRTRIM(line_array, 2)
       num_fields = N_ELEMENTS(line_array)


       ; Guess the field types from the first line if needed.
       IF ~ ISA(field_types) THEN BEGIN
         field_types = INTARR(num_fields)
         FOR ii=0,num_fields-1 DO BEGIN
           field_types[ii] = MIR_GUESS_DATA_TYPE(line_array[ii])
         ENDFOR
       ENDIF

       ; Generate field names if not given.
       IF ~ ISA(field_names) THEN BEGIN
         field_names = STRARR(num_fields)
         FOR ii=0,num_fields-1 DO BEGIN
           field_names[ii] = 'FIELD_'+STRING(ii, FORMAT='(i04)')
         ENDFOR
       ENDIF

       field_data = PTRARR(num_fields)
       FOR ii=0,num_fields-1 DO BEGIN
         field_data[ii] = PTR_NEW(MAKE_ARRAY(num_lines, TYPE=field_types[ii]))
       ENDFOR

       ; Go back to the beginning of the file.
       POINT_LUN, lun, start_position


       ; -----------------------------------------------------------------------
       ; Now start reading.
       line = ''
       ii_data = 0L

       WHILE ~ EOF(lun) DO BEGIN

         READF, lun, line
         line = STRTRIM(line, 2)

         IF line EQ '' THEN CONTINUE

         skip_line = 0
         FOR ii_sym=0, num_comment_symbols-1 DO BEGIN
           IF  comment_symbol_len[ii_sym] GT 0 THEN BEGIN
             IF STRCMP(line, comment_symbol[ii_sym], comment_symbol_len[ii_sym]) THEN skip_line=1
           ENDIF
         ENDFOR
         IF skip_line THEN CONTINUE

         line_array = STRSPLIT(line, separator, /EXTRACT, PRESERVE_NULL=preserve_null)
         line_array = STRTRIM(line_array, 2)

         FOR ii_fld=0,num_fields-1 DO BEGIN
           ptr_array = field_data[ii_fld]
           (*ptr_array)[ii_data] =  FIX(line_array[ii_fld], TYPE=field_types[ii_fld])
         ENDFOR

         ii_data += 1

       ENDWHILE

       FREE_LUN, lun


       IF hash THEN BEGIN
         ; Convert the array into a hash.
         output = MIR_HASH()
         FOR ii=0,num_fields-1 DO BEGIN
           output[field_names[ii]] = *field_data[ii]
         ENDFOR

       ENDIF ELSE BEGIN
         ; Build a structure with the tags in the correct order.

         ; Also make this an array of structures instead of a structure of arrays.
         ; This matches the old behavior of this routine.
         data_struct = {}
         FOR ii=0,num_fields-1 DO BEGIN
           data_struct = CREATE_STRUCT(data_struct, field_names[ii], FIX(0, TYPE=field_types[ii]))
         ENDFOR

         output = REPLICATE(data_struct, N_ELEMENTS(*field_data[0]))

         FOR ii=0,num_fields-1 DO BEGIN
           output[*].(ii) = TEMPORARY(*field_data[ii])
         ENDFOR

       ENDELSE

       MIR_TIMER, /STOP
       RETURN, output

     END ;FUNCTION MIR_ASCII_QUICK_READ
