

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
; DATE:
;   2009-05
;
; PURPOSE:
;   A collection of HTML utilites.
;   
; DESCRIPTION:
;   This file contains a number of routines to help deal with creating
;   html files.
;
; TAGS:
;   INDEX
;
;-======================================================================


     ;+=================================================================
     ; This function will return an opening html tag.
     ;-=================================================================
     FUNCTION HTML_TAG_OPEN, tag, ATTRIBUTES=attributes
       html_tag = '<'+tag

       IF KEYWORD_SET(attributes) THEN BEGIN
         html_tag = STRJOIN([html_tag, attributes], ' ')
       ENDIF

       html_tag += '>'

       RETURN, html_tag
     END ;FUNCTION HTML_TAG_OPEN


     ;+=================================================================
     ; This function will return a closing html tag.
     ;-=================================================================
     FUNCTION HTML_TAG_CLOSE, tag
       html_tag = '</'+tag+'>'

       RETURN, html_tag
     END ;FUNCTION HTML_TAG_OPEN


     ;+=================================================================
     ; This function will enclose the given text with HTML tags.
     ; 
     ; Input may either be a string, or an array of stings.
     ;
     ; If the input is an array of string strings, then the opening
     ; tag will be added before the first element, and the
     ; closing tag will be added after the last element.
     ;-=================================================================
     FUNCTION HTML_TAG, tag, string, ATTRIBUTES=attributes 
       
       ; Make a local copy
       out = string

       num_string = N_ELEMENTS(out)

       out[0] = HTML_TAG_OPEN(tag, ATTRIBUTES=attributes) + out[0]
       out[num_string-1] = out[num_string-1] + HTML_TAG_CLOSE(tag)
       
       RETURN, out

     END ;FUNCTION HTML_TAG_OPEN

     
     ;+=================================================================
     ; Creates a simple HTML head.
     ;
     ; All this adds is the title.
     ;-=================================================================
     FUNCTION HTML_HEAD, TITLE=title, STYLESHEET=stylesheet

       out_list = OBJ_NEW('MIR_LIST_IDL7')
       out_list->APPEND, HTML_TAG_OPEN('head')
       
       indent = '  '
       
       IF KEYWORD_SET(title) THEN BEGIN
         out_list->APPEND, indent + HTML_TAG('title', title)
       ENDIF

       IF KEYWORD_SET(stylesheet) THEN BEGIN
         FOR ii=0,N_ELEMENTS(stylesheet)-1 DO BEGIN
           attributes = [ $
                          'rel="StyleSheet"' $
                          ,'href="'+stylesheet[ii]+'"' $
                          ,'type="text/css"' $
                        ]
           out_list->APPEND, indent + HTML_TAG('link', '', ATTRIBUTES=attributes)
         ENDFOR
       ENDIF

       out_list->APPEND, HTML_TAG_CLOSE('head')
       
       
       out_array = out_list->TO_ARRAY()
       OBJ_DESTROY, out_list

       RETURN, out_array

     END ;FUNCTION HTML_HEAD


     ;+=================================================================
     ; Returns the <br> tag.
     ; 
     ;-=================================================================
     FUNCTION HTML_BR

       RETURN, '<br>'
     END ;FUNCTION HTML_BR


     ;+=================================================================
     ; Returns the <hr> tag.
     ; 
     ;-=================================================================
     FUNCTION HTML_HR

       RETURN, '<hr>'
     END ;FUNCTION HTML_HR


     ;+=================================================================
     ; Return the !DOCTYPE tag.
     ;
     ; For now this always returns the doctype as HTML 3.2
     ; 
     ;-=================================================================
     FUNCTION HTML_DOCTYPE

       RETURN, '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN" "http://www.w3.org/MarkUp/Wilbur/HTML32.dtd">'

     END ;FUNCTION HTML_DOCTYPE



     ;+=================================================================
     ; PURPOSE:
     ;   For automatic IDL compilation.
     ;
     ; TAGS:
     ;   EXCLUDE
     ; 
     ;-=================================================================
     PRO HTML_UTILITIES

     END ;PRO HTML_UTILITIES
