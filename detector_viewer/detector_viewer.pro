; ======================================================================
; DETECTOR_VIEWER.PRO
; ======================================================================
;
; AUTHOR: Novimir Antoniuk Pablant
; ======================================================================
; CHANGE LOG:
; 1.2.0 - 2016-04-27 - novi - Added the ability to load an image mask.
; 1.1.5 - 2012-05-02 - novi - The display range can now be set manually.
; 1.1.4 - 2010-08-31 - novi - Minor bug fix with 1D plotting.
; 1.1.3 - 2009-07-20 - novi - Can now handle floating type tiff ok.
; 1.1.2 - 2009-07-17 - novi - An image can now be specified on startup.
; 1.1.1 - 2009-06-09 - novi - Now only uses one saved path.
;                             Added a dialog box to adjust max and
;                               min range.
;
; 0.2.2 - 2006-09-01 - novi - This was a fully working version.
; 1.0.0 - 2009-01-22 - novi - Added a restore and clear feature.
;                             Now updates or clears the 1D window if
;                             a new file is loaded.
;                             A number of internal changes.
;
;                             Note: Some of this stuff here is now a 
;                                   bit of a mess since I did a
;                                   partial update of the structuring.
;
; 1.0.1 - 2009-02-18 - novi - A few other minor changes.
;                             Check to make sure only one instance
;                             of detector_viewer is allowed.
;
; 1.0.2 - 2009-04-24 - novi - Fixed bug with main window not reopening
;                             after being closed.
;                             Added a window menu.
; 1.0.3 - 2009-04-24 - novi - Added access routines for the common 
;                             variables.
; 1.0.4 - 2009-05-01 - novi - 1D plot windows now resets window index.
;                             DETECTOR_VIEWER now at bottom.
; 1.1.0 - 2009-05-27 - novi - Overhaul of the code.
;                             Added routines for loading image files
;                               that can be accessed externally.
;
;
; ======================================================================


     ;+=================================================================
     ;
     ; Returns a string with the version of DETECTOR_VIEWER.
     ;
     ;-=================================================================
     FUNCTION DV_VERSION
       ; ---------------------------
       version = '1.2.0 - 2016-04-27'
       ; ---------------------------
       RETURN, version
     END

     ;+=================================================================
     ;
     ; Define the common blocks.
     ;
     ;-=================================================================
     PRO DV_INIT_COMMON
       COMMON DV_GUI, c_dv_gui_param
       COMMON DV_IMAGE $
         ,image, image_data $
         ,image_back, image_back_data $
         ,image_final, image_final_data $
         ,image_1D, image_1D_data $
         ,image_bytscl, image_bytscl_zoom $
         ,image_mask, image_mask_data

     END


     ;+=================================================================
     ;
     ; Here we setup the gui parameters in a common varible.
     ;
     ; This also sets the default path.
     ;
     ;-=================================================================
     PRO DV_SET_GUI_PARAM, ID_STRUCT=id_struct $
                           ,POINTER_ARRAY=pointer_array
       COMMON DV_GUI

       ; Set the default image path.
       default_path = ''
       ;default_path = 'C:\sarnoff\images'
       ;default_path = '/u/antoniuk/b-stark/calibration'

       c_dv_gui_param = {id_struct:id_struct $
                         ,pointer_array:pointer_array $
                         ,default_path:default_path $
                         ,display_range:[0D,0D] $
                         ,active:1 $
                         ,gui_id:{main:0 $
                                  ,minmax:0 $
                                 } $
                        }

     END ;PRO DV_SET_GUI_PARAM


     ;+=================================================================
     ;
     ; This is the main event handler for DETECTOR_VIWER.
     ;
     ; This handles the menu and all of the controls.
     ;
     ; Events genereated in the various plot windows and handled
     ; separatly.
     ;
     ;-=================================================================
     PRO DV_UI_EVENT, event
       COMMON DV_IMAGE
       COMMON DV_GUI

       CATCH, error_status
       IF error_status NE 0 THEN BEGIN
         CATCH, /CANCEL
         DV_MESSAGE, 'Caught in DV_UI_EVENT', /LOG
         DV_MESSAGE, !ERROR_STATE.MSG, /LOG
       ENDIF


       WIDGET_CONTROL, event.top, GET_UVALUE=top_uval
       WIDGET_CONTROL, event.id, GET_UVALUE=uval

       IF TAG_NAMES(event, /STRUCTURE_NAME) EQ 'WIDGET_KILL_REQUEST' THEN BEGIN
         ui_dv_kill, event.top
         RETURN
       ENDIF

       case uval.name of
         'ui_menu_save_image_color': BEGIN
           dv_write, data=top_uval, /image_color
         END
         'ui_menu_save_image_bw': BEGIN
           dv_write, data=top_uval, /image_bw
         END
         'ui_menu_save_zoom_color': BEGIN
           dv_write, data=top_uval, /zoom_color
         END
         'ui_menu_save_zoom_bw': BEGIN
           dv_write, data=top_uval, /zoom_bw
         END
         'ui_menu_save_zoom_contour': BEGIN
           dv_write, data=top_uval, /zoom_contour
         END
         'ui_menu_save_image_bin': BEGIN
           dv_write, data=top_uval, /image_binary
         END
         'ui_menu_save_ped_bin': BEGIN
           dv_write, data=top_uval, /back_binary
         END
         'ui_menu_controls_minmax': BEGIN
           DV_MINMAX_INIT
         END
         'ui_menu_window_show_main': BEGIN
           DV_PLOT_IMAGE
         END
         'ui_menu_window_repaint': BEGIN
           DV_REPAINT_ALL
         END
         'ui_button_Done': BEGIN
           print, 'yo yo, I am outro'
           ui_dv_kill, event.top  
         END

         'ui_button_LoadFile': BEGIN
           print, 'mango mango mango mang'
           DV_PICK_IMAGE, STATUS=status
           IF status THEN DV_RELOAD_IMAGE
         END

         'ui_button_LoadBack': BEGIN
           print, 'mango mango mango mang'

           DV_PICK_IMAGE, /BACK, STATUS=status
           IF status THEN DV_RELOAD_IMAGE_BACK
         END

         'ui_button_LoadMask': BEGIN
           DV_PICK_IMAGE, /MASK, STATUS=status
           IF status THEN DV_RELOAD_IMAGE_MASK
         END         

         'ui_button_reload': BEGIN
           DV_RELOAD_ALL_IMAGES
         END
         'ui_button_clear': BEGIN
           DV_CLEAR_IMAGE
         END

         'ui_button_subtract': BEGIN
           image_final_data.image_subtraction = event.select

           print, '   you can get anything . . . '

           IF event.select eq 1 THEN BEGIN
             DV_MESSAGE, 'Image Subtraction Selected', /LOG
           ENDIF ELSE BEGIN
             DV_MESSAGE, 'Image Subtraction UnSelected', /LOG
           ENDELSE

           DV_RELOAD_FINAL_IMAGE

         END

         'ui_button_background': BEGIN
           image_final_data.back_subtraction = event.select

           PRINT, " . . . you want at Alice's restauraunt."

           IF event.select eq 1 THEN BEGIN
             DV_MESSAGE, 'Background Subtraction Selected', /LOG
           ENDIF ELSE BEGIN
             DV_MESSAGE, 'Background Subtraction UnSelected', /LOG
           ENDELSE

           DV_RELOAD_FINAL_IMAGE

         END

         'ui_button_mask': BEGIN
           image_final_data.use_mask = event.select

           PRINT, " (Excepting Alice.)"

           IF event.select eq 1 THEN BEGIN
             DV_MESSAGE, 'Use Mask Selected', /LOG
           ENDIF ELSE BEGIN
             DV_MESSAGE, 'Use Mask UnSelected', /LOG
           ENDELSE

           DV_RELOAD_FINAL_IMAGE
         END         

         'ui_button_invertmask': BEGIN
           image_final_data.invert_mask = event.select

           IF event.select eq 1 THEN BEGIN
             DV_MESSAGE, 'Invert Mask Selected', /LOG
           ENDIF ELSE BEGIN
             DV_MESSAGE, 'Invert Mask UnSelected', /LOG
           ENDELSE

           DV_RELOAD_FINAL_IMAGE
         END
         
         'ui_button_zoom': BEGIN
           uval.select=event.select
           WIDGET_CONTROL, event.id, set_uval=uval
           IF event.select eq 1 THEN BEGIN
             text = 'Zoom Window Activated'
             WIDGET_CONTROL, top_uval.id_struct.ui_base_control_slider_zoom, map=1
             WIDGET_CONTROL, top_uval.id_struct.ui_base_zoom, map=1
             WIDGET_CONTROL, top_uval.id_struct.ui_button_zoom_contour, sensitive=1
             WIDGET_CONTROL, top_uval.id_struct.ui_draw, GET_UVALUE=uval_draw, /no_copy
             uval_draw.zoom_frozen =0
             WIDGET_CONTROL, top_uval.id_struct.ui_draw, SET_UVALUE=uval_draw, /no_copy
             DV_PLOT_IMAGE_ZOOM, id_struct=top_uval.id_struct, 0, 0
           ENDIF ELSE BEGIN
             text = 'Zoom Window Deactivated'
             WIDGET_CONTROL, top_uval.id_struct.ui_base_zoom, map=0
             WIDGET_CONTROL, top_uval.id_struct.ui_base_control_slider_zoom, map=0
             WIDGET_CONTROL, top_uval.id_struct.ui_button_zoom_contour, sensitive=0
           ENDELSE

           DV_MESSAGE, text
         END
         'ui_button_zoom_contour': BEGIN
           uval.select=event.select
           WIDGET_CONTROL, event.id, set_uval=uval
           IF event.select eq 1 THEN BEGIN
             text = 'Zoom Contour Selected'
             WIDGET_CONTROL, top_uval.id_struct.ui_zoom, GET_UVALUE=uval_zoom, /no_copy
             uval_zoom.contour = 1
             WIDGET_CONTROL, top_uval.id_struct.ui_zoom, SET_UVALUE=uval_zoom
             DV_PLOT_IMAGE_ZOOM, id_struct=top_uval.id_struct, uval_zoom.old_x, uval_zoom.old_y
           ENDIF ELSE BEGIN
             text = 'Zoom Contour DeSelected'
             WIDGET_CONTROL, top_uval.id_struct.ui_zoom, GET_UVALUE=uval_zoom, /no_copy
             uval_zoom.contour = 0
             WIDGET_CONTROL, top_uval.id_struct.ui_zoom, SET_UVALUE=uval_zoom
             DV_PLOT_IMAGE_ZOOM, id_struct=top_uval.id_struct, uval_zoom.old_x, uval_zoom.old_y
           ENDELSE

           DV_MESSAGE, text
         END
         'ui_slider_zoom': BEGIN
           WIDGET_CONTROL, top_uval.id_struct.ui_zoom, GET_UVALUE=uval_ui_zoom, /no_copy
           uval_ui_zoom.zoom_scale = event.value
           WIDGET_CONTROL, top_uval.id_struct.ui_zoom, SET_UVALUE=uval_ui_zoom   
           DV_PLOT_IMAGE_ZOOM, uval_ui_zoom.old_x, uval_ui_zoom.old_y, id_struct=top_uval.id_struct
         END
         'ui_menu_1': BEGIN
           IF uval.text[event.value] eq 'Palette Dialog' THEN BEGIN
             XLOADCT, SILENT=0, UPDATECALLBACK='dv_plot_image'
           ENDIF ELSE BEGIN
             PRINT, event.value, uval.ct[event.value]
             LOADCT, uval.ct[event.value]
           ENDELSE

           DV_PLOT_IMAGE
           PLOT_PALETTE
           
           DV_MESSAGE, uval.text[event.value]
         END
         'ui_draw_palette': BEGIN
         END
         ELSE:
       ENDcase

     END ;PRO PRO ui_event

     
     ;+=================================================================
     ;-=================================================================
     PRO DV_UI_DRAW_EVENT, event
       COMMON DV_IMAGE
       WIDGET_CONTROL, event.top, GET_UVALUE=top_uval
       WIDGET_CONTROL, event.id, GET_UVALUE=uval

       IF TAG_NAMES(event, /STRUCTURE_NAME) EQ 'WIDGET_KILL_REQUEST' THEN BEGIN
         WIDGET_CONTROL, event.id, map=0
         RETURN
       ENDIF

       IF event.id eq top_uval.id_struct.ui_base_draw THEN BEGIN
         ;RESIZE EVENT
         WIDGET_CONTROL, event.id, xsize=event.x, ysize=event.y 
         ;CHECK IF IMAGE HAS BEEN LOADED
         IF ~keyword_set(image_final_data.init) THEN $
           RETURN
         WIDGET_CONTROL, top_uval.id_struct.ui_draw, scr_xsize=event.x, scr_ysize=event.y
         DV_PLOT_IMAGE
         RETURN
       ENDIF

       IF event.type eq 1 THEN BEGIN
         case event.release of
           1: BEGIN ;LEFT CLICK SHOW ROW
             ;image_1D = image[*,event.Y]
             IF ~widget_info(top_uval.id_struct.ui_base_draw_1D, /map) THEN $
               WIDGET_CONTROL, top_uval.id_struct.ui_base_draw_1D, map=1

             image_1d_data.x = event.x
             image_1d_data.y = event.y
            
             image_1D_data.title = string(format='(a5, i0)', 'ROW:', event.Y)
             image_1D_data.row_col='row'
             image_1D_data.zoom_out = [0,0]
             image_1D_data.zoom = [0,0]
             DV_PLOT_IMAGE_1D
           END
           4: BEGIN ;RIGHT CLICK SHOW COL
             ;image_1D = image[event.X,*]

             image_1d_data.x = event.x
             image_1d_data.y = event.y

             IF ~widget_info(top_uval.id_struct.ui_base_draw_1D, /map) THEN $
               WIDGET_CONTROL, top_uval.id_struct.ui_base_draw_1D, map=1

             image_1D_data.title = string(format='(a5, i0)', 'COL:', event.X)
             image_1D_data.row_col='col'
             image_1D_data.zoom_out = [0,0]
             image_1D_data.zoom = [0,0]
             DV_PLOT_IMAGE_1D
           END
           2: BEGIN ;MIDDLE CLICK FREEZE ZOOM
             IF uval.zoom_frozen eq 0 THEN $
               uval.zoom_frozen = 1 $
             ELSE $
               uval.zoom_frozen = 0
             WIDGET_CONTROL, event.id, SET_UVALUE=uval
           END
           ELSE:
         ENDcase
       ENDIF

       WIDGET_CONTROL, top_uval.id_struct.ui_position_label, set_value=$
         string(format='(a3, i5, a3, i5)', $
                'X:', event.X, 'Y:',event.Y)
       WIDGET_CONTROL, top_uval.id_struct.ui_value_label, set_value=$
         string(format='(a7, f7.1)', $
                'Value:', image_final[event.X, event.Y])
       WIDGET_CONTROL, top_uval.id_struct.ui_button_zoom, get_uval=uval_ui_button_zoom

       IF uval_ui_button_zoom.select eq 1 AND $
         widget_info(top_uval.id_struct.ui_base_zoom, /map) AND $
         ~uval.zoom_frozen THEN BEGIN
         DV_PLOT_IMAGE_ZOOM, event.X, event.Y, id_struct=top_uval.id_struct
       ENDIF


     END

     
     ;+=================================================================
     ; Controls events in the 1D plot window.
     ;
     ;-=================================================================
     PRO DV_UI_DRAW_1D_EVENT, event
       COMMON DV_IMAGE

       WIDGET_CONTROL, event.top, GET_UVALUE=top_uval
       WIDGET_CONTROL, event.id, GET_UVALUE=uval

       ; If the user trys to kill then window, then just hide it instead.
       IF TAG_NAMES(event, /STRUCTURE_NAME) EQ 'WIDGET_KILL_REQUEST' THEN BEGIN
         WIDGET_CONTROL, event.id, MAP=0
         RETURN
       ENDIF


       ; Deal with resizing events.
       ; After the window is resized I want to replot the data.
       IF event.id eq top_uval.id_struct.ui_base_draw_1D THEN BEGIN
         ; Resize the widgets
         WIDGET_CONTROL, event.id, XSIZE=event.x, YSIZE=event.y
         WIDGET_CONTROL, top_uval.id_struct.ui_draw_1D, XSIZE=event.x, YSIZE=event.y

         ; Save the size data
         image_1D_data.xsize = event.x
         image_1D_data.ysize = event.y
         
         ; Replot the 1D image.
         DV_PLOT_IMAGE_1D

         WIDGET_CONTROL, top_uval.id_struct.ui_draw_1d, GET_UVALUE=ui_draw_1d_uval, /NO_COPY
         ui_draw_1d_uval.tv =0
         WIDGET_CONTROL, top_uval.id_struct.ui_draw_1d, SET_UVALUE=ui_draw_1d_uval, /NO_COPY

         RETURN
       ENDIF

       ; Set the number of points to plot.
       IF TOTAL(image_1D_data.zoom) gt 0 THEN BEGIN
         ; Plot request was from the zoom window.
         image_size_slice = image_1D_data.zoom[1] - image_1D_data.zoom[0] + 1
       ENDIF ELSE BEGIN
         ; Plot request was from the image window.
         IF image_1D_data.row_col eq 'row' THEN BEGIN
           image_size_slice = image_final_data.size[0]
         ENDIF ELSE BEGIN
           image_size_slice = image_final_data.size[1]
         ENDELSE
       ENDELSE

       ; Get the scale factor in the x-direction..  
       ; This is the conversion between pixels on the screen and x-index of the plot window.
       scale = 1D * (image_1D_data.xsize - $
                     (image_1D_data.position_offset[0] +  image_1D_data.position_offset[2])) / $
         image_size_slice

       ; Get the x-index of the cursor location.
       x_1d = FIX((event.X - image_1D_data.position_offset[0]) / scale ) + image_1D_data.zoom[0]

       ;CHECK IF CURSOR IS IN VALID PLOT AREA
       IF x_1d GE 0 AND x_1d LT (image_size_slice + image_1D_data.zoom[0]) THEN BEGIN

         ;DISPLAY POSITION AND VALUE IN CONTROL WINDOW
         WIDGET_CONTROL, top_uval.id_struct.ui_position_label, SET_VALUE=$
           STRING(format='(a7, f7.1)', $
                  'X:', x_1d)
         WIDGET_CONTROL, top_uval.id_struct.ui_value_label, SET_VALUE=$
           STRING(format='(a7, f7.1)', $
                  'Value:', image_1D[x_1d])

         ; Set the current window to the 1D plot window.
         WIDGET_CONTROL, top_uval.id_struct.ui_draw_1D, GET_VALUE = index
         WINDOWSET, index

         ;ALLOW FOR ZOOMING
         IF event.type eq 0 THEN BEGIN ;BUTTON PRESS
           IF event.press eq 1 THEN BEGIN ;LEFT BUTTON
             uval.zoom[0] = x_1d
             uval.tv = 0
             PLOTS, [event.X, event.X] $
                   ,[image_1D_data.position_offset[1], $
                     image_1D_data.ysize - image_1D_data.position_offset[3]] $
                   ,/device $
                   , thick=2

             WIDGET_CONTROL, event.id, SET_UVALUE=uval
           ENDIF
         ENDIF
         IF event.type eq 1 THEN BEGIN ;BUTTON RELEASE
           IF event.release eq 1 THEN BEGIN ;LEFT BUTTON
             uval.zoom[1] = x_1d

             IF uval.zoom[1] lt uval.zoom[0] THEN $
               uval.zoom = [uval.zoom[1], uval.zoom[0]]
             IF uval.zoom[1] - uval.zoom[0] lt 5 THEN $
               uval.zoom=image_1D_data.zoom

             uval.tv = 0
             WIDGET_CONTROL, event.id, SET_UVALUE=uval
             image_1D_data.zoom = uval.zoom
             DV_PLOT_IMAGE_1D
           ENDIF ELSE $
             IF event.release eq 4 THEN BEGIN ;RIGHT BUTTON
             uval.zoom = image_1D_data.zoom_out
             WIDGET_CONTROL, event.id, SET_UVALUE=uval
             image_1D_data.zoom = image_1D_data.zoom_out
             DV_PLOT_IMAGE_1D
           ENDIF
         ENDIF

         ;MAKE CROSSHAIRS IN WINDOW
         IF uval.tv THEN BEGIN
           tv, *uval.PTR_tvy_data, uval.tvy_x, image_1D_data.position_offset[1], /device, true=1
         ENDIF

         tvy_x = event.X
         *uval.PTR_tvy_data  = TVRD(event.X, image_1D_data.position_offset[1], $
                                    1, image_1D_data.ysize - $
                                    (image_1D_data.position_offset[1] $
                                     + image_1D_data.position_offset[3]), TRUE=1)
         uval.tv = 1
         uval.tvy_x = tvy_x
         WIDGET_CONTROL, event.id, SET_UVALUE=uval, /NO_COPY

         PLOTS, [event.X, event.X] $
               ,[image_1D_data.position_offset[1], $
                 image_1D_data.ysize - image_1D_data.position_offset[3]] $
               ,/DEVICE

         ; Reset the window index
         WINDOWSET, /RESTORE
       ENDIF


     END ;PRO UI_DRAW_1D_EVENT

     
     ;+=================================================================
     ;-=================================================================
     PRO DV_UI_DRAW_ZOOM_EVENT, event
       COMMON DV_IMAGE
       WIDGET_CONTROL, event.top, GET_UVALUE=top_uval
       WIDGET_CONTROL, event.id, GET_UVALUE=uval

       IF TAG_NAMES(event, /STRUCTURE_NAME) EQ 'WIDGET_KILL_REQUEST' THEN BEGIN
         WIDGET_CONTROL, event.id, map=0
         WIDGET_CONTROL, top_uval.id_struct.ui_button_zoom, set_button=0
         RETURN
       ENDIF

       ;CHECK IF IMAGE HAS BEEN LOADED
       IF ~keyword_set(image_final_data.init) THEN $
         RETURN

       x_zoom = event.X/uval.zoom_scale + uval.zoom_pos[0]
       y_zoom = event.Y/uval.zoom_scale + uval.zoom_pos[1]

       IF x_zoom lt image_final_data.size[0] AND y_zoom lt image_final_data.size[1] THEN BEGIN

         ;1D PLOT
         IF event.type eq 1 THEN BEGIN
           case event.release of
             1: BEGIN
               ;LEFT CLICK SHOW ROW

               ;image_1D = image[*,y_zoom]

               image_1d_data.x = x_zoom
               image_1d_data.y = y_zoom

               IF widget_info(top_uval.id_struct.ui_base_draw_1D, /valid_id) THEN BEGIN
                 IF ~widget_info(top_uval.id_struct.ui_base_draw_1D, /map) THEN $
                   WIDGET_CONTROL, top_uval.id_struct.ui_base_draw_1D, map=1
                 
                 range_1D = [ uval.zoom_pos[0], $
                              uval.zoom_pos[0] + (uval.xsize-1)/uval.zoom_scale]

                 image_1D_data.title = string(format='(a5, i5)', 'ROW:', y_zoom)
                 image_1D_data.row_col='row'
                 image_1D_data.zoom_out = range_1d
                 image_1D_data.zoom = range_1d
                 DV_PLOT_IMAGE_1D
               ENDIF
             END
             4: BEGIN
               ;RIGHT CLICK SHOW COL
               ;image_1D = image[x_zoom,*]

               image_1d_data.x = x_zoom
               image_1d_data.y = y_zoom

               IF widget_info(top_uval.id_struct.ui_base_draw_1D, /valid_id) THEN BEGIN
                 IF ~widget_info(top_uval.id_struct.ui_base_draw_1D, /map) THEN $
                   WIDGET_CONTROL, top_uval.id_struct.ui_base_draw_1D, map=1

                 range_1D = [ uval.zoom_pos[1], $
                              uval.zoom_pos[1] + (uval.ysize-1)/uval.zoom_scale]
                 
                 image_1D_data.title = string(format='(a5, i5)', 'COL:', x_zoom)
                 image_1D_data.row_col='col'
                 image_1D_data.zoom_out = range_1d
                 image_1D_data.zoom = range_1d
                 DV_PLOT_IMAGE_1D
               ENDIF
             END
             ELSE:
           ENDcase
         ENDIF

         ;DISPLAY IMAGE VALUES
         WIDGET_CONTROL, top_uval.id_struct.ui_position_label, set_value=$
           string(format='(a3, i5, a3, i5)', $
                  'X:', x_zoom, 'Y:',y_zoom)
         WIDGET_CONTROL, top_uval.id_struct.ui_value_label, set_value=$
           string(format='(a7, f7.1)', $
                  'Value:', image_final[x_zoom, y_zoom])

       ENDIF
     END ;PRO ui_draw_zoom_event


     ;+=================================================================
     ;-=================================================================
     PRO UI_DV_KILL, top_id
       COMMON DV_GUI

       WIDGET_CONTROL, top_id, GET_UVALUE=uval

       ;DESTROY ALL WIDGETS
       IF widget_info(uval.id_struct.ui_base, /valid_id) THEN $
         WIDGET_CONTROL, uval.id_struct.ui_base, /destroy
       IF widget_info(uval.id_struct.ui_base_control, /valid_id) THEN $
         WIDGET_CONTROL, uval.id_struct.ui_base_control, /destroy
       IF widget_info(uval.id_struct.ui_base_draw, /valid_id) THEN $
         WIDGET_CONTROL, uval.id_struct.ui_base_draw, /destroy
       IF widget_info(uval.id_struct.ui_base_draw_1D, /valid_id) THEN $
         WIDGET_CONTROL, uval.id_struct.ui_base_draw_1D, /destroy
       IF widget_info(uval.id_struct.ui_base_zoom, /valid_id) THEN $
         WIDGET_CONTROL, uval.id_struct.ui_base_zoom, /destroy

       ; Set the state to inactive.
       c_dv_gui_param.active = 0

       ; CLEAN UP HEAP VARIABLES
       PTR_FREE, c_dv_gui_param.pointer_array

       ; CLEAN UP ANY UNACCOUNTED FOR HEAP VARIABLES (THIS IS DONE ON A GLOBAL SCOPE)
       ; If I programed every thing right this should never do anything.
       HEAP_GC, /verbose 

     END ; PRO UI_DV_KILL

     
     ;+=================================================================
     ;-=================================================================
     PRO DV_MESSAGE, text, LOG=log

       COMMON DV_GUI

       PRINT, text
       IF KEYWORD_SET(log) THEN RETURN

       WIDGET_CONTROL, c_dv_gui_param.id_struct.ui_text_status, GET_UVALUE=uval
       geometry = WIDGET_INFO(c_dv_gui_param.id_struct.ui_text_status, /GEOMETRY)
       ;  WIDGET_CONTROL, top_uval.id_struct.ui_text_status, set_value = text, /appEND

       *(uval.PTR_text_status) = [*(uval.PTR_text_status), text]
       num = n_elements(*(uval.PTR_text_status))
       IF num gt uval.ysize THEN $
         top_line = 0 > num - geometry.ysize $
       ELSE $
         top_line = 0

       WIDGET_CONTROL, c_dv_gui_param.id_struct.ui_text_status, SET_UVALUE=uval $
         ,set_value=*(uval.PTR_text_status), set_list_top=top_line

     END ;DV_MESSAGE


     ;+=================================================================
     ;-=================================================================
     PRO DV_PLOT_IMAGE
       COMMON DV_GUI
       COMMON DV_IMAGE
       
       ;CHECK IF IMAGE HAS BEEN LOADED
       IF ~KEYWORD_SET(image_final_data.init) THEN BEGIN
         RETURN
       ENDIF
       
       
       geometry = WIDGET_INFO(c_dv_gui_param.id_struct.ui_draw, /GEOMETRY)
       
       IF (geometry.draw_xsize NE image_final_data.size[0]) $
         OR (geometry.draw_ysize NE image_final_data.size[1]) THEN BEGIN

         WIDGET_CONTROL, c_dv_gui_param.id_struct.ui_draw $
           ,DRAW_XSIZE = image_final_data.size[0] $
           ,DRAW_YSIZE = image_final_data.size[1]
       ENDIF

       ; Set the title
       WIDGET_CONTROL, c_dv_gui_param.id_struct.ui_draw $
           ,TLB_SET_TITLE = image_final_data.title


       ;If scroll view has not been initialized then initialize it
       IF geometry.xsize EQ 1 THEN BEGIN
         print, 'Sizing window'
         IF image_final_data.size[0] gt 1024 THEN $
           x_scroll_size = 1024 $
         ELSE $
           x_scroll_size = image_final_data.size[0]
         
         IF image_final_data.size[1] gt 1024 THEN $
           y_scroll_size = 1024 $
         ELSE $
           y_scroll_size = image_final_data.size[1]
         
         WIDGET_CONTROL, c_dv_gui_param.id_struct.ui_draw, XSIZE=x_scroll_size, YSIZE=y_scroll_size
         
       ENDIF
       
       ; If the plot window is not currently mapped, then map it.
       IF ~WIDGET_INFO(c_dv_gui_param.id_struct.ui_draw, /MAP) THEN $
         WIDGET_CONTROL, c_dv_gui_param.id_struct.ui_draw, MAP=1


       WIDGET_CONTROL, c_dv_gui_param.id_struct.ui_draw, GET_VALUE=index


       WINDOWSET, index
       TV, image_bytscl
       WINDOWSET, /RESTORE
       
     END ;PRO DV_PLOT_IMAGE
     

     ;+=================================================================
     ;-=================================================================
     PRO DV_PLOT_IMAGE_1D, CLEAR=clear
       COMMON DV_GUI
       COMMON DV_IMAGE

       IF WIDGET_INFO(c_dv_gui_param.id_struct.ui_draw_1D, /valid_id) THEN BEGIN

         WIDGET_CONTROL, c_dv_gui_param.id_struct.ui_draw_1D, GET_VALUE=index

         plot_position = [image_1D_data.position_offset[0] $
                          ,image_1D_data.position_offset[1] $
                          ,image_1D_data.xsize - image_1D_data.position_offset[2] $
                          ,image_1D_data.ysize - image_1D_data.position_offset[3]]


         IF KEYWORD_SET(clear) THEN BEGIN
           image_1d = [0,1]
           image_1D_index = INDGEN(N_ELEMENTS(image_1D))

           WINDOWSET, index
           PLOT, image_1D_index $
                 ,image_1D $
                 ,TITLE = '' $
                 ,/DEVICE $
                 ,XSTYLE=1 $
                 ,YSTYLE=1+16 $
                 ,PSYM=10 $
                 ,POSITION=plot_position $
                 ,/NODATA
           WINDOWSET, /RESTORE

           image_1d_data.init = 0
           RETURN
         END


         CASE 1 OF
           image_1d_data.row_col EQ 'row': BEGIN
             ; Check that the requested row exists
             IF image_1d_data.y LT image_final_data.size[1] THEN BEGIN
               image_1d = image_final[*,image_1d_data.y]
             ENDIF ELSE BEGIN
               image_1d = INTARR(image_final_data.size[0])
               nodata = 1
             ENDELSE
           END
           image_1d_data.row_col EQ 'col': BEGIN
             ; Check that the requested col exists
             IF image_1d_data.x LT image_final_data.size[0] THEN BEGIN
               image_1d = image_final[image_1d_data.x, *]
             ENDIF ELSE BEGIN
               image_1d = INTARR(image_final_data.size[1])
               nodata = 1
             ENDELSE
           END
         ENDCASE

         IF ~WIDGET_INFO(c_dv_gui_param.id_struct.ui_draw_1D, /map) THEN $
           WIDGET_CONTROL, c_dv_gui_param.id_struct.ui_draw_1D, map=1
        
         
         
         plot_start = 0
         IF image_1D_data.row_col eq 'row' THEN $
           plot_end = image_final_data.size[0]-1 $
         ELSE $
           plot_end = image_final_data.size[1]-1
         
         
         IF TOTAL(image_1D_data.zoom) ne 0 THEN BEGIN
           IF (image_1D_data.zoom[1] - image_1D_data.zoom[0]) lt 5 THEN BEGIN
             image_1D_data.zoom = [0,0]
           ENDIF ELSE BEGIN
             plot_start = image_1D_data.zoom[0]
             plot_end = image_1D_data.zoom[1]
           ENDELSE
         ENDIF
         
         image_1D_index = INDGEN(N_ELEMENTS(image_1D))

         WINDOWSET, index
         PLOT, image_1D_index[plot_start:plot_end] $
               ,image_1D[plot_start:plot_end] $
               ,TITLE = image_1D_data.title $
               ,/DEVICE $
               ,XSTYLE=1 $
               ,YSTYLE=1+16 $
               ,PSYM=10 $
               ,POSITION=plot_position $
               ,NODATA=nodata
         WINDOWSET, /RESTORE
       ENDIF
     END ;PRO DV_PLOT_IMAGE_1D


     ;+=================================================================
     ;-=================================================================
     PRO DV_PLOT_IMAGE_ZOOM, newx, newy, id_struct=id_struct, scale=scale, no_wset=no_wset
       COMMON DV_IMAGE
       WIDGET_CONTROL, id_struct.ui_zoom, GET_UVAL=uval_ui_zoom
       scale = uval_ui_zoom.zoom_scale
       
       ;CHECK IF AN IMAGE HAS BEEN LOADED
       IF ~keyword_set(image_final_data.init) AND ~keyword_set(image_back_data.init)  THEN $
         RETURN
       
       ;CODE MODIFIED FROM CW_ZOOM.PRO
       
       ; compute size of rectangle in original image
       ; round up to make sure image fills zoom window
       rect_x = long(uval_ui_zoom.xsize / float(scale) + 0.999)
       rect_y = long(uval_ui_zoom.ysize / float(scale) + 0.999)
       
       ; Plan to erase IF the zoom rectangle is larger than the original
       ;  image size.
       doerase = (rect_x GT image_final_data.size[0] OR rect_y GT image_final_data.size[1])
       
       rect_x = rect_x < image_final_data.size[0]
       rect_y = rect_y < image_final_data.size[1]
       
       ; compute location of origin of rect (user specified center)
       x0 = newx - rect_x/2
       y0 = newy - rect_y/2
       
       ; make sure rectangle fits into original image
       ;left edge from center
       x0 = x0 > 0
       ; limit right position
       x0 = x0 < (image_final_data.size[0] - rect_x)
       
       ;bottom
       y0 = y0 > 0
       y0 = y0 < (image_final_data.size[1] - rect_y)
       
       IF (scale EQ 1) THEN BEGIN
         IF doerase THEN ERASE
         
         image_bytscl_zoom =  image_bytscl[x0:x0+rect_x-1,y0:y0+rect_y-1]
         
       ENDIF ELSE BEGIN
         ;Make integer rebin factors.  These may be larger than the zoom image
         dim_x = rect_x * scale
         dim_y = rect_y * scale
         
         ; Constrain upper right edge to within original image.
         x1 = (x0 + rect_x - 1) < (image_final_data.size[0]-1)
         y1 = (y0 + rect_y - 1) < (image_final_data.size[1]-1)
         
         temp_image = rebin(image_bytscl[x0:x1,y0:y1], $
                            dim_x, dim_y, $
                            /sample)
         
         ;Save the zoomed image
         fill_x = dim_x < uval_ui_zoom.xsize
         fill_y = dim_y < uval_ui_zoom.ysize
         image_bytscl_zoom[0:fill_x-1,0:fill_y-1] = temp_image[0:fill_x-1,0:fill_y-1]
         
         ; Pad with zoomed image with black IF necessary.
         IF (fill_x LT uval_ui_zoom.xsize) THEN $
           image_bytscl_zoom[fill_x:uval_ui_zoom.xsize-1, *] = 0
         IF (fill_y LT uval_ui_zoom.ysize) THEN $
           image_bytscl_zoom[*, fill_y:uval_ui_zoom.ysize-1] = 0
         
         ;Save the corners in original image
         ;state.x0 = x0
         ;state.y0 = y0
         ;state.x1 = x1
         ;state.y1 = y1
         
         ;Display the new zoomed image
         ;Save window number
       ENDELSE
       
       IF uval_ui_zoom.contour THEN BEGIN
         DV_PLOT_IMAGE_ZOOM_CONTOUR, id_struct=id_struct
       ENDIF ELSE BEGIN

         WIDGET_CONTROL, id_struct.ui_zoom, GET_VALUE=w_index

         ; don't use tvscl here, to preserve same range as in unzoomed image
         WINDOWSET, w_index
         tv, image_bytscl_zoom
         WINDOWSET, /RESTORE
       ENDELSE
       
       uval_ui_zoom.zoom_pos = [x0,y0]
       uval_ui_zoom.old_x = newx
       uval_ui_zoom.old_y = newy
       WIDGET_CONTROL, id_struct.ui_zoom, SET_UVAL=uval_ui_zoom, /NO_COPY

     END ;PRO DV_PLOT_IMAGE_ZOOM


     ;+=================================================================
     ;-=================================================================
     PRO DV_PLOT_IMAGE_ZOOM_CONTOUR, NO_WSET=no_wset, ID_STRUCT=id_struct
       COMMON DV_IMAGE


       WIDGET_CONTROL, id_struct.ui_zoom, GET_VALUE=w_index
       WINDOWSET, w_index


       ;CONTOUR LINES CHOSEN TO MATCH RAY TRAYCING
       levels = ((findgen(16)+1)/16)^2
       num_levels = n_elements(levels)
       color = findgen(num_levels)/(num_levels-1)*255
       
       ;PLOT LEGEND
       WIDGET_CONTROL, id_struct.ui_zoom, GET_UVALUE=uval_zoom
       posx = 0   ;(uval_zoom.xsize)/20
       posy = (uval_zoom.ysize)*19/20 - ((num_levels - 1) - indgen(num_levels))*20
       levels_text = strarr(num_levels)
       for i = 0, num_levels-1 do BEGIN
         levels_text[i] = string(format='(f8.4)', levels[i])
       ENDfor
       
       IF keyword_set(no_wset) THEN BEGIN
         contour, image_bytscl_zoom/255., /isotropic, c_color=color, levels=levels, $
           xsty=4, ysty=4, c_thick=2
       ENDIF ELSE BEGIN
         contour, image_bytscl_zoom/255., c_color=color, levels=levels, $
           xsty=4, ysty=4, pos=[0,0,499,499], /device, c_thick=2
       ENDELSE
       
       
       XYOUTS, posx, posy, levels_text, color=color, charthick=1, charsize=1.2, /data
       
       WINDOWSET, /RESTORE

     END ;PRO DV_PLOT_IMAGE_ZOOM_CONTOUR


     ;+=================================================================
     ;
     ; Repaint the images.
     ;
     ;-=================================================================
     PRO DV_REPAINT_IMAGES
       COMMON DV_GUI

       DV_PLOT_IMAGE

       ; Check to see if the 1D plot window is open.
       IF widget_info(c_dv_gui_param.id_struct.ui_base_draw_1D, /map) THEN BEGIN
         DV_PLOT_IMAGE_1D
       ENDIF


     END ; PRO DV_REPAINT_IMAGES


     ;+=================================================================
     ;
     ; Repaint all graphics.
     ; This includes any palette displays
     ;
     ;-=================================================================
     PRO DV_REPAINT_ALL
       COMMON DV_GUI

       DV_REPAINT_IMAGES

       ; Now refresh the minmax window
       DV_MINMAX_REPAINT


     END ; PRO DV_REPAINT_IMAGE



     ;+=================================================================
     ;
     ; Repaint all graphics.
     ; This includes any palette displays
     ;
     ;-=================================================================
     PRO DV_UPDATE_ALL
       COMMON DV_GUI

       ; Update the main palette display
       DV_PALETTE_UPDATE

       ; Update the minmax window
       DV_MINMAX_UPDATE

       ; Repaint all images.
       DV_REPAINT_IMAGES

     END ;PRO DV_UPDATE_ALL




     ;+=================================================================
     ;
     ; Update the palette dispay
     ;
     ;-=================================================================
     PRO DV_PALETTE_UPDATE
       COMMON DV_GUI
       COMMON DV_IMAGE

       ; Set a default display_range.
       display_range = c_dv_gui_param.display_range
       IF ARRAY_EQUAL(display_range, [0,0]) THEN BEGIN
         display_range = image_final_data.range
       ENDIF

       ;Place values on the palette label
       WIDGET_CONTROL, c_dv_gui_param.id_struct.ui_palette_label_top $
         ,SET_VALUE=string(FORMAT='(i0)', display_range[1])
       WIDGET_CONTROL, c_dv_gui_param.id_struct.ui_palette_label_bottom $
         ,SET_VALUE=string(FORMAT='(i0)', display_range[0])

     END ;PRO DV_PALETTE_UPDATE



     ;+=================================================================
     ;-=================================================================
     PRO PLOT_PALETTE
       COMMON DV_GUI

       WIDGET_CONTROL, c_dv_gui_param.id_struct.ui_draw_palette, GET_UVALUE=uval
       IF uval.init eq 0 THEN BEGIN
         ;INITIALIZE PALETTE DISPLAY
         *uval.PTR_image_palette =  fltarr(uval.xsize, uval.ysize)
         for i = 0, uval.xsize - 1 do BEGIN
           (*uval.PTR_image_palette)[i,*] = findgen(uval.ysize)/uval.ysize*256
         ENDfor
         uval.init = 1
         WIDGET_CONTROL, c_dv_gui_param.id_struct.ui_draw_palette, SET_UVALUE=uval
       ENDIF
       
       WIDGET_CONTROL, c_dv_gui_param.id_struct.ui_draw_palette, GET_VALUE = index

       WINDOWSET, index
       tv, *uval.PTR_image_palette
       WINDOWSET, /RESTORE
     END ;PRO PLOT_PALETTE



     ;+=================================================================
     ;
     ; STATUS = 1 - Successful
     ; STATUS = 0 - Error
     ;
     ;-=================================================================
     PRO DV_PICK_IMAGE, BACKGROUND=background, MASK=mask, STATUS=status

       COMMON DV_GUI
       COMMON DV_IMAGE

       CASE 1 OF
         KEYWORD_SET(background): BEGIN
           title = 'Select Background Image To Load'
         END
         KEYWORD_SET(mask): BEGIN
           title = 'Select Image Mask File To Load'
         END
         ELSE: BEGIN
           title='Select Image To Load'
         ENDELSE
       ENDCASE

       file = DIALOG_PICKFILE(/MUST_EXIST $
                              ,PATH=c_dv_gui_param.default_path $
                              ,/READ $
                              ,GET_PATH=filepath $
                              ,TITLE=tile)

       IF file EQ '' THEN BEGIN
         text = 'File load canceled'
         DV_MESSAGE, text
         status = 0
         RETURN
       END

       CASE 1 OF
         KEYWORD_SET(background): BEGIN
           image_back_data.filename = file
         END
         KEYWORD_SET(mask): BEGIN
           image_mask_data.filename = file
         END
         ELSE: BEGIN
           image_data.filename = file
         ENDELSE
       ENDCASE

       c_dv_gui_param.default_path = filepath
       status = 1
           
     END ;DV_PICK_IMAGE


     ;+=================================================================
     ;-=================================================================
     PRO DV_RELOAD_IMAGE, NO_REFRESH=no_refresh

       COMMON DV_GUI
       COMMON DV_IMAGE

       CATCH, error_status
       IF error_status NE 0 THEN BEGIN
         CATCH, /CANCEL
         DV_MESSAGE, 'Could not load image.'
       ENDIF

       filename = image_data.filename

       IF filename EQ '' THEN RETURN

       IF FILE_TEST(filename) THEN BEGIN
         text = STRING('Loading File: ' + filename)
         DV_MESSAGE, text

         DV_READ_IMAGE, filename

         IF NOT keyword_set(NO_REFRESH) THEN BEGIN
           DV_RELOAD_FINAL_IMAGE
         ENDIF
           

         ; Check to see if the 1D plot window is open.
         IF widget_info(c_dv_gui_param.id_struct.ui_base_draw_1D, /map) THEN BEGIN
           DV_PLOT_IMAGE_1D
         ENDIF

       ENDIF ELSE BEGIN
         IF image_data.init THEN BEGIN
           image_data.init = 0
           DV_MESSAGE, 'Image file not found: ' + filename
         ENDIF
       ENDELSE

     END ;DV_PRO LOAD_IMAGE

     
     ;+=================================================================
     ;-=================================================================
     PRO DV_RELOAD_IMAGE_BACK, NO_REFRESH=no_refresh

       COMMON DV_GUI
       COMMON DV_IMAGE

       CATCH, error_status
       IF error_status NE 0 THEN BEGIN
         CATCH, /CANCEL
         DV_MESSAGE, 'Could not load background image.'
       ENDIF

       filename = image_back_data.filename

       IF filename EQ '' THEN RETURN

       IF FILE_TEST(filename) THEN BEGIN
         text = STRING('Loading Background: ' + filename)
         DV_MESSAGE, text
         
         DV_READ_IMAGE, filename, /BACKGROUND

         IF NOT keyword_set(NO_REFRESH) THEN BEGIN
           DV_RELOAD_FINAL_IMAGE
         ENDIF

         ; Check to see if the 1D plot window is open.
         IF widget_info(c_dv_gui_param.id_struct.ui_base_draw_1D, /map) THEN BEGIN
           DV_PLOT_IMAGE_1D
         ENDIF

       ENDIF ELSE BEGIN
         IF image_back_data.init THEN BEGIN
           image_back_data.init = 0 
           DV_MESSAGE, 'Background file not found: ' + filename
         ENDIF
       ENDELSE

     END ;DV_PRO LOAD_IMAGE_BACK

     
     ;+=================================================================
     ;-=================================================================
     PRO DV_RELOAD_IMAGE_MASK, NO_REFRESH=no_refresh

       COMMON DV_GUI
       COMMON DV_IMAGE

       CATCH, error_status
       IF error_status NE 0 THEN BEGIN
         CATCH, /CANCEL
         DV_MESSAGE, 'Could not load image mask.'
       ENDIF

       filename = image_mask_data.filename

       IF filename EQ '' THEN RETURN

       IF FILE_TEST(filename) THEN BEGIN
         text = STRING('Loading image mask: ' + filename)
         DV_MESSAGE, text
         
         DV_READ_IMAGE, filename, /MASK

         IF NOT keyword_set(NO_REFRESH) THEN BEGIN
           DV_RELOAD_FINAL_IMAGE
         ENDIF

         ; Check to see if the 1D plot window is open.
         IF widget_info(c_dv_gui_param.id_struct.ui_base_draw_1D, /map) THEN BEGIN
           DV_PLOT_IMAGE_1D
         ENDIF

       ENDIF ELSE BEGIN
         IF image_mask_data.init THEN BEGIN
           image_mask_data.init = 0 
           DV_MESSAGE, 'Mask file not found: ' + filename
         ENDIF
       ENDELSE

     END ; PRO DV_RELOAD_IMAGE_MASK  


     ;+=================================================================
     ;-=================================================================
     PRO DV_RELOAD_ALL_IMAGES, NO_REFRESH=no_refresh

       COMMON DV_GUI

       ; First reload the images
       DV_RELOAD_IMAGE_BACK, /NO_REFRESH
       DV_RELOAD_IMAGE_MASK, /NO_REFRESH
       DV_RELOAD_IMAGE, NO_REFRESH=no_refresh
           
       ; Check to see if the 1D plot window is open.
       IF WIDGET_INFO(c_dv_gui_param.id_struct.ui_base_draw_1D, /map) THEN BEGIN
         DV_PLOT_IMAGE_1D
       ENDIF

       DV_MESSAGE, 'Images reloaded.'


     END ;DV_RELOAD_ALL_IMAGES


     ;+=================================================================
     ;-=================================================================
     PRO DV_CLEAR_IMAGE

       COMMON DV_GUI
       COMMON DV_IMAGE

       ; First clear the background image and data.
       image_back = !NULL
       image_back_data.filename = ''
       image_back_data.init = 0

       ; Clear the mask and mask data.
       image_mask =  !NULL
       image_mask_data.filename = ''
       image_mask_data.init = 0

       ; Now clear the image and image data
       image_final = image_final * 0
       image = image * 0
       image_bytscl = image_bytscl * 0

       image_data.filename = ''
       image_data.init = 0

       ; Clear the final image data
       image_final_data.title = ''
       image_final_data.init = 0


       IF WIDGET_INFO(c_dv_gui_param.id_struct.ui_draw, /map) THEN BEGIN
         WIDGET_CONTROL, c_dv_gui_param.id_struct.ui_draw, GET_VALUE=index
         WINDOWSET, index
         TV, image_bytscl
         WINDOWSET, /RESTORE
       ENDIF
       
       ; Check to see if the 1D plot window is open.
       IF widget_info(c_dv_gui_param.id_struct.ui_base_draw_1D, /map) THEN BEGIN
         DV_PLOT_IMAGE_1D, /CLEAR
       ENDIF

       DV_MESSAGE, 'Images cleared.'

       DV_UPDATE_SENSITIVE

     END ;DV_PRO LOAD_IMAGE


     ;+=================================================================
     ;
     ; This will rebuild the final images from the current
     ; image and background image, then rebuild the final scaled image.
     ;
     ; No image files are reread.
     ;
     ;-=================================================================
     PRO DV_RELOAD_FINAL_IMAGE

       COMMON DV_GUI
       COMMON DV_IMAGE


       ; Set the subtraction buttons to be be sensitive or not.
       DV_UPDATE_SENSITIVE
    

       IF image_data.init EQ 0 THEN BEGIN
         IF image_back_data.init EQ 1 THEN BEGIN
           title =  'Background Image'
           image_final = image_back

         ENDIF ELSE BEGIN
           DV_MESSAGE, 'Image has not been loaded'
           RETURN
         ENDELSE
       ENDIF ELSE BEGIN
         title =  'Image'
         image_final = image
         IF image_final_data.back_subtraction THEN BEGIN
           ; Background subtraction is requested.
           ; Check if a background image has been loaded.
           ;
           ; If not, then disable background subtraction
           IF image_back_data.init THEN BEGIN
             image_final -= image_back
             title += ' - With Background Subtraction'
           ENDIF ELSE BEGIN
             DV_MESSAGE, 'Background file not loaded.', /LOG
           ENDELSE
         ENDIF

         IF image_final_data.use_mask THEN BEGIN
           ; Use image mask has been requested.
           ; Check if a mask image has been loaded.
           ;
           ; If not, then disable mask
           IF image_mask_data.init THEN BEGIN
             IF ~ARRAY_EQUAL(image_data.size, image_mask_data.size) THEN BEGIN
               MESSAGE, 'Image and mask have different sizes.'
             ENDIF
             
             IF image_final_data.invert_mask THEN BEGIN
               image_final[WHERE(image_mask, /NULL)] = 0
             ENDIF ELSE BEGIN
               image_final[WHERE(~image_mask, /NULL)] = 0
             ENDELSE
              
             title += ' (Masked)' 
           ENDIF ELSE BEGIN
             DV_MESSAGE, 'Image mask file not loaded.', /LOG
           ENDELSE
         ENDIF
       ENDELSE
         
       ; This is here to handle data from the DDD detector (A.C. Milazzo).
       ; For that system the image and the bacground image were combined into a single
       ; image, with one on top of the other.
       ;
       ; This will only work for image sizes with dimensions of A x 2*A
       IF image_final_data.image_subtraction THEN BEGIN
         IF image_data.size[1] NE 2*image_data.size[0] THEN BEGIN
           DV_MESSAGE, 'Image not of correct ratio for image subtraction.', /LOG
         ENDIF ELSE BEGIN
           y_size = image_final_data.size[1]
           image_final = image_final[*, y_size/2:y_size-1] - image_final[*, 0:y_size/2-1]
           IF image_final_data.back_subtraction THEN BEGIN
             title += ' And Image Subtraction'
           ENDIF ELSE BEGIN
             title += ' - With Image Subtraction'
           ENDELSE
         ENDELSE
       ENDIF
  
       ; Set the title.
       image_final_data.title = title

       ; Save the final image size
       image_final_data.size = SIZE(image_final, /DIM)

       ; Save the final image_range
       max = MAX(image_final, MIN=min)
       image_final_data.range = [min,max]

       ; Set the final image init flag
       image_final_data.init = 1

       ; Reset the cutoff range
       c_dv_gui_param.display_range = image_final_data.range

       ;Convert image to scaled bytarray
       DV_RESCALE_IMAGE

       ; Update all displays
       DV_UPDATE_ALL


     END ; DV_RELOAD_FINAL_IMAGE


 
     ;+=================================================================
     ;
     ; This proceduce rescales the final image, and calls any other
     ; needed scaling routines.
     ;
     ;-=================================================================
     PRO DV_RESCALE_IMAGE, range

       DV_SCALE_IMAGE, range
       DV_MINMAX_REBUILD_PALETTE, range

     END ; PRO DV_RESCALE_IMAGE


     ;+=================================================================
     ;
     ; This proceduce take a filename for the image to load and
     ; reads the images file and loads it into the common block.
     ;
     ;
     ;-=================================================================
     PRO DV_READ_IMAGE, filename $
                        ,BACKGROUND=background $
                        ,MASK=mask
       COMMON DV_IMAGE
       
       CATCH, error_status
       IF error_status NE 0 THEN BEGIN
         CATCH, /CANCEL
         DV_MESSAGE, 'ERROR: Could not load image.'
         DV_MESSAGE, !ERROR_STATE.MSG, /LOG
         MESSAGE, /REISSUE_LAST
       ENDIF
       
       IF NOT FILE_TEST(filename) THEN BEGIN
         MESSAGE, 'Image file not found: ' + filename
       ENDIF

       filename_split = STRSPLIT(filename, '.', /EXTRACT, COUNT=count)
       suffix = STRLOWCASE(filename_split[count-1])

       CASE 1 OF
         (suffix EQ 'txt'): BEGIN
           WIDGET_CONTROL, /hourglass
           image_read = FIX( (READ_ASCII(filename, DELIMITER =  ' ')).field0001 )
         END

         ((suffix EQ 'tif') OR (suffix EQ 'tiff')): BEGIN
           image_read = READ_TIFF(filename)
         END

         (suffix EQ 'bin'): BEGIN
           ; This was put here to allow A.C. Milazzo to read binary data
           ; from the DDD detector system.
           data_dims = [1024,2048]
           image_read = read_binary(filename, DATA_TYPE=2, DATA_DIMS=data_dims)
         END

         ELSE: BEGIN
           MESSAGE, 'File type unknown.'
         ENDELSE

       ENDCASE
         
       ; If the image was saved as an unsigned or short integer image, then
       ; covert it to a long integer image.
       type = -1
       CASE SIZE(image_read, /TYPE) OF
         2: type = 3
         12: type = 3
         13: type = 3
         15: type = 14
         ELSE:
       ENDCASE
       IF type NE -1 THEN BEGIN
         image_read = FIX(image_read, TYPE=type)
       ENDIF

       DV_MESSAGE, 'Image Read', /LOG
         
       
       ;Use TEMPORARY() to conserve memory while renaming
       CASE 1 OF
         KEYWORD_SET(background): BEGIN
           image_back = TEMPORARY(image_read)
           image_back_data.size = SIZE(image_back, /DIM)
           image_back_data.init = 1
         END
         KEYWORD_SET(mask): BEGIN
           image_mask = TEMPORARY(image_read)
           image_mask_data.size = SIZE(image_mask, /DIM)
           image_mask_data.init = 1
         END
         ELSE: BEGIN
           image = TEMPORARY(image_read)
           image_data.size = SIZE(image, /DIM)
           image_data.init = 1
         ENDELSE
       ENDCASE
       
     END ; PRO DV_READ_IMAGE


     ;+=================================================================
     ;
     ; Check the display range, then return a corrected range.
     ;
     ; This will reverse the display range indexes if necessary
     ; to find a valid range.
     ;
     ;-=================================================================
     FUNCTION DV_GET_DISPLAY_RANGE, MAX_RANGE=max_range $
                                    ,NO_REVERSE=no_reverse
       COMMON DV_GUI
       COMMON DV_IMAGE

       IF image_final_data.init THEN BEGIN
         MIR_DEFAULT, max_range, image_final_data.range
       ENDIF ELSE BEGIN
         MIR_DEFAULT, max_range, [0D,255D]
       ENDELSE

       ; Set a default display_range.
       display_range = c_dv_gui_param.display_range
       IF ARRAY_EQUAL(display_range, [0,0]) THEN BEGIN
         display_range = max_range
       ENDIF

       ; Now check the range.
       IF ~ KEYWORD_SET(no_reverse) THEN BEGIN
         display_range = display_range[SORT(display_range)]
       ENDIF
         

       display_range[0] = display_range[0] > max_range[0]
       display_range[0] = display_range[0] < max_range[1]

       display_range[1] = display_range[1] > max_range[0]
       display_range[1] = display_range[1] < max_range[1]

       RETURN, display_range
         
     END ; FUNCTION DV_GET_DISPLAY_RANGE


     ;+=================================================================
     ;
     ; Set the display range to the given range.
     ;
     ;-=================================================================
     PRO DV_SET_DISPLAY_RANGE, range
       COMMON DV_GUI
       COMMON DV_IMAGE

       IF N_ELEMENTS(range) EQ 0 THEN BEGIN
         range = DV_GET_DISPLAY_RANGE()
       ENDIF ELSE BEGIN
         c_dv_gui_param.display_range[0] = range[0]
         c_dv_gui_param.display_range[1] = range[1]
       ENDELSE

       DV_RESCALE_IMAGE, range
       DV_UPDATE_ALL
       
     END ;PRO DV_MINMAX_EVENT_SLIDER


     ;+=================================================================
     ;
     ; Convert the final image to sclaled byte array.
     ;
     ; Here we also set the palette labels.
     ;
     ; This will allow the ploting of the image to be done very
     ; quickly.
     ;
     ;-=================================================================
     PRO DV_SCALE_IMAGE, range
       COMMON DV_IMAGE
       COMMON DV_GUI

       IF N_ELEMENTS(range) EQ 0 THEN BEGIN
         range = DV_GET_DISPLAY_RANGE()
       ENDIF

       ; do the cutoff
       where_lt = WHERE(image_final LT range[0], num_lt)
       where_gt = WHERE(image_final GT range[1], num_gt)

       image_local = image_final
       IF num_lt GT 0 THEN BEGIN
         image_local[where_lt] = range[0]
       ENDIF
       IF num_gt GT 0 THEN BEGIN
         image_local[where_gt] = range[1]
       ENDIF

       ;Convert image to scaled BYTARRAY
       image_bytscl = BYTSCL(image_local, MIN=range[0], MAX=range[1], TOP=!D.TABLE_SIZE-1)


     END ; PRO DV_SCALE_IMAGE


     ;+=================================================================
     ;-=================================================================
     PRO DV_WRITE, DATA=data $
                   ,IMAGE_COLOR=image_color $
                   ,IMAGE_BW=image_bw $
                   ,ZOOM_COLOR=zoom_color $
                   ,ZOOM_BW=zoom_bw $
                   ,ZOOM_CONTOUR=zoom_contour $
                   ,IMAGE_BINARY=image_binary $
                   ,BACK_BINARY=back_binary
       COMMON DV_IMAGE
       COMMON DV_GUI
       
       
       ;GET FILENAMES AND PATH
       image_filename = image_data.filename
       DV_FILE_NAME_PARSE, image_filename, FILETITLE=image_filetitle

       
       back_filename = image_back_data.filename
       DV_FILE_NAME_PARSE, back_filename, FILETITLE=back_filetitle
       
       
       IF KEYWORD_SET(image_color) THEN BEGIN
         TVLCT, red, green, blue, /get
         status = DIALOG_WRITE_IMAGE(image_bytscl, red, green, blue $
                                     ,/WARN_EXIST $
                                     ,TYPE='PNG' $
                                     ,TITLE='Save Color Image' $
                                     ,OPTIONS=save_options $
                                     ,FILE = string(image_filetitle+'.png') $
                                     ,PATH = c_dv_gui_param.default_path)
         IF status eq 1 THEN $
           text = ('Color Image Saved To: ' + save_options.filename) $
         ELSE $
           text = ('Image Save Canceled')
       ENDIF
       
       IF KEYWORD_SET(image_bw) THEN BEGIN
         status = DIALOG_WRITE_IMAGE(image_bytscl, /WARN_EXIST, TYPE='PNG' $
                                     ,TITLE='Save Black and White Image' $
                                     ,OPTIONS=save_options $
                                     ,FILE = STRING(image_filetitle+'.png') $
                                     ,PATH = c_dv_gui_param.default_path)
         IF status eq 1 THEN $
           text = ('B/W Image Saved To: ' + save_options.filename) $
         ELSE $
           text = ('Image Save Canceled')
       ENDIF
       
       IF KEYWORD_SET(zoom_color) THEN BEGIN
         TVLCT, red, green, blue, /get
         status = DIALOG_WRITE_IMAGE(image_bytscl_zoom, red, green, blue, /WARN_EXIST, TYPE='PNG' $
                                     ,TITLE='Save Color Zoom Image', OPTIONS=save_options $
                                     ,FILE = STRING(image_filetitle+'_zoom.png') $
                                     ,PATH = c_dv_gui_param.default_path)
         IF status eq 1 THEN $
           text = ('Color Image Saved To: ' + save_options.filename) $
         ELSE $
           text = ('Image Save Canceled')
       ENDIF
       
       IF KEYWORD_SET(zoom_bw) THEN BEGIN
         status = DIALOG_WRITE_IMAGE(image_bytscl_zoom, /WARN_EXIST, TYPE='PNG' $
                                     ,TITLE='Save Black and White Zoom Image' $
                                     ,OPTIONS=save_options $
                                     ,FILE = string(image_filetitle+'_zoom.png') $
                                     ,PATH = c_dv_gui_param.default_path)
         IF status eq 1 THEN $
           text = ('B/W Zoom Image Saved To: ' + save_options.filename) $
         ELSE $
           text = ('Image Save Canceled')
       ENDIF
       
       IF KEYWORD_SET(zoom_contour) THEN BEGIN
         filename = DIALOG_PICKFILE(DEFAULT_EXTENSION='ps', /WRITE, /OVERWRITE_PROMPT $
                                    ,TITLE='Write Zoom Contour To PostScript Format' $
                                    ,FILTER = ['*.*','*.ps']  $
                                    ,FILE = string(image_filetitle+'_contour.ps') $
                                    ,PATH = c_dv_gui_param.default_path)
         IF filename ne '' THEN BEGIN
           original_device = !D.NAME
           set_plot, 'ps'
           DEVICE, FILE=filename, /COLOR
           
           WIDGET_CONTROL, data.id_struct.ui_zoom, GET_UVALUE=uval_zoom, /NO_COPY
           original_contour = uval_zoom.contour
           uval_zoom.contour = 1
           WIDGET_CONTROL, data.id_struct.ui_zoom, SET_UVALUE=uval_zoom, /NO_COPY
           DV_PLOT_IMAGE_ZOOM, ID_STRUCT=data.id_struct, uval_zoom.old_x, uval_zoom.old_y, /NO_WSET
           
           DEVICE, /CLOSE
           SET_PLOT, original_device
           
           uval_zoom.contour = original_contour
           WIDGET_CONTROL, data.id_struct.ui_zoom, SET_UVALUE=uval_zoom
           
           text = ('Contour Image Saved To: ' + filename)
         ENDIF ELSE $
           text = ('Image Save Canceled')
         
       ENDIF
       
       IF KEYWORD_SET(image_binary) THEN BEGIN
         filename = DIALOG_PICKFILE(DEFAULT_EXTENSION='bin', /WRITE, /OVERWRITE_PROMPT $
                                    ,TITLE='Write Image To Binary Format' $
                                    ,FILTER = ['*.txt; *.bin','*.txt', '*.bin', '*.*']  $
                                    ,FILE = string(image_filetitle+'.bin') $
                                    ,PATH = c_dv_gui_param.default_path)
         IF filename ne '' THEN BEGIN
           openw, 1, filename
           writeu, 1, image_final
           close, 1
           text = ('Binary Image Saved To: ' + filename)
         ENDIF ELSE $
           text = ('Image Save Canceled')
       ENDIF
       
       IF KEYWORD_SET(back_binary) THEN BEGIN
         filename = DIALOG_PICKFILE(DEFAULT_EXTENSION='bin', /WRITE, /OVERWRITE_PROMPT $
                                    ,TITLE='Write Background To Binary Format' $
                                    ,FILTER = ['*.txt; *.bin', '*.txt', '*.bin', '*.*' ]  $
                                    ,FILE = string(back_filetitle+'.bin') $
                                    ,PATH = c_dv_gui_param.default_path)
         IF filename ne '' THEN BEGIN
           openw, 1, filename
           writeu, 1, image_back
           close, 1
           text = ('Binary Background Saved To: ' + filename)
         ENDIF ELSE $
           text = ('Background Save Canceled')
       ENDIF
       
       DV_MESSAGE, text
       
     END ;PRO dv_write


     ;+=================================================================
     ;-=================================================================
     ; NOTE: There are native IDL routines to do this.
     PRO DV_FILE_NAME_PARSE, file_full_path, FILETITLE=filetitle, FILEPATH=filepath
       
       last_period = strpos(file_full_path, '.', /reverse_search)
       last_slash = strpos(file_full_path, '/', /reverse_search)
       IF last_slash eq -1 THEN $
         last_slash = strpos(file_full_path, '\', /reverse_search)
       IF last_slash eq -1 THEN $
         last_slash = 0
       
       IF last_period ne -1 THEN $
         filetitle = strmid(file_full_path, last_slash+1, last_period-last_slash-1) $
       ELSE $
         filetitle = file_full_path
       
       filepath = strmid(file_full_path, 0, last_slash+1)
       
     END ;PRO dv_file_name_parse


     ;+=================================================================
     ;
     ; Sets the sensitivity of the subtraction buttons depending on
     ; what is loaded.
     ;
     ;-=================================================================
     PRO DV_UPDATE_SENSITIVE
       COMMON DV_IMAGE
       COMMON DV_GUI

       IF image_data.init AND image_back_data.init THEN BEGIN
         WIDGET_CONTROL, c_dv_gui_param.id_struct.ui_button_background $
           ,SENSITIVE=1
       ENDIF ELSE BEGIN
         WIDGET_CONTROL, c_dv_gui_param.id_struct.ui_button_background $
           ,SENSITIVE=0
       ENDELSE

       IF (image_data.init OR image_back_data.init) $
         AND (image_data.size[1] EQ 2*image_data.size[0]) THEN BEGIN
         WIDGET_CONTROL, c_dv_gui_param.id_struct.ui_button_subtract $
           ,SENSITIVE=1
       ENDIF ELSE BEGIN
         WIDGET_CONTROL, c_dv_gui_param.id_struct.ui_button_subtract $
           ,SENSITIVE=0
       ENDELSE

       IF image_data.init AND image_mask_data.init THEN BEGIN
         WIDGET_CONTROL, c_dv_gui_param.id_struct.ui_button_mask $
           ,SENSITIVE=1
         WIDGET_CONTROL, c_dv_gui_param.id_struct.ui_button_invertmask $
           ,SENSITIVE=1
       ENDIF ELSE BEGIN
         WIDGET_CONTROL, c_dv_gui_param.id_struct.ui_button_mask $
           ,SENSITIVE=0
         WIDGET_CONTROL, c_dv_gui_param.id_struct.ui_button_invertmask $
           ,SENSITIVE=0
       ENDELSE

     END ; DV_UPDATE_SENSITIVE


; =================================================================
; =================================================================
; #################################################################
;
; BEGIN CONTROL ROUTINES
;
; #################################################################
; =================================================================
; =================================================================


     ;+=================================================================
     ;
     ; Load an image from the given file name.
     ;
     ; /BACKGROUND can be used to specifed that this should be
     ;   loaded as a backgound image.
     ;
     ; /NO_REFRESH will cause the image to be read and stored, but
     ;   will supress the final display of the new image. This is
     ;   useful when this routine will be called twice in order to
     ;   load both the image and the background image.
     ; 
     ;
     ;-=================================================================
     PRO DV_LOAD_IMAGE, filename_in $
                        ,PATH=path $
                        ,BACKGROUND=background $
                        ,NO_REFRESH=no_refresh
       COMMON DV_IMAGE
       
       IF filename_in EQ '' THEN BEGIN
         IF KEYWORD_SET(background) THEN BEGIN
           image_back_data.filename = ''
           image_back_data.init = ''
         ENDIF ELSE BEGIN
           image_data.filename = ''
           image_data.init = ''
         ENDELSE

         IF NOT KEYWORD_SET(NO_REFRESH) THEN BEGIN
           DV_RELOAD_FINAL_IMAGE
         ENDIF
           
       ENDIF ELSE BEGIN

         IF KEYWORD_SET(path) THEN BEGIN
           filename = MIR_PATH_JOIN([path, filename_in])
         ENDIF ELSE BEGIN
           filename = filename_in
         ENDELSE

         IF NOT FILE_TEST(filename) THEN BEGIN
           MESSAGE, 'File could not be found: '+filename
         ENDIF

         IF KEYWORD_SET(background) THEN BEGIN
           image_back_data.filename = filename
           DV_RELOAD_IMAGE_BACK, NO_REFRESH=no_refresh
         ENDIF ELSE BEGIN
           image_data.filename = filename
           DV_RELOAD_IMAGE, NO_REFRESH=no_refresh
         ENDELSE
       ENDELSE
       
     END ;DV_LOAD_IMAGE


     ;+=================================================================
     ;
     ; Sets the current image (or background image) to the given
     ; image.
     ;
     ; /BACKGROUND can be used to specifed that image should be
     ;   loaded as a backgound image.
     ; 
     ;
     ;-=================================================================
     PRO DV_SET_IMAGE, image_in $
                       ,BACKGROUND=background $
                       ,NO_REFRESH=no_refresh
       COMMON DV_IMAGE
       
       image_in_size = SIZE(image_in, /DIM)
       IF N_ELEMENTS(image_in_size) NE 2 THEN BEGIN
         MESSAGE, 'Not a valid image. Image must be a two dimentional array.'
       ENDIF

       IF KEYWORD_SET(background) THEN BEGIN
         image_back = image_in
         image_back_data.size = image_in_size
         image_back_data.init = 1
       ENDIF ELSE BEGIN
         image = image_in
         image_data.size = image_in_size
         image_data.init = 1
       ENDELSE

       DV_RELOAD_FINAL_IMAGE
       
     END ;DV_SET_IMAGE


     ;+=================================================================
     ;
     ; Set weather or not to use background subtraction.
     ;
     ;-=================================================================
     PRO DV_SET_BACK_SUBTRACTION, back_subtraction, NO_REFRESH=no_refresh
       COMMON DV_IMAGE
       COMMON DV_GUI

       image_final_data.back_subtraction = KEYWORD_SET(back_subtraction)

       IF NOT KEYWORD_SET(no_refresh) THEN BEGIN
         DV_RELOAD_FINAL_IMAGE
       ENDIF

       WIDGET_CONTROL, c_dv_gui_param.id_struct.ui_button_background $
         ,SET_BUTTON=image_final_data.back_subtraction

     END ; PRO DV_SET_BACK_SUBTRACTION, back_subtraction


     ;+=================================================================
     ;
     ; Set whether or not to use image subtraction.
     ; Image subtraction is where the bottom half of the image is
     ; subtracted from the top half.  This will only work if
     ; the image has the appropriate dimentions (A x 2A).
     ;
     ;-=================================================================
     PRO DV_SET_IMAGE_SUBTRACTION, image_subtraction, NO_REFRESH=no_refresh
       COMMON DV_IMAGE
       COMMON DV_GUI

       image_final_data.image_subtraction = KEYWORD_SET(image_subtraction)

       IF NOT KEYWORD_SET(no_refresh) THEN BEGIN
         DV_RELOAD_FINAL_IMAGE
       ENDIF

       WIDGET_CONTROL, c_dv_gui_param.id_struct.ui_button_subtract $
         ,SET_BUTTON=image_final_data.image_subtraction

     END ; PRO DV_SET_IMAGE_SUBTRACTION, image_subtraction


     ;+=================================================================
     ;
     ; Set weather or not to use an image mask.
     ;
     ;-=================================================================
     PRO DV_SET_USE_MASK, use_mask, NO_REFRESH=no_refresh
       COMMON DV_IMAGE
       COMMON DV_GUI

       image_final_data.use_mask= KEYWORD_SET(use_mask)

       IF NOT KEYWORD_SET(no_refresh) THEN BEGIN
         DV_RELOAD_FINAL_IMAGE
       ENDIF

       WIDGET_CONTROL, c_dv_gui_param.id_struct.ui_button_mask $
         ,SET_BUTTON=image_final_data.use_mask

     END ; PRO DV_SET_USE_MASK, use_mask


     ;+=================================================================
     ;
     ; Set weather or not to use an image mask.
     ;
     ;-=================================================================
     PRO DV_SET_INVERT_MASK, invert_mask, NO_REFRESH=no_refresh
       COMMON DV_IMAGE
       COMMON DV_GUI

       image_final_data.invert_mask= KEYWORD_SET(invert_mask)

       IF NOT KEYWORD_SET(no_refresh) THEN BEGIN
         DV_RELOAD_FINAL_IMAGE
       ENDIF

       WIDGET_CONTROL, c_dv_gui_param.id_struct.ui_button_mask $
         ,SET_BUTTON=image_final_data.invert_mask

     END ; PRO DV_SET_INVERT_MASK, invert_mask

     
; =================================================================
; =================================================================
; #################################################################
;
; BEGIN DATA ACCESS FUNCTIONS
;
; #################################################################
; =================================================================
; =================================================================


     ;+=================================================================
     ;-=================================================================
     FUNCTION DV_IS_IMAGE_LOADED
       COMMON DV_IMAGE

       RETURN, KEYWORD_SET(N_ELEMENTS(image))
       
     END ;FUNCTION DV_IS_IMAGE_LOADED

     
     ;+=================================================================
     ;-=================================================================
     FUNCTION DV_IS_IMAGE_BACK_LOADED
       COMMON DV_IMAGE

       RETURN, KEYWORD_SET(N_ELEMENTS(image_back))
       
     END ;FUNCTION DV_IS_IMAGE_BACK_LOADED

     
     ;+=================================================================
     ;-=================================================================
     FUNCTION DV_IS_IMAGE_1D_LOADED
       COMMON DV_IMAGE

       RETURN, KEYWORD_SET(N_ELEMENTS(image_1d))
       
     END ;FUNCTION DV_IS_IMAGE_1D_LOADED

     
     ;+=================================================================
     ;-=================================================================
     FUNCTION DV_GET_IMAGE_FINAL
       COMMON DV_IMAGE

       IF N_ELEMENTS(image_final) EQ 0 THEN BEGIN
         MESSAGE, 'Image not loaded.'
       ENDIF

       RETURN, image_final
     END ;FUNCTION DV_GET_IMAGE_FINAL

     
     ;+=============================================================
     ;-=================================================================
     FUNCTION DV_GET_IMAGE_FINAL_DATA
       COMMON DV_IMAGE

       RETURN, image_final_data
     END ;FUNCTION DV_GET_IMAGE_FINAL_DATA

     
     ;+=================================================================
     ;-=================================================================
     FUNCTION DV_GET_IMAGE
       COMMON DV_IMAGE

       RETURN, image
     END ;FUNCTION DV_GET_IMAGE

     
     ;+=================================================================
     ;-=================================================================
     FUNCTION DV_GET_IMAGE_DATA
       COMMON DV_IMAGE

       IF N_ELEMENTS(image_data) EQ 0 THEN BEGIN
         MESSAGE, 'Image data not loaded.'
       ENDIF

       RETURN, image_data
     END ;FUNCTION DV_GET_IMAGE_DATA

     
     ;+=================================================================
     ;-=================================================================
     FUNCTION DV_GET_IMAGE_BACK
       COMMON DV_IMAGE

       RETURN, image_back
     END ;FUNCTION DV_GET_IMAGE_BACK

     
     ;+=================================================================
     ;-=================================================================
     FUNCTION DV_GET_IMAGE_BACK_DATA
       COMMON DV_IMAGE

       RETURN, image_back_data
     END ;FUNCTION DV_GET_IMAGE_BACK_DATA

     
     ;+=================================================================
     ;-=================================================================
     FUNCTION DV_GET_IMAGE_1D
       COMMON DV_IMAGE

       IF N_ELEMENTS(image_1d) EQ 0 THEN BEGIN
         MESSAGE, '1D spectrum not loaded.'
       ENDIF

       RETURN, image_1D
     END ;FUNCTION DV_GET_IMAGE_1D

     
     ;+=================================================================
     ;-=================================================================
     FUNCTION DV_GET_IMAGE_1D_DATA
       COMMON DV_IMAGE

       RETURN, image_1D_data
     END ;FUNCTION DV_GET_IMAGE_1D


; ======================================================================
; ======================================================================
; #################################################################
;
; BUILD THE CUTOFF (MIN/MAX) WINDOW
;
; #################################################################
; ======================================================================
; ======================================================================


     ;+=================================================================
     ;
     ; Build the GUI for the min/max controls.
     ;
     ;-=================================================================
     FUNCTION DV_MINMAX_BUILD_GUI, UPDATE=update

       COMMON DV_GUI

       ui_base = WIDGET_BASE(/COLUMN $
                             ,GROUP_LEADER=c_dv_gui_param.id_struct.ui_base $
                             ,/ALIGN_CENTER $
                            )
       ui_base_top = WIDGET_BASE(ui_base, /COLUMN, /ALIGN_CENTER)
       ui_base_bottom = WIDGET_BASE(ui_base, /COLUMN, /ALIGN_CENTER)
                             
       xsize = 256
       hist_size = [xsize, 100]
       ui_draw_hist = WIDGET_DRAW(ui_base_top, UNAME='ui_draw_hist' $
                                  ,XSIZE=hist_size[0] $
                                  ,YSIZE=hist_size[1] $
                                 )

       palette_size = [xsize, 10]
       ui_draw_palette = WIDGET_DRAW(ui_base_top, UNAME='ui_draw_palette' $
                                  ,XSIZE=palette_size[0] $
                                  ,YSIZE=palette_size[1] $
                                 )

       slider_x_add = 38
       ui_slider_max = WIDGET_SLIDER(ui_base_bottom, UNAME='ui_slider_max' $
                                     ,XSIZE=xsize+slider_x_add $
                                    )

       ui_slider_min = WIDGET_SLIDER(ui_base_bottom, UNAME='ui_slider_min' $
                                       ,XSIZE=xsize+slider_x_add $
                                      )


       id_struct = { $
                     ui_base:ui_base $
                     ,ui_draw_hist:ui_draw_hist $
                     ,ui_draw_palette:ui_draw_palette $
                     ,ui_slider_max:ui_slider_max $
                     ,ui_slider_min:ui_slider_min $
                   }

       state_holder = WIDGET_INFO(ui_base, /CHILD)
       state = { $
                 id_struct:id_struct $
                 ,image_palette:FLTARR(palette_size) $
                 ,image_palette_size:palette_size $
               }

       WIDGET_CONTROL, state_holder, SET_UVALUE=state, UPDATE=update

       WIDGET_CONTROL, ui_base, /realize
       XMANAGER, 'dv_build_gui_minmax', ui_base, /JUST_REG, EVENT_HANDLER='DV_MINMAX_EVENT'
              
       RETURN, ui_base

     END ;FUNCTION DV_MINMAX_BUILD_GUI


     ;+=================================================================
     ;
     ; Initialize the MINMAX window.
     ;
     ;-=================================================================
     PRO DV_MINMAX_INIT
       COMMON DV_GUI

       ui_minmax = DV_MINMAX_BUILD_GUI(UPDATE=0)
       c_dv_gui_param.gui_id.minmax = ui_minmax

       DV_MINMAX_UPDATE

       WIDGET_CONTROL, ui_minmax, UPDATE=1

     END ;PRO DV_UI_MINMAX_INIT


     ;+=================================================================
     ;
     ; Update the histogram as slider values for the min/max
     ; dialog box.
     ;
     ;-=================================================================
     PRO DV_MINMAX_UPDATE
       COMMON DV_GUI
       COMMON DV_IMAGE
       
       IF ~ WIDGET_INFO(c_dv_gui_param.gui_id.minmax, /VALID_ID) THEN RETURN

       ; Note that this changes the state variable.
       DV_MINMAX_REBUILD_PALETTE



       state_holder = WIDGET_INFO(c_dv_gui_param.gui_id.minmax, /CHILD)
       WIDGET_CONTROL, state_holder, GET_UVALUE=state


       display_range = DV_GET_DISPLAY_RANGE(/NO_REVERSE)
       IF image_final_data.init THEN BEGIN
         image_range = image_final_data.range
       ENDIF ELSE BEGIN
         image_range = display_range
       ENDELSE


       ; Plot the histogram.
       ; ---------------------------------------------------------------

       ; Get the histogram draw window geometry.
       draw_hist_geom = WIDGET_INFO(state.id_struct.ui_draw_hist, /GEOMETRY) 

       WIDGET_CONTROL, state.id_struct.ui_draw_hist, GET_VALUE=hist_window_index

       IF image_final_data.init THEN BEGIN
         binsize = (image_range[1] - image_range[0])/draw_hist_geom.xsize
         IF MIR_IS_INTEGER(image_final) THEN BEGIN
           binsize = binsize > 1
         ENDIF


; WARNING:  This currently does not work if the data type is UINT
;           (probably also for ULONG).

         hist = HISTOGRAM(image_final $
                          ,LOCATIONS=hist_loc $
                          ,BINSIZE=binsize $
                          ,MIN=image_range[0] $
                          ,MAX=image_range[1])
       ENDIF ELSE BEGIN
         hist = DBLARR(draw_hist_geom.xsize)
         hist_loc = FINDGEN(draw_hist_geom.xsize)
       ENDELSE

       WINDOWSET, hist_window_index
       PLOT, hist_loc $
             ,hist $
             ,XSTYLE=5 $
             ,YSTYLE=5 $
             ,POSITION=[0,0,!D.X_SIZE, !D.Y_SIZE]
       WINDOWSET, /RESTORE

       ; Set the sliders
       WIDGET_CONTROL, state.id_struct.ui_slider_min $
         ,SET_VALUE=display_range[0] $
         ,SET_SLIDER_MIN=image_range[0] $
         ,SET_SLIDER_MAX=image_range[1]
 
       WIDGET_CONTROL, state.id_struct.ui_slider_max $
         ,SET_VALUE=display_range[1] $
         ,SET_SLIDER_MIN=image_range[0] $
         ,SET_SLIDER_MAX=image_range[1]
     

       WIDGET_CONTROL, state_holder, SET_UVALUE=state


       DV_MINMAX_REPAINT

     END ;PRO DV_UI_MINMAX_UPDATE


     ;+=================================================================
     ;
     ; This is the event handler for the MIN/MAX window.
     ;
     ;-=================================================================
     PRO DV_MINMAX_EVENT, event
       COMMON DV_GUI
       COMMON DV_IMAGE
       
       state_holder = WIDGET_INFO(c_dv_gui_param.gui_id.minmax, /CHILD)
       WIDGET_CONTROL, state_holder, GET_UVALUE=state

       CASE event.id OF
         state.id_struct.ui_slider_min: BEGIN
           DV_MINMAX_EVENT_SLIDER
         END

         state.id_struct.ui_slider_max: BEGIN
           DV_MINMAX_EVENT_SLIDER
         END

         ELSE:
       ENDCASE
       

     END ;PRO DV_MINMAX_EVENT


     ;+=================================================================
     ;
     ; This is the event handler for the MIN/MAX window sliders.
     ;
     ;-=================================================================
     PRO DV_MINMAX_EVENT_SLIDER
       COMMON DV_GUI
       COMMON DV_IMAGE
       
       state_holder = WIDGET_INFO(c_dv_gui_param.gui_id.minmax, /CHILD)
       WIDGET_CONTROL, state_holder, GET_UVALUE=state

       ; Get the values from the sliders.
       WIDGET_CONTROL, state.id_struct.ui_slider_min $
         ,GET_VALUE=value_min
 
       WIDGET_CONTROL, state.id_struct.ui_slider_max $
         ,GET_VALUE=value_max


       ; Check the values from the sliders.
       IF value_max LE value_min THEN BEGIN
         value_max = value_min+1
       ENDIF

  
       c_dv_gui_param.display_range[0] = value_min
       c_dv_gui_param.display_range[1] = value_max

       DV_RESCALE_IMAGE
       DV_UPDATE_ALL
       
     END ;PRO DV_MINMAX_EVENT_SLIDER


     ;+=================================================================
     ;
     ; This will rebuild the palette image.
     ;
     ;-=================================================================
     PRO DV_MINMAX_REBUILD_PALETTE, range
       COMMON DV_GUI
       COMMON DV_IMAGE

       IF ~ WIDGET_INFO(c_dv_gui_param.gui_id.minmax, /VALID_ID) THEN RETURN
             

       IF N_ELEMENTS(range) EQ 0 THEN BEGIN
         range = DV_GET_DISPLAY_RANGE()
       ENDIF

       IF image_final_data.init THEN BEGIN
         image_range = image_final_data.range
       ENDIF ELSE BEGIN
         image_range = range
       ENDELSE


       state_holder = WIDGET_INFO(c_dv_gui_param.gui_id.minmax, /CHILD)
       WIDGET_CONTROL, state_holder, GET_UVALUE=state


       ; Build the palette image
       ; ---------------------------------------------------------------
       image_palette_step = (image_range[1] - image_range[0]) / state.image_palette_size[0]
       
       FOR ii=0,state.image_palette_size[0]-1 DO BEGIN
         value = ii*image_palette_step + image_range[0]
           
         ; Do the value cutoff
         CASE 1 OF
           (value LT range[0]): BEGIN
             value = range[0]
           END
           (value GT range[1]): BEGIN
             value = range[1]
           END
           ELSE:
         ENDCASE

       display_range = DV_GET_DISPLAY_RANGE()
         state.image_palette[ii,*] = value
       ENDFOR

       WIDGET_CONTROL, state_holder, SET_UVALUE=state

     END ; PRO DV_MINMAX_REBUILD_PALETTE




     ;+=================================================================
     ;
     ; This will refresh any plots on the MIN/MAX window.
     ;
     ; At this point all this does is refresh the palette bar.
     ;
     ;-=================================================================
     PRO DV_MINMAX_REPAINT
       COMMON DV_GUI

       IF ~ WIDGET_INFO(c_dv_gui_param.gui_id.minmax, /VALID_ID) THEN RETURN
             
       state_holder = WIDGET_INFO(c_dv_gui_param.gui_id.minmax, /CHILD)
       WIDGET_CONTROL, state_holder, GET_UVALUE=state


       ; Plot the palette
       ; ---------------------------------------------------------------
       WIDGET_CONTROL, state.id_struct.ui_draw_palette, GET_VALUE=palette_window_index

       WINDOWSET, palette_window_index
       TVSCL, state.image_palette
       WINDOWSET, /RESTORE


     END ; PRO DV_MINMAX_REPAINT


; ======================================================================
; ======================================================================
; #################################################################
;
; BUILD THE GUI
;
; #################################################################
; ======================================================================
; ======================================================================


     ;+=================================================================
     ;
     ; Build the main gui for DECTECTOR_VIEWER.
     ;
     ;-=================================================================
     FUNCTION DV_BUILD_GUI, version, POINTER_ARRAY=pointer_array

       COMMON DV_GUI
       COMMON DV_IMAGE


       ; SETUP VARIOUS POINTERS
       PTR_image_palette = PTR_new(/allocate_heap)
       PTR_tvy_data = PTR_new(/allocate_heap)
       PTR_text_status = PTR_new(strarr(3))

       ; SETUP A POINTER ARRAY TO AID IN CLEANUP
       pointer_array = [PTR_tvy_data, PTR_image_palette, PTR_text_status]


       ; --------------------------------------------------------------
       ; Setup the status and menu window.
       title = 'Detector Viewer v'+version
       ui_base = WIDGET_BASE(/column, xoffset=10, yoffset=10, $
                             title=title, /tlb_kill_request_events, mbar=ui_menu_bar)

       ui_menu_file = WIDGET_BUTTON(ui_menu_bar, value='File', /menu)
         ui_button_Done = WIDGET_BUTTON(ui_menu_file, VALUE='Exit', $
                                        uvalue={name:'ui_button_Done'}, xsize = 150)

       ui_menu_load = WIDGET_BUTTON(ui_menu_bar, value='Load', /menu)
         ui_button_LoadFile = WIDGET_BUTTON(ui_menu_load, VALUE='Load Image', $
                                            uvalue={name:'ui_button_LoadFile' $
                                                    ,file_full_path:'' $
                                                    ,file_name:'' $
                                                    ,filepath:''})
         ui_button_LoadBack = WIDGET_BUTTON(ui_menu_load, VALUE='Load Background', $
                                            uvalue={name:'ui_button_LoadBack' $
                                                    ,file_full_path:'' $
                                                    ,file_name:'' $
                                                    ,filepath:''})
         ui_button_LoadMask = WIDGET_BUTTON(ui_menu_load, VALUE='Load Mask', $
                                            uvalue={name:'ui_button_LoadMask' $
                                                    ,file_full_path:'' $
                                                    ,file_name:'' $
                                                    ,filepath:''})
         ui_button_reload = WIDGET_BUTTON(ui_menu_load, VALUE='Reload Images' $
                                          ,uvalue={name:'ui_button_reload'} $
                                          ,/SEPARATOR)
         ui_button_clear = WIDGET_BUTTON(ui_menu_load, VALUE='Clear Images' $
                                          ,uvalue={name:'ui_button_clear'})



       ui_menu_save = WIDGET_BUTTON(ui_menu_bar, value='Save', /menu)
         ui_menu_save_jpeg = WIDGET_BUTTON(ui_menu_save, value='Save Image', /menu)
         ui_menu_save_image_color = WIDGET_BUTTON(ui_menu_save_jpeg, value='Color', $
                                                  uvalue={name:'ui_menu_save_image_color'})
         ui_menu_save_image_bw = WIDGET_BUTTON(ui_menu_save_jpeg, value='B/W', $
                                               uvalue={name:'ui_menu_save_image_bw'})
         ui_menu_save_zoom_color = WIDGET_BUTTON(ui_menu_save_jpeg, value='Zoom - Color', $
                                                 uvalue={name:'ui_menu_save_zoom_color'}, /separator)
         ui_menu_save_zoom_bw = WIDGET_BUTTON(ui_menu_save_jpeg, value='Zoom - B/W', $
                                              uvalue={name:'ui_menu_save_zoom_bw'})
         ui_menu_save_zoom_contour = WIDGET_BUTTON(ui_menu_save_jpeg, value='Zoom - Contour', $
                                                   uvalue={name:'ui_menu_save_zoom_contour'})
         ui_menu_save_image_bin = WIDGET_BUTTON(ui_menu_save, value='Save Image As Binary', $
                                                uvalue={name:'ui_menu_save_image_bin'})
         ui_menu_save_ped_bin = WIDGET_BUTTON(ui_menu_save, value='Save Pedastal As Binary', $
                                              uvalue={name:'ui_menu_save_ped_bin'})

       ui_menu_controls = WIDGET_BUTTON(ui_menu_bar, VALUE='Controls', /MENU)
         ui_menu_controls_minmax = WIDGET_BUTTON(ui_menu_controls $
                                                 ,UNAME=' ui_menu_controls_minmax' $
                                                 ,VALUE='Adjust Cutoffs' $
                                                 ,UVALUE={NAME:'ui_menu_controls_minmax'})

       ui_menu_window = WIDGET_BUTTON(ui_menu_bar, VALUE='Window', /MENU)
         ui_menu_window_show_main = WIDGET_BUTTON(ui_menu_window, VALUE='Show Main Window' $
                                                 ,UVALUE={NAME:'ui_menu_window_show_main'})
         ui_menu_window_repaint = WIDGET_BUTTON(ui_menu_window, VALUE='Repaint Windows' $
                                                 ,UVALUE={NAME:'ui_menu_window_repaint'})

       ui_base_bottom =  WIDGET_BASE(ui_base, /col)
         ui_text_status = WIDGET_LIST(ui_base_bottom, VALUE='', $
                                      scr_xsize=610, ysize=4 $
                                      , uvalue={name:'ui_text_status' $
                                                ,PTR_text_status:PTR_text_status $
                                                ,ysize:4})


       ; --------------------------------------------------------------
       ; Set up the control window
       ;ui_base_control = WIDGET_BASE(/row, xoffset=10, yoffset=150, $
         ;                              /tlb_kill_request_events)
       ui_base_control = WIDGET_BASE(ui_base, /row)
       ui_base_control_left = WIDGET_BASE(ui_base_control, /column)
       ui_base_control_right = WIDGET_BASE(ui_base_control, /column)
       ui_menu_button = WIDGET_BUTTON(ui_base_control_left, UNAME='ui_menu_button' $
                                      ,VALUE='Load Palette' $
                                      ,UVALUE='ui_menu' $
                                      ,/MENU)
       drop_menu = replicate({flags:0, name:''}, 43)
       drop_menu[0].flags = 1
       drop_menu[20].flags = 2
       drop_menu.name = [ 'More Palettes', $
                          '21-   Hue Sat Value 1', $
                          '22-    Hue Sat Value ', $
                          '23- Purple-Red + Stri', $
                          '24-             Beach', $
                          '25-          Mac Styl', $
                          '26-             Eos A', $
                          '27-             Eos B', $
                          '28-         Hardcandy', $
                          '29-            Nature', $
                          '30-             Ocean', $
                          '31-        Peppermint', $
                          '32-            Plasma', $
                          '33-          Blue-Red', $
                          '34-           Rainbow', $
                          '35-        Blue Waves', $
                          '36-           Volcano', $
                          '37-             Waves', $
                          '38-         Rainbow18', $
                          '39-   Rainbow + white', $
                          '40-   Rainbow + black', $

       'Palette Dialog', $
         '0-         B-W LINEAR', $
         '1-         BLUE/WHITE', $
         '2-    GRN-RED-BLU-WHT', $
         '3-    RED TEMPERATURE', $
         '4-  BLUE/GREEN/RED/YE', $
         '5-       STD GAMMA-II', $
         '6-              PRISM', $
         '7-         RED-PURPLE', $
         '8-  GREEN/WHITE LINEA', $
         '9-  GRN/WHT EXPONENTI', $
         '10-         GREEN-PIN', $
         '11-          BLUE-RED', $
         '12-          16 LEVEL', $
         '13-           RAINBOW', $
         '14-              STEP', $
         '15-     STERN SPECIAL', $
         '16-              Haze', $
         '17- Blue - Pastel - R', $
         '18-           Pastels', $
         '19- Hue Sat Lightness', $
         '20-  Hue Sat Lightnes']

       ct = indgen(43)
       ct[0:20] = ct[20:40]
       ct[21] = 0
       ct[22:42] = indgen(21)

       ui_menu = CW_PDMENU(ui_menu_button, drop_menu, $
                           uvalue={name:'ui_menu_1', ct:ct, text:drop_menu.name}, /mbar)
       ui_base_control_button = WIDGET_BASE(ui_base_control_left, /COL, /NONEXCLUSIVE)

       ui_button_background = WIDGET_BUTTON(ui_base_control_button, VALUE='Background' $
                                            ,uvalue={name:'ui_button_background'} $
                                            ,SENSITIVE=0)
       ui_button_subtract = WIDGET_BUTTON(ui_base_control_button, VALUE='Subtract' $
                                          ,uvalue={name:'ui_button_subtract'} $
                                          ,SENSITIVE=0)
       ui_button_mask = WIDGET_BUTTON(ui_base_control_button, VALUE='Mask' $
                                            ,uvalue={name:'ui_button_mask'} $
                                      ,SENSITIVE=0)
       ui_button_invertmask = WIDGET_BUTTON(ui_base_control_button, VALUE='Invert Mask' $
                                            ,uvalue={name:'ui_button_invertmask'} $
                                            ,SENSITIVE=0)
       WIDGET_CONTROL, ui_button_background, SET_BUTTON=1
       WIDGET_CONTROL, ui_button_subtract, SET_BUTTON=0
       WIDGET_CONTROL, ui_button_mask, SET_BUTTON=1
       WIDGET_CONTROL, ui_button_invertmask, SET_BUTTON=0

       ui_base_control_zoom = WIDGET_BASE(ui_base_control_left, /row, /nonexclusive)
       ui_button_zoom = WIDGET_BUTTON(ui_base_control_zoom, VALUE='Zoom', $
                                      uvalue={name:'ui_button_zoom', select:0})
       ui_button_zoom_contour = WIDGET_BUTTON(ui_base_control_zoom, VALUE='Contour', $
                                              sensitive = 0, $
                                              uvalue={name:'ui_button_zoom_contour', select:0})
       ui_base_control_slider_zoom = WIDGET_BASE(ui_base_control_left, /col, map=0)
       ui_slider_zoom = WIDGET_SLIDER(ui_base_control_slider_zoom, maximum=20, minimum=1, scroll=1, $
                                      uvalue={name:'ui_slider_zoom'}, value=10, xsize=170)
       ui_base_draw_pos = WIDGET_BASE(ui_base_control_left, /col)
       ui_position_label = WIDGET_LABEL(ui_base_draw_pos, value='.', xsize=170, /align_left)
       ui_value_label = WIDGET_LABEL(ui_base_draw_pos, value='.', xsize=170, /align_left)

       ui_base_palette_display = WIDGET_BASE(ui_base_control_right, /col, /base_align_center)
       palette_xsize = 30 & palette_ysize = 256
       ui_palette_label_top = WIDGET_LABEL(ui_base_palette_display, value='255', $
                                           yoffset=0, /align_center, xsize=40, $
                                           uvalue={name:'ui_palette_label_top', value:255})
       
       ui_draw_palette = WIDGET_DRAW(ui_base_palette_display, xsize=palette_xsize, ysize=palette_ysize, $
                                     uvalue={name:'ui_draw_palette' $
                                             ,xsize:palette_xsize $
                                             ,ysize:palette_ysize, $
                                             PTR_image_palette:PTR_image_palette, init:0})
       ui_palette_label_bottom = WIDGET_LABEL(ui_base_palette_display, value='0', $
                                              yoffset=palette_ysize-15, /align_center, xsize=40, $
                                              uvalue={name:'ui_palette_label_bottom', value:0})

       ; --------------------------------------------------------------
       ; Set up main image window
       ui_base_draw = WIDGET_BASE(/column, xoffset=270, yoffset=150, $
                                  /TLB_SIZE_EVENT, /tlb_kill_request_events, map=0)

       ;CHOOSE INITIAL IMAGE SIZE
       xsize=1024 & ysize=1024

       ;WHEN CALLING UI_DRAW INITIALIZE X_SCROLL_SIZE TO 1
       ui_draw = WIDGET_DRAW(ui_base_draw, xsize=xsize, ysize=ysize, $
                             /motion_events, /button_events, uvalue={name:'ui_draw', zoom_frozen:0}, $
                             x_scroll_size=1, /scroll)


       ; --------------------------------------------------------------
       ; Set up 1D image window
       image_1D_data.xsize = 500
       image_1D_data.ysize = 420
       ui_base_draw_1D = WIDGET_BASE(xoffset=10, yoffset=340, $
                                     /TLB_SIZE_EVENT, /tlb_kill_request_events, map=0)
       ui_draw_1D = WIDGET_DRAW(ui_base_draw_1D, xsize=image_1D_data.xsize, ysize=image_1D_data.ysize, $
                                /motion_events, /button_events, retain=2, $
                                uvalue={name:'ui_draw_1D' $
                                        ,tv:0 $
                                        ,tvy_x:0 $
                                        ,PTR_tvy_data:PTR_tvy_data $
                                        ,zoom:[0,0] $
                                        ,line_thick:1})


       ; --------------------------------------------------------------
       ; Set up zoom window
       ui_base_zoom = WIDGET_BASE(/column, map=0, xoffset=10, yoffset=340, /tlb_kill_request_events)
       xsize=500
       ysize=500
       ui_zoom = WIDGET_DRAW(ui_base_zoom, /motion_events, /button_events $
                             ,xsize=xsize, ysize=ysize $
                             ,uvalue={name:'ui_zoom' $
                                      ,xsize:xsize, ysize:ysize $
                                      ,zoom_scale:4, zoom_pos:[0,0] $
                                      ,old_x:0, old_y:0, contour:0} $
                            )

       image_bytscl_zoom = BYTARR(xsize,ysize)


       ; --------------------------------------------------------------
       ; Save ids and initialize widgets
       id_struct = { $
                     ui_base:ui_base $
                     ,ui_button_LoadFile:ui_button_LoadFile $
                     ,ui_button_LoadBack:ui_button_LoadBack $
                     ,ui_button_LoadMask:ui_button_LoadMask $
                     ,ui_button_reload:ui_button_reload $
                     ,ui_button_clear:ui_button_clear $
                     ,ui_text_status:ui_text_status $
                     ,ui_base_control:ui_base_control $
                     ,ui_base_draw:ui_base_draw $
                     ,ui_draw:ui_draw $
                     ,ui_base_draw_1D:ui_base_draw_1D $
                     ,ui_draw_1D:ui_draw_1D $
                     ,ui_menu:ui_menu $
                     ,ui_position_label:ui_position_label $
                     ,ui_value_label:ui_value_label $
                     ,ui_button_subtract:ui_button_subtract $
                     ,ui_button_background:ui_button_background $
                     ,ui_button_mask:ui_button_mask $
                     ,ui_button_invertmask:ui_button_invertmask $
                     ,ui_button_zoom:ui_button_zoom $
                     ,ui_button_zoom_contour:ui_button_zoom_contour $
                     ,ui_base_control_slider_zoom:ui_base_control_slider_zoom $
                     ,ui_slider_zoom:ui_slider_zoom $
                     ,ui_base_zoom:ui_base_zoom $
                     ,ui_zoom:ui_zoom $
                     ,ui_palette_label_top:ui_palette_label_top $
                     ,ui_palette_label_bottom:ui_palette_label_bottom $
                     ,ui_draw_palette:ui_draw_palette $
                     ,ui_menu_controls_minmax:ui_menu_controls_minmax $
                   }

       uval = {id_struct:id_struct}

       WIDGET_CONTROL, ui_base, SET_UVALUE=uval
       WIDGET_CONTROL, ui_base_control, SET_UVALUE=uval
       WIDGET_CONTROL, ui_base_draw, SET_UVALUE=uval
       WIDGET_CONTROL, ui_base_draw_1D, SET_UVALUE=uval
       WIDGET_CONTROL, ui_base_zoom, SET_UVALUE=uval


       WIDGET_CONTROL, ui_base, /realize
       WIDGET_CONTROL, ui_base_control, /realize
       WIDGET_CONTROL, ui_base_draw, /realize
       WIDGET_CONTROL, ui_base_draw_1D, /realize
       WIDGET_CONTROL, ui_base_zoom, /realize

       XMANAGER, 'dv_ui', ui_base, /no_block
       XMANAGER, 'dv_ui', ui_base_control, /no_block
       XMANAGER, 'dv_ui_draw', ui_base_draw, /no_block
       XMANAGER, 'dv_ui_draw_1D', ui_base_draw_1D, /no_block
       XMANAGER, 'dv_ui_draw_zoom', ui_base_zoom, /no_block

       RETURN, id_struct


     END ; FUNCTION DV_BUILD_GUI




; =================================================================
; =================================================================
; #################################################################
;
; BEGIN MAIN ROUTINE
;
; #################################################################
; =================================================================
; =================================================================


     ;+=================================================================
     ;
     ; Because of the use of common variables, multiple instances of
     ; DETECTOR_VIEWER can not be run from a single IDL session.
     ;-=================================================================
     PRO DETECTOR_VIEWER, image_input, DEBUG=debug, _EXTRA=extra
       COMMON DV_GUI
       COMMON DV_IMAGE

       ; ---------------------------
       version = DV_VERSION()
       ; ---------------------------

       ; First check to see if the inst_dv is already open.
       ; If so do not allow a new instance. 
       IF N_ELEMENTS(c_dv_gui_param) NE 0 THEN BEGIN
         IF c_dv_gui_param.active THEN BEGIN
           IF WIDGET_INFO(c_dv_gui_param.id_struct.ui_base, /VALID_ID) THEN BEGIN

             ; Bring the window to the front
             WIDGET_CONTROL, c_dv_gui_param.id_struct.ui_base, /SHOW

             ; If an initial image was given, then load it.
             IF N_ELEMENTS(image_input) NE 0 THEN BEGIN
               DV_SET_IMAGE, image_input
             ENDIF

             RETURN
           ENDIF
         ENDIF
       ENDIF


       ; SETUP SOME COLOR OPTIONS
       DEVICE, DECOMPOSED=0, RETAIN=2
       !P.BACKGROUND = 255
       !P.COLOR = 0

       ;IMAGE HAS NOT BEEN INITIALIZED
       image_1D_data = {title:'' $
                        ,x:0 $
                        ,y:0 $
                        ,xsize:0 $
                        ,ysize:0 $
                        ,position_offset:[50,30,30,30] $
                        ,zoom:[0,0] $
                        ,zoom_out:[0,0] $
                        ,row_col:'' $
                        ,scale:0 $
                        ,init:0}

       image_data = {filename:'' $
                     ,init:0 $
                     ,size:[0,0]}

       image_back_data = {filename:'' $
                          ,init:0 $
                          ,size:[0,0]}
       
       image_mask_data = {filename:'' $
                          ,init:0 $
                          ,size:[0,0]}
       
       image_final_data = {init:0 $
                           ,title:'' $
                           ,size:[0,0] $
                           ,back_subtraction:1 $
                           ,image_subtraction:0 $
                           ,use_mask:1 $
                           ,invert_mask:0 $
                           ,range:[0D,0D] $
                          }


       ;Create the gui, save the current window focus.
       WINDOWSET, /SAVE
       id_struct = DV_BUILD_GUI(version, POINTER_ARRAY=pointer_array)
       WINDOWSET, /RESTORE

       ; Set the gui parameter common structure
       DV_SET_GUI_PARAM, ID_STRUCT=id_struct, POINTER_ARRAY=pointer_array


       PLOT_PALETTE


       ; If an initial image was given, then load it.
       IF N_ELEMENTS(image_input) NE 0 THEN BEGIN
         DV_SET_IMAGE, image_input
       ENDIF

     END ;PRO DETECTOR_VIEWER
