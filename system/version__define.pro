
;+====================================================================== 
; 
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;   amicitas@gmail.com
;
; DATE:
;   2010-01
;
; PURPOSE:
;   Define a version structure.
;
; DESCRIPTION:
;   The version structure contains two properties:
;  
;   number
;       [0L, 0L, 0L]
;
;   date
;       [0L, 0L, 0L]
;       [YEAR, MONTH, DAY]
;
;
;-======================================================================
     PRO VERSION__DEFINE
       
       version = {VERSION $
                  ,number:[0L, 0L, 0L] $
                  ,date:[0L, 0L, 0L] $
                 }

     END ;PRO VERSION__DEFINE
