

;+==============================================================================
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
; PURPOSE:
;   Quickly report on IDL memory usage.
;
;-==============================================================================


     PRO MIR_MEMORY_REPORT
       mem = MEMORY(/STRUCTURE, /L64)
       PRINT, mem.current/(2L^20), mem.highwater/(2L^20) $
              ,FORMAT='("Current Memory usage: ",i0,"MB",5x, "Maximum memory usage: ",i0,"MB")'
     END ; PRO MIR_MEMORY_REPORT
