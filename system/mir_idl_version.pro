
;+====================================================================== 
; 
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
; DATE:
;   2010-10
;
; PURPOSE:
;   Return the IDL version in a way that makes comparisons possible.
;
; DESCRIPTION:
;   The version structure contains two properties:
;  
;   number
;       [0L, 0L, 0L]
;
;   date
;       [0L, 0L, 0L]
;       [YEAR, MONTH, DAY]
;
;
;-======================================================================
     FUNCTION MIR_IDL_VERSION
       

       version = {VERSION $
                  ,number:[0L, 0L, 0L] $
                  ,date:[0L, 0L, 0L] $
                 }

       release_parts = STRSPLIT(!version.release, '.', /EXTRACT)

       version.number[0:1] = LONG(release_parts[0:1])

       
       ; I eventually need to parse the build_date string as well.

       RETURN, version

     END ;FUNCTION MIR_IDL_VERSION
