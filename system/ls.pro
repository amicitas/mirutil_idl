

;+==============================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
; DATE:
;   2010-09
;
; PURPOSE:
;   Display files in the given directory.
;
;   This is here mostly because I keep trying to do this.
;
;-==============================================================================


     PRO LS, input

       MIR_DEFAULT, input, ''
       SPAWN, 'ls '+input

     END ; PRO LS
