
;+====================================================================== 
; 
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
; DATE:
;   2010-10
;
; PURPOSE:
;   Check if the idl version is greater that the given number.
;
;
;-======================================================================
     FUNCTION MIR_CHECK_IDL_VERSION, number, DATE=date
       
       IF ISA(date) THEN BEGIN
         MESSAGE, 'DATE keyword not yet implemented.'
       ENDIF


       idl_version = MIR_IDL_VERSION()

       num = N_ELEMENTS(number)
       FOR ii=0,num-1 DO BEGIN
         CASE 1 OF
           idl_version.number[ii] GT number[ii]: RETURN, 1
           idl_version.number[ii] LT number[ii]: RETURN, 0           
           ELSE:
         ENDCASE
       ENDFOR

       RETURN, 1

     END ;FUNCTION MIR_IDL_VERSION
