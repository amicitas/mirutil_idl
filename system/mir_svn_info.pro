

;+==============================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
; DATE:
;   2012-03
;
; PURPOSE:
;   Parse info returned from an SVN server.
;
;-==============================================================================




     FUNCTION MIR_SVN_INFO, svn_address

       command = "svn info "+svn_address
       
       MIR_LOGGER, /INFO, 'Getting subversion repository info from: '+ svn_address

       SPAWN, command, result, errresult

       output_hash = MIR_ORDERED_HASH()

       FOR ii=0,N_ELEMENTS(result)-1 DO BEGIN
         s = STRSPLIT(result[ii], ': ', /EXTRACT, /REGEX)
         IF N_ELEMENTS(s) EQ 2 THEN BEGIN
           output_hash[s[0]] = s[1]
         ENDIF
       ENDFOR

       RETURN, output_hash.tostruct()
     END ; FUNCTION MIR_SVN_INFO
