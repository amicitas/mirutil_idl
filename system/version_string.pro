

;+====================================================================== 
; 
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-08
;
; PURPOSE:
;   Return a string give one of the following:
;     A version structure.
;     A array containing the version number.
;     A array containing the version date.
;
;
;-======================================================================


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Return a string give one of the following:
     ;     A version structure.
     ;     A array containing the version number.
     ;     A array containing the version date.
     ;
     ; INPUTS:
     ;   version_struct (optional)
     ;       Should be of the form {number:[0,0,0], date:[YYYY, MM, DD]}
     ;
     ;       If not given then either the NUMBER and DATE keywords
     ;       should be used.
     ;
     ;-=================================================================
     FUNCTION VERSION_STRING, version_struct $
                              ,NUMBER=version_num_in $
                              ,DATE=version_date_in $
                              
                              ,GET_NUMBER_STRING=get_number_string $
                              ,GET_DATE_STRING=get_date_string
       
       IF N_PARAMS() EQ 1 THEN BEGIN
         version_num = version_struct.number
         version_date = version_struct.date
       ENDIF ELSE BEGIN
         
         IF ISA(version_num_in) THEN BEGIN
           version_num = version_num_in
         ENDIF
         IF ISA(version_date_in) THEN BEGIN
           version_date = version_date_in
         ENDIF
         
       ENDELSE
       
       
       IF ( ~ KEYWORD_SET(get_number_string)) AND ( ~ KEYWORD_SET(get_date_string)) THEN BEGIN
         get_number_string = 1
         get_date_string = 1
       ENDIF
       
       
       string = ''
       
       IF KEYWORD_SET(get_number_string) AND ISA(version_num) THEN BEGIN
         fmt = '(i0,".",i0,".",i0)'
         string += STRING(version_num, FORMAT=fmt)
       ENDIF
       
       IF KEYWORD_SET(get_date_string) AND ISA(version_date) THEN BEGIN

         IF string NE '' THEN BEGIN
           string += ' - '
         ENDIF

         fmt = '(i04,"-",i02,"-",i02)'
         string += STRING(version_date, FORMAT=fmt)
       ENDIF
       
       RETURN, string
     END ;FUNCTION VERSION_STRING
