
; ======================================================================
; 2009-04 - Novimir Antoniuk Pablant
; ======================================================================
; GET_UNIQUE.PRO
;
; 
; ======================================================================

     ; =================================================================
     ; =================================================================
     ; This will return the unique elements of the given array.
     ; The elements can be either returned sorted or unsorted.
     ;
     ; If unsorted then the first element found will be returned.
     ; =================================================================
     FUNCTION GET_UNIQUE, array, NOSORT=nosort

       IF KEYWORD_SET(nosort) THEN BEGIN
         idx_uniq = UNIQ(array, REVERSE(SORT(array)))
         idx_unsort = idx_uniq(SORT(idx_uniq))
         RETURN, array[idx_unsort]
       ENDIF ELSE BEGIN
         idx_uniq = UNIQ(array, SORT(array))
         RETURN, array[idx_uniq]
       ENDELSE


     END ;FUNCTION GET_UNIQUE
