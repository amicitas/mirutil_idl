
;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-07
;
; PURPOSE:
;   Return the last element of an array.
;
; TAGS:
;   EXCLUDE_FILE_HEADER
;-======================================================================



     ;+=================================================================
     ;
     ; This function will return the lass element of a given array.
     ;
     ;-=================================================================
     FUNCTION LAST, array
       RETURN, array[N_ELEMENTS(array)-1]
     END ;FUNCTION LAST
