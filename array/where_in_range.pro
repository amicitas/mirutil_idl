

;+====================================================================== 
; 
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-06
;
; PURPOSE:
;   Return the elements of an array that are within a given range.
; 
;
;-====================================================================== 



     ;+=================================================================
     ;
     ;   Return the elements of an array that are within a given range.
     ;
     ;   Also return the percentage of elements within the range, 
     ;   if desired.
     ;  
     ;-=================================================================
     FUNCTION WHERE_IN_RANGE, array, range, count $
                              ,PERCENT=percent $
                              ,COMPLEMENT=complement $
                              ,NCOMPLEMENT=ncomplement $
                              ,PCOMPLEMENT=pcomplement

       w = WHERE(array LE range[1] AND array GE range[0], count $
                 ,COMPLEMENT=complement $
                 ,NCOMPLEMENT=ncomplement)

       IF ARG_PRESENT(percent) THEN BEGIN
         percent = FLOAT(count)/N_ELEMENTS(array)
       ENDIF

       IF ARG_PRESENT(pcomplement) THEN BEGIN
         pcomplement = FLOAT(ncomplement)/N_ELEMENTS(array)
       ENDIF

       RETURN, w

     END ; FUNCTION WHERE_IN_RANGE
