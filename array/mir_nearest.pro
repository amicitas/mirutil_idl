
;+==============================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
;   antoniuk@fusion.gat.com
;   novimir.pablant@amicitas.com
;
; DATE:
;   2010-07
;
; PURPOSE:
;   Find the nearest number in an array.
;
;
;
;-==============================================================================



     FUNCTION MIR_NEAREST, array, values_in, index_out, GREATER=greater, LESS=less


       IF SIZE(values_in, /N_DIM) EQ 0 THEN BEGIN
         values = [values_in]
         is_scalar = 1
       ENDIF ELSE BEGIN
         values = values_in
         is_scalor = 0
       ENDELSE

       num_values = N_ELEMENTS(values)

       output = MAKE_ARRAY(num_values, TYPE=SIZE(array, /TYPE))
       index_out = LONARR(num_values)

       FOR ii=0,num_values-1 DO BEGIN
         diff = values[ii] - array

         CASE 1 OF
           KEYWORD_SET(greater): BEGIN
             w = WHERE(diff LE 0, count)
             IF count EQ 0 THEN MESSAGE, 'No values are greater.'
             _null = MIN(diff[w], index, /ABS)
             index_out[ii] = w[index]
           END
           KEYWORD_SET(less): BEGIN
             w = WHERE(diff GE 0, count)
             IF count EQ 0 THEN MESSAGE, 'No values are less.'
             _null = MIN(diff[w], index)
             index_out[ii] = w[index]
           END
           ELSE: BEGIN
             _null = MIN(diff, index, /ABS)
             index_out[ii] = index
           END
         ENDCASE
         output[ii] = array[index_out[ii]]
       ENDFOR

       IF is_scalar THEN BEGIN
         output = output[0]
         index_out = index_out[0]
       ENDIF
       
       RETURN, output

     END ; FUNCTION MIR_NEAREST
