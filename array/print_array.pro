
;+====================================================================== 
; 
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-05
;
; PURPOSE:
;   Nicely print arrays.
;
; TAGS:
;   EXCLUDE_FILE_HEADER
;-======================================================================


     ;+=================================================================
     ;
     ; Nicely print arrays out.
     ;
     ; NOTE: this will only work for arrays with 8 or less dimensions.
     ; 
     ; NOTE: At this point this only works for 1 dimentional arrays
     ;  
     ;-=================================================================
     PRO PRINT_ARRAY, array, INDEX=index, FORMAT=format

       array_dim = SIZE(array, /DIMENSION)

       num_dim = N_ELEMENTS(array_dim)
       IF num_dim NE 1 THEN BEGIN
         MESSAGE, 'More that 1 dimension not supported.'
       ENDIF
       
       FOR ii=0,array_dim[0]-1 DO BEGIN
         IF KEYWORD_SET(index) THEN BEGIN
           PRINT, ii, FORMAT='(i4, "  ", $)'
         ENDIF
         PRINT, array[ii,*,*,*,*,*,*,*], FORMAT=format
       ENDFOR

     END ;PRO PRINT_ARRAY

