

;+==============================================================================
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
; PURPOSE:
;   Perform an array shift with subpixel shifts.  When doing this an assumption
;   is implicitly made that the data within a pixel (such an intensity) is 
;   evenly distributed over the pixel. 
;
;   This procedure cannot be reversed without data loss as this implicit 
;   assumpiton is by definition wrong after a subpixel shift has been 
;   performed.
;
;   This will only work for 1d arrays.
;
;
;
;-=============================================================================


     FUNCTION MIR_SHIFT_SPECTRUM, spectrum_in, shift
       COMPILE_OPT STRICTARR

       shift_floor = FLOOR(shift)

       percent_high = shift - shift_floor
       percent_low = 1 - percent_high

       spectrum = SHIFT(spectrum_in, shift_floor)*percent_low
       spectrum += SHIFT(spectrum_in, shift_floor+1)*percent_high

       RETURN, spectrum

     END ; FUNCTION MIR_SHIFT_SPECTRUM
