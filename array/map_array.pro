

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
; DATE:
;   2009-09
;
; VERSION:
;   0.0.0
;
; PURPOSE:
;   Create a map between two arrays.
;
; TAGS:
;   Exclude file header
;
;-======================================================================



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Create a map between two arrays. 
     ;   This will return an array of indexes that point to where
     ;   a given element in the first array can be found in the
     ;   second array.
     ;
     ;   This will only work if the second array contains only
     ;   unique elements.
     ;
     ;
     ;-=================================================================
     FUNCTION MAP_ARRAY, array_1, array_2

       num_1 = N_ELEMENTS(array_1)
       num_2 = N_ELEMENTS(array_2)

       array_out = INTARR(num_1)

       FOR ii=0,num_1-1 DO BEGIN
         w = WHERE(array_2 EQ array_1[ii], count)
         IF count EQ 0 THEN BEGIN
           MESSAGE, 'Element in array_1 not found in array_2.'
         ENDIF
         array_out[ii] = w[0]
       ENDFOR
      
       RETURN, array_out
     END ; FUNCTION MAP_ARRAY
