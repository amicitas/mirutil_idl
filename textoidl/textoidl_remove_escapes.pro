
;
;+
; NAME:
;       TEXTOIDL_REMOVE_ESCAPES
;
; PURPOSE:
;       Remove the escape character '\' from the given string.
;
; DESCRIPTION:
;       Will remove any escapes, '\', from the input string
;       unless the escape is escaped, '\\'.
;
;       If the escape character is found at the end of a string
;       it will not be removed.
;
; CATEGORY:
;       text/strings
;
; CALLING SEQUENCE:
;       new = texttoidl_remove_escapes( old )
;
; INPUTS:
;       old       -- string from which to remove the escapes.
;
; OUTPUTS:
;       new       -- string without the escapes.
;
; EXAMPLE:
;       out = TEXTOIDL_REMOVE_ESCAPES( 'some\_stuff \\ more stuff' )
;       Then out='some_stuff \ more stuff'
;
; AUTHOR:
;       Novimir Antoniuk Pablant
;       novimir.pablant@amicitas.com
;
; COPYRIGHT:
;       This software has been released into the public domain by the author.
;
;-


     FUNCTION TEXTOIDL_REMOVE_ESCAPES, string_in

       IF string_in EQ '' THEN RETURN, string_in

       string_array = STRSPLIT(string_in, '\', /EXTRACT, /PRESERVE_NULL)
       w = WHERE(string_array EQ '', count)
       IF COUNT GT 0 THEN BEGIN
         string_array[w] = '\'
       ENDIF

       RETURN, STRJOIN(string_array, '')

     END ;FUNCTION TEXTOIDL_REMOVE_ESCAPES
