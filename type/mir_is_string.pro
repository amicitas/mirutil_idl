

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
;   antoniuk@fusion.gat.com
;   amicitas@gmail.com
;
; DATE:
;   2009-11
;
; PURPOSE:
;   Check if the given value is of string type.
;
;
;-======================================================================


     FUNCTION MIR_IS_STRING, value

       RETURN, (SIZE(value, /TYPE) EQ 7)

     END ;FUNCTION MIR_IS_STRING()
