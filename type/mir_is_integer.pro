

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
;   antoniuk@fusion.gat.com
;   amicitas@gmail.com
;
; DATE:
;   2009-11
;
; PURPOSE:
;   Check if the given value is of string integer.
;
;
;-======================================================================


     FUNCTION MIR_IS_INTEGER, value, SHORT=shot, LONG=long

       type = SIZE(value, /TYPE)
       
       RETURN, ((type EQ 2) OR (type EQ 3))

     END ;FUNCTION MIR_IS_INTEGER
