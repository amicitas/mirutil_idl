; Copyright (c) 1998, Forschungszentrum Juelich GmbH ICG-3
; All rights reserved.
; Unauthorized reproduction prohibited.
;
;+
; NAME:
;	icg_is_number
;
; PURPOSE:
;	Test if variable represents a decimal number even if passed in as a string.
;
; CATEGORY:
;	PROG_TOOLS
;
; CALLING SEQUENCE:
;	Result = icg_is_number(var)
;
; INPUTS:
;	var: The variable to be tested (scalar or array).
;
; KEYWORDS:
;	/EXPAND: If set and var is of type string the result is of the same dimension as var
;
; OUTPUTS:
;	This function returns 1 if var can be converted into a number else it returns 0.
;	Strings are filtered for number-typical characters ('+-.DE')
;	If var is a vector and the keyword /EXPAND is set the returned result is a vector as well
;
; EXAMPLE:
;	PRINT, icg_is_number('-12.3D+03')		--> 1
;	PRINT, icg_is_number('-12.3A+03')		--> 0
;	PRINT, icg_is_number(SINDGEN(10))	  --> 1
;	PRINT, icg_is_number(SINDGEN(10), /EXPAND)	  -->        1       1       1       1       1       1       1       1       1       1
;	PRINT, icg_is_number([SINDGEN(10), 'A'])	  --> 0
;	PRINT, icg_is_number([SINDGEN(10), 'A'], /EXPAND)	  -->        1       1       1       1       1       1       1       1       1       1       0
;
; MODIFICATION HISTORY:
; 	Written by:	Frank Holland, 27.05.98
;	22.03.1999:	Strings are handled separately
;	16.07.2003: var can now be a vector or array
;	27.04.2005:	Added keyword EXPAND
;
;       2012.06.20:     Completely rewritten by Novimir Pablant.
;-
     FUNCTION MIR_IS_NUMBER, var, EXPAND=expand

       num = N_ELEMENTS(var)
       is_num_vec = INTARR(num, /NOZERO)

       IF ICG_IS_STRING(var)THEN BEGIN
         FOR ii = 0L, num - 1 DO BEGIN
           is_num_vec[ii] = ICG_STRING_IS_NUMBER(var[ii])
         ENDFOR
       ENDIF ELSE BEGIN
           is_num_vec[*] = ISA(var, /NUMBER)
       ENDELSE

       IF num EQ 1 THEN BEGIN
         RETURN, is_num_vec[0]
       ENDIF

       IF KEYWORD_SET(expand) THEN BEGIN
         RETURN, is_num_vec
       ENDIF ELSE BEGIN
         RETURN, ARRAY_EQUAL(is_num_vec, 1)
       ENDELSE


     END ; FUNCTION mir_is_number
