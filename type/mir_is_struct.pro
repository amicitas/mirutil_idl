

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
;   antoniuk@fusion.gat.com
;   amicitas@gmail.com
;
; DATE:
;   2009-11
;
; PURPOSE:
;   Check if the given value is of structure type.
;
;
;-======================================================================


     FUNCTION MIR_IS_STRUCT, value

       RETURN, (SIZE(value, /TYPE) EQ 8)

     END ;FUNCTION MIR_IS_STRUCT()
