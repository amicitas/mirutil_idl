; Copyright (c) 2003, Forschungszentrum Juelich GmbH ICG-I
; All rights reserved.
; Unauthorized reproduction prohibited.
; This software may be used, copied, or redistributed as long as it is not
; sold and this copyright notice is reproduced on each copy made.  This
; routine is provided as is without any express or implied warranties
; whatsoever.
;
;+==============================================================================
; NAME:
;   MIR_VALUE_FROM_STRING
;
;
; PURPOSE: 
;    This function recognizes the data type from the submitted String and 
;    returns the value as an array or scalar of the appropriate data type.
;
;    The characteristics of the data type (number an length of 
;    dimensions, datatype number and name) can be returned in form of a structure.
;   
;   
; CALLING SEQUENCE:
;   value = MIR_VALUE_FROM_STRING(string[, info])
;
;
; INPUTS:
;   string: the string, where the value / array is defined
;
;
; RESTRICTIONS:
;   The string should not contain complex/dcomplex types or pointers 
;   on struktures
;
;
; EXAMPLE:
;   1)  Example for skalar values:
;		help, icg_get_data_type('1.3d'),/str
;		help, icg_get_data_type('1.3'),/str
;
;   2)  Example for arrays:
;		help, icg_get_data_type('[[1,2],[3,4]]'),/str
;
;   3)  Example for strings:
;		help, icg_get_data_type("[['aa','bb'],['cc','dd']]"),/str		
;   4)   Example for arrays with diffrent types
;		help, icg_get_data_type('[[1.e,2.d],[3.d,4e]]'),/str;
;
;   5)   Example for arrays with diffrent types and Keyword [/value]
;		help, icg_get_data_type('[[1.e,2.d],[3.d,4e]]',/VALUE),/str
;
;
; PROGRAMMING NOTES:
;   This routine is kinda of terrible and should be rewritten entirely.
;   It works now though for my purposes, so why fix what ain't broke.
;
;		
; MODIFICATION HISTORY:
;       Code modified from 'GET_DATA_TYPE' from the ICG library.
;       Written by:     M.Baumgardt (ICG-I), 2005-AUG-12
;-==============================================================================
     
     FUNCTION MIR_VALUE_FROM_STRING, data_in, info, STRICT=strict
       
       strict = KEYWORD_SET(strict)

       ; Copy the data
       data = STRTRIM(data_in, 2)

       ; Structure to return.
       info = {n_elements:0L $
               ,n_dimensions:0L}

       init_operators = ['b','l','ll','d','e','e+','e-','d+','d-']
       data_type=''

       ; This routine was written to expect arrays to always be surrounded by
       ; brackets (eg. [1,2,3]).  I want this to work for one dimentional arrays
       ; without the bracket requirement.  Rather than modify the code below,
       ; I'll just add brackets around the data, then check at the end if the
       ; value was a scalar.
       IF STRMID(data,0,1) NE '[' THEN BEGIN
         data = '['+data+']'
         is_possible_scalar = 1
       ENDIF ELSE BEGIN
         is_possible_scalar = 0
       ENDELSE
       
       ;------------------------------------------------------------------------
       ; count the number of dimensions
       string='['
       ii_char=0
       count_bracket=0
       WHILE ((string EQ '[') OR (string EQ ' ')) DO BEGIN
         string = STRMID(data, ii_char, 1)
         IF string EQ '[' THEN count_bracket++
         ii_char++
       ENDWHILE

       info.n_dimensions = count_bracket
       IF info.n_dimensions GT 8 THEN BEGIN
         MESSAGE, 'SYNTAX ERROR: The number of dimensions must be less than or equal to 8!'
       ENDIF

       ;count dimensions
       dimensions = LONARR(info.n_dimensions)
       info = CREATE_STRUCT(info, {dimensions:dimensions})

       help_array = STRSPLIT(data,',',/EXTRACT)
       FOR y=0,info.n_dimensions-2 DO BEGIN
         help_string = ''
         FOR x=0,y DO BEGIN
           help_string = '['+help_string
         ENDFOR
         FOR x=0,SIZE(help_array,/N_ELEMENTS)-1 DO BEGIN
           IF STRPOS(help_array[x], help_string) NE -1 THEN BEGIN
             info.dimensions[y+1] = info.dimensions[y+1]+1
           ENDIF
         ENDFOR
       ENDFOR


       help_string = ''
       FOR x=0,info.n_dimensions-1 DO BEGIN
         help_string = ']'+help_string
       ENDFOR

       IF STRPOS(data, help_string) GT 0 THEN BEGIN
         IF STRCOMPRESS(STRMID(data,STRPOS(data,help_string)+STRLEN(help_string),$
                               STRLEN(data)-STRPOS(data,help_string)-1),/REMOVE_ALL) NE '' THEN BEGIN
           MESSAGE, 'SYNTAX ERROR: H:'
           RETURN,-STRPOS(data,help_string)-1
         ENDIF
       ENDIF ELSE BEGIN
         MESSAGE, 'SYNTAX ERROR: I:'
         RETURN,-STRLEN(data)
       ENDELSE

       help_array=STRSPLIT(data,'[',/EXTRACT)
       help_array=STRSPLIT(help_array[0],',',/EXTRACT)
       info.dimensions[0]=SIZE(help_array,/N_ELEMENTS)
       FOR x=1,info.n_dimensions-2 DO BEGIN
         info.dimensions[x]=info.dimensions[x]/info.dimensions[x+1]
       ENDFOR
       help_array=STRSPLIT(data,',',/EXTRACT)
       ;count N_ELEMENTS
       info.n_elements=1
       FOR x=0 , info.n_dimensions-1 DO BEGIN
         info.n_elements=info.n_elements*info.dimensions[x]
       ENDFOR
     
       ;transform all values on a string vector without '[' , ']' ,  ','
       check_array=info.dimensions
       FOR x=0,size(help_array,/N_ELEMENTS)-1 DO BEGIN
         y=0
         WHILE y NE -1 DO BEGIN 
           y=STRPOS(help_array[x],'[')
           IF y NE -1 THEN $
              help_array[x]=STRMID(help_array[x],y+1,STRLEN(help_array[x])-1)
         ENDWHILE
         y=STRPOS(help_array[x],']')
         IF y NE -1 THEN $
            help_array[x]=STRMID(help_array[x],0,y)
       ENDFOR
     
     
       
       ;check types of vector elements
     
       
       type_matrix=make_array(info.n_elements,/LONG)
       type_matrix_name=make_array(info.n_elements,/STRING)
       is_in_quotes=make_array(info.n_elements,/INT)
       data_type='STRING'
       FOR x=0,size(help_array,/N_ELEMENTS)-1 DO BEGIN
         IF data_type EQ 'NUMBER' THEN data_type=''
         FOR y=0,size(init_operators,/N_ELEMENTS)-1 DO BEGIN 
           
           IF STRPOS(STRLOWCASE(help_array[x]),init_operators[y]) NE -1 THEN BEGIN
             ;delete init_operators
             help_data=STRMID(help_array[x],0,STRPOS(STRLOWCASE(help_array[x]),init_operators[y])) + $
                       STRMID(STRLOWCASE(help_array[x]),STRPOS(STRLOWCASE(help_array[x]),init_operators[y]) + 1, $
                              STRLEN(help_array[x])-STRPOS(STRLOWCASE(help_array[x]),init_operators[y]) - $
                              STRLEN(init_operators[y]))
            
             IF MIR_IS_NUMBER(help_data) EQ 1 THEN BEGIN
               IF STRPOS(STRLOWCASE(help_array[x]),init_operators[y]) EQ $
                  STRLEN(help_array[x])-STRLEN(init_operators[y])	THEN BEGIN	
                 data_type='NUMBER'
               ENDIF ELSE BEGIN
                 ;cause of conflict between'l' and 'll' or 'e+' and 'e'
                 IF (y LT 1) AND (y GT 4) THEN $ 
                    data_type='STRING'
               ENDELSE
             ENDIF ELSE BEGIN
               ;cause of conflict between'l' and 'll' or 'e+' and 'e'
               IF (y LT 1) AND (y GT 4) THEN $ 
                  data_type='STRING'
             ENDELSE
           ENDIF 
         ENDFOR
         
     
         IF (MIR_IS_NUMBER(help_array[x]) EQ 1) THEN $
            data_type='NUMBER'
         
         
         
         IF (STRPOS(help_array[x],'.') EQ -1) $
            AND (STRPOS(STRUPCASE(help_array[x]),'D') EQ -1) $
            AND (STRPOS(STRUPCASE(help_array[x]),'E') EQ -1) $
            AND (data_type NE 'STRING')  THEN BEGIN
           ; data is (UNSIGNED) Byte/INT/LINT 
           
           IF (STRPOS(STRUPCASE(help_array[x]),'B') NE -1) $
              AND (STRPOS(STRUPCASE(help_array[x]),'L') EQ -1) $
              AND (STRPOS(STRUPCASE(help_array[x]),'U') EQ -1) THEN BEGIN
             ;data is BYTE 
             type_matrix[x]=1
             type_matrix_name[x]='BYTE'
           ENDIF
     
           IF(FIX(help_array[x]) EQ LONG(help_array[x])) AND (STRPOS(STRUPCASE(help_array[x]),'L') EQ -1) AND $
              (STRPOS(STRUPCASE(help_array[x]),'B') EQ -1)   		THEN BEGIN ;data is (UNSIGNED) INT       
             IF(STRPOS(STRUPCASE(help_array[x]),'U') NE -1)                         THEN BEGIN  ;data is UNSIGNED-INT
               type_matrix[x]=2
               type_matrix_name[x]='UINT'
             ENDIF ELSE BEGIN			
               type_matrix[x]=2
               type_matrix_name[x]='INT'
             ENDELSE
           ENDIF
     
           IF ((FIX(help_array[x]) NE LONG(help_array[x])) $
               OR (STRPOS(STRUPCASE(help_array[x]),'L') NE -1)) $
              AND (STRPOS(STRUPCASE(help_array[x]),'B') EQ -1) $
              AND (LONG64(help_array[x]) EQ LONG(help_array[x])) $
              AND (STRPOS(STRUPCASE(help_array[x]),'LL') EQ -1) THEN BEGIN
             ;data is (UNSIGNED) LONG

             IF (STRPOS(STRUPCASE(help_array[x]),'U') NE -1) THEN BEGIN
               ; data is UNSIGNED-LONG
               type_matrix[x]=3
               type_matrix_name[x]='ULONG'
             ENDIF ELSE BEGIN
               ;data is LONG
               type_matrix[x]=3
               type_matrix_name[x]='LONG'
             ENDELSE
           ENDIF
     
           IF(STRPOS(STRUPCASE(help_array[x]),'LL') NE -1) $
              OR ((STRPOS(STRUPCASE(help_array[x]),'B') EQ -1) $
                  AND (LONG64(help_array[x]) NE LONG(help_array[x]))) THEN BEGIN
             ;data is (UNSIGNED) LONG64 
             IF(STRPOS(STRUPCASE(help_array[x]),'U') NE -1) THEN BEGIN
               ;data is UNSIGNED-LONG64
               type_matrix[x]=14
               type_matrix_name[x]='ULONG64'
             ENDIF ELSE BEGIN
               ;data is LONG64
               type_matrix[x]=14
               type_matrix_name[x]='LONG64'
             ENDELSE
           ENDIF	
           
         ENDIF ELSE BEGIN
           
           IF data_type NE 'STRING' THEN BEGIN	
             
             FOR y=0,1 DO BEGIN 
               IF(STRPOS(STRLOWCASE(help_array[x]), init_operators[y]) NE -1) THEN BEGIN
                 MESSAGE, 'SYNTAX ERROR: D'
               ENDIF
             ENDFOR		
             IF(STRPOS(STRUPCASE(help_array[x]),'D') NE -1) THEN BEGIN
               ;data is double
               type_matrix[x]=5
               type_matrix_name[x]='DOUBLE'
             ENDIF ELSE BEGIN
               ;data is FLOAT
               type_matrix[x]=4
               type_matrix_name[x]='FLOAT'
             ENDELSE
             
             
             
           ENDIF ELSE BEGIN
             ; data is STRING
             y=0
             WHILE ((STRMID(help_array[x],y,1) EQ ' ') AND (y LT STRLEN(help_array[x]))) DO BEGIN
               y=y+1
             ENDWHILE
             IF STRMID(help_array[x],y,1) EQ "'" THEN BEGIN
               y=y+1
               WHILE ((STRMID(help_array[x],y,1) NE "'") AND (y LT STRLEN(help_array[x]))) DO BEGIN
                 y=y+1
               ENDWHILE
               IF y EQ STRLEN(help_array[x])-1 THEN BEGIN                ;string is correct
                 type_matrix[x]=7
                 type_matrix_name[x]='STRING'
                 is_in_quotes[x] = 1
               ENDIF ELSE BEGIN
                 y=y+1
                 WHILE ((STRMID(help_array[x],y,1) EQ ' ') AND (y LT STRLEN(help_array[x]))) DO BEGIN
                   y=y+1
                 ENDWHILE 
                 IF y EQ STRLEN(help_array[x]) THEN BEGIN                ;string is correct
                   type_matrix[x]=7
                   type_matrix_name[x]='STRING'
                   is_in_quotes[x] = 1
                 ENDIF ELSE BEGIN				 ;string is not correct
                   IF ~ strict THEN BEGIN
                     type_matrix[x]=7
                     type_matrix_name[x]='STRING'
                     is_in_quotes[x] = 0
                   ENDIF ELSE BEGIN
                     MESSAGE, 'String type detected but characters after last quote.'
                   ENDELSE
                 ENDELSE					
               ENDELSE	
             ENDIF ELSE BEGIN
               IF ~ strict THEN BEGIN
                 type_matrix[x]=7
                 type_matrix_name[x]='STRING'
                 is_in_quotes[x] = 0
               ENDIF ELSE BEGIN
                 MESSAGE, 'String type detected but characters before first quote.'
               ENDELSE
             ENDELSE
             
             
             
           ENDELSE
         ENDELSE
         data_type='STRING'
         
       ENDFOR


       IF ARRAY_EQUAL(type_matrix, type_matrix[0]) THEN BEGIN
         ; All array elements have the same data type.
 
         info = CREATE_STRUCT(info,'TYPE_NAME',type_matrix_name[0])
         info = CREATE_STRUCT(info,'TYPE',type_matrix[0])
  
     
         IF (info.TYPE_NAME)[0] EQ 'BYTE' THEN $
     
            value=REFORM(BYTE(FIX(help_array)),info.dimensions)
         IF (info.TYPE_NAME)[0] EQ 'INT' THEN $
            value=REFORM(FIX(help_array),info.dimensions)
         IF (info.TYPE_NAME)[0] EQ 'UINT' THEN $
            value=REFORM(UINT(help_array),info.dimensions)		
         IF (info.TYPE_NAME)[0] EQ 'LONG' THEN $
            value=REFORM(LONG(help_array),info.dimensions)
         IF (info.TYPE_NAME)[0] EQ 'ULONG' THEN $
            value=REFORM(ULONG(help_array),info.dimensions)	
         IF (info.TYPE_NAME)[0] EQ 'LONG64' THEN $
            value=REFORM(LONG64(help_array),info.dimensions)	
         IF (info.TYPE_NAME)[0] EQ 'ULONG64' THEN $
            value=REFORM(ULONG64(help_array),info.dimensions)		
         IF (info.TYPE_NAME)[0] EQ 'FLOAT' THEN $
            value=REFORM(FLOAT(help_array),info.dimensions)	
         IF (info.TYPE_NAME)[0] EQ 'DOUBLE' THEN $
            value=REFORM(DOUBLE(help_array),info.dimensions)	
         IF (info.TYPE_NAME)[0] EQ 'STRING' THEN BEGIN
           ; Remove quotation marks.
           FOR ii=0,info.n_elements-1 DO BEGIN
             help_array[ii] = STRTRIM(help_array[ii], 2)
             IF is_in_quotes[ii] THEN BEGIN
               help_array[ii] = STRMID(help_array[ii], 1, STRLEN(help_array[ii])-2)
             ENDIF
           ENDFOR
           value=REFORM(help_array, info.dimensions)
         ENDIF


       ENDIF ELSE BEGIN  

         ; Array elements have different data-types
         FOR x=0,info.n_elements-1 DO BEGIN
           IF type_matrix_name[x] EQ 'STRING' THEN BEGIN
             MESSAGE, 'Mixed type arrays cannot contain strings.'
           ENDIF	
         ENDFOR
         
         info = CREATE_STRUCT(info, 'TYPE_NAME', REFORM(type_matrix_name, info.dimensions))
         info = CREATE_STRUCT(info, 'TYPE', REFORM(type_matrix, info.dimensions))	
     

         values=make_array(value=help_array[0],type=5,1)
         FOR x=1,info.n_elements-1 DO BEGIN
           IF (info.TYPE_NAME)[x] EQ 'BYTE' THEN $
              values=[values,BYTE(FIX(help_array[x]))]
           IF (info.TYPE_NAME)[x] EQ 'INT' THEN $
              values=[values,INT(help_array[x])]
           IF (info.TYPE_NAME)[x] EQ 'UINT' THEN $
              values=[values,UINT(help_array[x])]
           IF (info.TYPE_NAME)[x] EQ 'LONG' THEN $
              values=[values,LONG(help_array[x])]
           IF (info.TYPE_NAME)[x] EQ 'ULONG' THEN $
              values=[values,ULONG(help_array[x])]
           IF (info.TYPE_NAME)[x] EQ 'LONG64' THEN $
              values=[values,LONG64(help_array[x])]
           IF (info.TYPE_NAME)[x] EQ 'ULONG64' THEN $
              values=[values,ULONG64(help_array[x])]
           IF (info.TYPE_NAME)[x] EQ 'FLOAT' THEN $
              values=[values,FLOAT(help_array[x])]
           IF (info.TYPE_NAME)[x] EQ 'DOUBLE' THEN $ 
              values=[values,DOUBLE(help_array[x])]	          
         ENDFOR	
           
           
         value=REFORM(values,info.dimensions)	
       ENDELSE
    

       ; -----------------------------------------------------------------------
       ; Now check to see if this should have been a scalar instead of an array.
       IF is_possible_scalar THEN BEGIN
         IF info.n_elements EQ 1 THEN BEGIN

           value = value[0]

           info.n_elements = 0
           info.n_dimensions = 0
           info.dimensions = 0
         ENDIF
       ENDIF

       RETURN, value
     
     END ;  FUNCTION MIR_VALUE_FROM_STRING
     
