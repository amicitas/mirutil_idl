

;+==============================================================================
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
; PURPOSE:
;   A standard way to communicate between IDL and python.
;
; DESCRIPTION:
;   I need a way to call python functions from within IDL.  This should
;   eventually be done by using some sort of standard C wrapper.
;   
;   For now though I will use a much simpler solution of transfering data
;   using netCDF files.  Not efficent, but quick to implement.
;
; INPUTS:
;   input
;       A structure containing the data to be passed to python.
;
;   python_filename
;       A string specificing the python filename to be called.
;   
;
;-=============================================================================

     FUNCTION MIR_PYIDL, input, python_filename, SSH=ssh
       COMPILE_OPT STRICTARR

       filename_in = '/u/npablant/temp/__mir_pyidl_in.nc'
       filename_out = '/u/npablant/temp/__mir_pyidl_out.nc'
       
       CATCH, error
       IF error NE 0 THEN BEGIN
         CATCH, /CANCEL
         ; Clean up temporary files.
         FILE_DELETE, filename_in, filename_out, /ALLOW_NONEXISTENT
         MESSAGE, /REISSUE
       ENDIF

       MIR_NETCDF_QUICK_WRITE, input, filename_in

       ; Call the python routine that does the actual calculations..
       ; 
       IF KEYWORD_SET(ssh) THEN BEGIN
         ; I am having a problem with a mismatch of libcurl between IDL and my
         ; python program.  Looks like I can't run my python program as a
         ; sub-process.  This is a workaround, though it is quite ugly.
         SPAWN, ['ssh', 'localhost', 'echo Connected ; python '+python_filename], /NOSHELL
       ENDIF ELSE BEGIN
         SPAWN, ['python', python_filename], /NOSHELL
       ENDELSE

       output = MIR_NETCDF_QUICK_READ(filename_out, /STRUCTURE)

       FILE_DELETE, filename_in, filename_out, /ALLOW_NONEXISTENT

       RETURN, output

     END ; FUNCTION MIR_PYIDL
