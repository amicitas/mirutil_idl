;+=================================================================
; This is a procedure to be used to output plots to postscript
; files.  
;
; It can be called the same way as PLOT. 
;-=================================================================


     ;+=================================================================
     ; 
     ; Setup for the decomposed color workaround.
     ;
     ; Decomposed color does not work for ps devices in IDL 6
     ; I am providing a work around here.  This can be removed
     ; once it is likly that IDL 6 won't be used anymore.
     ;
     ;-=================================================================
     PRO PSPLOT_DECOMPOSED_WORKAROUND_INIT
       COMMON  PSPLOT_DECOMPOSED_WORKAROUND, c_decomposed_workaround
       
 
       ; Save the current color tables
       TVLCT, orig_red, orig_green, orig_blue, /GET
       
       c_decomposed_workaround = { orig_red:orig_red $
                                   ,orig_blue:orig_blue $
                                   ,orig_green:orig_green $
                                 }


     END  ;PRO PSPLOT_DECOMPOSED_WORKAROUND_INIT



     ;+=================================================================
     ; 
     ; Restore the original color table after the 
     ; decomposed color workaround.
     ;
     ;-=================================================================
     PRO PSPLOT_DECOMPOSED_WORKAROUND_CLEANUP
       COMMON  PSPLOT_DECOMPOSED_WORKAROUND
       
 
       ; Set the current table to the original valueds.
       TVLCT, c_decomposed_workaround.orig_red $
              ,c_decomposed_workaround.orig_green $
              ,c_decomposed_workaround.orig_blue

     END  ;PRO PSPLOT_DECOMPOSED_WORKAROUND_CLEANUP



     ;+=================================================================
     ; 
     ; Take as input an array of decomposed colors.  These
     ; colors will be set into the color table.  An array of 
     ; idexes corrisponding to the colors will be returned.
     ;
     ;-=================================================================
     FUNCTION PSPLOT_DECOMPOSED_WORKAROUND, color
       COMMON  PSPLOT_DECOMPOSED_WORKAROUND
       
       IF N_ELEMENTS(color) GT 256 THEN BEGIN
         MESSAGE, 'The current workaround for decomposed color in IDL 6' $
         +' does not support more than 256 colors.'
       ENDIF

       rgb = MIR_COLOR_TO_RGB(color)
       
       ; Set the first part of the color table to the colors
       ; that we need.
       TVLCT, rgb.red $
              ,rgb.green $
              ,rgb.blue

       color_out = INDGEN(N_ELEMENTS(color))

       IF N_ELEMENTS(color_out) EQ 1 THEN BEGIN
         color_out = color_out[0]
       ENDIF

       RETURN, color_out

     END  ;PRO PSPLOT_DECOMPOSED_WORKAROUND



     ;+=================================================================
     ; 
     ; SYNTAX:
     ;   PSOPLOT, [ x,] y [, /LEAVEOPEN][, /CLOSE][, /HELP]
     ;            [, KEYWORDS]
     ;
     ; DESCRIPTION:
     ;   Same usage as OPLOT but will output to a postscript file.
     ;   A postscript file must be open to use this command.
     ;
     ;   See [PSPLOT] for documentation.
     ;
     ;
     ; KEYWORDS: 
     ;
     ;   Accepts all OPLOT keywords.
     ;
     ;   /PDF
     ;       Convert the file to pdf after saving.
     ;
     ;   /LEAVEOPEN
     ;       Leave the postscript file open.
     ;       If not given the output file will be closed
     ;       and the device reset to its previous state.
     ;
     ;   /CLOSE
     ;       Close the postscript file.
     ;       This overrides /LEAVEOPEN overides.
     ;
     ;
     ;
     ;-=================================================================
     PRO PSOPLOT, x_in, y_in $
                  ,COLOR=color_in $
 
                  ,LEAVEOPEN=leaveopen $
                  ,CLOSE=close $
                  ,PDF=pdf $

                  ,VERBOSE=verbose $
                  ,HELP=help $

                  ,_REF_EXTRA=extra
                  
       COMMON PSPLOT_COMMON, c_psplot

       
       IF KEYWORD_SET(help) THEN BEGIN
         INFO
         RETURN
       ENDIF


       ;---------------------------------
       ; Find the number of input vectors
       ; If only 1 vector is input, then we take it to be the y values.
       ; Otherwise we assume both x & y values have been given.
       num_params = N_PARAMS()
       CASE num_params OF
         1: BEGIN
           y = x_in
           x = INDGEN(N_ELEMENTS(y))
         END
         2: BEGIN
           x = x_in
           y = y_in
         END
         ELSE: BEGIN
           PRINT, 'PSOPLOT: Incorrect number of input parameters.'
           RETURN
         ENDELSE
       ENDCASE


       ; Begin ploting

       ; Check to see if a ploting unit is already open.
       ; If not we need to open it.
       IF c_psplot.device_open THEN BEGIN


         ; Deal with the color workaround for IDL 6
         IF c_psplot.decomposed AND c_psplot.decomposed_workaround THEN BEGIN
           color = PSPLOT_DECOMPOSED_WORKAROUND(color_in)
         ENDIF ELSE BEGIN
           color = color_in
         ENDELSE


         ; Do the actual plot
         OPLOT, x, y $
                ,COLOR=color $
                ,_EXTRA=extra
         
         IF KEYWORD_SET(verbose) THEN BEGIN
           PRINT, 'PSOPLOT: Ploting postscript image to: ', c_psplot.filename
         ENDIF

       ENDIF ELSE BEGIN
         PRINT, 'PSOPLOT: No postscript file open.'
       ENDELSE

       PS_CLOSE, LEAVEOPEN=leaveopen, CLOSE=close, PDF=pdf

     END ;PSOPLOT


     ;+=================================================================
     ; 
     ; SYNTAX:
     ;   PSPLOTS, [ x,] y [, FILENAME=string][, PATH=string]
     ;            [, /LEAVEOPEN][, /CLOSE]
     ;            [, /GRAYSCALE][, GET_PATH=get_path][, /HELP]
     ;            [, KEYWORDS]
     ;
     ; DESCRIPTION:
     ;   Same usage as PSPLOT but will output to a postscript file.
     ;   A postscript file must be open to use this command.
     ;
     ;   See [PSPLOT] for documentation.
     ;
     ;
     ; KEYWORDS: 
     ;
     ;   Accepts all PSPLOT keywords.
     ;
     ;   /PDF
     ;       Convert the file to pdf after saving.
     ;
     ;   /LEAVEOPEN
     ;       Leave the postscript file open.
     ;       If not given the output file will be closed
     ;       and the device reset to its previous state.
     ;
     ;   /CLOSE
     ;       Close the postscript file.
     ;       This overrides /LEAVEOPEN overides.
     ;
     ;
     ;
     ;-=================================================================
     PRO PSPLOTS, x_in, y_in $
                  ,COLOR=color_in $
                  ,FILENAME=filename_in $

                  ,LEAVEOPEN=leaveopen $
                  ,CLOSE=close $
                  ,PDF=pdf $
                  ,GET_PATH=get_path $

                  ,HELP=help $
                  ,VERBOSE=verbose $

                  ,_REF_EXTRA=extra

       COMMON PSPLOT_COMMON, c_psplot

       
       IF KEYWORD_SET(help) THEN BEGIN
         INFO
         RETURN
       ENDIF


       ;---------------------------------
       ; Find the number of input vectors
       ; If only 1 vector is input, then we take it to be the y values.
       ; Otherwise we assume both x & y values have been given.
       num_params = N_PARAMS()
       CASE num_params OF
         1: BEGIN
           y = x_in
           x = INDGEN(N_ELEMENTS(y))
         END
         2: BEGIN
           x = x_in
           y = y_in
         END
         ELSE: BEGIN
           PRINT, 'PSPLOTS: Incorrect number of input parameters.'
           RETURN
         ENDELSE
       ENDCASE


       ; Open the plot device
       PS_OPEN, FILENAME=filename_in $
                ,GREYSCALE=greyscale $
                ,PDF=pdf $
                ,GET_PATH=get_path $
                ,_EXTRA=extra

       ; Deal with the color workaround for IDL 6
       IF c_psplot.decomposed AND c_psplot.decomposed_workaround THEN BEGIN
         color = PSPLOT_DECOMPOSED_WORKAROUND(color_in)
       ENDIF ELSE BEGIN
         color = color_in
       ENDELSE


       ; Do the actual plot
       PLOTS, x, y $
              ,COLOR=color $
              ,_EXTRA=extra

       IF KEYWORD_SET(verbose) THEN BEGIN
         PRINT, 'PSPLOTS: Ploting postscript image to: ', c_psplot.filename
       ENDIF

       PS_CLOSE, LEAVEOPEN=leaveopen, CLOSE=close, PDF=pdf

     END ;PSPLOTS



     ;+=================================================================
     ; 
     ; SYNTAX:
     ;   PSXYOUTS, [ x, y,] string [, FILENAME=string][, PATH=string]
     ;             [, /LEAVEOPEN][, /CLOSE]
     ;             [, /GRAYSCALE][, GET_PATH=get_path][, /HELP]
     ;             [, KEYWORDS]
     ;
     ; DESCRIPTION:
     ;   Same usage as XYOUTS but will output to a postscript file.
     ;   A postscript file must be open to use this command.
     ;
     ;   See [PSPLOT] for documentation.
     ;
     ;
     ; KEYWORDS: 
     ;
     ;   Accepts all XYOUTS keywords.
     ;
     ;   /PDF
     ;       Convert the file to pdf after saving.
     ;
     ;   /LEAVEOPEN
     ;       Leave the postscript file open.
     ;       If not given the output file will be closed
     ;       and the device reset to its previous state.
     ;
     ;   /CLOSE
     ;       Close the postscript file.
     ;       This overrides /LEAVEOPEN overides.
     ;
     ;
     ;
     ;-=================================================================
     PRO PSXYOUTS, x_in, y_in, string_in $
                   ,COLOR=color_in $
                   ,FONT=font $

                   ,LEAVEOPEN=leaveopen $
                   ,CLOSE=close $
                   ,PDF=pdf $
                   ,GRAYSCALE=grayscale $

                   ,FILENAME=filename_in $
                   ,GET_PATH=get_path $

                   ,HELP=help $
                   ,VERBOSE=verbose $

                   ,_REF_EXTRA=extra

       COMMON PSPLOT_COMMON, c_psplot


       
       IF KEYWORD_SET(help) THEN BEGIN
         INFO
         RETURN
       ENDIF


       ;---------------------------------
       ; Find the number of input vectors
       ; If only 1 vector is input, then we take it to be the y values.
       ; Otherwise we assume both x & y values have been given.
       num_params = N_PARAMS()
       CASE num_params OF
         1: BEGIN
           string = x_in
         END
         3: BEGIN
           x = x_in
           y = y_in
           string = string_in
         END
         ELSE: BEGIN
           PRINT, 'PSXYOUTS: Incorrect number of input parameters.'
           RETURN
         ENDELSE
       ENDCASE


       ; Open the plot device
       PS_OPEN, FILENAME=filename_in $
                ,GREYSCALE=greyscale $
                ,PDF=pdf $
                ,GET_PATH=get_path $
                ,_EXTRA=extra


       ; Deal with the color workaround for IDL 6
       IF c_psplot.decomposed AND c_psplot.decomposed_workaround THEN BEGIN
         color = PSPLOT_DECOMPOSED_WORKAROUND(color_in)
       ENDIF ELSE BEGIN
         color = color_in
       ENDELSE


       ; Do the actual plot
       CASE num_params OF
         1: BEGIN
           XYOUTS, string $
                   ,COLOR=color $
                   ,FONT=font $
                   ,_EXTRA=extra
         END
         3: BEGIN
           XYOUTS, x, y, string $
                   ,COLOR=color $
                   ,FONT=font $
                   ,_EXTRA=extra
         END
       ENDCASE

       IF KEYWORD_SET(verbose) THEN BEGIN
         PRINT, 'PSXYOUTS: Ploting postscript image to: ', c_psplot.filename
       ENDIF

       PS_CLOSE, LEAVEOPEN=leaveopen, CLOSE=close, PDF=pdf

     END ;PSXYOUTS



     ;+=================================================================
     ; 
     ; SYNTAX:
     ;   PSERRORPLOT, [ x,] low, high [, /LEAVEOPEN][, /CLOSE][, /HELP]
     ;                [, KEYWORDS]
     ;
     ; DESCRIPTION:
     ;   Same usage as ERRORPLOT but will output to a postscript file.
     ;   A postscript file must be open to use this command.
     ;
     ;   See [PSPLOT] for documentation.
     ;
     ;
     ;
     ; KEYWORDS: 
     ;
     ;   Accepts all ERRORPLOT keywords.
     ;
     ;   /PDF
     ;       Convert the file to pdf after saving.
     ;
     ;   /LEAVEOPEN
     ;       Leave the postscript file open.
     ;       If not given the output file will be closed
     ;       and the device reset to its previous state.
     ;
     ;   /CLOSE
     ;       Close the postscript file.
     ;       This overrides /LEAVEOPEN overides.
     ;
     ;
     ;
     ;-=================================================================
     PRO PSERRORPLOT, x, low, high $
                  ,COLOR=color_in $

                  ,LEAVEOPEN=leaveopen $
                  ,CLOSE=close $
                  ,PDF=pdf $

                  ,HELP=help $
                  ,VERBOSE=verbose $

                  ,_REF_EXTRA=extra

       COMMON PSPLOT_COMMON, c_psplot

       
       IF KEYWORD_SET(help) THEN BEGIN
         INFO
         RETURN
       ENDIF

       ; Begin ploting

       ; Check to see if a ploting unit is already open.
       IF c_psplot.device_open THEN BEGIN

         ; Deal with the color workaround for IDL 6
         IF c_psplot.decomposed AND c_psplot.decomposed_workaround THEN BEGIN
           color = PSPLOT_DECOMPOSED_WORKAROUND(color_in)
         ENDIF ELSE BEGIN
           color = color_in
         ENDELSE

         ; Do the actual plot
         ERRORPLOT, x, low, high $
                    ,COLOR=color $
                    ,_EXTRA=extra
         
         IF KEYWORD_SET(verbose) THEN BEGIN
           PRINT, 'PSERRORPLOT: Ploting postscript image to: ', c_psplot.filename
         ENDIF

       ENDIF ELSE BEGIN
         PRINT, 'PSERRORPLOT: No postscript file open.'
       ENDELSE

       PS_CLOSE, LEAVEOPEN=leaveopen, CLOSE=close, PDF=pdf

     END ;PSERRORPLOT



     ;+=================================================================
     ; 
     ; PURPOSE:
     ;   Initialize the c_psplot COMMON variable
     ;
     ; TAGS:
     ;   INTERNAL
     ;
     ;-=================================================================
     PRO PS_INIT_COMMON

       COMMON PSPLOT_COMMON, c_psplot

         c_psplot = {device_open:0 $
                     ,old_device:'' $
                     ,old_color:0L $
                     ,old_background:0L $
                     ,decomposed:0 $
                     ,decomposed_workaround:0 $
                     ,filename:'' $
                     ,path:'' $
                     ,pdf:0}

     END; PRO PS_INIT_COMMON


     ;+=================================================================
     ; 
     ; PURPOSE:
     ;   Handle initial setup and opening of the postscript file
     ;   for [PSPLOT]
     ;
     ; TAGS:
     ;   INTERNAL
     ;
     ;-=================================================================
     PRO PS_OPEN, FILENAME=filename_in $
                  ,PDF=pdf $
                  ,PATH=path $
                  ,GET_PATH=get_path $
                  ,DIALOG_FILENAME=dialog_filename_in $
                  ,GREYSCALE=greyscale $
                  ,COLOR=plot_color $
                  ,DECOMPOSED=decomposed $
                  ,ENCAPSULATED=encapsulated $
                  ,LANDSCAPE=landscape $
                  ,PORTRAIT=portrait $
                  ,LETTER=letter $
                  ,XSIZE=xsize $
                  ,YSIZE=ysize $
                  ,INCHES=inches $
                  ,_EXTRA=extra

       COMMON PSPLOT_COMMON, c_psplot

       IF ~ ISA(c_psplot) THEN BEGIN
         PS_INIT_COMMON
       ENDIF

       ; Check to see if a ploting unit is already open.
       ; If not we need to open it.
       IF ~ c_psplot.device_open OR (!d.name NE 'PS') THEN BEGIN

         dialog_filename = KEYWORD_SET(dialog_filename_in)
         
         ; Check to see if a filename was provided.  If no filename
         ; was provided then we always open a file dialog.
         IF KEYWORD_SET(filename_in) THEN BEGIN
           filename = filename_in
           get_path = FILE_DIRNAME(filename_in)
         ENDIF ELSE BEGIN
           dialog_filename = 1
         ENDELSE

         IF dialog_filename THEN BEGIN
           ; See if we can extract the title.
           IF HAS_TAG(extra, 'TITLE') THEN BEGIN
             title = 'Choose output filename for: '+extra.title
           ENDIF ELSE BEGIN
             title = 'Choose output filename for postscript output.'
           ENDELSE

           ; Try to pickup the initial path.
           ; If it was given as an input then use it. 
           ; Otherwise check to see if the common block has been initialized.
           ; If it has use the path from the last save.
           CASE 1 OF
             KEYWORD_SET(path): 
             KEYWORD_SET(filename): path = FILE_DIRNAME(filename)
             N_ELEMENTS(c_psplot) NE 0: path = c_psplot.path
             ELSE:
           ENDCASE

           filename = DIALOG_PICKFILE(DEFAULT_EXTENSION='ps' $
                                      ,FILTER = ['*.ps;*.eps;*.pdf','*.ps','*.eps','*.pdf'] $
                                      ,/WRITE $
                                      ,/OVERWRITE_PROMPT $
                                      ,GET_PATH=get_path $
                                      ,PATH=path $
                                      ,FILE=filename $
                                      ,TITLE=title)

           IF filename EQ '' THEN BEGIN
             MESSAGE, 'PSPLOT canceled by user.'
           ENDIF

         ENDIF


         ; Now we need to check if the extention is *.pdf. If it is then convert to *.ps
         pdf_pos = STRPOS(STRUPCASE(filename), '.PDF', /REVERSE_SEARCH)
         IF pdf_pos NE -1 THEN BEGIN
           file_len = STRLEN(filename)
           IF (file_len - pdf_pos) EQ 4 THEN BEGIN
             ; Replace *.pdf WITH *.ps
             IF STRMID(filename, pdf_pos) EQ '.PDF' THEN BEGIN
               filename = STRMID(filename, 0, pdf_pos) + '.PS'
             ENDIF ELSE BEGIN
               filename = STRMID(filename, 0, pdf_pos) + '.ps'
             ENDELSE
           ENDIF
         ENDIF


         IF KEYWORD_SET(greyscale) THEN BEGIN
           color = 0
         ENDIF ELSE BEGIN
           color = 1
         ENDELSE

         old_device = !d.name
         old_color = !p.color
         old_background = !p.background
         

         SET_PLOT, 'ps'



         ; Set the page size
         MIR_DEFAULT, xsize, 0D
         MIR_DEFAULT, ysize, 0D

         ; Make sure some size is given
         IF ((xsize EQ 0D) OR (ysize EQ 0D)) AND ~ KEYWORD_SET(letter) THEN BEGIN
           letter = 1
         ENDIF

         IF KEYWORD_SET(letter) THEN BEGIN
           IF NOT KEYWORD_SET(landscape) XOR KEYWORD_SET(portrait) THEN BEGIN
             landscape = 1
             portrait = 0
           ENDIF
           
           inches = 1
           
           IF KEYWORD_SET(landscape) THEN BEGIN
             xsize = 11.0
             ysize = 8.5
             portrait = 1
             landscape = 0
           ENDIF ELSE BEGIN
             xsize = 8.5
             ysize = 11.0
             portrait = 1
             landscape = 0
           ENDELSE
         ENDIF
         
         ;MIR_DEFAULT, encapsulated, 1
         DEVICE, FILENAME=filename $
                 ,ENCAPSULATED=encapsulated $
                 ,COLOR=color $

                 ,LANDSCAPE=landscape $
                 ,PORTRAIT=portrait $
                 ,XSIZE=xsize $
                 ,YSIZE=ysize $
                 ,INCHES=inches $

                 ,/ISOLATIN1 $
                 
                 ,_EXTRA=extra


         ; Decomposed color does not work for ps devices in IDL 6
         ; I am providing a work around here.  This can be removed
         ; once it is likly that IDL 6 wont be used anymore.
         decomposed_workaround = (FIX(!version.release) LT 7.0)
         IF ~ decomposed_workaround THEN BEGIN
           DEVICE, DECOMPOSED=decomposed
         ENDIF ELSE BEGIN
           IF KEYWORD_SET(decomposed) THEN BEGIN
             PSPLOT_DECOMPOSED_WORKAROUND_INIT
           ENDIF
         ENDELSE


         c_psplot.device_open = 1
         c_psplot.old_device = old_device
         c_psplot.old_color = old_color
         c_psplot.old_background = old_background
         c_psplot.decomposed = KEYWORD_SET(decomposed)
         c_psplot.decomposed_workaround = decomposed_workaround
         c_psplot.filename = filename
         c_psplot.path = get_path
         c_psplot.pdf = KEYWORD_SET(pdf)

         PRINT, 'PSPLOT: Opened postscript file: ', filename

       ENDIF

     END; PRO PS_OPEN



     ;+=================================================================
     ; 
     ; PURPOSE:
     ;   Handle closing of the postscript file and conversion to pdf.
     ;
     ; TAGS:
     ;   INTERNAL
     ;
     ;-=================================================================
     PRO PS_CLOSE, LEAVEOPEN=leaveopen $
                   ,CLOSE=close $
                   ,PDF=pdf

       COMMON PSPLOT_COMMON, c_psplot


       IF ~ ISA(c_psplot) THEN BEGIN
         PS_INIT_COMMON
       ENDIF

       ; /CLOSE overrides /LEAVEOPEN
       IF KEYWORD_SET(close) THEN BEGIN
         leaveopen = 0
       ENDIF

       IF ~ KEYWORD_SET(leaveopen) THEN BEGIN
         ; Check to see if a ploting unit is already open.
         ; If not we dont need to do anything.
         IF  (!d.name EQ 'PS') AND c_psplot.device_open THEN BEGIN
           DEVICE, /CLOSE 

           PRINT, 'PSPLOT: Saved postscript image to: ', c_psplot.filename 
           SET_PLOT, c_psplot.old_device
           !p.color = c_psplot.old_color
           !p.background = c_psplot.old_background

           IF c_psplot.decomposed AND c_psplot.decomposed_workaround THEN BEGIN
             PSPLOT_DECOMPOSED_WORKAROUND_CLEANUP
           ENDIF
           
           IF KEYWORD_SET(pdf) OR c_psplot.pdf THEN BEGIN
             command = 'epstopdf ' + c_psplot.filename
             SPAWN, command
             PRINT, 'PSPLOT: Saved pdf image.'
           ENDIF
         ENDIF

         c_psplot.device_open = 0
       ENDIF

     END; PRO PS_CLOSE



     ;+=================================================================
     ; 
     ; SYNTAX:
     ;   PSPLOT, [ x,] y [, FILENAME=string][, PATH=string]
     ;           [, /LEAVEOPEN][, /CLOSE]
     ;           [, /GRAYSCALE][, GET_PATH=get_path][, /HELP]
     ;           [, KEYWORDS]
     ;           
     ;
     ; Same usage as PLOT but will output to a postscript file.
     ;
     ;
     ; KEYWORDS: 
     ;   Accepts graphics and device keywords
     ;
     ;   FILENAME = string
     ;       Filename to save output to.
     ;       If not given a dialog will be displayed.
     ;
     ;   /PDF
     ;       Convert the file to pdf after saving.
     ;       The system must have the command 'epstopdf'
     ;
     ;   /LEAVEOPEN
     ;       Leave the postscript file open.
     ;       If not given the output file will be closed
     ;       and the device reset to its previous state.
     ;
     ;   /CLOSE
     ;       Close the postscript file.
     ;       This overrides /LEAVEOPEN overides.
     ;
     ;       If no input parameters are given then the
     ;       file is closed with out without plotting
     ;       anything. Otherwise the file is closed after
     ;       the plotting.
     ;                
     ;   /GREYSCALE
     ;       Save in greyscale
     ;
     ;   PATH = string
     ;       Default path to show in the file dialog.
     ;
     ;   GET_PATH = string
     ;       The directory path of the file chosen.
     ;
     ;
     ;
     ;-=================================================================
     PRO PSPLOT, x_in, y_in $
                 ,COLOR=color_in $

                 ,FILENAME=filename_in $
                 ,FONT=font $
                 ,LEAVEOPEN=leaveopen $
                 ,HELP=help $
                 ,GRAYSCALE=grayscale $
                 ,DECOMPOSED=decomposed $

                 ,PDF=pdf $
                 ,CLOSE=close $
                 ,GET_PATH=get_path $

                 ,VERBOSE=verbose $
                 ,_EXTRA=extra

       COMMON PSPLOT_COMMON, c_psplot

       
       IF KEYWORD_SET(help) THEN BEGIN
         INFO
         RETURN
       ENDIF


       ;=============================
       version = '2.2.1 - 2012-12-02'
       ;=============================


       ;---------------------------------
       ; Find the number of input vectors
       ; If only 1 vector is input, then we take it to be the y values.
       ; Otherwise we assume both x & y values have been given.
       num_params = N_PARAMS()
       CASE num_params OF
         0: BEGIN
           IF KEYWORD_SET(close) THEN BEGIN
             PS_CLOSE
             RETURN
           ENDIF ELSE BEGIN
             PRINT, 'PSPLOT: Incorrect number of input parameters.'
             RETURN
           ENDELSE
         END
         1: BEGIN
           y = x_in
           x = INDGEN(N_ELEMENTS(y))
         END
         2: BEGIN
           x = x_in
           y = y_in
         END
         ELSE: BEGIN
           PRINT, 'PSPLOT: Incorrect number of input parameters.'
           RETURN
         ENDELSE
       ENDCASE


       ; Open the plot device
       PS_OPEN, FILENAME=filename_in $
                ,GREYSCALE=greyscale $
                ,PDF=pdf $
                ,GET_PATH=get_path $
                ,DECOMPOSED=decomposed $
                ,_EXTRA=extra


       ; Deal with the color workaround for IDL 6
       IF c_psplot.decomposed AND c_psplot.decomposed_workaround THEN BEGIN
         color = PSPLOT_DECOMPOSED_WORKAROUND(color_in)
       ENDIF ELSE BEGIN
         color = color_in
       ENDELSE

       ; Do the actual plot
       PLOT, x, y $
             ,COLOR=color $
             ,FONT=font $
             ,_EXTRA=extra

       IF KEYWORD_SET(verbose) THEN BEGIN
         PRINT, 'PSPLOT: Ploting postscript image to: ', c_psplot.filename
       ENDIF

       PS_CLOSE, LEAVEOPEN=leaveopen, CLOSE=close, PDF=pdf

     END ;PSPLOT
