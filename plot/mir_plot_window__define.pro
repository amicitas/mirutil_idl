


;+==============================================================================
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;     npablant@pppl.gov
;     novimir.pablant@amicitas.com
;
; PURPOSE:
;   
;
;-=============================================================================




     ;+=================================================================
     ; PURPOSE:
     ;   Initialize the object
     ;
     ;-=================================================================
     FUNCTION MIR_PLOT_WINDOW::INIT, _REF_EXTRA=extra
       
       status = self.WIDGET_OBJECT::INIT(_EXTRA=extra)

       self.GUI_INIT, _EXTRA=extra

       self.SAVE_WINDOW_PROPERTIES

       RETURN, status

     END ; FUNCTION BST_INST_WIDGET_XCRYSTAL::INIT


     ;+=================================================================
     ;
     ; 
     ;
     ;-=================================================================
     PRO MIR_PLOT_WINDOW::SAVE_WINDOW_PROPERTIES


       id = self.GET_ID_STRUCT()

       ; Save relative size of the draw widget.
       geom_window = WIDGET_INFO(self.gui_param.gui_id, /GEOMETRY)
       geom_graphic = WIDGET_INFO(id.ui_graphic, /GEOMETRY)

       self.window_param.graphic = [geom_window.xsize - geom_graphic.xsize $
                                    ,geom_window.ysize - geom_graphic.ysize]

       self.window_param.padding = [geom_window.xpad, geom_window.ypad]

     END ; PRO MIR_PLOT_WINDOW::SAVE_WINDOW_PROPERTIES



     ;+=================================================================
     ;
     ; Build the GUI components within the base widget.
     ;
     ;-=================================================================
     PRO MIR_PLOT_WINDOW::GUI_DEFINE

       ui_graphic = WIDGET_WINDOW(self.gui_param.gui_id $
                                  ,UNAME='ui_graphic' $
                                 )

       id_struct = { $
                     ui_graphic:ui_graphic $
                   }

       self->SET_ID_STRUCT, id_struct

     END ; PRO MIR_PLOT_WINDOW::GUI_DEFINE




     ;+=================================================================
     ;
     ; Here we have the event handler for this widget.
     ;
     ;-=================================================================
     PRO MIR_PLOT_WINDOW::EVENT_HANDLER, event, EVENT_HANDLED=event_handled

       ; Establish catch block.
       CATCH, error                  
       IF (error NE 0) THEN BEGIN 
         CATCH, /CANCEL
         PRINT_MESSAGE, 'Error handing event.', /ERROR
         RETURN
       ENDIF  

       ; First call the superclass method
       self.WIDGET_OBJECT::EVENT_HANDLER, event, EVENT_HANDLED=event_handled
       IF event_handled THEN RETURN


       id = self.GET_ID_STRUCT()


       ; Start main event loop
       CASE event.id OF
         
         self.gui_param.gui_id: BEGIN
           ; Here I have a workaround for IDL generating multiple resize
           ; events and causing flickering.  This completely solves that problem,
           ; though sometimes the final window state does not get updated.
           CASE TAG_NAMES(event, /STRUCTURE_NAME) OF
             'WIDGET_BASE': self.EVENT_RESIZE, event
             'WIDGET_TIMER': WIDGET_CONTROL, self.gui_param.gui_id, UPDATE=1
             ELSE:
           ENDCASE
         END

         ELSE: BEGIN
           MESSAGE, 'No handler for event from widget: ' + WIDGET_INFO(event.id, /UNAME)
         ENDELSE

       ENDCASE


     END ; PRO MIR_PLOT_WINDOW::EVENT_HANDLER



     ;+=================================================================
     ;
     ; Handle resize events.
     ;
     ;-=================================================================
     PRO MIR_PLOT_WINDOW::EVENT_RESIZE, event
       id = self.GET_ID_STRUCT()

       WIDGET_CONTROL, self.gui_param.gui_id, UPDATE=0

       WIDGET_CONTROL, self.gui_param.gui_id $
                       ,XSIZE=event.x-self.window_param.padding[0]*2 $
                       ,YSIZE=event.y-self.window_param.padding[0]*2

       WIDGET_CONTROL, id.ui_graphic $
                       ,XSIZE=event.x-self.window_param.graphic[0] $
                       ,YSIZE=event.y-self.window_param.graphic[1]

       WIDGET_CONTROL,  self.gui_param.gui_id, TIMER=0.1
       
     END ; PRO MIR_PLOT_WINDOW::EVENT_RESIZE



     ;+=========================================================================
     ;
     ; PURPOSE:
     ;   Setup the MIR_PLOT_WINDOW object
     ;
     ;
     ;-=========================================================================
     PRO MIR_PLOT_WINDOW__DEFINE

       struct = { MIR_PLOT_WINDOW $
                  ,window_param:{MIR_PLOT_WINDOW__WINDOW_PARAM $
                                 ,padding:[0L,0L] $
                                 ,graphic:[0L,0L] $
                                } $
                  ,INHERITS WIDGET_OBJECT $
                  ,INHERITS MIR_OBJECT $
                }

     END ; PRO MIR_PLOT_WINDOW
