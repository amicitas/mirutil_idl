     ;+
     ; NAME:
     ;        CROSSHAIRS
     ;
     ; PURPOSE:
     ;        wall to wall cross hairs to help read numbers off of plot
     ;        axis. The graphics function is set to 6 for eXclusive OR.
     ;        This allows the cross hairs to be drawn and erased without
     ;        disturbing the contents of the window.  Hit RMB to exit
     ;
     ; CALLING SEQUENCE:
     ;        CROSSHAIRS
     ;
     ; INPUTS/OUTPUTS: none
     ;
     ; RESTRICTIONS:
     ;        Works only with window system drivers.
     ;
     ; CATEGORY:
     ;        Interactive graphics.
     ;
     ;
     ; AUTHOR:   
     ;        1993-04 - Paul Ricchiazzi
     ;        2003-08 - Wayne Solomon
     ;        2009-06 - Novimir Antoniuk Pablant
     ;-

     PRO CROSSHAIRS, CHARSIZE=charsize $
                     ,XCLICKS=xclicks $
                     ,YCLICKS=yclicks $
                     ,NUMCLICKS=numclicks $
                     ,COLOR=color $
                     ,RESET=reset $
                     ,_EXTRA=_extra

       IF KEYWORD_SET(reset) THEN BEGIN
         DEVICE, SET_GRAPHICS=3, CURSOR_STANDARD=34
         RETURN
       ENDIF

       xclicks = -1
       yclicks = -1
       MIR_DEFAULT, numclicks, 1e10
       MIR_DEFAULT, color, !p.background

       DEVICE, CURSOR_STANDARD=33, GET_GRAPHICS=old, SET_GRAPHICS=6 ;Set xor
       CURSOR, ox1, oy1, /NOWAIT, _EXTRA=_extra

       ; 1mouse is always in device coordinates.  
       ; Alternatively, could use the IDL function convert_coord(ox1, oy1, /norm, /to_device)
       ox = !mouse.x & oy = !mouse.y 
       TVCRS, ox, oy
       PLOTS, [0, !d.x_vsize], [oy, oy], /DEVICE, COLOR=color, _EXTRA=_extra
       PLOTS, [ox, ox], [0, !d.y_vsize], /DEVICE, COLOR=color, _EXTRA=_extra
       XYOUTS, 0.01, 0.01, '('+STRTRIM(ox1, 1)+', '+STRTRIM(oy1, 1)+')' $
         ,/NORM, CHARSIZE=charsize, FONT=0, COLOR=color
       lastx = -1e30
       lasty = lastx
       WHILE !mouse.button NE 4 AND n_elements(xclicks) LE numclicks DO BEGIN 
         cursor, x1, y1, /nowait, _extra=_extra
         x = !mouse.x & y = !mouse.y
         IF !mouse.button EQ 1 AND (lastx NE x1 OR lasty NE y1) THEN BEGIN
           print, x1, y1
           xclicks = [xclicks, x1]
           yclicks = [yclicks, y1]
           lastx = x1
           lasty = y1
         ENDIF
         IF x NE ox OR y NE oy THEN BEGIN
           PLOTS, [0, !d.x_vsize], [oy, oy], /DEVICE, COLOR=color, _EXTRA=_extra
           PLOTS, [ox, ox], [0, !d.y_vsize], /DEVICE, COLOR=color, _EXTRA=_extra 
           XYOUTS, 0.01, 0.01, '('+strtrim(ox1, 1)+', '+strtrim(oy1, 1)+')' $
             , /NORM, CHARSIZE=charsize, FONT=0, COLOR=color
           PLOTS, [0, !d.x_vsize], [y, y], /DEVICE, COLOR=color, _EXTRA=_extra
           PLOTS, [x, x], [0, !d.y_vsize], /DEVICE, COLOR=color, _EXTRA=_extra
           XYOUTS, 0.01, 0.01, '('+strtrim(x1, 1)+', '+strtrim(y1, 1)+')' $
             ,/NORM, CHARSIZE=charsize, FONT=0, COLOR=color
           wait, .1
           ox1 = x1
           oy1 = y1
           ox = x
           oy = y
         ENDIF
         wait, .01
       ENDWHILE
       PLOTS, [0, !d.x_vsize], [y, y], /DEVICE, COLOR=color, _EXTRA=_extra ; erase cross hairs
       PLOTS, [x, x], [0, !d.y_vsize], /DEVICE, COLOR=color, _EXTRA=_extra
       XYOUTS, 0.01, 0.01, '('+strtrim(x1, 1)+', '+strtrim(y1, 1)+')' $
         , /NORM, CHARSIZE=charsize, FONT=0, COLOR=color

       DEVICE, SET_GRAPHICS=old, CURSOR_STANDARD=34

       IF N_ELEMENTS(xclicks) GT 1 THEN BEGIN
         xclicks = xclicks[1:*]
         yclicks = yclicks[1:*]
       ENDIF

       RETURN
     END
