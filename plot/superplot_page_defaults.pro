



;+ ================================================================
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;   novimir.pablant@amicitas.com
;
; DATE:
;   2009-09
;
; VERSION:
;   0.0.0
;
; PURPOSE:
;   Return a structure with the page settings for SUPERPLOT_MULTI.
;
; DESCRIPTION:
;   These options are uses as part of SUPERPLOT_MULTI and provide
;   values for page options, and defaults for the individual
;   plot options.
;
; OPTIONS:
;   Here is a description of the available options.
;
;   pagetitle = string
;       The title for the page.  This will be plotted above
;       all of the plots. Each plot can also have it's own title.
;
;   xrange = [float,float]
;       If this is set, it will be used for all plots
;
;   yrange = [float,float]
;       If this is set, it will be used for all plots
;
;   pdf = bool
;       Plot output to a pdf file (a postscript file will also be 
;       created).
;
;
;   The folowing options only have an effect if /PDF is set.
;   If none of these option are given, then the default page size
;   will be Letter in portrait mode.
;
;   ysize = float
;       Height of the page.
;
;   xsize
;       Width of the page
;
;   inches
;       Sets the units for XSIZE & YSIZE.
;       Default units are cm.
;
;   portrait = bool
;       (default = 1)
;       Page size is Letter, in portait mode.
;
;   landscape = bool
;       (default = 0)
;       Page size is Letter, in landscape mode.
;       This will overide the portrait options.
;
; 
;
;
;
;
;- ================================================================



     FUNCTION SUPERPLOT_PAGE_DEFAULTS

       page_struct  = { SUPERPLOT_PAGE_DEFAULTS $
                        ,pdf:0 $
                        ,postscript:0 $
                        ,leaveopen:0 $
                        ,filename:'' $
                        ,path:'' $
                        ,dialog:0 $

                        ,xrange:[0D,0D] $
                        ,yrange:[0D,0D] $

                        ,pagetitle:'' $
                        ,pagetitle_charsize:1.5E $

                        ,caption:'' $

                        ,single_xtitle:0 $
                        
                        ,thesis_size:1 $
                        ,thesis_yfactor:-1E $

                        ,portrait:1 $
                        ,landscape:0 $
                        ,xsize:0E $
                        ,ysize:0E $
                        ,inches:1 $

                        ,margin_left:1E $

                        ,ticklen:0E $
                        ,ticklen_inches:-1 $

                        ,decomposed:1 $
                        ,background:0L $
                        ,color:0L $
                      }
       
       page_struct.background = MIR_COLOR('white')
       page_struct.color = MIR_COLOR('black')

       RETURN, page_struct
       
     END ; FUNCTION SUPERPLOT_PAGE_DEFAULTS
