

;+ =====================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
;   antoniuk@fusion.gat.com
;   amicitas@gmail.com
;
; DATE:
;   2009-04
;
; PURPOSE:
;   Easyer plotting for IDL
;
; DESRIPTION:
;   This contains two main routines:
;
;     SUPERPLOT
;       Unified interface to plotting routines.
;       Simple plotting to postscript and pdf. 
;
;     SUPERPLOT_MULTI
;       Simple plotting of multiple plots in one window/page.
;
;
; TO DO:
;   <SUPERPLOT_DATA> and <SUPERPLOT_SYMBOL> will currently not work
;     unless y values are given.  I should check for this condition
;     and generate y values if needed.
;
;   I need to make sure that the GET_PATH keyword will return the
;     path from PSPLOT.
;
;- =====================================================================



     ;+ ================================================================
     ; Return a structure with the default settings for
     ; SUPERPLOT multi.
     ;
     ; This structure is loaded into a common variable by 
     ; SUPERPLOT_INIT.
     ;- ================================================================
     FUNCTION SUPERPLOT_DEFAULTS

       ; NOTE:  Some of the options here are used for SUPERPLOT_MULTI.

       plot_struct  = { $
                        psym:-1 $
                        ,linestyle:0 $
                        ,thick:1.2E $

                        ,isotropic:0 $

                        ,background:MIR_COLOR('white') $

                        ,symbol_color:-1L $
                        ,symbol_thick:-1E $
                        ,symsize:1E $

                        ,axis_color:0L $
                        ,axis_thick:1E $
                        ,ticklen:0.02E $
                        ,ticklen_inches:-1 $

                        ,xtitle:'' $
                        ,xrange:[0D,0D] $
                        ,xstyle:1 $
                        ,xlog:0 $
                        ,xthick:2E $
                        ,xticks:0L $
                        ,xminor:0L $
                        ,xmajor:0L $
                        ,xticklen:0E $
                        ,xtickinterval:0.0D $
                        ,xtickname:STRARR(60) $
                        ,xtickv:DBLARR(60) $
                        ,xgridstyle:0 $
                        ,xtickformat:'' $

                        ,ytitle:'' $
                        ,yrange:[0D,0D] $
                        ,ystyle:0 $
                        ,ylog:0 $
                        ,ythick:2E $
                        ,yticks:0L $
                        ,yminor:0L $
                        ,ymajor:0L $
                        ,yticklen:0E $
                        ,ytickinterval:0.0D $
                        ,ytickname:STRARR(60) $
                        ,ytickv:DBLARR(60) $
                        ,ygridstyle:0 $
                        ,ytickformat:'' $


                        ,title:'' $
                        ,subtitle:'' $

                        ,charsize:1.1E $
                        ,xcharsize:1.0E $
                        ,ycharsize:1.0E $
                        ,charthick:1.0E $
                        ,font_type:2 $

                        ,alignment:0.0D $

                        ,normal:0 $
                        ,data:0 $
                        ,device:0 $

                        ,plotnum:-1 $
                        ,plot_ysize:0E $
                        ,below_last:0 $
                        ,above_last:0 $

                        ,suppress_margin_top:0 $
                        ,suppress_margin_bottom:0 $
                        
                        ,legend_label:'' $
                        ,legend_charsize:0.9E $
                        ,legend_charthick:0E $
                        ,legend_x:0.25E $
                        ,legend_y:0.80E $
                        ,legend_spacing_x:0E $
                        ,legend_spacing_y:0E $
                        ,legend_placement:'' $
                        ,legend_position:[0E,0E] $

                        ,error_color:-1L $
                        ,error_thick:-1E $
                        ,error_width:0.01E $

                        ,oplot:0 $
                        ,xyouts:0 $
                        ,plots:0 $
                        ,nodata:0 $
                        ,errorplot:0 $
                        ,erase:0 $
                        
                        ,decomposed:1 $
                        
                        ,pdf:0 $
                        ,postscript:0 $
                        ,leaveopen:0 $
                        ,filename:'' $
                        ,dialog:0 $
                        ,xsize:0D $
                        ,ysize:0D $
                        ,inches:1 $
                      }
       
       RETURN, plot_struct
       
     END ; FUNCTION SUPERPLOT_DEFAULTS



     ;+ ================================================================
     ; PURPOSE:
     ;   Check the current options and add any missing options.
     ;
     ; DESCRIPTION:
     ;   There are a number of options than cannot be added to
     ;   the structure in <SUPERPLOT_DEFAULTS> because they are of
     ;   variable length.
     ;
     ;   This procedure provides a way of dealing with these options.
     ;- ================================================================
     PRO SUPERPLOT_CHECK_DEFAULTS, options $
                                   ,user_options $
                                   ,keyword_options $
                                   ,INTERNAL=internal

       internal = KEYWORD_SET(internal)

       IF ~ISA(user_options) THEN user_options = {NULL}
       IF ~ISA(keyword_options) THEN keyword_options = {NULL}

       CASE 1 OF
         HAS_TAG(keyword_options, 'COLOR'): color = keyword_options.color
         HAS_TAG(user_options, 'COLOR'): color = user_options.color
         ELSE: color = 0L
       ENDCASE
       
       ; Convert any color strings to RGB.
       IF ISA(color, 'STRING') THEN BEGIN
         color = MIR_COLOR(color)
       ENDIF


       ; I want to be able to use this internally as well as allow
       ; it to be called externally (for example from <SUPERPLOT_MULTI>.
       ;
       ; Internally I need to not use the tag "COLOR" so I can override it
       ; later.
       IF internal THEN BEGIN
         options = CREATE_STRUCT(options, {superplot_color:color})
       ENDIF ELSE BEGIN
         options = CREATE_STRUCT(options, {color:color})
       ENDELSE

     END ;PRO SUPERPLOT_CHECK_DEFAULTS



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Allows [SUPERPLOT_SWITCH] to be controled though an input
     ;   structure.
     ;
     ; PROGMMING NOTES:
     ;   This routine should not be called directly by the user.
     ;   it is mean to be used internally by [SUPERPLOT].
     ;-=================================================================
     PRO SUPERPLOT_STRUCT, arg_1, arg_2, arg_3, arg_4 $
                           ,NO_ABSCISSA=no_abscissa $
                           ,GET_PATH=get_path $
                           ,OPTIONS=options $
                           ,QUIET=quiet
       
       ; NOTE: This routines assumes that the given options structure
       ;       has every tag defined in [SUPERPLOT_DEFAULTS]    

       SUPERPLOT_SWITCH, arg_1, arg_2, arg_3, arg_4 $
                         ,NO_ABSCISSA=no_abscissa $
                         ,GET_PATH=get_path $
                         ,QUIET=quiet $
                         ,_EXTRA=options

     END ;PRO SUPERPLOT_STRUCT



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Converts a set of keywords into an input structure.
     ;-=================================================================
     FUNCTION SUPERPLOT_KEYWORDS_TO_STRUCT, _EXTRA=keyword_struct
       
       IF ~ ISA(keyword_struct) THEN keyword_struct = {NULL}

       RETURN, keyword_struct
     END ;FUNCTION SUPERPLOT_KEYWORDS_TO_STRUCT




     ;+ ================================================================
     ;
     ; NAME:
     ;   SUPERPLOT_SWITCH
     ;
     ; PURPOSE:
     ;
     ;   This routine gives access to all the basic plot commands from 
     ;   a single procedure.
     ;
     ;   It also deals with:
     ;      Switching the color mode for screen output 
     ;        (the color mode  for postscript output is handled 
     ;        within [PSPLOT]).
     ;
     ;      Font handling.
     ;
     ;
     ; SYNTAX:
     ;   SUPERPLOT[, x][, y][, string]
     ;            [, /OPLOT][, /XYOUTS][, /PLOTS][, /ERRORPLOT]
     ;            [, /PS][, /PDF][, OPTIONS] 
     ;
     ;
     ; DESCRIPTION:
     ;   This routine is meant to be used internally by <SUPERPLOT>.
     ; 
     ;   Documentation can be found in the header for <SUPERPLOT>.
     ;
     ;
     ;   NOTE:  This routine uses the routine ERRORPLOT instead fo
     ;          ERRPLOT.  ERRORPLOT is a slight modifed version
     ;          or ERRPLOT that does not complain about unknown
     ;          keywords.
     ;
     ;
     ;
     ;- ================================================================
     PRO SUPERPLOT_SWITCH, arg_1, arg_2, arg_3, arg_4 $
                           ,NO_ABSCISSA=no_abscissa $
                           
                           ,CLOSE=close $
                           ,GET_PATH=get_path $

                           ,QUIET=quiet $
                           ,HELP=help $

                           ,_EXTRA=extra

       RESOLVE_ROUTINE, [ $
                          'psplot' $
                        ], /EITHER, /COMPILE_FULL_FILE, /NO_RECOMPILE

       IF KEYWORD_SET(help) THEN BEGIN
         INFO
         RETURN
       ENDIF

       ; Start the error handling
       CATCH, error
       IF error NE 0 THEN BEGIN
         CATCH, /CANCEL

         ; Restore the original device.
         IF ISA(old_device_name) THEN BEGIN
           SET_PLOT, old_device_name
         ENDIF

         ; Restore the original color mode.
         IF ISA(old_decomposed_status) THEN BEGIN
           DEVICE, DECOMPOSED=old_decomposed_status
         ENDIF

         MESSAGE, /REISSUE
       ENDIF



       ; ---------------------------------------------------------------
       ; Check input parameters.
       num_params = ISA(arg_1) + ISA(arg_2) + ISA(arg_3) + ISA(arg_4)

       IF num_params EQ 0 THEN BEGIN
          ; Close the postscript file if /CLOSE was set.
         IF KEYWORD_SET(close) THEN BEGIN
           ; If no postscript file is open this won't do anything.
           PSPLOT, /CLOSE
         ENDIF ELSE BEGIN
           MESSAGE, 'No input parameters given.', /CONTINUE
         ENDELSE
         RETURN
       ENDIF


       ; ---------------------------------------------------------------
       ; Set keyword defaults.
       ;
       ; Here the order of precedence is defined.
       erase = 0
       plots = 0
       xyouts = 0
       plot = 0
       errorplot = 0

       oplot = KEYWORD_SET(extra.oplot)

       CASE 1 OF
         KEYWORD_SET(extra.erase): erase = 1
         KEYWORD_SET(extra.xyouts): xyouts = 1
         KEYWORD_SET(extra.plots): plots = 1
         KEYWORD_SET(extra.errorplot): errorplot = 1
         ELSE: plot = 1
       ENDCASE

       extra.postscript = KEYWORD_SET(extra.postscript)
       extra.pdf = KEYWORD_SET(extra.pdf)
       IF extra.pdf THEN extra.postscript =1

       extra.decomposed = KEYWORD_SET(extra.decomposed)


       ; ---------------------------------------------------------------
       ; Rename the input variables based on the keyword and number of parameters.
       CASE 1 OF
         xyouts: BEGIN
           CASE num_params OF
             1: BEGIN
               string = arg_1
             END
             3: BEGIN
               x = arg_1
               y = arg_2
               string = arg_3
             END
             ELSE: BEGIN
               MESSAGE, 'Incorrect number of input parameters for /XYOUTS.'
             ENDELSE
           ENDCASE
         END
         errorplot: BEGIN
           CASE num_params OF
             2: BEGIN
               error_min = arg_1
               error_max = arg_2
               x = INDGEN(N_ELEMENTS(error_min))
             END
             3: BEGIN
               error_min = arg_2
               error_max = arg_3
               x = INDGEN(N_ELEMENTS(error_min))
             END
             4: BEGIN
               x = arg_1
               error_min = arg_3
               error_max = arg_4
             END
             ELSE: BEGIN
               MESSAGE, 'Incorrect number of input parameters for /ERRORPLOT.'
             ENDELSE
           ENDCASE
         END
         ELSE: BEGIN
           ; If the keyword /NO_ABSCISSA is set, we assume that
           ; arguments cannot be determined from the number of arguments.
           IF num_params EQ 1 OR KEYWORD_SET(no_abscissa) THEN BEGIN
             y = arg_1
             x = INDGEN(N_ELEMENTS(y))
           ENDIF ELSE BEGIN
             x = arg_1
             y = arg_2
             IF num_params GT 2 and ~ KEYWORD_SET(quiet) THEN BEGIN
               MESSAGE, 'To many input parameters. Expected two or less.', /CONTINUE
             ENDIF
           ENDELSE

           ; Make sure that the inputs are arrays.
           IF (SIZE(x, /DIM))[0] EQ 0 THEN BEGIN
             x = [x]
           ENDIF
           IF (SIZE(y, /DIM))[0] EQ 0 THEN BEGIN
             y = [y]
           ENDIF


         ENDELSE
       ENDCASE


       ; ---------------------------------------------------------------
       ; Deal with tick marks.
       IF extra.yticks EQ 0 AND extra.ymajor NE 0 THEN BEGIN
         IF ISA(y) THEN BEGIN
           extra.yticks = extra.ymajor

           IF extra.ymajor GT 1 THEN BEGIN

             ; First we need to pick the best interval that is
             ; a muliple of 5, 4, 2 or 1
             IF extra.yrange[1] GT extra.yrange[0] THEN BEGIN
               y_min = extra.yrange[0]
               y_max = extra.yrange[1]
             ENDIF ELSE BEGIN
               y_min = MIN(y, MAX=y_max, /NAN)
             ENDELSE

             interval = (y_max - y_min)/extra.ymajor

             exp = FLOOR(ALOG10(ABS(MAX(interval, /ABS))))

             int = ROUND(interval/(10D^exp))
             factors = [10, 5, 4, 2, 1]
             min_diff = MIN(int - factors, min_index, /ABS)
             int = factors[min_index]

             interval = int * 10D^exp



             min_tick = y_min - (y_min MOD interval)
             num_tick = extra.ymajor+1


             ; With the  interval that we have chosen the number of 
             ; ticks reqested by the user may be too few or too
             ; many to extend the range of the data.
             ;
             ; Adjust the number of ticks here.
             ;
             ; Note: max_tick =  (num_tick-1)*interval + min_tick
             IF extra.ystyle EQ 1 THEN BEGIN
               WHILE (num_tick-1)*interval + min_tick LT y_max - interval DO BEGIN
                 num_tick += 1
               ENDWHILE

               WHILE (num_tick-1)*interval + min_tick GT y_max DO BEGIN
                 num_tick -= 1
               ENDWHILE
             ENDIF ELSE BEGIN
               WHILE (num_tick-1)*interval + min_tick LT y_max DO BEGIN
                 num_tick += 1
               ENDWHILE

               IF min_tick GT y_min THEN BEGIN
                 num_tick += 1
                 min_tick -= interval
               ENDIF
             ENDELSE
             
             extra.yticks = num_tick - 1
             ytickv = DINDGEN(num_tick)*interval + min_tick

             extra.ytickv = ytickv
           ENDIF
         ENDIF
       ENDIF

       IF extra.xticks EQ 0 AND extra.xmajor NE 0 THEN BEGIN
         IF ISA(x) THEN BEGIN
           extra.xticks = extra.xmajor

           IF extra.xmajor GT 1 THEN BEGIN

             ; First we need to pick the best interval that is
             ; a muliple of 5, 4, 2 or 1
             IF extra.xrange[1] GT extra.xrange[0] THEN BEGIN
               x_min = extra.xrange[0]
               x_max = extra.xrange[1]
             ENDIF ELSE BEGIN
               x_min = MIN(x, MAX=x_max, /NAN)
             ENDELSE

             interval = (x_max - x_min)/extra.xmajor

             exp = FLOOR(ALOG10(ABS(MAX(interval, /ABS))))

             int = ROUND(interval/(10D^exp))
             factors = [10, 5, 4, 2, 1]
             min_diff = MIN(int - factors, min_index, /ABS)
             int = factors[min_index]

             interval = int * 10D^exp



             min_tick = x_min - (x_min MOD interval)
             num_tick = extra.xmajor+1


             ; With the  interval that we have chosen the number of 
             ; ticks reqested by the user may be too few or too
             ; many to extend the range of the data.
             ;
             ; Adjust the number of ticks here.
             ;
             ; Note: max_tick =  (num_tick-1)*interval + min_tick
             IF extra.xstyle EQ 1 THEN BEGIN
               WHILE (num_tick-1)*interval + min_tick LT x_max - interval DO BEGIN
                 num_tick += 1
               ENDWHILE

               WHILE (num_tick-1)*interval + min_tick GT x_max DO BEGIN
                 num_tick -= 1
               ENDWHILE
             ENDIF ELSE BEGIN
               WHILE (num_tick-1)*interval + min_tick LT x_max DO BEGIN
                 num_tick += 1
               ENDWHILE

               IF min_tick GT x_min THEN BEGIN
                 num_tick += 1
                 min_tick -= interval
               ENDIF
             ENDELSE
             
             extra.xticks = num_tick - 1
             xtickv = DINDGEN(num_tick)*interval + min_tick

             extra.xtickv = xtickv
           ENDIF
         ENDIF
       ENDIF


       ; ---------------------------------------------------------------
       ; Deal with xrange and yrange.
       ; Make sure to do this after the tickmark handling.
       IF extra.ystyle EQ 0 THEN BEGIN
         IF ISA(y) THEN BEGIN
           extra.ystyle = 1
           IF ARRAY_EQUAL(extra.yrange, 0) THEN BEGIN
             y_min = MIN(y, MAX=y_max, /NAN)
           
             IF extra.ylog EQ 0 THEN BEGIN
               yrange_span = y_max - y_min
               extra.yrange = [y_min-yrange_span*0.1, y_max+yrange_span*0.1]
             ENDIF ELSE BEGIN
               yrange_span = alog10(y_max) - alog10(y_min)
               extra.yrange = [y_min/(yrange_span), y_max*(yrange_span)]
             ENDELSE
           ENDIF
         ENDIF
       ENDIF

       IF extra.xstyle EQ 0 THEN BEGIN
         extra.xstyle = 1
         IF ARRAY_EQUAL(extra.xrange, 0) THEN BEGIN
           x_min = MIN(x, MAX=x_max, /NAN)
           xrange_span = x_max - x_min
           
           extra.xrange = [x_min-xrange_span*0.1, x_max+xrange_span*0.1]
         ENDIF
       ENDIF


       ; ---------------------------------------------------------------
       ; Check the device.
       old_device_name = !d.name
       IF !d.name EQ 'PS' THEN extra.postscript=1



       ; ---------------------------------------------------------------
       ; Set up for decomposed color. 
       ; If possible save the current state so we can restore it later.
       ; Let psplot do it's own color handling though.
       IF (~ extra.postscript) AND extra.decomposed THEN BEGIN
         DEVICE, GET_DECOMPOSED=old_decomposed_status
         DEVICE, DECOMPOSED=extra.decomposed
       ENDIF


       ; ---------------------------------------------------------------
       ; Deal with color arrays.
       IF plots THEN BEGIN
         extra = CREATE_STRUCT(extra, {color:extra.superplot_color})
       ENDIF ELSE BEGIN
         extra = CREATE_STRUCT(extra, {color:extra.superplot_color[0]})
       ENDELSE



       ; ---------------------------------------------------------------
       ; Convert titles from TEX
       extra.title = TEXTOIDL_EXTRA(extra.title, POSTSCRIPT=extra.postscript)
       extra.ytitle = TEXTOIDL_EXTRA(extra.ytitle, POSTSCRIPT=extra.postscript)
       extra.xtitle = TEXTOIDL_EXTRA(extra.xtitle, POSTSCRIPT=extra.postscript)      



       ; ---------------------------------------------------------------
       ; Setup font defaults:
       ;   x: Hershey font
       ;  ps: PS device font

       ; If font_type == 2 then use the following:
       ;   For postscript output use the device font.
       ;   For Display output use Hershey fonts.
       IF extra.font_type NE 2 THEN BEGIN
         font = extra.font_type
       ENDIF ELSE BEGIN
         IF extra.postscript THEN BEGIN
           font = 0
             font_string = '!3'
             IF ISA(string) THEN string = font_string + string 
             extra.title = font_string + extra.title
             extra.xtitle = font_string + extra.xtitle
             extra.ytitle = font_string + extra.ytitle 
         ENDIF ELSE BEGIN
             font = -1
             font_string = '!3'
             IF ISA(string) THEN string = font_string + string 
             extra.title = font_string + extra.title
             extra.xtitle = font_string + extra.xtitle
             extra.ytitle = font_string + extra.ytitle 
           ENDELSE
       ENDELSE
       
       ; ---------------------------------------------------------------
       ; Erase the current window.
       IF erase AND (~ extra.postscript) AND (!d.window NE -1) THEN BEGIN
         ERASE, extra.background
       ENDIF

       ; ---------------------------------------------------------------
       ; Now plot using the aprropriate command.

       ; Parameter validity is checked in the individual routines.
       ; NOTE: The keyword precedence here is important. Everything
       ;       in superplot assumes that the order of precendce is:
       ;         xyouts
       ;         plots
       ;         errorplot
       ;         oplot
       ;         plot
       ;
       CASE 1 OF
         xyouts AND extra.postscript: BEGIN
           CASE num_params OF
             1: BEGIN
               PSXYOUTS, string $
                         ,FONT=font $
                         ,CLOSE=close $
                         ,_EXTRA=extra
             END
             3: BEGIN
               PSXYOUTS, x, y, string $
                         ,FONT=font $
                         ,CLOSE=close $
                         ,_EXTRA=extra
             END
           ENDCASE
         END
         plots AND extra.postscript: BEGIN
           PSPLOTS, x, y $
                    ,NOCLIP=0 $
                    ,CLOSE=close $
                    ,_EXTRA=extra
         END
         errorplot AND extra.postscript: BEGIN
           PSERRORPLOT, x, error_min, error_max $
                        ,WIDTH=extra.error_width $
                        ,CLOSE=close $
                        ,GET_PATH=get_path $
                        ,_EXTRA=extra
         END
         oplot AND extra.postscript: BEGIN
           PSOPLOT, x, y $
                    ,CLOSE=close $
                    ,_EXTRA=extra
         END
         extra.postscript: BEGIN
           PSPLOT, x, y $
                   ,FONT=font $
                   ,GET_PATH=get_path $
                   ,_EXTRA=extra
         END
         xyouts: BEGIN
           CASE num_params OF
             1: BEGIN
               XYOUTS, string $
                       ,FONT=font $
                       ,_EXTRA=extra
             END
             3: BEGIN
               XYOUTS, x, y, string $
                       ,FONT=font $
                       ,_EXTRA=extra
             END
           ENDCASE
         END
         plots: BEGIN
           PLOTS, x, y $
                  ,NOCLIP=0 $
                  ,_EXTRA=extra 
         END
         errorplot: BEGIN
           ERRORPLOT, x, error_min, error_max $
                    ,WIDTH=extra.error_width $
                    ,_EXTRA=extra 
         END
         oplot: BEGIN
           OPLOT, x, y $
                  ,_EXTRA=extra           
         END
         plot: BEGIN
           PLOT, x, y $
                 ,FONT=font $
                 ,_EXTRA=extra 
         END
         ELSE:
       ENDCASE
  


       ; Restore the original color mode if we set it before.
       IF (~ extra.postscript) AND ISA(old_decomposed_status) THEN BEGIN
         DEVICE, DECOMPOSED=old_decomposed_status
       ENDIF



     END ;PRO SUPERPLOT_SWITCH


     ;+ ================================================================
     ;
     ; NAME:
     ;   SUPERPLOT_ERASE
     ;
     ; PURPOSE:
     ;   Erase the current window.
     ;
     ;
     ;- ================================================================
     PRO SUPERPLOT_ERASE, arg_1, arg_2, arg_3, arg_4 $
                         ,NO_ABSCISSA=no_abscissa $
                         ,OPTIONS=options $
                         ,QUIET=quiet

       IF KEYWORD_SET(help) THEN BEGIN
         INFO
         RETURN
       ENDIF

       ; For now, only erase if erase was explicitly requested.
       IF options.erase NE 1 THEN RETURN
       
       SUPERPLOT_STRUCT, arg_1, arg_2, arg_3, arg_4 $
                         ,NO_ABSCISSA=no_abscissa $
                         ,OPTIONS=options $
                         ,QUIET=quiet

     END ;PRO SUPERPLOT_ERASE



     ;+ ================================================================
     ; PURPOSE:
     ;   More options for plotting the axis
     ;
     ;- ================================================================
     PRO SUPERPLOT_AXIS, arg_1, arg_2, arg_3, arg_4 $
                         ,NO_ABSCISSA=no_abscissa $
                         ,GET_PATH=get_path $
                         ,OPTIONS=options $
                         ,QUIET=quiet

       ; First check the plot type.
       ; Don't plot axis unless necessary
       IF options.oplot OR options.plots OR options.xyouts THEN RETURN


       ; Setup for the axis plot.
       color = options.superplot_color
       xthick = options.thick
       ythick = options.thick
       nodata = options.nodata
       errorplot = options.errorplot
       erase = options.erase

       IF options.axis_color GE 0 THEN BEGIN
         options.superplot_color[0] = options.axis_color
       ENDIF ELSE BEGIN
         options.superplot_color[0] = !p.color
       ENDELSE

       IF options.axis_thick GE 0 THEN BEGIN
         xthick = options.axis_thick
         ythick = options.axis_thick
       ENDIF

       options.nodata = 1
       options.errorplot = 0

       SUPERPLOT_STRUCT, arg_1, arg_2 $
                         ,NO_ABSCISSA=no_abscissa $
                         ,GET_PATH=get_path $
                         ,OPTIONS=options $
                         ,QUIET=quiet



       options.superplot_color = color
       options.xthick = xthick
       options.ythick = ythick
       options.nodata = nodata
       options.errorplot = errorplot
       options.erase = erase


     END ; PRO SUPERPLOT_AXIS



     ;+ ================================================================
     ; PURPOSE:
     ;   More options for plotting the data.
     ;   It is assumed that the axis has already been plotted.
     ;
     ;- ================================================================
     PRO SUPERPLOT_DATA, arg_1, arg_2, arg_3, arg_4 $
                         ,NO_ABSCISSA=no_abscissa $
                         ,OPTIONS=options $
                         ,QUIET=quiet

       IF options.nodata THEN RETURN

       ; Always alow SUPERPLOT_SYMBOL to deal with symbols.
       ; So if only symbols are to be plotted, don't do anything.
       IF options.psym GE 1 AND options.psym LE 9 THEN RETURN
      
       oplot = options.oplot
       plots = options.plots
       psym = options.psym
       errorplot = options.errorplot
       erase = options.erase

       ; Assume that the axis has already been plotted.
       IF ~ options.xyouts THEN BEGIN
         options.oplot = 0
         options.plots = 1
       ENDIF

       ; Only plot the line, unless hystigram mode is specified.
       IF options.psym NE 10 THEN options.psym = 0

       ; Don't plot error bars
       options.errorplot = 0

       SUPERPLOT_STRUCT, arg_1, arg_2, arg_3, arg_4 $
                         ,NO_ABSCISSA=no_abscissa $
                         ,OPTIONS=options $
                         ,QUIET=quiet


       options.oplot = oplot
       options.plots = plots
       options.psym = psym
       options.errorplot = errorplot
       options.erase = erase

     END ; PRO SUPERPLOT_DATA


     ;+ ================================================================
     ; PURPOSE:
     ;   More options for plotting the axis
     ;
     ;- ================================================================
     PRO SUPERPLOT_SYMBOL, arg_1, arg_2, arg_3, arg_4 $
                           ,NO_ABSCISSA=no_abscissa $
                           ,OPTIONS=options $
                           ,QUIET=quiet

       IF options.nodata THEN RETURN
       IF options.xyouts THEN RETURN

       ; Check if there are symbols to be plotted.
       IF options.psym EQ 0 or options.psym EQ 10 THEN RETURN

       
      
       oplot = options.oplot
       plots = options.plots
       psym = options.psym
       errorplot = options.errorplot
       color = options.superplot_color
       thick = options.thick
       erase = options.erase

       ; Assume that the axis has already been plotted.
       IF ~ options.xyouts THEN BEGIN
         options.oplot = 0
         options.plots = 1
       ENDIF

       ; Only plot symbols, not lines.
       options.psym = ABS(options.psym)

       ; Don't plot error bars
       options.errorplot = 0

       IF options.symbol_color GT 0 THEN options.superplot_color[0] = options.symbol_color
       IF options.symbol_thick GT 0 THEN options.thick = options.symbol_thick

        
       SUPERPLOT_STRUCT, arg_1, arg_2 $
                         ,NO_ABSCISSA=no_abscissa $
                         ,OPTIONS=options $
                         ,QUIET=quiet
 

       options.oplot = oplot
       options.plots = plots
       options.psym = psym
       options.superplot_color = color
       options.thick = thick
       options.errorplot = errorplot
       options.erase = erase

     END ; PRO SUPERPLOT_SYMBOL




     ;+ ================================================================
     ; PURPOSE:
     ;   Plot error bars.
     ;
     ;- ================================================================
     PRO SUPERPLOT_ERROR, arg_1, arg_2, arg_3, arg_4 $
                          ,NO_ABSCISSA=no_abscissa $
                          ,OPTIONS=options $
                          ,QUIET=quiet


       IF ~ options.errorplot THEN RETURN
       IF options.nodata THEN RETURN
       IF options.xyouts THEN RETURN

       color = options.superplot_color
       thick = options.thick
       erase = options.erase


       IF options.error_color GT 0 THEN options.superplot_color[0] = options.error_color
       IF options.error_thick GT 0 THEN options.thick = options.error_thick
         
       SUPERPLOT_STRUCT, arg_1, arg_2, arg_3, arg_4 $
                         ,NO_ABSCISSA=no_abscissa $
                         ,OPTIONS=options $
                         ,QUIET=quiet

       options.superplot_color = color
       options.thick = thick
       options.erase = erase

     END ; PRO SUPERPLOT_ERROR



     ;+ ================================================================
     ; PURPOSE:
     ;   More options for plotting the axis
     ;
     ;- ================================================================
     PRO SUPERPLOT_LEGEND, OPTIONS=options $
                           ,QUIET=quiet

       IF options.xyouts THEN RETURN

       ; Check if any labels were given.
       IF options.legend_label EQ '' THEN RETURN

       xyouts = options.xyouts
       charsize = options.charsize
       normal = options.normal
       erase = options.erase

       options.xyouts = 1
       IF options.legend_charsize GE 0 THEN options.charsize = options.legend_charsize
       IF ~ options.data AND ~ options.device THEN options.normal = 1

       ; Split the label by newlines.
       labels = STRSPLIT(options.legend_label, STRING(10B), /EXTRACT, /PRESERVE_NULL)

       num_labels = N_ELEMENTS(labels)
       x = FINDGEN(num_labels) * options.legend_spacing_x + options.legend_x
       y = FINDGEN(num_labels) * options.legend_spacing_y + options.legend_y

       SUPERPLOT_STRUCT, x, y, labels $
                         ,OPTIONS=options $
                         ,QUIET=quiet

       options.xyouts = xyouts
       options.charsize = charsize
       options.normal = normal
       options.erase = erase

     END ; PRO SUPERPLOT_LEGEND



     ;+=================================================================
     ; NAME:
     ;   SUPERPLOT
     ;
     ; PURPOSE:
     ;   This routine allows for an easer solution to plotting in IDL.
     ;
     ;   SUPERPLOT gives access to all the basic plot commands from 
     ;   a single procedure.
     ;
     ;   It also handles ploting to postscript or pdf files using
     ;   a standard interface.
     ;
     ;
     ;
     ;   It also allows for more control of the plot output
     ;   from a single command:
     ;     - Can plot errorbars, use /ERRORPLOT
     ;
     ;     - Can change the color and thickness of
     ;       of the symbols, errorbars and axis independent of the
     ;       line.
     ;
     ;     - Can easly switch between truecolor and indexed color.
     ;
     ;     - Can plot a legend.
     ;
     ;  
     ;
     ; SYNTAX:
     ;   SUPERPLOT[, x], y[, string][OPTIONS=struct]
     ;            [, /OPLOT][, /XYOUTS][, /PLOTS][, /ERRORPLOT]
     ;            [, /PS][, /PDF][, KEYWORDS]
     ;
     ;
     ;
     ; KEYWORDS:
     ;
     ;   Keywords will be passed to the underlying routines.
     ;   So all graphics keywords, as well as most ploting
     ;   routine specific keywords will be accepted.
     ;
     ;   Below are keywords specific to SUPERPLOT that add additonal
     ;   functionality to the stardard IDL plotting routines.
     ;
     ;
     ;   /POSTSCRIPT
     ;       Save output to a postscript file.
     ;       A dialog will pop up to allow the file to be 
     ;       saved. To overplot, or for multiple plots, 
     ;       use /LEAVEOPEN.  
     ;
     ;       See usage example below.
     ;       See PSPLOT, /HELP for postscript plotting options.
     ;
     ;   /PDF
     ;       Plot to pdf.  Usage is the same as for /POSTSCRIPT.
     ;       This requires the command 'epstopdf' to be available
     ;       on your computer.  Both a *.ps and a *.pdf will be
     ;       be created.
     ;
     ;   /LEAVEOPEN
     ;       Leave the postscript file open.  This is used to overplot
     ;       or for multiple plots.
     ;       If this keyword is not set, then the postscript file will
     ;       be closed and saved after the plot is complete.
     ;
     ;   /CLOSE
     ;      Close the postscript file.  If no input parameters are
     ;      given then the postcript file will be closed immideatly.
     ;      Otherwise the file will be closed after the plotting.
     ;  
     ;      his will override /LEAVEOPEN.
     ;
     ;
     ;
     ;   DECOMPOSED
     ;      Set this keyword to 1 to use true color, 0 to use indexed
     ;      color.
     ;      The default is to use the current device setting.
     ;
     ;   AXIS_COLOR
     ;       The color for the axis.
     ;       This has no effect if /OPLOT, /PLOTS or /XYOUTS are set.
     ;
     ;   AXIS_THICK
     ;       Set the thickness for all axis.
     ;       This is equivilent to setting XTHICK & YTHICK
     ;
     ;   SYMBOL_COLOR
     ;       The color for the symbols.
     ;       This has no effect if PSYM is 0 or 10
     ;     
     ;   SYMBOL_THICK
     ;       The thickness for the symbols.
     ;
     ;   ERROR_COLOR
     ;       The color for the error bars.
     ;
     ;   ERRROR_THICK
     ;       The thickness for the error bars.
     ;
     ;   ERROR_WIDTH
     ;       <default: 0.01>
     ;       The width of the error bars. The value is in percent of
     ;       the window width.
     ;
     ;       In ERRORPLOT the width is controled thorough the keyword
     ;       WIDTH.  ERROR_WIDTH is used instead to avoid conflicts
     ;       with the XYOUTS keyword.
     ;
     ;   [XY]MAJOR = integer
     ;       The approximate number of major tick marks to display.
     ;       This keyword is used to give <SUPERPLOT> a hint
     ;
     ;
     ; DESCRIPTION:
     ;
     ;   Use as a replacement for PLOT, OPLOT, PLOTS, XYOUTS and ERRPLOT.
     ;   To choose which procedure to use use the keywords:
     ;   /OPLOT, /PLOTS, /XYOUTS and /ERRORPLOT.
     ;
     ;   Usage other wise is the same as for the routine specifed.
     ;
     ; 
     ;   This routine is particularly usefull when used from within
     ;   other applications:
     ;     - Allows for easy output to POSTSCRIPT and PDF files.
     ;
     ;     - Can be controled with an input structure.
     ;
     ;     - Can switch to overplot with an option.
     ;
     ;     - It will accept any keywords and swallow
     ;       those that are invalid.  
     ;       While this makes debuging more difficult, it also allows 
     ;       for more flexability, such as a loop where /OPLOT is
     ;       set after the first interation.
     ;
     ;     - Allows easy use of DECOMPOSED color.
     ;
     ;
     ; POSTSCRIPT OR PDF EXAMPLES:
     ;
     ;   To plot to a postscript or pdf file use the keywords
     ;     /POSTSCRIPT
     ;     /PDF
     ;
     ;   When plotting a dialog will be shown to allow the file to be 
     ;   Specified.
     ;
     ;   To overplot, or add multiple plots, to the postscript file
     ;   the /LEAVEOPEN keyword must be used.  
     ;   To close the file use /CLOSE or ommit /LEAVEOPEN from the 
     ;   last plot
     ;
     ;
     ;   Example 1: Simple plot to pdf.
     ;
     ;     SUPERPLOT, y, /PDF
     ;
     ;   Example 2: Plot to pdf with oplot.
     ;
     ;     SUPERPLOT, y,  /PDF, /LEAVEOPEN
     ;     SUPERPLOT, y2, /OPLOT, /PDF, /LEAVEOPEN
     ;     SUPERPLOT, y3, /OPLOT, /PDF
     ;
     ;   Example 3: Plot to pdf with oplot using /CLOSE.
     ;
     ;     SUPERPLOT, y,  /PDF, /LEAVEOPEN
     ;     SUPERPLOT, y2, /OPLOT, /PDF, /LEAVEOPEN
     ;     SUPERPLOT, y3, /OPLOT, /PDF, /LEAVEOPEN
     ;     SUPERPLOT, /CLOSE
     ;
     ;
     ;
     ;
     ;-=================================================================
     PRO SUPERPLOT, arg_1, arg_2, arg_3, arg_4 $
                    ,OPTIONS=options_in $

                    ,CLOSE=close $
                    ,GET_PATH=get_path $

                    ,QUIET=quiet $
                    ,DEBUG=debug $
                    ,HELP=help $

                    ,_REF_EXTRA=extra

       IF KEYWORD_SET(help) THEN BEGIN
         INFO
         RETURN
       ENDIF


       ; Start the error handling
       CATCH, error
       IF error NE 0 THEN BEGIN
         CATCH, /CANCEL

         IF KEYWORD_SET(debug) THEN BEGIN
           MESSAGE, /REISSUE
         ENDIF ELSE BEGIN
           ; Reissue the error from this level, stop at the calling level.
           ON_ERROR, 2
           error_message = STREGEX(!ERROR_STATE.MSG, '[[:alnum:]_]*(: )*(.*)', /EXTRACT, /SUBEXPR)
           MESSAGE, error_message[2]
         ENDELSE
       ENDIF



       ; Handle the /CLOSE keyword when no parameters are present
       IF KEYWORD_SET(close) THEN BEGIN
         IF N_PARAMS() EQ 0 THEN BEGIN
           SUPERPLOT_SWITCH, /CLOSE
           RETURN
         ENDIF
       ENDIF



       ; ---------------------------------------------------------------
       ; Convert any keywords into a structure.
       keyword_options = SUPERPLOT_KEYWORDS_TO_STRUCT(_EXTRA=extra)



       ; ---------------------------------------------------------------
       ; Create the options structure with defaults.
       
       ; Get the default structure.
       options = SUPERPLOT_DEFAULTS()
       ; Copy the given options over the default ones.
       IF ISA(options_in) THEN BEGIN
         STRUCT_ASSIGN, options_in, options, /NOZERO
       ENDIF

       ; Now copy any keyword options.  
       ; Note that this takes precedence.
       IF ~ IS_NULL(keyword_options) THEN BEGIN
         STRUCT_ASSIGN, keyword_options, options, /NOZERO
       ENDIF

       SUPERPLOT_CHECK_DEFAULTS, options, options_in, keyword_options, /INTERNAL


       ; NOTE: The routines below will not do anything unless
       ;       the proper options are set.  Therefore I do not
       ;       need to all of the options checking here.


       ; ---------------------------------------------------------------
       ; Do some setup for postscript printing.

       ; Save the leaveopen option.
       leaveopen = options.leaveopen
       options.leaveopen = 1


       ; If /ERRORPLOT is specifed then we need to explcitly tell
       ; SUPERPLOT_SWITCH whether or not abscissa were given.
       ; Also set /QUIET so we don't get messages about too many
       ; parameters.
       IF options.errorplot THEN BEGIN
         quiet = 1
         IF ~ ISA(arg_4) THEN BEGIN
           no_abscissa = 1
         ENDIF
       ENDIF


       ; ---------------------------------------------------------------
       ; Erase the current window.
       SUPERPLOT_ERASE, arg_1, arg_2, arg_3, arg_4 $
                       ,NO_ABSCISSA=no_abscissa $
                       ,OPTIONS=options $
                       ,QUIET=quiet


       ; ---------------------------------------------------------------
       ; Plot the axis.
       SUPERPLOT_AXIS, arg_1, arg_2, arg_3, arg_4 $
                       ,NO_ABSCISSA=no_abscissa $
                       ,GET_PATH=get_path $
                       ,OPTIONS=options $
                       ,QUIET=quiet


       ; ---------------------------------------------------------------
       ; Plot error bars
       SUPERPLOT_ERROR, arg_1, arg_2, arg_3, arg_4 $
                        ,NO_ABSCISSA=no_abscissa $
                        ,OPTIONS=options $
                        ,QUIET=quiet
       
       ; ---------------------------------------------------------------
       ; Plot the data.
       SUPERPLOT_DATA, arg_1, arg_2, arg_3, arg_4 $
                       ,NO_ABSCISSA=no_abscissa $
                       ,OPTIONS=options $
                       ,QUIET=quiet


       ; ---------------------------------------------------------------
       ; Plot the symbols
       SUPERPLOT_SYMBOL, arg_1, arg_2, arg_3, arg_4 $
                         ,NO_ABSCISSA=no_abscissa $
                         ,OPTIONS=options $
                         ,QUIET=quiet

       ; ---------------------------------------------------------------
       ; Plot the legend
       SUPERPLOT_LEGEND, OPTIONS=options $
                         ,QUIET=quiet
 

       ; ---------------------------------------------------------------
       ; Close the postscript file if necessary
       ; /CLOSE overrides /LEAVEOPEN
       IF ~ leaveopen OR KEYWORD_SET(close)THEN BEGIN
         SUPERPLOT_SWITCH, /CLOSE
       ENDIF

       
     END ;PRO SUPERPLOT
