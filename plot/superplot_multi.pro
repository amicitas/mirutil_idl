


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;   novimir.pablant@amicitas.com
;
; DATE:
;   2009-09
;
; VERSION:
;   0.0.0
;
; PURPOSE:
;   Make multiple plots on a single page.
;
; PROGRAMMING NOTES:
;   This routine ultimatly calls [SUPERPLOT].
;
;   The default plot settings are in the routine [SUPERPLOT_DEFAULTS]
;   in the file [File:superplot]
;
;-======================================================================




     ;+ =================================================================
     ;
     ; Close the postscript file, if open and LEAVEOPEN is not set..
     ; Reset the the plot device state.
     ;
     ;- =================================================================
     PRO SUPERPLOT_MULTI_CLEANUP, orig_plot_state, DEBUG=debug


       ; Close any open postscript files.
       SUPERPLOT_MULTI_CLOSE, DEBUG=debug


       IF ISA(orig_plot_state) THEN BEGIN
         !P.POSITION = orig_plot_state.position
         !P.NOERASE = orig_plot_state.noerase
         !P.BACKGROUND = orig_plot_state.background
         !P.COLOR = orig_plot_state.color
       ENDIF ELSE BEGIN
         !P.POSITION = 0
         !P.NOERASE = 0
         !P.BACKGROUND = 255
         !P.COLOR = 0
       ENDELSE

     END ;  PRO SUPERPLOT_MULTI_CLEANUP



     ;+ =================================================================
     ;
     ; Close the postscript file, if open.
     ;
     ;- =================================================================
     PRO SUPERPLOT_MULTI_CLOSE, DEBUG=debug

       ; Close any open postscript files.
       SUPERPLOT, /CLOSE, DEBUG=debug

     END ;  PRO SUPERPLOT_MULTI_CLOSE




     ;+=================================================================
     ;
     ; PURPOSE:
     ;   SUPERPLOT_MULTI allows for an easy way to place multiple plots
     ;   in one window page.  It correctly deals with spacing for
     ;   both x and ps output.
     ; 
     ; EXAMPLE:
     ;
     ;    ; Create a plotlist object
     ;    plotlist = OBJ_NEW('MIR_PLOTLIST')
     ; 
     ;    ; Add a plot
     ;    plotlist->APPEND, {y:FINDGEN(100) $
     ;                       ,psym:-1 $
     ;                       ,color:50}
     ;
     ;    ; Do the plot
     ;    SUPERPLOT_MULTI, plotlist
     ;
     ;    ; Destroy the plotlist object
     ;    plotlist->DESTROY
     ;   
     ;
     ;
     ; USAGE NOTES:
     ; 
     ;   OPLOT, PLOTS, XYOUTS:
     ;     If no plotnum is given, then the previous unnumbered
     ;       plot that was added will be used.
     ;       If no unnumbered primary plots were added, then the last
     ;       plot with the largest number will be used.
     ;     If these options are set and a plotnum is given,
     ;       then the given plotnum will be used.
     ;       If no primary plots were added with same plotnum, an error
     ;       will be raised.
     ;
     ;
     ;
     ; PAGE AND DEFAULT OPTIONS:
     ;   To set page options and defaults set the defaults in the plotlist.
     ;
     ;   For example:
     ;     plotlist->SET_DEFAULTS, {pdf:1 $
     ;                              ,landscape:1 $
     ;                              ,xrange:[0,100]}
     ;                              
     ;   The available page/default options are described in
     ;   [SUPERPLOT_PAGE_DEFAULTS].
     ;
     ;
     ; DESCRIPTION:
     ;
     ;   Plot sizing is done using 1 of two ways:
     ;     1. Constant margins are set.
     ;        This is achieved by setting thesis_size=1
     ;
     ;     2. Margins are dynamically created.
     ;
     ;
     ;
     ;-=================================================================
     ; Oh man is this ugly.  This needs to be cleaned up
     ;
     ; 
     PRO SUPERPLOT_MULTI, plotlist $
                          ,PAGE_OPTIONS=page_options_user $
                          ,GET_PATH=get_path $
                          ,HELP=help $
                          ,STOP=stop $
                          ,DEBUG=debug $
                          ,_EXTRA=_extra
       COMPILE_OPT STRICTARR


       RESOLVE_ROUTINE, [ $
                          'superplot' $
                        ], /EITHER, /COMPILE_FULL_FILE, /NO_RECOMPILE


       IF KEYWORD_SET(help) THEN BEGIN
         INFO
         RETURN
       ENDIF

       ; Get the default page options.
       page_options = SUPERPLOT_PAGE_DEFAULTS()

       ; Replace defaults with user values.  
       ; These options can be provided in three different ways:
       ;   1. Through the SET_DEFAULTS method of the plotlist.
       ;   2. Through the PAGE_OPTIONS keyword in SUPERPLOT_MULTI
       ;   3. As individual keywords given to SUPERPLOT_MULTI.
       STRUCT_ASSIGN, plotlist->GET_DEFAULTS(), page_options, /NOZERO

       IF ISA(page_options_user) THEN BEGIN
         STRUCT_ASSIGN, page_options_user, page_options, /NOZERO
       ENDIF

       IF ISA(_extra) THEN BEGIN
         STRUCT_ASSIGN, _extra, page_options, /NOZERO
       ENDIF


       ; ---------------------------------------------------------------
       ; Setup line thicknesses
       IF KEYWORD_SET(page_options.pdf) THEN BEGIN
         thick_scale = 2.0
       ENDIF ELSE BEGIN
         thick_scale = 1.0
       ENDELSE


       ; ---------------------------------------------------------------
       ; Adjust character sized
       IF KEYWORD_SET(page_options.pdf) THEN BEGIN
         charsize_scale = 1.0
       ENDIF ELSE BEGIN
         charsize_scale = 1.15
       ENDELSE




       ; ---------------------------------------------------------------
       ; Setup fixed margins and heights for thesis sizing.
       height_title_inch = 0.5

       margin_page_left_inch = 1.2
       margin_page_right_inch = 0.25

       margin_plot_top_inch = 0.4
       margin_plot_bottom_inch = 0.5

       legend_x_offset_inch = 0.25
       legend_y_offset_inch = 0.25



       ; ---------------------------------------------------------------
       ; Setup the defaults for thesis_size
       IF page_options.thesis_size THEN BEGIN
         page_options.ticklen = 0.125
         page_options.ticklen_inches = 1
       ENDIF




       ; ---------------------------------------------------------------
       ; First save the orignial state of the plotting device.
       orig_plot_state = { $
                           position:!P.POSITION $
                           ,noerase:!P.NOERASE $
                           ,background:!P.BACKGROUND $
                           ,color:!P.COLOR $
                         }
       

       ; ---------------------------------------------------------------
       ; Establish catch block.
       CATCH, error
       IF (error NE 0) THEN BEGIN 
         ; Cancel catch block.
         CATCH, /CANCEL
           
         ; Take cleanup actions
         SUPERPLOT_MULTI_CLEANUP, orig_plot_state, DEBUG=debug
         HEAP_GC, VERBOSE = KEYWORD_SET(debug)

         ; Print out error messages.
         PRINT, 'SUPERPLOT_MULTI encounterd an error.'

         IF KEYWORD_SET(debug) THEN BEGIN
           MESSAGE, /REISSUE_LAST
         ENDIF

         RETURN
       ENDIF 


       num_plots = plotlist->N_ELEMENTS()
       IF num_plots EQ 0 THEN BEGIN
         MESSAGE, 'Plotlist is empty, nothing to plot.', /CONTINUE
         RETURN
       ENDIF

       ; Create a list with the parameters for all plots,
       ; including default values for any parameters not given.
       paramlist = OBJ_NEW('MIR_LIST_IDL7')
       FOR ii=0, num_plots-1 DO BEGIN
         default_plotparam = SUPERPLOT_DEFAULTS()
         STRUCT_ASSIGN, plotlist->GET(ii), default_plotparam, /NOZERO
         SUPERPLOT_CHECK_DEFAULTS, default_plotparam, plotlist->GET(ii)
         paramlist->APPEND, default_plotparam, /NO_COPY
       ENDFOR




       ; ---------------------------------------------------------------
       ; Get number of plot windows.

       ; I need to find out how many of the given plots had a plotnum 
       ; specified, and how many do I need to generate a plot number
       ; for.  For now ignore any plots that will be overplotted.
       ;
       ; First loop through all of the plots and see how many separate
       ; plots are needed.
       specified_paramlist = OBJ_NEW('MIR_LIST_IDL7')
       
       num_auto_plot_windows = 0

       FOR ii=0,num_plots-1 DO BEGIN      
         plotparam = paramlist->GET(ii)

         IF ~plotparam.oplot AND ~plotparam.plots AND ~plotparam.xyouts THEN BEGIN
           IF plotparam.plotnum EQ -1 THEN BEGIN
             ; Count up how many windows are found with no plotnum given.
             num_auto_plot_windows += 1
           ENDIF ELSE BEGIN
             ; Save any plotnums given.
             specified_paramlist->APPEND, plotparam.plotnum
           ENDELSE
         ENDIF
       ENDFOR


       num_specified_plot_windows = specified_paramlist->N_ELEMENTS()
       IF num_specified_plot_windows NE 0 THEN BEGIN
         ; Get a sorted array containing all of the plot numbers that were specifed.
         specified_plot_windows = GET_UNIQUE(specified_paramlist->TO_ARRAY())
         num_specified_plot_windows = N_ELEMENTS(specified_plot_windows)
       ENDIF ELSE BEGIN
         specified_plot_windows = 0
       ENDELSE

       OBJ_DESTROY, specified_paramlist

       ; Find the total number of plot frames.
       ; This will either be determined by the maximum specified plotnum or by
       ; the total number of required plot frames.
       num_plot_windows = num_auto_plot_windows + num_specified_plot_windows
       num_plot_windows = num_plot_windows > (MAX(specified_plot_windows)+1)

       IF num_plot_windows EQ 0 THEN BEGIN
         MESSAGE, 'No plot windows found. All plots were specifed with OPLOT, PLOTS or XYOUTS.'
       ENDIF



       ; ---------------------------------------------------------------
       ; Generate a location map for the autoplot windows.
       ;
       ; The plots with given plot number may not be sequential.
       ; I want to go though and pick out any unused plotnumbers
       ; for the automatic numbering.
       IF num_auto_plot_windows GT 0 THEN BEGIN
         IF num_specified_plot_windows NE 0 THEN BEGIN
           autoplot_windows = INTARR(num_auto_plot_windows)
           iautoplot = 0
           FOR ii=0,num_plot_windows-1 DO BEGIN
             IF (WHERE(specified_plot_windows EQ ii))[0] EQ -1 THEN BEGIN
               autoplot_windows[iautoplot] = ii
               iautoplot += 1
               IF iautoplot EQ num_auto_plot_windows THEN BREAK
             ENDIF
           ENDFOR
         ENDIF ELSE BEGIN
           ; No plotnum was specifed for any plots.
           autoplot_windows = INDGEN(num_plot_windows)
         ENDELSE
       ENDIF ; num_auto_plot_windows GT 0




       ; ---------------------------------------------------------------
       ; Setup the ysize of each plot.

       plot_ysize_array = FLTARR(num_plot_windows)
       iautoplot = 0
       FOR ii=0,num_plots-1 DO BEGIN
         plotparam = paramlist->GET(ii)

         IF ~plotparam.oplot AND ~plotparam.plots AND ~plotparam.xyouts THEN BEGIN
           IF plotparam.plotnum EQ -1 THEN BEGIN
             ; Deal with the auto numbered plots.
             plot_ysize_array[autoplot_windows[iautoplot]] = plotparam.plot_ysize
             iautoplot += 1
           ENDIF ELSE BEGIN
             ; Deal with the numbered plots.
             plot_ysize_array[plotparam.plotnum] = plotparam.plot_ysize
           ENDELSE
         ENDIF
       ENDFOR

       ; ---------------------------------------------------------------
       ; Setup the default y extent.
       IF page_options.thesis_yfactor LT 0 THEN BEGIN
         page_options.thesis_yfactor = num_plot_windows
       ENDIF




       ; ---------------------------------------------------------------
       ; Setup page sizing and scale factors

       xsize = 0D
       ysize = 0D
       inches = 0D
       IF KEYWORD_SET(page_options.pdf) THEN BEGIN
         ; First deal with postscript plotting.


         ; Explicit sizes take precedence.
         IF (page_options.xsize GT 0) AND (page_options.ysize GT 0) THEN BEGIN
           inches = page_options.inches
           xsize = page_options.xsize
           ysize = page_options.ysize

         ENDIF ELSE BEGIN
           ; No explicit size is given.
           inches=1

           CASE 1 OF
             page_options.landscape: BEGIN
               letter=1
               landscape=1
               portrait=0
               xsize=11.0
               ysize=8.5
             END
             ELSE: BEGIN
               ; Default to portrait mode.
               letter=1
               landscape=0
               portrait=1
               xsize=8.5
               ysize=11.0
             END
           ENDCASE

           IF page_options.thesis_size THEN BEGIN
               letter = 0
               ; Set up the height of the page without the page title.
               ysize = (ysize - height_title_inch) * num_plot_windows / page_options.thesis_yfactor
               ; Add the height of the page title if necessary.
               IF page_options.pagetitle THEN BEGIN
                 ysize = ysize + height_title_inch
               ENDIF
             END
         ENDELSE
             
         ; Convert size into device units
         ; Postscript device units are in units of .0001cm
         device_xsize = xsize/(.001)
         device_ysize = ysize/(.001)

         IF KEYWORD_SET(inches) THEN BEGIN
           device_xsize = device_xsize*2.54
           device_ysize = device_ysize*2.54
         ENDIF


 
         ; The space scales allow conversion of normalized spacing
         ; to a spacing standard.  The standard spacing is based
         ; on an 8.5 x 11.0 inch page.
         x_space_scale = device_xsize/(8.5/0.001*2.54)
         y_space_scale = device_ysize/(11.0/0.001*2.54)

         ; Conversion from inches to normalized coordinates.
         ; Assume that inches is set.
         x_inches_to_norm = 1E/xsize
         y_inches_to_norm = 1E/ysize


         x_char_scale = 0.05
         y_char_scale = 0.015
         x_margin = 0.1


       ENDIF ELSE BEGIN
         ; Now deal with plotting to a window.

         ; First check that a window is open, if not, open one.
         IF !D.WINDOW EQ -1 THEN BEGIN
           WINDOWSET
         ENDIF


         device_xsize = !D.X_SIZE
         device_ysize = !D.Y_SIZE

         ; The space scales allow conversion of normalized spacing
         ; to a spacing standard.  The standard spacing is based
         ; on a 850x1100 pixel window.
         x_space_scale = !D.X_SIZE/850E
         y_space_scale = !D.Y_SIZE/1200E


         ; Conversion from inches to normalized coordinates.
         ; Assume 100 pixels per inch.
         x_inches_to_norm = 1E/!D.X_SIZE*100E
         y_inches_to_norm = 1E/!D.Y_SIZE*100E

         x_char_scale = 0.03
         y_char_scale = 0.012
         x_margin = 0.05

       ENDELSE

       ; ---------------------------------------------------------------
       ; Clear the plot window.
       IF (!d.name EQ 'X') AND ~ page_options.pdf THEN BEGIN
           ERASE, page_options.background
       ENDIF


       ; Deal with the charater sizes.
       x_char_scale = x_char_scale * charsize_scale
       y_char_scale = y_char_scale * charsize_scale

       ; ---------------------------------------------------------------
       ; Loop through and find the maximum charsize and ycharisze.
       ; We use these values to make sure that the same left margin
       ; is used for all plots.
       max_ycharsize = 0E
       max_charsize = 0E
       FOR ii=0,num_plots-1 DO BEGIN
         plotparam = paramlist->GET(ii)

         max_ycharsize = max_ycharsize > plotparam.ycharsize
         max_charsize = max_charsize > plotparam.charsize
       ENDFOR






       ; ---------------------------------------------------------------
       ; Do inital plot sizing


       ; POSITION [(x0,y0),(x1,y1)], lowerleft, upperright
       !P.POSITION = [0.00, 0.00, 0.00, 0.00]
       !P.NOERASE = 1
 

       ; First determine the size of the left margin.
       IF page_options.thesis_size THEN BEGIN
         ; Set the margin to fixed size, in inches.
         !P.POSITION[0] = margin_page_left_inch * x_inches_to_norm
       ENDIF ELSE BEGIN
         ; Dynamically adjust the left margin to fit all the titles.
         !P.POSITION[0] = (x_margin + ((max_charsize > max_ycharsize)-1)*x_char_scale)/x_space_scale $
                          * page_options.margin_left
       ENDELSE


       ; Now determine the size of the right margin.
       IF page_options.thesis_size THEN BEGIN
         ; Set the margin to fixed size, in inches.
         !P.POSITION[2] = 1E -  margin_page_right_inch * x_inches_to_norm
       ENDIF ELSE BEGIN
         ; Set an amount in normalized coordinates.
         !P.POSITION[2] = 0.98
       ENDELSE


       ; Setup for the page title.
       plot_geom_title = !P.POSITION
       plot_geom_title[1] = 1E
       IF page_options.pagetitle THEN BEGIN
         IF page_options.thesis_size THEN BEGIN
           plot_geom_title[1] -= height_title_inch * y_inches_to_norm
         ENDIF ELSE BEGIN
           plot_geom_title[1] -= 0.01-0.05/y_space_scale
         ENDELSE
       ENDIF

       ; This is the height of the page availible for the actual plots.
       ; That is, everything execept the title area.
       total_plot_ysize = plot_geom_title[1]


       ; This is how much of the plot area has already been used by plots
       ; that explicitly define their heights.
       used_plot_ysize = TOTAL(plot_ysize_array)
       ; This is list of the plots that do not explicitly define heigts.
       where_auto_ysize = WHERE(plot_ysize_array EQ 0E, num_auto_ysize)

       ; Now set the default size for the plots that did not have a ysize specification.
       ; The default is to split the available size evenly between the plots.
       ; We set a minimum size of 0.05 normalized units.
       IF where_auto_ysize[0] NE -1 THEN BEGIN
         plot_ysize_array[where_auto_ysize] = (.05 > (1 - used_plot_ysize)/ num_auto_ysize) $
           * total_plot_ysize
       ENDIF

       ; After creating all the plots, if the total requested size is more than the
       ; available size, then reduced all of the plots.
       IF TOTAL(plot_ysize_array) GT total_plot_ysize THEN BEGIN
         MESSAGE, 'Sum of plot ysizes exceeds one.', /CONTINUE
         plot_ysize_array = plot_ysize_array * total_plot_ysize/TOTAL(plot_ysize_array)
       ENDIF


       ; Here I set the y exents of all the plots, this includes margins and labels.
       IF num_plot_windows GT 0 THEN BEGIN

         ; bottom_y keeps track of the bottom of the last plot.
         bottom_y = total_plot_ysize

         plot_geom_all = FLTARR(4, num_plot_windows)
         FOR ii=0,num_plot_windows-1 DO BEGIN
           plot_geom_all[*,ii] = !P.POSITION
           plot_geom_all[3,ii] = bottom_y
           bottom_y = bottom_y - plot_ysize_array[ii]
           plot_geom_all[1,ii] = bottom_y
         ENDFOR
       ENDIF




       ; ---------------------------------------------------------------
       ; Create an array to keep track of y legend position.
       ;
       ; The idea here is that every time that a legend is requested
       ; on a plot we use this y value.  Then increment the y value
       ; by a given amount.
       ;
       legend_space = FLTARR(num_plot_windows)
       legend_y = FLTARR(num_plot_windows)
       legend_x = FLTARR(num_plot_windows)





       ; ---------------------------------------------------------------
       ; Now loop though all the plots and set the size of the axis
       ; box for each individual plot.
       ;
       ; This takes into account the character sizes.
       ;
       ; Here we also set up the spacing for the legend.
       iautoplot = 0
       plot_geom = FLTARR(4, num_plot_windows)

       FOR ii=0,num_plots-1 DO BEGIN
         plotparam = paramlist->GET(ii)

         IF ~plotparam.oplot AND ~plotparam.plots AND ~plotparam.xyouts THEN BEGIN
           
           ; Check if a plot number was specified. 
           ; If not get the next available plot number.
           IF plotparam.plotnum EQ -1 THEN BEGIN
             iplot = autoplot_windows[iautoplot]
             iautoplot += 1
           ENDIF ELSE BEGIN
             iplot = plotparam.plotnum
           ENDELSE

           ; Check to see if this plot has already been sized.
           ; This will happen if a given plot number is used twice.
           IF plot_geom[0,iplot] EQ plot_geom[2,iplot] THEN BEGIN
             ; Copy the values for the total plot area.
             plot_geom[*,iplot] = plot_geom_all[*,iplot]
             
             ; Find the space needed for the title and x labels.
             IF page_options.thesis_size THEN BEGIN
               margin_plot_top = margin_plot_top_inch * y_inches_to_norm
               margin_plot_bottom = margin_plot_bottom_inch * y_inches_to_norm
             ENDIF ELSE BEGIN
               margin_plot_bottom = (plotparam.charsize + plotparam.xcharsize) $
                                    * .025/y_space_scale
               margin_plot_top = (plotparam.charsize)*.025/y_space_scale
             ENDELSE
             
             IF plotparam.suppress_margin_top THEN margin_plot_top = 0
             IF plotparam.suppress_margin_bottom THEN margin_plot_bottom = 0
             
             plot_geom[3,iplot] -= margin_plot_top
             plot_geom[1,iplot] += margin_plot_bottom

             ;Setup spacing for the legend
             legend_space[iplot] = plotparam.legend_charsize * y_char_scale/y_space_scale

             IF ~ ARRAY_EQUAL(plotparam.legend_position, 0E) THEN BEGIN
               legend_y[iplot] =  plot_geom[1,iplot] + $
                                  (plot_geom[3,iplot] - plot_geom[1,iplot])*plotparam.legend_position[1]

               legend_x[iplot] =  plot_geom[0,iplot] + $
                                  (plot_geom[2,iplot] - plot_geom[0,iplot])*plotparam.legend_position[0]
             ENDIF ELSE BEGIN
               IF plotparam.legend_placement NE '' THEN BEGIN
                 legend_placement = STRUPCASE(plotparam.legend_placement)
               ENDIF ELSE BEGIN
                 legend_placement = 'UPPER LEFT'
               ENDELSE
               
               CASE legend_placement OF
                 'TOP LEFT': legend_placement = 'UPPER LEFT'
                 'TOP RIGHT': legend_placement = 'UPPER RIGHT'
                 'BOTTOM LEFT': legend_placement = 'LOWER LEFT'
                 'BOTTOM RIGHT': legend_placement = 'LOWER RIGHT'
                 ELSE:
               ENDCASE
               
               CASE legend_placement OF
                 'UPPER LEFT': BEGIN
                   legend_y[iplot] = plot_geom[3,iplot] - y_char_scale/y_space_scale*2
                   legend_x[iplot] = plot_geom[0,iplot] + legend_x_offset_inch * x_inches_to_norm
                 END
                 'LOWER LEFT': BEGIN
                   legend_y[iplot] = (plot_geom[1,iplot]+plot_geom[3,iplot])/2.
                   legend_x[iplot] = plot_geom[0,iplot] + legend_x_offset_inch * x_inches_to_norm
                 END
                 'UPPER RIGHT': BEGIN
                   legend_y[iplot] = plot_geom[3,iplot] - y_char_scale/y_space_scale*2
                   legend_x[iplot] = (plot_geom[0,iplot] + plot_geom[2,iplot])/2.
                 END
                 'LOWER RIGHT': BEGIN
                   legend_y[iplot] = (plot_geom[1,iplot]+plot_geom[3,iplot])/2
                   legend_x[iplot] = (plot_geom[0,iplot] + plot_geom[2,iplot])/2.
                 END
                 ELSE: BEGIN
                   ; Default to upper left
                   MESSAGE, 'Unknown legend placement:' + legend_placement, /INFORMATIONAL
                   legend_y[iplot] = plot_geom[3,iplot] - y_char_scale/y_space_scale*2
                   legend_x[iplot] = plot_geom[0,iplot] + legend_x_offset_inch * x_inches_to_norm
                 ENDELSE
               ENDCASE
             ENDELSE
           ENDIF
         ENDIF
       ENDFOR









       ; ---------------------------------------------------------------
       ; Choose the correct order to plot in.
       ; This is important so that OPLOTS can be added to SUPERPLOT 
       ; out of order.
       ; (Man I should have learned object graphics instead of this).
       ;
       ; Plots with out a plotnum specified will still need to be
       ; placed in with the correct order.
       plotnum_array = INTARR(num_plots)
       FOR ii=0,num_plots-1 DO BEGIN
         plotparam = paramlist->GET(ii)
         plotnum_array[ii] = plotparam.plotnum
       ENDFOR

       where_specified = WHERE(plotnum_array[*] GE 0)
       IF where_specified[0] NE -1 THEN BEGIN
         plot_sort_uni = GET_UNIQUE(plotnum_array[where_specified])
       ENDIF

       paramarray_order = INTARR(num_plots)
       paramarray_num = 0

       IF where_specified[0] NE -1 THEN BEGIN
         ; Loop over unique plot numbers.
         FOR ii=0,N_ELEMENTS(plot_sort_uni)-1 DO BEGIN
           plotnum = plot_sort_uni[ii]

           where_plot_num = WHERE(plotnum_array[*] EQ plotnum, num_with_plotnum)

           ; Check if there is one that does not have OPLOT, PLOTS, or XYOUTS.
           primary_plot_found = 0
           plots_oplot_found = 0
           ; Seach backwards, this will preserve the order if more than one
           ; plot with a given plotnumber does not have OPLOT, PLOTS, or XYOUTS.
           FOR jj=num_with_plotnum-1, 0, -1  DO BEGIN
             index = where_plot_num[jj]
             plotparam = paramlist->GET(index)

             IF ~plotparam.oplot $
                AND ~plotparam.plots  $
                AND ~plotparam.xyouts THEN BEGIN

               first_index = index
               primary_plot_found = 1
             ENDIF
             IF plotparam.oplot OR plotparam.plots THEN BEGIN
               plots_oplot_found = 1
             ENDIF
           ENDFOR

             
           IF primary_plot_found THEN BEGIN
             paramarray_order[paramarray_num] = first_index
             IF num_with_plotnum GT 1 THEN BEGIN
               paramarray_order[paramarray_num+1:paramarray_num+num_with_plotnum-1] = $
                 where_plot_num[WHERE(where_plot_num NE first_index)]
             ENDIF
           ENDIF ELSE BEGIN
             IF plots_oplot_found THEN BEGIN
               MESSAGE, 'OPLOT or PLOTS found with a designated plotnum, but primary plot not found.'
             ENDIF
             paramarray_order[paramarray_num:paramarray_num+num_with_plotnum-1] = $
               where_plot_num
           ENDELSE

           paramarray_num += num_with_plotnum
         ENDFOR
       ENDIF
        
       where_not_specified = WHERE(plotnum_array[*] EQ -1)
       IF where_not_specified[0] NE -1 THEN BEGIN
         paramarray_order[paramarray_num:num_plots-1] = $
           where_not_specified
       ENDIF

       ;PRINT, 'PARAMARRAY_ORDER:' ,paramarray_order       


       ; ---------------------------------------------------------------
       ; Begin ploting
       ;
       ;
       ; ---------------------------------------------------------------
       iautoplot = 0
       FOR ii=0,num_plots-1 DO BEGIN

         ; Get the next plot to plot, in the order given in paramarray_order.
         plotparam = paramlist->GET(paramarray_order[ii])
         plotdata = plotlist->GET(paramarray_order[ii])
         
         ; Check if a plot number was specified.  
         ; If not get the next available plot number.
         IF plotparam.plotnum EQ -1 THEN BEGIN
           IF ~plotparam.oplot AND ~plotparam.plots AND ~plotparam.xyouts THEN BEGIN
             iplot = autoplot_windows[iautoplot]
             iautoplot += 1
           ENDIF
         ENDIF ELSE BEGIN
           iplot = plotparam.plotnum
         ENDELSE

         ; Deal with /SINGLE_XTITLE
         xtitle = plotparam.xtitle
         IF page_options.single_xtitle THEN BEGIN
           IF iplot NE num_plot_windows-1 THEN BEGIN
             plotparam.xtitle = ''
           ENDIF
         ENDIF
            

         ; Set the position of the plot.
         !P.POSITION = plot_geom[*,iplot]
         
         ; Do some special handling for xyouts
         IF plotparam.xyouts THEN BEGIN
           plotdata.x = plotdata.x *(!P.POSITION[2]-!P.POSITION[0]) + !P.POSITION[0]
           plotdata.y = plotdata.y *(!P.POSITION[3]-!P.POSITION[1]) + !P.POSITION[1]
         ENDIF
         

         ; Allow for a global setting for xrange and yrange.
         IF ~ARRAY_EQUAL(page_options.xrange, [0D,0D]) THEN BEGIN
           plotparam.xrange = page_options.xrange
         ENDIF

         IF ~ARRAY_EQUAL(page_options.yrange, [0D,0D]) THEN BEGIN
           plotparam.yrange = page_options.yrange
         ENDIF


         ; Allow global color settings.
         IF page_options.decomposed GE 0 THEN BEGIN
           plotparam.decomposed = page_options.decomposed
         ENDIF

             
         ; Adjust thicknesses and charsizes
         plotparam.charsize = plotparam.charsize * charsize_scale
         plotparam.xcharsize = plotparam.xcharsize * charsize_scale
         plotparam.ycharsize = plotparam.ycharsize * charsize_scale
         plotparam.legend_charsize = plotparam.legend_charsize * charsize_scale

         IF plotparam.thick GT 0 THEN BEGIN
           plotparam.thick = plotparam.thick * thick_scale
         ENDIF
         IF plotparam.xthick GT 0 THEN BEGIN
           plotparam.xthick = plotparam.xthick * thick_scale
         ENDIF
         IF plotparam.ythick GT 0 THEN BEGIN
           plotparam.ythick = plotparam.ythick * thick_scale
         ENDIF
         IF plotparam.symbol_thick GT 0 THEN BEGIN
           plotparam.symbol_thick = plotparam.symbol_thick * thick_scale
         ENDIF
         IF plotparam.error_thick GT 0 THEN BEGIN
           plotparam.error_thick = plotparam.error_thick * thick_scale
         ENDIF
         IF plotparam.axis_thick GT 0 THEN BEGIN
           plotparam.axis_thick = plotparam.axis_thick * thick_scale
         ENDIF
                      

         ; Set the location of the legend.
         plotparam.legend_x = legend_x[iplot]
         plotparam.legend_y = legend_y[iplot]
         plotparam.legend_spacing_y = -1* legend_space[iplot]



         ; Handle ticklengths.

         ; First get the page defaults.
         IF page_options.ticklen NE 0.0 THEN BEGIN
           plotparam.ticklen = page_options.ticklen
         ENDIF
         IF plotparam.ticklen_inches EQ -1 THEN BEGIN
           IF page_options.ticklen_inches NE -1 THEN BEGIN
             plotparam.ticklen_inches = page_options.ticklen_inches
           ENDIF ELSE BEGIN
             plotparam.ticklen_inches = 0
           ENDELSE
         ENDIF

         ; Sow set default lengths
         IF plotparam.xticklen EQ 0 AND plotparam.yticklen EQ 0 THEN BEGIN
           ; Check if ticklen was given if set set a default.
           IF plotparam.ticklen EQ 0 THEN BEGIN
             IF plotparam.ticklen_inches THEN BEGIN
               plotparam.ticklen = 0.125
             ENDIF ELSE BEGIN
               plotparam.ticklen = 0.01
             ENDELSE
           ENDIF

           IF plotparam.ticklen_inches THEN BEGIN
             plotparam.xticklen = plotparam.ticklen
             plotparam.yticklen = plotparam.ticklen
           ENDIF ELSE BEGIN
             ; Here make both the x and y ticks lengths equal
             ; the setting is given by yticklen.
             plotparam.yticklen = plotparam.ticklen
             plotparam.xticklen = plotparam.yticklen $
                                  * (device_xsize * (!P.POSITION[2]-!P.POSITION[0])) $
                                  / (device_ysize * (!P.POSITION[3]-!P.POSITION[1]))

           ENDELSE
         ENDIF

         IF plotparam.ticklen_inches THEN BEGIN
           ; Convert the ticklengths from inches to normalized coordinatates.
           plotparam.xticklen = plotparam.xticklen*y_inches_to_norm/(!P.POSITION[3]-!P.POSITION[1])
           plotparam.yticklen = plotparam.yticklen*x_inches_to_norm/(!P.POSITION[2]-!P.POSITION[0])
         ENDIF
         ; Check if data was given.
         IF ~ HAS_TAG(plotdata, 'Y') THEN BEGIN
           MESSAGE, "Plot sturcture does not contain the 'y' tag."
         ENDIF

         ; Generate an index for the x axis if needed.
         IF HAS_TAG(plotdata, 'X') THEN BEGIN
           x = plotdata.x
         ENDIF ELSE BEGIN
           x = INDGEN(N_ELEMENTS(plotdata.y))
         ENDELSE


         ; Choose what arguments to send to SUPERPLOT.
         CASE 1 OF
           plotparam.errorplot: BEGIN
             num_args = 4
             ; Extract error information
             IF HAS_TAG(plotdata, 'ERROR_MIN') THEN BEGIN
               arg_3 = plotdata.error_min
             ENDIF
             IF HAS_TAG(plotdata, 'ERROR_MAX') THEN BEGIN
               arg_4 = plotdata.error_max
             ENDIF
             IF HAS_TAG(plotdata, 'ERROR') THEN BEGIN
               arg_3 = plotdata.y - plotdata.error
               arg_4 = plotdata.y + plotdata.error
             ENDIF
           END


           plotparam.xyouts: BEGIN
             num_args = 3
             IF HAS_TAG(plotdata, 'STRING') THEN BEGIN
               arg_3 = plotdata.string
             ENDIF
           END
           ELSE: num_args = 2
         ENDCASE


         ; Set any options for postscript printing.
         plotparam.leaveopen = 1
         plotparam.pdf = page_options.pdf
         plotparam.filename = page_options.filename
         plotparam.dialog = page_options.dialog
         plotparam.xsize = xsize
         plotparam.ysize = ysize
         plotparam.inches = inches   

         ; Call superplot with the correct number of arguments.
         CASE num_args OF
           2: BEGIN
             SUPERPLOT, x, plotdata.y $
                        ,OPTIONS=plotparam $
                        ,GET_PATH=get_path $
                        ,DEBUG=debug
           END
           3: BEGIN
             SUPERPLOT, x, plotdata.y, arg_3 $
                        ,OPTIONS=plotparam $
                        ,GET_PATH=get_path $
                        ,DEBUG=debug
           END
           4: BEGIN
             SUPERPLOT, x, plotdata.y, arg_3, arg_4 $
                        ,OPTIONS=plotparam $
                        ,GET_PATH=get_path $
                        ,DEBUG=debug         
           END
         ENDCASE

                      
         ; Adjust the legend label location
         IF plotparam.legend_label NE '' THEN BEGIN
           legend_y[iplot] -= legend_space[iplot]
         ENDIF

       ENDFOR

       IF page_options.pagetitle THEN BEGIN
         
         IF page_options.thesis_size THEN BEGIN
           title_y = plot_geom_title[1] + .125*y_inches_to_norm
         ENDIF ELSE BEGIN
           title_y = plot_geom_title[1] + .025/y_space_scale
         ENDELSE
         SUPERPLOT, 0.5, title_y, page_options.pagetitle $
                    ,/XYOUTS $
                    ,/NORMAL $
                    ,ALIGNMENT=0.5 $
                    ,CHARSIZE=page_options.pagetitle_charsize * charsize_scale $
                    ,FONT_TYPE=plotparam.font_type $
                    ,COLOR=page_options.color $
                    ,DECOMPOSED=plotparam.decomposed $
                    ,PDF=page_options.pdf $
                    ,/LEAVEOPEN $
                    ,DEBUG=debug

                    
       ENDIF


       IF KEYWORD_SET(stop) THEN STOP


       OBJ_DESTROY, paramlist


       ; Reset the the plot device state.
       ; Close any open post script files
       SUPERPLOT_MULTI_CLEANUP, orig_plot_state
       
     END ; PRO SUPERPLOT_MULTI
