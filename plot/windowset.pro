




;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;
;   antoniuk@fusion.gat.com
;   amicitas@gmail.com
;
; DATE:
;   2009-10
;
; PURPOSE:
;   Easier window creation and display.
;
; TAGS:
;   EXCLUDE_FILE_HEADER
;
;
;-======================================================================


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   This is a program to make creating, setting and showing windows
     ;   easyer.
     ;  
     ;   What this program does is to:
     ;   1.) Check if the requested window is open.
     ;   2.) Create or set the window.
     ;  
     ;   This program solves the problem that WINDOW resets the window size
     ;   when called, and WSET can only be used when the window is already
     ;   opened.
     ;
     ; SYNTAX:
     ;   WINDOWSET [window][, /SHOW][, /COPY][, WINDOWCOPY=int][, /RESTORE]
     ;             [, WINDOW=var]
     ;             [, XSIZE=int][, YSIZE=int][, XPOS=int][, YPOS=int]
     ;             [/HELP]
     ;
     ; DESCRIPTION:
     ;   This routines aids the creation of new plotting windows, and the
     ;   changing of which window is in focus.
     ;  
     ;   It replaces both the WINDOW and WSET routines.
     ;   Addionally it allows for windows sizes to be cloned from existing
     ;   windows.  It also includes a restore command to set focus to the
     ;   window that was in focus before the last WINDOWSET command was
     ;   used.
     ;
     ;
     ; OPTIONS:
     ;  
     ;   If no option are given a new window will be opend with the next
     ;   available window number.
     ;  
     ;   window
     ;       Number of the window to create or bring to focus.
     ;       If not given the current or next available window number will be
     ;       used depending on the other options.
     ;  
     ;   /SHOW
     ;       Bring focused window to the front
     ;  
     ;   /COPY
     ;       Will create a new window with the same dimensions as the
     ;       currently focus window.
     ;   
     ;   WCOPY = integer
     ;       Will create a new window with the same dimensions
     ;       as the window number given.
     ;  
     ;   /RESTORE
     ;       Will restore to focus the window that was in focus
     ;       before the last time that WINDOWSET was called
     ;
     ;   /SAVE
     ;       Will save the current window that is in focus so that
     ;       it can be restored using /RESTORE.  If no window is in
     ;       in focus that will be saved as well.
     ;  
     ;   WINDOW = variable
     ;       Will return the window number to the given named variable.
     ;  
     ;  
     ;  
     ;-=================================================================
     PRO WINDOWSET, window_in $
                    ,SHOW=show $

                    ,COPY=copy $
                    ,WCOPY=windowcopy $

                    ,SAVE=save $
                    ,RESTORE=restore $
                    ,WINDOW=window $
                    ,CHECK=check $

                    ,XSIZE=xsize $
                    ,YSIZE=ysize $
                    ,XPOS=xpos $
                    ,YPOS=ypos $
                    ,RESIZE=resize $

                    ,HELP=help $

                    ,_EXTRA=extra

       IF KEYWORD_SET(help) THEN BEGIN
         INFO
         RETURN
       END

       COMMON WINDOWSET, c_params

       ; Initialize the windowset common block   
       IF N_ELEMENTS(c_params) EQ 0 THEN BEGIN
         c_params = {previous_window:-1 $
                     ,next_window:-1}
       ENDIF

       ; Check the device
       IF (!d.name NE 'X') AND (!d.name NE 'WIN') THEN BEGIN
         MESSAGE, STRING('Device must be "X" or "WIN". Current Device: ', !d.name) $
           ,/INFORMATIONAL
         RETURN
       ENDIF

       ; Check for the restore command
       IF KEYWORD_SET(restore) THEN BEGIN
         ; Restore theprevious window
         window = c_params.previous_window

         ; When there is no previous window to restore, just save the
         ; current window and set the window focus to undefined.
         IF window EQ -1 THEN BEGIN
           c_params.previous_window = !d.window
           c_params.next_window = window
           RETURN
         ENDIF
       ENDIF

       ; Set the default for resize to true.
       MIR_DEFAULT, resize, 0

       ; Save the current window to the common block
       c_params.previous_window = !d.window

       ; If all we are doing is saving the current window, then return now.
       IF KEYWORD_SET(save) THEN RETURN


       ; Begin checks
       ; ------------

       ; First check the state of the window
       DEVICE, WINDOW_STATE=window_state


       IF ISA(window_in) THEN window = window_in
       MIR_DEFAULT, window, c_params.next_window

       ; If the /CHECK keyword was given then check if the given window exists.
       IF KEYWORD_SET(check) AND window GE 0 THEN BEGIN
         IF window_state[window] EQ 0 THEN BEGIN
           window = -1
         ENDIF
       ENDIF

       ; If we don't have a valid window number, then find the first avaliable.
       IF window LT 0 THEN BEGIN
         w = WHERE(window_state EQ 0)
         window=w[0]
       ENDIF

       IF window_state[window] EQ 0 $
          OR KEYWORD_SET(copy) $
          OR N_ELEMENTS(windowcopy) NE 0 $
          OR (KEYWORD_SET(resize) AND (KEYWORD_SET(xsize) OR KEYWORD_SET(ysize))) $
       THEN BEGIN

         ; If the copy keyword is set then copy the current window
         IF KEYWORD_SET(copy) THEN BEGIN
           windowcopy = !d.window
         ENDIF

         ; If the WINDOWCOPY keyword is set the copy the given window
         ; Note that the /COPY keyword will set windowcopy and over rule this. 
         IF N_ELEMENTS(windowcopy) NE 0 THEN BEGIN
           ; Make sure a valid window was given. 
           ; If /COPY was given this checks that there was a window to copy.
           IF windowcopy GE 0 THEN BEGIN
             ; Check tha the window given exists.
             IF window_state[windowcopy] EQ 1 THEN BEGIN
               WSET, windowcopy
               xsize = !d.x_size
               ysize = !d.y_size
               
               DEVICE, GET_WINDOW_POSITION=windowcopy_pos
               xpos = windowcopy_pos[0]
               ypos = windowcopy_pos[1]
             ENDIF
           ENDIF
         ENDIF
           
         MIR_DEFAULT, xsize, 800
         MIR_DEFAULT, ysize, 600

         WINDOW, window, XSIZE=xsize, YSIZE=ysize, $
           XPOS=xpos, YPOS=ypos, _EXTRA=extra

       ENDIF ELSE BEGIN
         WSET, window

         IF KEYWORD_SET(show) THEN BEGIN
           WSHOW, window, _EXTRA=extra
         ENDIF
       ENDELSE
     END ;PRO WINDOWSET
