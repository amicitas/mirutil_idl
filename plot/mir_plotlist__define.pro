



;+======================================================================
;
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-07
;
; PURPOSE:
;   Define a MIR_PLOTLIST object.
;
;   This object holds a list of plots along with a structure of
;   default plot parameters.
;   
;
;-======================================================================





     ;+=================================================================
     ; 
     ; A convenience routine to get the plot caption.
     ;
     ;-=================================================================
     FUNCTION MIR_PLOTLIST::GET_CAPTION

       RETURN, self.defaults.caption

     END ; FUNCTION MIR_PLOTLIST::GET_CAPTION


     ;+=================================================================
     ; 
     ; A convenience routine to set the plot caption.
     ;
     ;-=================================================================
     PRO MIR_PLOTLIST::SET_CAPTION, caption

       MIR_DEFAULT, caption, ''

       self.defaults.caption = caption

     END ; PRO MIR_PLOTLIST::SET_CAPTION



     ;+=================================================================
     ; 
     ; Remove the default plot parameters
     ;
     ;-=================================================================
     PRO MIR_PLOTLIST::RESET_DEFAULTS


       self.defaults = SUPERPLOT_PAGE_DEFAULTS()

     END ; PRO MIR_PLOTLIST::RESET_DEFAULTS




     ;+=================================================================
     ; 
     ; Reset the state of the mir_plotlist object.
     ; Remove all plots and settings.
     ;
     ;-=================================================================
     PRO MIR_PLOTLIST::RESET

       self->RESET_DEFAULTS
       self->REMOVE, /ALL

     END ; PRO MIR_PLOTLIST::RESET



     ;+=================================================================
     ; 
     ; Set the default parameters.
     ;
     ;-=================================================================
     PRO MIR_PLOTLIST::SET_DEFAULTS, struct

       STRUCT_ASSIGN, {defaults:struct}, self, /NOZERO

     END ; PRO MIR_PLOTLIST::SET_DEFAULTS



     ;+=================================================================
     ; 
     ; Get the default parameters.
     ;
     ;-=================================================================
     FUNCTION MIR_PLOTLIST::GET_DEFAULTS

       RETURN, self.defaults

     END ; FUNCTION MIR_PLOTLIST::GET_DEFAULTS



     ;+=================================================================
     ; 
     ; Get a copy of the object.
     ;
     ;-=================================================================
     FUNCTION MIR_PLOTLIST::GET_COPY, _REF_EXTRA=extra

       new_copy = self->MIR_LIST_IDL7::GET_COPY(_STRICT_EXTRA=extra)
       new_copy.defaults = self.defaults
       RETURN, new_copy

     END ; FUNCTION MIR_PLOTLIST::GET_DEFAULTS



     ;+=================================================================
     ; 
     ; Destroy all pointers.
     ;
     ;-=================================================================
     PRO MIR_PLOTLIST::CLEANUP, _REF_EXTRA=extra
       
       self->MIR_LIST_IDL7::CLEANUP, _STRICT_EXTRA=extra

     END ; PRO MIR_PLOTLIST::CLEANUP


     ;+=================================================================
     ;
     ; Initialize the object
     ;
     ;-=================================================================
     FUNCTION MIR_PLOTLIST::INIT
       COMPILE_OPT STRICTARR

       status = self->MIR_LIST_IDL7::INIT()
 
       self->RESET_DEFAULTS
           
       RETURN, status

     END ; FUNCTION MIR_PLOTLIST::INIT


     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Define a MIR_PLOTLIST object.
     ;
     ;   This object holds a list of plots along with a structure of
     ;   default plot parameters.
     ;
     ;   This is an extention of the standard LIST object and can
     ;   be used with [SUPEPLOT_MULTI] and [MULTIPAGE_PLOT_WIDGET]
     ;
     ; PROGRAMMING NOTES:
     ;   I wrote this while using IDL 7.1 which does note have a LIST
     ;   data type.  For this reason I used my own custom list object.
     ;   Now that IDL 8.0 is out, I have renamed my list object to
     ;   MIR_LIST_IDL7:: from which this object inherits.
     ;
     ;-=================================================================
     PRO MIR_PLOTLIST__DEFINE
       COMPILE_OPT STRICTARR
       
       struct = {MIR_PLOTLIST $
                 ,defaults:SUPERPLOT_PAGE_DEFAULTS() $
                 ,INHERITS MIR_LIST_IDL7 $
                }

     END ;PRO MIR_PLOTLIST__DEFINE
