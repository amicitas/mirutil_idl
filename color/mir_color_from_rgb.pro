



;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2010-05
;
; PURPOSE:
;   Convert an rgb color into a true color.  
;   Rgb colors are given as a real number in range between 0 and 1.
;
;-======================================================================

     FUNCTION MIR_COLOR_FROM_RGB, red, blue, green

       RETURN, LONG(red*255L) + 256L*LONG(blue*255l) + 256l^2*LONG(green*255L)

     END ; FUNCTION MIR_COLOR_FROM_RGB
