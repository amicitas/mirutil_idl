


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-07
;
; PURPOSE:
;   Switch to indexed color.
;
; TAGS:
;   EXCLUDE_FILE_HEADER
;
;-======================================================================

     ;+=================================================================
     ;
     ; Switch to indexed color then set:
     ;   !p.color = 0
     ;   !p.background = 255
     ;
     ;-=================================================================
     PRO MIR_COLOR_INDEXED

       DEVICE, DECOMPOSED=0
       !p.color = '000000'x
       !p.background = 'ffffff'x

     END ; PRO MIR_COLOR_INDEXED
