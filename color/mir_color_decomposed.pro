


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-07
;
; PURPOSE:
;   Switch to decomposed color.
;
; TAGS:
;   EXCLUDE_FILE_HEADER
;
;-======================================================================

     ;+=================================================================
     ;
     ; Switch to decomposed color then set:
     ;   !p.color = black
     ;   !p.background = white
     ;
     ;-=================================================================
     PRO MIR_COLOR_DECOMPOSED, decomposed

       ; First check if the decomposed parameter was given.
       ; If so see if decomposed was wanted or not.
       IF N_ELEMENTS(decomposed) NE 0 THEN BEGIN
         IF KEYWORD_SET(decomposed) THEN BEGIN
           MIR_COLOR_INDEXED
           RETURN
         ENDIF
       ENDIF

       DEVICE, DECOMPOSED=1
       !p.color = '000000'x
       !p.background = 'ffffff'x

     END ; PRO MIR_COLOR_DECOMPOSED
