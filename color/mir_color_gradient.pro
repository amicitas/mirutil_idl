




;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;   novimir.pablant@amicitas.com
;
; DATE:
;   2009-08
;
; PURPOSE:
;   Retrieve truecolor color gradients.
;
;-======================================================================



     ;+=================================================================
     ; PURPOSE:
     ;   Return a node array given a gradient name.
     ;
     ; PROGRAMMING NOTES:
     ;   This routine will return an array of nodes.
     ;   Each node is a four element array. The first three elements
     ;   correspond the RGB values for that node. The last element
     ;   corresponds to the location of the node.
     ;
     ;-=================================================================
     FUNCTION COLOR_GRADIENT_NAMES, color_name
       
       CASE STRUPCASE(color_name) OF
         'RED-BLACK-BLUE': color = [ [0.8E, 0.0E, 0.0E, 0.00E] $
                                    ,[0.4E, 0.0E, 0.0E, 0.25E] $
                                    ,[0.0E, 0.0E, 0.0E, 0.50E] $
                                    ,[0.0E, 0.0E, 0.4E, 0.75E] $
                                    ,[0.0E, 0.0E, 0.8E, 1.00E] ]

         'BLACK-RED-CYAN': color = [ [0.0E, 0.0E, 0.0E, 0.0E] $
                                    ,[0.8E, 0.0E, 0.0E, 0.5E] $
                                    ,[0.0E, 0.8E, 0.8E, 1.0E] ]

         'RED-BLACK-CYAN': color = [ [0.8E, 0.0E, 0.0E, 0.0E] $
                                    ,[0.0E, 0.0E, 0.0E, 0.5E] $
                                    ,[0.0E, 0.8E, 0.8E, 1.0E] ]

         'BLACK-RED-YELLOW-CYAN': color = [ [0.0E, 0.0E, 0.0E, 0.0E] $
                                           ,[0.8E, 0.0E, 0.0E, 0.33E] $
                                           ,[0.8E, 0.8E, 0.0E, 0.66E] $
                                           ,[0.0E, 0.8E, 0.8E, 1.0E] ]

         'BLACK-RED-YELLOW-BLUE': color = [ [0.0E, 0.0E, 0.0E, 0.0E] $
                                           ,[0.8E, 0.0E, 0.0E, 0.33E] $
                                           ,[0.8E, 0.8E, 0.0E, 0.66E] $
                                            ,[0.0E, 0.0E, 0.8E, 1.0E] ]

         'PURPLE-YELLOW': color = [[0.65E, 0.0E, 0.55E, 0.0E] $
                                   ,[0.5E, 0.5E, 0.5E, 0.5E] $
                                   ,[1.0E, 0.7E, 0.4E, 1.0E] ]

         'RAINBOW': color = [ [0.8E, 0.0E, 0.0E, 0.2E] $
                             ,[0.8E, 0.8E, 0.0E, 0.4E] $
                             ,[0.0E, 0.8E, 0.0E, 0.6E] $
                             ,[0.0E, 0.8E, 0.8E, 0.8E] $
                             ,[0.0E, 0.0E, 0.8E, 1.0E] ]

         'RED-CYAN': color = [ [0.8E, 0.0E, 0.0E, 0.5E] $
                              ,[0.0E, 0.8E, 0.8E, 1.0E] ]

         'WHITE-RED': color = [ [1.0E, 1.0E, 1.0E, 0.0E] $
                               ,[0.8E, 0.0E, 0.0E, 1.0E] ]

         'GRAY50-RED': color = [ [0.5E, 0.5E, 0.5E, 0.0E] $
                               ,[0.8E, 0.0E, 0.0E, 1.0E] ]

         ; Same as 'RAINBOW'
         ELSE: BEGIN
           PRINT, 'Gadient: '+color_name+' not found.  Using RAINBOW instead.'
           color = [ [0.8E, 0.0E, 0.0E, 0.2E] $
                     ,[0.8E, 0.8E, 0.0E, 0.4E] $
                     ,[0.0E, 0.8E, 0.0E, 0.6E] $
                     ,[0.0E, 0.8E, 0.8E, 0.8E] $
                     ,[0.0E, 0.0E, 0.8E, 1.0E] ]
         ENDELSE

       ENDCASE

       RETURN, color

     END ; FUNCTION COLOR_GRADIENT_NAMES



     ;+=================================================================
     ;
     ; Retrive hexadecimal colors given several options.
     ;
     ;-=================================================================
     FUNCTION COLOR_GRADIENT_RGB, node_array, num_colors

       output = LONARR(num_colors)
       num_nodes = (SIZE(node_array, /DIM))[1]

       ; Set the first and last colors.
       output[0] = MIR_COLOR_FROM_RGB(node_array[0,0], node_array[1,0], node_array[2,0])
       output[-1] = MIR_COLOR_FROM_RGB(node_array[0,-1], node_array[1,-1], node_array[2,-1])
       
       IF num_colors GT 2 THEN BEGIN
         fraction = FINDGEN(num_colors)/(num_colors-1)
         FOR ii=1,num_colors-2 DO BEGIN
           ; Figure out which is the next node. 
           next_node = (WHERE(node_array[3,*] GT fraction[ii]))[0]

           sub_fraction = (fraction[ii] - node_array[3,next_node-1]) $
                          / (node_array[3,next_node] - node_array[3,next_node-1])
           rgb = (node_array[0:2,next_node]-node_array[0:2,next_node-1])*sub_fraction + node_array[0:2,next_node-1]

           output[ii] = MIR_COLOR_FROM_RGB(rgb[0], rgb[1], rgb[2])
           ;STOP
         ENDFOR
       ENDIF

       RETURN, output
         
     END ;FUNCTION COLOR_GRADIENT_RGB

     
     ;+=================================================================
     ;
     ; Retrive hexadecimal colors given several options.
     ;
     ; NOTE:
     ;   This was my old way of doing this, but it has a problem in
     ;   that it currently duplicates the last color.  I don't really
     ;   understand what I did here, so I just wrote a new routine
     ;   which makes more sence to me. For a large number of colors
     ;   this older routine would be faster.
     ;
     ;-=================================================================
     FUNCTION COLOR_GRADIENT_RGB_OLD, node_array, num_colors

       output = LONARR(num_colors)
       num_nodes = (SIZE(node_array, /DIM))[1]

       ; Set the color of the first node
       output[0] = MIR_COLOR_FROM_RGB(node_array[0,0], node_array[1,0], node_array[2,0])
       output[-1] = MIR_COLOR_FROM_RGB(node_array[0,-1], node_array[1,-1], node_array[2,-1])

       IF num_colors GT 2 THEN BEGIN
         last_node = 0
         last_color = node_array[0:2,0]
          
         FOR ii=1,num_nodes-1 DO BEGIN

           ;Number of colors from before this node.
           node = CEIL(node_array[3,ii]*num_colors-1)-1

           ; Number of colors between the previous node and the last one.
           num_to_node = node - last_node
            
           IF num_to_node EQ 0 THEN CONTINUE
              
           red   = FINDGEN(num_to_node+1)/(num_to_node)*(node_array[0,ii] - last_color[0]) + last_color[0]
           green = FINDGEN(num_to_node+1)/(num_to_node)*(node_array[1,ii] - last_color[1]) + last_color[1]
           blue  = FINDGEN(num_to_node+1)/(num_to_node)*(node_array[2,ii] - last_color[2]) + last_color[2]

           output[last_node+1:node] = MIR_COLOR_FROM_RGB(red[1:*], green[1:*], blue[1:*])
             
           last_node = node
           last_color = node_array[*,ii]
         ENDFOR
       ENDIF
         

       RETURN, output
     END ;FUNCTION COLOR_GRADIENT_RGB



     ;+=================================================================
     ;
     ; PURPOSE:
     ;   Retrive truecolor colors given several options.
     ;
     ; AVAILABLE COLORS:
     ;
     ;    BLACK-RED-CYAN
     ;    BLACK-RED-YELLOW-CYAN
     ;    BLACK-RED-YELLOW-BLUE
     ;    GRAY50-RED
     ;    RAINBOW
     ;    RED-BLACK-BLUE
     ;    RED-CYAN
     ;    WHITE-RED
     ;
     ;
     ;
     ;   
     ;
     ;-=================================================================
     FUNCTION MIR_COLOR_GRADIENT, color, num_colors
       
       node_array = COLOR_GRADIENT_NAMES(color)
       RETURN, COLOR_GRADIENT_RGB(node_array, num_colors)

     END ;FUNCTION MIR_COLOR_GRADIENT
