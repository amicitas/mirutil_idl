


; $Id: //depot/idl/IDL_71/idldir/lib/loadct.pro#1 $
;
; Copyright (c) 1982-2009, ITT Visual Information Solutions. All
;       rights reserved. Unauthorized reproduction is prohibited.



;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-08
;
; PURPOSE:
;   Load paletts from a custom color table.
;
; NAME:
;	MIR_LOADCT
;
; CATEGORY:
;	Image display.
;
; CALLING SEQUENCE:
;	MIR_LOADCT [, Table]
;
; OPTIONAL INPUTS:
;	Table:	The number of the pre-defined color table to load, from 0
;		to 15.  If this value is omitted, a menu of the available
;		tables is printed and the user is prompted to enter a table
;		number.
;
; KEYWORD PARAMETERS:
;	FILE:	If this keyword is set, the file by the given name is used
;		instead of the file colors1.tbl in the IDL directory.  This
;		allows multiple IDL users to have their own color table file.
;		The specified file must exist.
;	GET_NAMES: If this keyword is present AND DEFINED, the names
;		of the color tables are returned as a string array.
;		No changes are made to the color table.
;	NCOLORS = number of colors to use.  Use color indices from 0
;		to the smaller of !D.TABLE_SIZE-1 and NCOLORS-1.
;		Default = !D.TABLE_SIZE = all available colors.
;	SILENT:	If this keyword is set, the Color Table message is suppressed.
;	BOTTOM = first color index to use. Use color indices from BOTTOM to
;		BOTTOM+NCOLORS-1.  Default = 0.
;   RGB_TABLE: Set this keyword to a named variable in which to return
;       the desired color table as an [NCOLORS, 3] array.
;       If this keyword is set, then the color table is not loaded into
;       the display, but is simply returned to the user. In addition,
;       if RGB_TABLE is set then SILENT is also set to true.
;
; OUTPUTS:
;	No explicit outputs.
;
; COMMON BLOCKS:
;	COLORS:	The IDL color common block.
;
; SIDE EFFECTS:
;	The color tables of the currently-selected device are modified.
;
; RESTRICTIONS:
;	Works from the file: $IDL_DIR/resource/colors/colors1.tbl or the file specified
;	with the FILE keyword.
;
; PROCEDURE:
;	The file "colors1.tbl" or the user-supplied file is read.  If
;       the currently selected device doesn't have 256 colors, the color
;	data is interpolated from 256 colors to the number of colors
;	available.
;
;	The colors loaded into the display are saved in the common
;	block COLORS, as both the current and original color vectors.
;
;	Interpolation:  If the current device has less than 256 colors,
;	the color table data is interpolated to cover the number of
;	colors in the device.
;
; MODIFICATION HISTORY:
;	Old.  For a widgetized version of this routine, see XLOADCT in the IDL
;		widget library.
;	DMS, 7/92, Added new color table format providing for more than
;		16 tables.  Now uses file colors1.tbl.  Old LOADCT procedure
;		is now OLD_LOADCT.
;	ACY, 9/92, Make a pixmap if no windows exist for X windows to
;		determine properly the number of available colors.
;		Add FILE keyword.
;	WSO, 1/95, Updated for new directory structure
;	AB, 10/3/95, The number of entries in the COLORS common block is
;		now always !D.TABLE_SIZE instead of NCOLORS + BOTTOM as
;		before. This better reflects the true state of the device and
;		works with other color manipulations routines.
;   DLD, 09/98, Avoid repeating a color table name in the printed list.
;   CT, Nov 2006: Added RGB_TABLE keyword.
;
;-======================================================================



     ;+=================================================================
     ;
     ; This is a modification of LOADCT that can handle color table
     ; files with any number of table entries.
     ;
     ;-=================================================================
     PRO LOADCT_CORE, table_number $
                      ,SILENT=silent $
                      ,GET_NAMES=names $
                      ,FILE=file $
                      ,NCOLORS=nc1 $
                      ,BOTTOM=bottom $
                      ,RGB_TABLE=rgbTable
     
       COMPILE_OPT idl2
     
       COMMON COLORS, r_orig, g_orig, b_orig, r_curr, g_curr, b_curr
     
     
       ON_IOERROR, bad
       ;ON_ERROR, 2		;Return to caller if error
       get_lun, lun
     
       rgbTable = Arg_Present(rgbTable)
       silent = N_Elements(silent) ? silent : rgbTable
     
       IF !d.name EQ 'X' AND !d.window EQ -1 THEN BEGIN  ;Uninitialized?
         ;	If so, make a dummy window to determine the # of colors available.
         WINDOW,/FREE,/PIXMAP,XS=4, YS=4
         WDELETE, !d.window
       ENDIF
     
       IF N_ELEMENTS(bottom) GT 0 THEN cbot = bottom > 0 < (!d.table_size-1) $
       ELSE cbot = 0
       nc = !d.table_size - cbot
       IF N_ELEMENTS(nc1) GT 0 THEN nc = nc < nc1
     
       IF nc EQ 0 THEN MESSAGE, 'Device has static color tables.  Can''t load.'
     
       IF (n_elements(file) GT 0) THEN filename = file $
       ELSE filename = FILEPATH('colors1.tbl', SUBDIR=['resource', 'colors'])
     
       OPENR, lun, filename, /BLOCK
     
       ntables = 0b
       READU, lun, ntables
     
       ; Read names?
       IF (n_params() EQ 0 || arg_present(names) || ~silent) THEN BEGIN
         names = bytarr(32, ntables)
         point_lun, lun, ntables * 768L + 1	;Read table names
         readu, lun, names
         names = strtrim(names, 2)
         IF arg_present(names) THEN GOTO, close_file  ;Return names?
       ENDIF
     
       ;Summarize table?
       IF n_params() LT 1 THEN BEGIN


         FOR ii=0,ntables-1 DO BEGIN
           PRINT, FORMAT="(i2,' - ',a25, '   ', $)", ii, names[ii]
           IF (((ii+1) MOD 3) EQ 0) OR (ii EQ ntables-1) THEN BEGIN
             PRINT, ''
           ENDIF
         ENDFOR
     
         table_number = 0
         READ, table_number, PROMPT='Enter table number: '
       ENDIF
     
       IF (table_number GE ntables) OR (table_number LT 0) THEN BEGIN
         MESSAGE, 'Table number must be from 0 to ' + strtrim(ntables-1, 2)
       ENDIF
     
       ;Tables defined?
       IF (~rgbTable && n_elements(r_orig) LT !d.table_size) THEN BEGIN
         r_orig = BYTSCL(indgen(!d.table_size))
         g_orig = r_orig
         b_orig = r_orig
       ENDIF
     
     
       IF KEYWORD_SET(silent) EQ 0 THEN $
          MESSAGE,'Loading table ' + names[table_number], /INFO
       aa=assoc(lun, bytarr(256),1)	;Read 256 long ints
       r = aa[table_number*3]
       g = aa[table_number*3+1]
       b = aa[table_number*3+2]
     
       IF nc NE 256 THEN BEGIN	;Interpolate
         p = (lindgen(nc) * 255) / (nc-1)
         r = r[p]
         g = g[p]
         b = b[p]
       ENDIF
     
       IF (rgbTable) THEN BEGIN
         rgbTable = [[r], [g], [b]]
       ENDIF ELSE BEGIN
         r_orig[cbot] = r
         g_orig[cbot] = g
         b_orig[cbot] = b
         r_curr = r_orig
         g_curr = g_orig
         b_curr = b_orig
         tvlct,r, g, b, cbot
       ENDELSE
       GOTO, close_file
     
     BAD:
       MESSAGE, /CONTINUE, 'Error reading file: ' + filename + ', ' + !error_state.msg
     
     CLOSE_FILE:
       free_lun,lun
     
     END ; PRO LOADCT_CORE
     


     ;+=================================================================
     ;
     ; Load a palette from a custom palette table.
     ;
     ;-=================================================================
     PRO MIR_LOADCT, palette_num $
                       ,USE_STANDARD_TABLE=use_standard_table $
                       ,USE_ANTONIUK_TABLE=use_antoniuk_table $
                       ,FILENAME=filename_in $
                       ,_EXTRA=extra

       color_table_file = ''
       CASE 1 OF
         KEYWORD_SET(use_standard_table):
         ELSE: BEGIN
           color_table_file = "/u/antoniuk/idl/utilities/color_utilities/color_table_antoniuk.tbl"
         ENDELSE
       ENDCASE

       IF color_table_file NE '' THEN BEGIN
         IF FILE_TEST(color_table_file) THEN BEGIN
           filename = color_table_file
         ENDIF ELSE BEGIN
           MESSAGE, 'Color table file could not be found: ' + color_table_file, /CONTINUE
         ENDELSE
       ENDIF
       
       IF N_ELEMENTS(palette_num) NE 0 THEN BEGIN
         LOADCT_CORE, palette_num, FILE=filename, _EXTRA=extra
       ENDIF ELSE BEGIN
         LOADCT_CORE,  FILE=filename, _EXTRA=extra
       ENDELSE

     END ;PRO MIR_LOADCT
  
