

;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;   novimir.pablant@amicitas.com
;
; DATE:
;   2009-08
;
; PURPOSE:
;   Retrieve a color from a truecolor gradients given a value and
;   a range.
;
;-======================================================================
     

     FUNCTION MIR_COLOR_GRADIENT_VALUE, color, values, range, NUM_COLORS=num_colors
       COMPILE_OPT STRICTARR

       MIR_DEFAULT, num_colors, 256

       ; First retrive a color gradient
       all_colors = MIR_COLOR_GRADIENT(color, num_colors)

       ; Now chose the appropriate index from the values.
       color_value = (values-range[0])/(range[1]-range[0])

       w = WHERE(color_value LT 0.0, count)
       IF count GT 0 THEN BEGIN
         color_value[w] = 0.0
       ENDIF

       w = WHERE(color_value GT 1.0, count)
       IF count GT 0 THEN BEGIN
         color_value[w] = 1.0
       ENDIF
       
       color_index = ROUND(color_value * (num_colors-1))

       RETURN, all_colors[color_index]

     END ; FUNCTION MIR_COLOR_GRADIENT_VALUE
