


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-08
;
; PURPOSE:
;   Convert an indexed color to true color.
;
; TAGS:
;   EXCLUDE_FILE_HEADER
;-======================================================================


     ;+=================================================================
     ; 
     ; PURPOSE:
     ;   Convert an indexed color to true color.
     ;
     ;-=================================================================
     FUNCTION MIR_COLOR_FROM_INDEX, index
       COMMON COLORS, r_orig, g_orig, b_orig, r_curr, g_curr, b_curr
       
       ; Make sure that a color pallet has been loaded.
       IF ~ ISA(r_curr) THEN BEGIN
         LOADCT, 13
       ENDIF

       RETURN, r_curr[index] + 256L*g_curr[index] + 256L^2*b_curr[index]
                
     END ; FUNCTION MIR_COLOR_FROM_INDEX
