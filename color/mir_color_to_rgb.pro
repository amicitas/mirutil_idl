



;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-08
;
; PURPOSE:
;   Convert a true color value into rgb value
;
; TAGS:
;   EXCLUDE_FILE_HEADER
;-======================================================================


     ;+=================================================================
     ; 
     ; PURPOSE:
     ;   Convert a true color value into its R,G,B components.
     ;
     ;-=================================================================
     FUNCTION MIR_COLOR_TO_RGB, color

       RETURN, {red:(color MOD 256L) $
                ,green:((color MOD 256L^2)/256L) $
                ,blue:((color MOD 256L^3)/256L^2) $
               }
                
     END ; FUNCTION MIR_COLOR_TO_RGB
