


;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;
; DATE:
;   2009-08
;
; PURPOSE:
;   Call XLOADCT with a custom file table
;
;-======================================================================



     ;+=================================================================
     ;
     ; Load a palette from a custom palette table.
     ;
     ;-=================================================================
     PRO MIR_XLOADCT, USE_STANDARD_TABLE=use_standard_table $
                       ,USE_ANTONIUK_TABLE=use_antoniuk_table $
                       ,FILENAME=filename_in $
                       ,_EXTRA=extra

       color_table_file = ''
       CASE 1 OF
         KEYWORD_SET(use_standard_table):
         ELSE: BEGIN
           color_table_file = "/u/antoniuk/idl/utilities/color_utilities/color_table_antoniuk.tbl"
         ENDELSE
       ENDCASE

       IF color_table_file NE '' THEN BEGIN
         IF FILE_TEST(color_table_file) THEN BEGIN
           filename = color_table_file
         ENDIF ELSE BEGIN
           MESSAGE, 'Color table file could not be found: ' + color_table_file, /CONTINUE
         ENDELSE
       ENDIF
       
       XLOADCT,  FILE=filename, _EXTRA=extra

     END ;PRO MIR_XLOADCT
