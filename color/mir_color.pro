




;+======================================================================
; AUTHOR:
;   Novimir Antoniuk Pablant
;   antoniuk@fusion.gat.com
;   novimir.pablant@amicitas.com
;
; DATE:
;   2009-08
;
; PURPOSE:
;   Retrieve hexadecimal colors given several options.
;-======================================================================


     ;+=================================================================
     ;
     ; Create a <HASH::> object with all of our defined color names.
     ;
     ;-=================================================================
     FUNCTION MIR_COLOR_HASH

       color_hash = MIR_HASH()

       ; These are the 147 standard CSS/HTML colors.
       color_hash['aliceblue'] = 'fff8f0'x
       color_hash['antiquewhite'] = 'd7ebfa'x
       color_hash['aqua'] = 'ffff00'x
       color_hash['aquamarine'] = 'd4ff7f'x
       color_hash['azure'] = 'fffff0'x
       color_hash['beige'] = 'dcf5f5'x
       color_hash['bisque'] = 'c4e4ff'x
       color_hash['black'] = '000000'x
       color_hash['blanchedalmond'] = 'cdebff'x
       color_hash['blue'] = 'ff0000'x
       color_hash['blueviolet'] = 'e22b8a'x
       color_hash['brown'] = '2a2aa5'x
       color_hash['burlywood'] = '87b8de'x
       color_hash['cadetblue'] = 'a09e5f'x
       color_hash['chartreuse'] = '00ff7f'x
       color_hash['chocolate'] = '1e69d2'x
       color_hash['coral'] = '507fff'x
       color_hash['cornflowerblue'] = 'ed9564'x
       color_hash['cornsilk'] = 'dcf8ff'x
       color_hash['crimson'] = '3c14dc'x
       color_hash['cyan'] = 'ffff00'x
       color_hash['darkblue'] = '8b0000'x
       color_hash['darkcyan'] = '8b8b00'x
       color_hash['darkgoldenrod'] = '0b86b8'x
       color_hash['darkgray'] = 'a9a9a9'x
       color_hash['darkgrey'] = 'a9a9a9'x
       color_hash['darkgreen'] = '006400'x
       color_hash['darkkhaki'] = '6bb7bd'x
       color_hash['darkmagenta'] = '8b008b'x
       color_hash['darkolivegreen'] = '2f6b55'x
       color_hash['darkorange'] = '008cff'x
       color_hash['darkorchid'] = 'cc3299'x
       color_hash['darkred'] = '00008b'x
       color_hash['darksalmon'] = '7a96e9'x
       color_hash['darkseagreen'] = '8fbc8f'x
       color_hash['darkslateblue'] = '8b3d48'x
       color_hash['darkslategray'] = '4f4f2f'x
       color_hash['darkslategrey'] = '4f4f2f'x
       color_hash['darkturquoise'] = 'd1ce00'x
       color_hash['darkviolet'] = 'd30094'x
       color_hash['deeppink'] = '9314ff'x
       color_hash['deepskyblue'] = 'ffbf00'x
       color_hash['dimgray'] = '696969'x
       color_hash['dimgrey'] = '696969'x
       color_hash['dodgerblue'] = 'ff901e'x
       color_hash['firebrick'] = '2222b2'x
       color_hash['floralwhite'] = 'f0faff'x
       color_hash['forestgreen'] = '228b22'x
       color_hash['fuchsia'] = 'ff00ff'x
       color_hash['gainsboro'] = 'dcdcdc'x
       color_hash['ghostwhite'] = 'fff8f8'x
       color_hash['gold'] = '00d7ff'x
       color_hash['goldenrod'] = '20a5da'x
       color_hash['gray'] = '808080'x
       color_hash['grey'] = '808080'x
       color_hash['green'] = '008000'x
       color_hash['greenyellow'] = '2fffad'x
       color_hash['honeydew'] = 'f0fff0'x
       color_hash['hotpink'] = 'b469ff'x
       color_hash['indianred'] = '5c5ccd'x
       color_hash['indigo'] = '82004b'x
       color_hash['ivory'] = 'f0ffff'x
       color_hash['khaki'] = '8ce6f0'x
       color_hash['lavender'] = 'fae6e6'x
       color_hash['lavenderblush'] = 'f5f0ff'x
       color_hash['lawngreen'] = '00fc7c'x
       color_hash['lemonchiffon'] = 'cdfaff'x
       color_hash['lightblue'] = 'e6d8ad'x
       color_hash['lightcoral'] = '8080f0'x
       color_hash['lightcyan'] = 'ffffe0'x
       color_hash['lightgoldenrodyellow'] = 'd2fafa'x
       color_hash['lightgray'] = 'd3d3d3'x
       color_hash['lightgrey'] = 'd3d3d3'x
       color_hash['lightgreen'] = '90ee90'x
       color_hash['lightpink'] = 'c1b6ff'x
       color_hash['lightsalmon'] = '7aa0ff'x
       color_hash['lightseagreen'] = 'aab220'x
       color_hash['lightskyblue'] = 'face87'x
       color_hash['lightslategray'] = '998877'x
       color_hash['lightslategrey'] = '998877'x
       color_hash['lightsteelblue'] = 'dec4b0'x
       color_hash['lightyellow'] = 'e0ffff'x
       color_hash['lime'] = '00ff00'x
       color_hash['limegreen'] = '32cd32'x
       color_hash['linen'] = 'e6f0fa'x
       color_hash['magenta'] = 'ff00ff'x
       color_hash['maroon'] = '000080'x
       color_hash['mediumaquamarine'] = 'aacd66'x
       color_hash['mediumblue'] = 'cd0000'x
       color_hash['mediumorchid'] = 'd355ba'x
       color_hash['mediumpurple'] = 'd87093'x
       color_hash['mediumseagreen'] = '71b33c'x
       color_hash['mediumslateblue'] = 'ee687b'x
       color_hash['mediumspringgreen'] = '9afa00'x
       color_hash['mediumturquoise'] = 'ccd148'x
       color_hash['mediumvioletred'] = '8515c7'x
       color_hash['midnightblue'] = '701919'x
       color_hash['mintcream'] = 'fafff5'x
       color_hash['mistyrose'] = 'e1e4ff'x
       color_hash['moccasin'] = 'b5e4ff'x
       color_hash['navajowhite'] = 'addeff'x
       color_hash['navy'] = '800000'x
       color_hash['oldlace'] = 'e6f5fd'x
       color_hash['olive'] = '008080'x
       color_hash['olivedrab'] = '238e6b'x
       color_hash['orange'] = '00a5ff'x
       color_hash['orangered'] = '0045ff'x
       color_hash['orchid'] = 'd670da'x
       color_hash['palegoldenrod'] = 'aae8ee'x
       color_hash['palegreen'] = '98fb98'x
       color_hash['paleturquoise'] = 'eeeeaf'x
       color_hash['palevioletred'] = '9370d8'x
       color_hash['papayawhip'] = 'd5efff'x
       color_hash['peachpuff'] = 'b9daff'x
       color_hash['peru'] = '3f85cd'x
       color_hash['pink'] = 'cbc0ff'x
       color_hash['plum'] = 'dda0dd'x
       color_hash['powderblue'] = 'e6e0b0'x
       color_hash['purple'] = '800080'x
       color_hash['red'] = '0000ff'x
       color_hash['rosybrown'] = '8f8fbc'x
       color_hash['royalblue'] = 'e16941'x
       color_hash['saddlebrown'] = '13458b'x
       color_hash['salmon'] = '7280fa'x
       color_hash['sandybrown'] = '60a4f4'x
       color_hash['seagreen'] = '578b2e'x
       color_hash['seashell'] = 'eef5ff'x
       color_hash['sienna'] = '2d52a0'x
       color_hash['silver'] = 'c0c0c0'x
       color_hash['skyblue'] = 'ebce87'x
       color_hash['slateblue'] = 'cd5a6a'x
       color_hash['slategray'] = '908070'x
       color_hash['slategrey'] = '908070'x
       color_hash['snow'] = 'fafaff'x
       color_hash['springgreen'] = '7fff00'x
       color_hash['steelblue'] = 'b48246'x
       color_hash['tan'] = '8cb4d2'x
       color_hash['teal'] = '808000'x
       color_hash['thistle'] = 'd8bfd8'x
       color_hash['tomato'] = '4763ff'x
       color_hash['turquoise'] = 'd0e040'x
       color_hash['violet'] = 'ee82ee'x
       color_hash['wheat'] = 'b3def5'x
       color_hash['white'] = 'ffffff'x
       color_hash['whitesmoke'] = 'f5f5f5'x
       color_hash['yellow'] = '00ffff'x
       color_hash['yellowgreen'] = '32cd9a'x


       ; define custom color names
       color_hash['lightred'] = 'aaaaff'x

       ; define colors that are suitable for printing
       ; on a white background.
       color_hash['print_red'] = '0000ff'x
       color_hash['print_blue'] = 'ff0000'x
       color_hash['print_green'] = '006600'x
       color_hash['print_yellow'] = '00aaaa'x
       color_hash['print_orange'] = '0044cc'x
       color_hash['print_purple'] = 'aa0077'x
       color_hash['print_cyan'] = 'aaaa00'x
       color_hash['print_magenta'] = 'aa00aa'x


       ; define some greys.
       color_hash['gray_10'] = '1a1a1a'x
       color_hash['gray_20'] = '333333'x
       color_hash['gray_30'] = '4d4d4d'x
       color_hash['gray_40'] = '666666'x
       color_hash['gray_50'] = '808080'x
       color_hash['gray_60'] = '9a9a9a'x
       color_hash['gray_70'] = 'b3b3b3'x
       color_hash['gray_80'] = 'cdcdcd'x
       color_hash['gray_90'] = 'e6e6e6'x


       ; define some colors for b-stark plotting.
       color_hash['bst_full'] = 'aa0077'x ; purple
       color_hash['bst_half'] = '0044cc'x ; orange
       color_hash['bst_third'] = '006600'x ; green


       RETURN, color_hash

     END ; FUNCTION MIR_COLOR_HASH



     ;+=================================================================
     ;
     ; Retrive a hexadecimal color given a name.
     ;
     ;-=================================================================
     FUNCTION MIR_GET_COLOR_BY_NAME, color_name_in

       color_name = IDL_VALIDNAME(STRLOWCASE(color_name_in), /CONVERT_SPACES)


       color_hash = MIR_COLOR_HASH()

       CATCH, error
       IF error NE 0 THEN BEGIN
         CATCH, /CANCEL
         MESSAGE, 'Color unknown: ' + color_name
       ENDIF

       color = color_hash[color_name]

       RETURN, color
     END ; FUNCTION MIR_GET_COLOR_BY_NAME


     
     ;+=================================================================
     ;
     ; Retrive hexadecimal colors given several options.
     ; 
     ; AVAILABLE COLORS:
     ;
     ;   aliceblue
     ;   antiquewhite
     ;   aqua
     ;   aquamarine
     ;   azure
     ;   beige
     ;   bisque
     ;   black
     ;   blanchedalmond
     ;   blue
     ;   blueviolet
     ;   brown
     ;   burlywood
     ;   cadetblue
     ;   chartreuse
     ;   chocolate
     ;   coral
     ;   cornflowerblue
     ;   cornsilk
     ;   crimson
     ;   cyan
     ;   darkblue
     ;   darkcyan
     ;   darkgoldenrod
     ;   darkgray
     ;   darkgrey
     ;   darkgreen
     ;   darkkhaki
     ;   darkmagenta
     ;   darkolivegreen
     ;   darkorange
     ;   darkorchid
     ;   darkred
     ;   darksalmon
     ;   darkseagreen
     ;   darkslateblue
     ;   darkslategray
     ;   darkslategrey
     ;   darkturquoise
     ;   darkviolet
     ;   deeppink
     ;   deepskyblue
     ;   dimgray
     ;   dimgrey
     ;   dodgerblue
     ;   firebrick
     ;   floralwhite
     ;   forestgreen
     ;   fuchsia
     ;   gainsboro
     ;   ghostwhite
     ;   gold
     ;   goldenrod
     ;   gray
     ;   grey
     ;   green
     ;   greenyellow
     ;   honeydew
     ;   hotpink
     ;   indianred
     ;   indigo
     ;   ivory
     ;   khaki
     ;   lavender
     ;   lavenderblush
     ;   lawngreen
     ;   lemonchiffon
     ;   lightblue
     ;   lightcoral
     ;   lightcyan
     ;   lightgoldenrodyellow
     ;   lightgray
     ;   lightgrey
     ;   lightgreen
     ;   lightpink
     ;   lightsalmon
     ;   lightseagreen
     ;   lightskyblue
     ;   lightslategray
     ;   lightslategrey
     ;   lightsteelblue
     ;   lightyellow
     ;   lime
     ;   limegreen
     ;   linen
     ;   magenta
     ;   maroon
     ;   mediumaquamarine
     ;   mediumblue
     ;   mediumorchid
     ;   mediumpurple
     ;   mediumseagreen
     ;   mediumslateblue
     ;   mediumspringgreen
     ;   mediumturquoise
     ;   mediumvioletred
     ;   midnightblue
     ;   mintcream
     ;   mistyrose
     ;   moccasin
     ;   navajowhite
     ;   navy
     ;   oldlace
     ;   olive
     ;   olivedrab
     ;   orange
     ;   orangered
     ;   orchid
     ;   palegoldenrod
     ;   palegreen
     ;   paleturquoise
     ;   palevioletred
     ;   papayawhip
     ;   peachpuff
     ;   peru
     ;   pink
     ;   plum
     ;   powderblue
     ;   purple
     ;   red
     ;   rosybrown
     ;   royalblue
     ;   saddlebrown
     ;   salmon
     ;   sandybrown
     ;   seagreen
     ;   seashell
     ;   sienna
     ;   silver
     ;   skyblue
     ;   slateblue
     ;   slategray
     ;   slategrey
     ;   snow
     ;   springgreen
     ;   steelblue
     ;   tan
     ;   teal
     ;   thistle
     ;   tomato
     ;   turquoise
     ;   violet
     ;   wheat
     ;   white
     ;   whitesmoke
     ;   yellow
     ;   yellowgreen
     ;
     ;   lightred
     ;
     ;   print_red
     ;   print_blue
     ;   print_green
     ;   print_yellow
     ;   print_orange
     ;   print_purple
     ;   print_cyan
     ;   print_magenta
     ;
     ;   gray_10
     ;   gray_20
     ;   gray_30
     ;   gray_40
     ;   gray_50
     ;   gray_60
     ;   gray_70
     ;   gray_80
     ;   gray_90
     ;
     ;   bst_full
     ;   bst_half
     ;   bst_third
     ;-=================================================================
     FUNCTION MIR_COLOR, color, HELP=help

       IF KEYWORD_SET(help) THEN BEGIN
         INFO
         RETURN, 0
       ENDIF

       CASE 1 OF
         (SIZE(color, /TYPE) EQ 7): BEGIN
           RETURN, MIR_GET_COLOR_BY_NAME(color)
         END
       ENDCASE

     END ;FUNCTION MIR_COLOR
